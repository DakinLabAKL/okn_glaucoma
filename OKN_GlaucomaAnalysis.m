% OKN glaucoma (GOKN) Analysis
% TrialByTrial OKN Analysis

% clearvars;
% close all;
% if exist(fullfile('C:\Users\smoh205\Google Drive\GOKN_Data'),'dir')
%     RootDir = fullfile('C:\Users\smoh205\Google Drive\GOKN_Data');
% elseif exist(fullfile('C:\Users\DakinLab\Google Drive\GOKN_Data'),'dir')
%     RootDir = fullfile('C:\Users\DakinLab\Google Drive\GOKN_Data');
% end
% % just the initials

% ObserverName =  { 'AK','SA','MP','BC','MH','SP','BH','YHH','AG','AS','HL','GW','GH' ,   'WC','BH','RH','PB','SS','SY','ND','AM','OB','KC','JC','HD'}; % 'SMD' 'AK'};
% WorseEye     =  [  2  , 2  , 2  , 2  , 1  ,  2 , 2  ,  1  , 1  , 2  ,  2 , 2  ,  2  ,     2 ,  2 , 2  , 2  , 2  , 2  ,  1 ,  2,  2  ,  2 , 1  , 2]; % 1: left eye worst eye    2: right eye worst eye
% BetterEye    =  [  1  , 1  , 1  , 1  , 2  ,  1 , 1  ,  2  , 2  , 1  ,  1 , 1  ,  1  ,     1 ,  1 , 1  , 1  , 1  , 1  ,  2 ,  1,  1  ,  1 , 2  , 1];

% %  ObserverName =  { 'SP'}; %  ,'SA' , 'MP', 'BC' , 'MH', 'SP'}; % 'SMD' 'AK'};
% %  WorseEye     =  [  2 ]; %    , 2   ,  2  ,  2   ,   1 ,  2]; % 1: left eye worst eye    2: right eye worst eye
% %  BetterEye    =  [  1 ];%   , 1   ,  1  ,  1   ,   2 ,  1] ;
%
% EyeList       = 'LR';
% EyeListFileNames = ['D', 'S', 'D', 'S'];
% ExperimentConditions = {'direction', 'direction', 'contrast' ,'contrast'};

% conditions used in GOKN
% 1 = RE Direction
% 2 = LE Direction
% 3 = RE Contrast
% 4 = LE Contrast


%  WorseEye  = [ 1     2     1     2     2     2     2     2     2     2     2     2     1     1     1     2     2     1     1     1     2     2     2     2     2     1];
%  BetterEye = [ 2     1     2     1     1     1     1     1     1     1     1     1     2     2     2     1     1     2     2     2     1     1     1     1     1     2];


OKN_Glaucoma_GoogleDrive_FileNameCheck
%%
if 1
    % if you need a specific participant type the name here
    IndividualObserverName = {'JD'}; % 'CH', 'WK', 'IR'the IDs for controls 'CH_22091942', 'WK_06091953', 'IR_02081941'
end

MaxSaccSpeedDegPerSec = 500; % fixed - determined by minimisation
paStdThresh           = 2.75; %  used to find and remove blinks
FrameMargin           = 5; % number of eye tracking samples to disgard either side of a blink. was 5
SSpdWS                = 4; % window in milisecond, after and before each point to calculate sliding window speed.
Smooth4SpeedCalc =1; % 1= use sgolay filter to smooth eye-position for speed calculation
Sgolength = 5;
JustFirstSample = 0;
DiscardFirst500MS4GainCalc = 1;

if exist('IndividualObserverName','var') % i.e if we want just the analysis of the selected observers
    ObserverRagne = find(ismember(ObserverName,IndividualObserverName));
else % i.e we want the analysis of all the observers in the google spread sheet
    ObserverRagne = 1:length(ObserverName);
end

for subLoop  = ObserverRagne% subjects
    for conLoop = 1: length(ExperimentConditions)
        clear fNameString TheName
        % load up the this subject name files
        fNameString  = sprintf('%s%c%s*O%s*%s*%s*.mat',RootDir,filesep,ObserverName{subLoop},EyeListFileNames(conLoop),ObserverDOB{subLoop},ExperimentConditions{conLoop});
        
        AllFiles    = dir(fNameString);
        if isempty({AllFiles.name}) % the naming was not consistent
            fNameString2  = sprintf('%s%c%s*%s*O%s*%s*.mat',RootDir,filesep,ObserverName{subLoop},ObserverDOB{subLoop},EyeListFileNames(conLoop),ExperimentConditions{conLoop});
            AllFiles    = dir(fNameString2);
        end
        TheRuns(subLoop) = numel({AllFiles.name}); % will find how many runs we have for this subject
        TheLoadedRun = TheRuns(subLoop);
        
        for runLoop =  numel(TheLoadedRun)% 1: TheRuns(subLoop) TheRuns(subLoop) % loading the last run
            TheName     =  sprintf('%s%c%s',RootDir,filesep,AllFiles(TheLoadedRun).name); % runLoop
            load(TheName);
            if iscell(E)
                E = E{1};
                P = P{1};
                S = S{1};
            end
            if ~isrow(P.StimInds)
                P.StimInds = P.StimInds';
            end
            TheEyes = E.WhichEye; % the recorded eye(s)
            TheOpenEye = E.Settings.UncoveredEye; % 1 = LE;   2 = RE.
            NoInterleavedInCondition(subLoop,conLoop,:) = P.NoInterleaved; % used for preparing the data for plotting
            ContrastValsInCondition(subLoop,conLoop,1:numel(P.PossContrastValues{1}))= (P.PossContrastValues{1});
            MaskValsInCondition(subLoop,conLoop,1:numel(P.PossMaskValues{1})) = (P.PossMaskValues{1});
            StimValsInCondition(subLoop,conLoop,1:numel(P.PossStimValues{1})) = (P.PossStimValues{1});
            OpenEyeNoInCondition(subLoop,conLoop,:) = E.Settings.UncoveredEye; % 1: LE was open   2: RE was open
            if  strcmp( E.Eyetracker,'Tobii')
                X2PixConvertRatio = E.Window.PTBScreenWidth;
                Y2PixConvertRatio = E.Window.PTBScreenHeight;
                ETtimeConvtRatio = 1e6; % used to convert speed per sample to speed per microsecs
            else
                X2PixConvertRatio = 1;
                Y2PixConvertRatio = 1;
                ETtimeConvtRatio = 1e3; % used to convert speed per sample to speed per ms
            end
            
            %     cmap = linspecer(numel(ALLDesiredTrialTimeMS));
            
            %         OKNGainVectorByThisTrial = [];
            %         OKNMeanGainVector = [];
            %         OKNSTDGainVector = [];
            %         OKNSEGainVector = [];
            % % Kb Browsing betweein trials
            %             TrialLoop = 0;
            %             keepGoing = 1;
            %             while keepGoing
            %                 [ keyIsDoqwn, seconds, keyCode ] = KbCheck(-1);
            %                 if keyCode(KbName('right')) && TrialLoop< P.TotalTrials% right for increasing the trialLoop
            %                     TrialLoop = TrialLoop+1 ;
            %                 elseif keyCode(KbName('left')) && TrialLoop>1 % right for decreasing the  trialLoop
            %                     TrialLoop = TrialLoop-1 ;
            %                 elseif TrialLoop< P.TotalTrials
            %                     TrialLoop = TrialLoop+1 ;
            %                 end
            %                 if keyCode(KbName('q')) % KbName(keyCode)== 'q' % quit
            %                     keepGoing = 0;
            %                 end
            
            for TrialLoop = 1 :  P.CurrentTrialNo %P.TotalTrials
                ThisTrialIntLe=[]; ThisIntLeRept=[]; ThisTrialMaskInd=[]; ThisTrialStimInd=[]; ThisTrialContInd=[];
                ThisTrialIntLe    = P.InterList(TrialLoop);     % the interleaved condition of this trial (1=rightward  2=leftward stimulus movement )
                ThisIntLeRept     = P.TrialNumbers(TrialLoop);  % the number of repetitions of a direction reached by this trial
                ThisTrialMaskInd  = P.MaskInds(TrialLoop);      % the mask index of this trial
                ThisTrialStimInd  = P.StimInds(TrialLoop);      % the stimulus index of this trial
                ThisTrialContInd  = P.ContrastInds(TrialLoop);  % the contrast index of this trial
                
                TheSimilarTrialIndices=find((P.InterList==ThisTrialIntLe) & (P.MaskInds==ThisTrialMaskInd) & (P.ContrastInds==ThisTrialContInd) & (P.StimInds==ThisTrialStimInd));
                ThisTrialOverallIntLeavedRep = find(TheSimilarTrialIndices==TrialLoop);
                for eyeLoop = 1:length(TheEyes)
                    % Get eye-position data
                    xDataIn         = squeeze(E.EyePos.X(ThisTrialIntLe, ThisIntLeRept, :,TheEyes(eyeLoop) )).* X2PixConvertRatio;
                    yDataInvIn      = squeeze(E.EyePos.Y(ThisTrialIntLe, ThisIntLeRept, :, TheEyes(eyeLoop))).* Y2PixConvertRatio;
                    yDataIn         = -(yDataInvIn(:) - E.Window.PTBScreenHeight);% turn the Y direction from screen coordiante to mathematic coordinate. if this line is in the code, make sure that in dirdiff the angles are in correct order which should be [0 pi/2 pi 3pi/2]
                    FrameFlipDataIn = squeeze(E.Window.FrameFlipTime(ThisTrialIntLe, ThisIntLeRept, :)); % keep the frame flip time for later processing
                    
                    paDataIn      = squeeze(E.EyePos.PA(ThisTrialIntLe,  ThisIntLeRept, :, TheEyes(eyeLoop)));  % pa: pupil area
                    tDataIn       = squeeze(E.EyePos.Time(ThisTrialIntLe, ThisIntLeRept, :));
                    
                    StimulusDriftAngle = DegToRad(E.Settings.StimulusDirection(ThisTrialIntLe));
                    
                    SamplingRate = 1; % IVvalues(IVloop); % used as sliding gain sampling interval (window length).
                    
                    OKN_GlaucomaProcessing
                    %                         OKN_Processor % for  R&D regardings OKN gain
                    
                    if 1
                        if  1; % conLoop == 3 && eyeLoop==2 % GLMgain<=0% ThisTrialContInd == 5 % HistGain<0.5 %&& (ThisTrialContInd==4) %(ThisTrialMaskInd==9)&&SldGain<0.3 OKNscore==0 %(subLoop==3)% &(abs(OKNGain) <0.4)& % ismember(ThisTrialMaskInd, [1 5 9 ] ) && ismember(ThisIntLeRept, [ 1 8])  && ismember(ThisTrialIntLe,[1 2])%|| ThisTrialStimInd == 5
                            
                            BelowThresh = find(Speed<Threshold);
                            AboveThresh = find(Speed>=Threshold);
                            PursOrNot = Speed<Threshold;
                            
                            %                             SeqBelowThresh = find((PursSpeed) < Threshold);
                            %                             SeqAboveThresh = find((PursSpeed) >= Threshold);
                            
                            SpeedDb = 20.*log10(Speed);
                            xDallDb = 20.*log10(abs(xDall));
                            yDallDb = 20.*log10(abs(yDall));
                            
                            %                             PursSpeedDb = 20.*log10(PursSpeed);
                            
                            if ~isempty(Speed) % for plotting
                                maxSpdVal = max(Speed);
                            else
                                maxSpdVal = 1;
                            end
                            %                                 StimSpeedPlt = S.SpeedPixPerMS.*Tall;
                            
                            StimPosition = (S.SpeedPixPerSec/ETtimeConvtRatio).*Tall;
                            EstOffsetFromGLM_H = Tall.*(SpeedFromGLMPixPerSec_H/ETtimeConvtRatio);
                            EstOffsetFromGLM_V = Tall.*(SpeedFromGLMPixPerSec_V/ETtimeConvtRatio);
                            
                            %                     EstOffsetFromHist = EstOffsetFromHist+ abs(Xall(round(numel(Tall)/2)) - EstOffsetFromHist(round(numel(Tall)/2)));
                            EstOffsetFromGLM_H  = EstOffsetFromGLM_H+ abs(Xall(round(numel(Tall)/2)) - EstOffsetFromGLM_H(round(numel(Tall)/2)));
                            EstOffsetFromGLM_V  = EstOffsetFromGLM_V+ abs(Xall(round(numel(Tall)/2)) - EstOffsetFromGLM_V(round(numel(Tall)/2)));
                            
                            figure(9)
                            subplot(2,4,1);
                            plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimPosition,'k-.',...
                                Tall, EstOffsetFromGLM_H,'b-.','Linewidth',2,'MarkerSize',15);
                            % subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall,EstOffsetFromHist,'b-',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimSpeedPlt,'k-.',Tall,(StimSpeedPlt.*HistGain)+E.Window.PTBScreenWidthCenter,'m-.' );
                            legend('Horizontal eye position', 'Saccade', 'Tracking', 'Stimulus position',...
                                sprintf('GLM gain: %2.2f',GLMgain), 'Location', 'Best');% ,sprintf('Estimated tracking position, TLocal gain: %2.2f',TlocGain)
                            ylim([0 E.Window.PTBScreenWidth]),
                            title(sprintf('Subject:%i %sE %iAFC Contrast:%2.2f ; Mask:%i Inter:%i Repetition:%i   XData',subLoop, EyeList(eyeLoop), P.NoInterleaved, E.Settings.BaseContrastLevels(ThisTrialContInd)*100, ThisTrialMaskInd, ThisTrialIntLe , ThisIntLeRept)); shg
                            subplot(2,4,2);plot( Tall,Yall,'b-',Tall(AboveThresh),Yall(AboveThresh),'r.',Tall(BelowThresh),Yall(BelowThresh),'g.');
                            title('YData');
                            ylim([0 E.Window.PTBScreenHeight]),
                            subplot(2,4,3);plot( xData0,yData0,'b-',xData00, yData00,'r.');
                            title(sprintf('Unprocessed Eye Gaze on Screen (%2.0f deg) ',E.Settings.StimulusDirection(ThisTrialIntLe)));
                            ylim([0 E.Window.PTBScreenHeight]),  xlim([0 E.Window.PTBScreenWidth]),
                            legend('Original Eye Gaze', 'Rotated Eye Gaze')
                            
                            %                                     subplot(2,4,4),plot( Tall,Xall,'b-',Tall(PursStartInd),Xall(PursStartInd),'r*',Tall(PursEndInd),Xall(PursEndInd),'ks');ylim([0 E.Window.PTBScreenWidth]),
                            %                                     legend('eye position', 'Star of Pursuit', 'End of Pursuit')
                            subplot(2,4,5);plot( tDall,xDall,'b-',tDall(AboveThresh),xDall(AboveThresh),'r.',tDall(BelowThresh),xDall(BelowThresh),'g.',...
                                tDall,StimSpeedPixPerSample.*ones(size(tDall)),'K-',...
                                tDall,SpeedFromGLMPixPerSamp_H.*ones(size(tDall)),'C-.','Linewidth',1.2,'MarkerSize',12);%HistGain 20 is calculated from 1.2pix/s 1.2*16.667 = 20 pixel per sample
                            title('XSpeed'); legend('Speed','Saccade', 'Tracking'),ylim([-maxSpdVal maxSpdVal])
                            xlabel('Time(ms)');ylabel('Speed pixel per sample')
                            subplot(2,4,6);plot(tDall,yDall,'b-',tDall(AboveThresh),yDall(AboveThresh),'r.',tDall(BelowThresh),yDall(BelowThresh),'g.',tDall,StimSpeedPixPerSample.*ones(size(tDall)),'K-');
                            ylim([-maxSpdVal maxSpdVal]),title('YSpeed');
                            subplot(2,4,7);plot(tDall,Speed,'b-',tDall(AboveThresh),Speed(AboveThresh),'r.',tDall(BelowThresh),Speed(BelowThresh),'g.',tDall,StimSpeedPixPerSample.*ones(size(tDall)),'K-');
                            ylim([-maxSpdVal maxSpdVal]),title('Directional Speed');
                            subplot(2,4,7);polarplot(SpeedAng,SpeedDb,'b.',SpeedAng(AboveThresh),SpeedDb(AboveThresh),'ro',SpeedAng(BelowThresh),SpeedDb(BelowThresh),'go')
                            %                                     subplot(2,4,8),plot(2:length(tData),1./(diff(tData./1000)),'b.-'),
                            %                                     title(sprintf('Mean first sample frequency %2.2f',mean(1./(diff(tData./1000)))))
                            %                                     %                                     subplot(2,4,8);plot(1:length(tData0),tData0,'b.')
                            %                                         polarplot(PursSpeedAng,PursSpeedDb,'b.',PursSpeedAng(SeqAboveThresh),PursSpeedDb(SeqAboveThresh),'ro',PursSpeedAng(SeqBelowThresh),PursSpeedDb(SeqBelowThresh),'go')
                            set (gcf, 'Units', 'normalized', 'Position', [0  0.1  1  0.8]); %  [1.5000    0.0778    0.7500    0.6718]);
                            
                            fprintf('Trial: %i; %sE: OKNscore: %i;  Threshold: %2.2f; GLMGain: %2.2f; OKN Consistency: %2.2f  TotalDisRatio: %2.2f\n',...
                                TrialLoop, EyeList(eyeLoop), OKNscore, Threshold, GLMgain, OKNConsistency, TotalDisRatio);
                            fprintf('Trial:%i; ThisTrialIntLe:%i; ThisIntLeRept:%i; ThisTrialMaskInd:%i; ThisTrialStimInd:%i;  ThisTrialContInd:%i; \n',TrialLoop, ThisTrialIntLe, ThisIntLeRept, ThisTrialMaskInd,ThisTrialStimInd, ThisTrialContInd)
                            drawnow
                            pause;
                            if E.Settings.ExperimentSet == 1 % just show title in experiment one, to reduce cluttering
                                txt1Str = sprintf('T%i Int%i Rep%i\nGain:%2.2f',TrialLoop, ThisTrialIntLe, ThisIntLeRept, GLMgain_H);
                                TxtHndl = text(.05 , 0.87 , txt1Str,'units','normalized');
                                uistack(TxtHndl, 'top')
                            end
                        end
                        
                    end
                    
                    AllXstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                  = Xall;
                    AllYstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                  = Yall;
                    AllTstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                  = Tall;
                    AllPstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                 = Pall;
                    AllXSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}               = xDall;
                    AllYSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}               = yDall;
                    AllTSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}               = tDall;
                    AllSpdVecstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}             = Speed;
                    AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}           = OKNscore;
                    AllOKNConsistencystore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}     = double(OKNConsistency);
                    AllKeyRespstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}            = P.Response(TrialLoop);
                    %                     AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = HistGain; %SpeedFromHistDegPerSec/S.SpeedDegPerSec;
                    Allgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = OKNGain;
                    Allgainstore_H{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = OKNGain_H;
                    Allgainstore_V{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = OKNGain_V;
                    
                    AllMADgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = MADgain;
                    AllMADgainstore_H{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = MADgain_H;
                    AllMADgainstore_V{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = MADgain_V;
                    
                    
                    AllGLMgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = GLMgain;
                    AllGLMgainstore_H{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = GLMgain_H;
                    AllGLMgainstore_GofH{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} =gof_H.rsquare;
                    AllGLMgainstore_V{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = GLMgain_V;
                    AllGLMgainstore_GofV{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} =gof_V.rsquare;
                    
                    
                    
                    %                 AllManualGainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = ManualGain;
                    %                 AllSlidingHistGainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = SlidingHistGain;
                    AllTotalDisRatiostore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = TotalDisRatio;
                    %                     AllConsistentDistRatiostore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = ConsistentDistRatio;
                end
            end
        end
    end
end
%% saving the work space
if 0
    save('OKN_Glaucoma_WorkeSpcae12092019') % using the new processing H and V
    %    save('OKN_Glaucoma_WorkeSpcae16042019')
    %       save('OKN_Glaucoma_WorkeSpcaeWithControls08032019')
    
end
%% prepare the data for plot and statistics DIRECTION Data for RE and LE (notice that each of RE and LE has both eyes recorded)
% clear GainMatrix    MeanOfGain    STDofGain
% Experimental conditions
% 1 = RE Direction
% 2 = LE Direction
% 3 = RE Contrast
% 4 = LE Contrast
if 0
    % load('OKN_Glaucoma_WorkeSpcae12092019') %  using the new processing H and V
    load('OKN_Glaucoma_WorkeSpcaeWithControls08032019')
    
end
% conLoop=1:2;   % 1 = RE Direction % 2 = LE Direction
runLoop =1;
IntLeRept = 1: E.Settings.LocalTrialNo;
Xdata = 1:4; %P.PossContrastValues{1}*100;
Cmap = linspecer(numel(ObserverName));
% eyeLoop = 1:2; % both eye are recorded
% if E.Settings.DirectionNums ==2
%     DriftingDirections_DirectionCondition = {'Right', 'Left'};
% elseif E.Settings.DirectionNums ==4
DriftingDirections_DirectionCondition = {'Right', 'Up', 'Left', 'Down'};
% end

for subLoop = ObserverRagne% 1: numel(ObserverName)
    for conLoop =  1:2 % direction conditions 1:RE open    2:LE open
        for eyeLoop = 1: numel(TheEyes) % 1: LE data in ET 2: RE data in ET
            clear OKN_GainAllmean    OKN_GainAllStd    OKN_GainAll pupilAll timeAll
            IntLe = 1: NoInterleavedInCondition(subLoop,conLoop);
            StimInd = 1: numel(StimValsInCondition(subLoop,conLoop,:));
            ContInd = 1: sum(squeeze(ContrastValsInCondition(subLoop,conLoop,:))>0);
            MaskInd  = 1: numel(MaskValsInCondition(subLoop,conLoop,:));
            
            OKN_GainAll = (cell2mat(reshape(squeeze({AllGLMgainstore_H{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),E.Settings.LocalTrialNo)));
            OKN_ScoreAll = (cell2mat(reshape(squeeze({AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),E.Settings.LocalTrialNo)));
            
            % repetition , contrast levels
            OKN_GainMatrix = OKN_GainAll';
            
            %             GainMatrix{subLoop,conLoop,eyeLoop,:} = OKN_Gain;
            %             MeanOfGain(subLoop,:) = mean((GainMatrix{subLoop}));
            %             STDofGain(subLoop,:) =std(GainMatrix{subLoop});
            if 0  % % plotting
                OKN_GainAllmean = nanmean(OKN_GainMatrix,1); % mean calculated for each drifting direction and contrast level
                OKN_GainAllStd  = nanstd (OKN_GainMatrix,0,1);
                
                OKN_GainAllmedian = nanmedian(OKN_GainMatrix,1); % mean calculated for each drifting direction and contrast level
                OKN_GainAllMad  = mad (OKN_GainMatrix,1);
                
                
                figure(10+conLoop),
                FigNameStr = sprintf('Name:%s Open eye: O%s ',ObserverName{subLoop}, EyeListFileNames(conLoop));
                set(gcf,'Name',FigNameStr);
                hold on
                subplot(1,numel(TheEyes),eyeLoop);
                %     errorbar(Xdata,mean(OKN_Gain,1),std(OKN_Gain),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap,'Color',Cmap);
                errorbar(Xdata,OKN_GainAllmedian,OKN_GainAllMad,'o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                
                hold on;
                plot(Xdata+0.2,OKN_GainMatrix,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                drawnow;
                if eyeLoop ==1
                    ylabel('OKN-Gain')
                    xlabel('Drifting Direction')
                    legend('Median � MAD','Gain of each trial','location','best')
                end
                
                if ismember(TheEyes(eyeLoop), OpenEyeNoInCondition(subLoop,conLoop,:))
                    OKNtypeStr ='Direct Response';
                else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
                    OKNtypeStr ='Indirect Response';
                end
                
                title(sprintf('%sE %s',EyeList(TheEyes(eyeLoop)), OKNtypeStr))
                %     axis([min(Xdata)-5 max(Xdata)+5 0.2 1.3])
                axis([0 5 -0.2 1.4])
                xticks(1:4)
                xticklabels({'Right', 'Up', 'Left', 'Down' })
                %             set(gcf,'position', [360, 198, 343.*length(TheEyes), 420])
            end
        end
    end
    
end
% BetweenObserverSTD = std(MeanOfGain);
% WithinObserverSTD = rms(STDofGain);
%% Summary plot direction condition,  JUST THE DIRECT RESPONSES

% Experimental conditions
% 1 = RE Direction
% 2 = LE Direction
% 3 = RE Contrast
% 4 = LE Contrast
% conLoop=3:4;   % 3 = RE contrast % 4 = LE contrast
runLoop =1;
IntLeRept = 1: E.Settings.LocalTrialNo;
% Cmap = [0.4 0.4 0.4]; use previous Cmap
% eyeLoop = 1:2; % both eye are recorded
Xdata = 1:4;
Cmap = linspecer(numel(ObserverName));

colMap = linspecer(3,'Qualitative');
red = colMap(2,:);
green = colMap(3,:);
blue = colMap(1,:);
DriftingDirections_DirectionCondition =  {'Right', 'Up', 'Left', 'Down'};
% dimention of AllSub_DirectionOKNGain :
% observer number, eyelist,repetition, direction of drifting
% drifting directions are (1:right, 2:up, 3:left and 4: down)
AllSubOKNGain_Direction = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,NoInterleavedInCondition(ObserverRagne(1),1)); % size of all contrast + 100%
AllSubPupilData_Direction = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,NoInterleavedInCondition(ObserverRagne(1),1));
for subLoop = ObserverRagne % 1: numel(ObserverName)
    for conLoop =  1:2 % 1:RE open Direction Experiment   2:LE open Direction Experiment
        for eyeLoop = OpenEyeNoInCondition(subLoop,conLoop,:) % Just load the stimulated eye 1: LE data in ET 2: RE data in ET
            clear OKN_GainAllmean    OKN_GainAllStd    OKN_GainAll
            IntLe = 1: NoInterleavedInCondition(subLoop,conLoop);
            StimInd = 1: numel(StimValsInCondition(subLoop,conLoop,:));
            ContInd = 1: sum(squeeze(ContrastValsInCondition(subLoop,conLoop,:))>0);
            MaskInd  = 1: numel(MaskValsInCondition(subLoop,conLoop,:));
            Xdata_Direction = 1:NoInterleavedInCondition(subLoop,conLoop,:);
            
            OKN_GainAll = (cell2mat(reshape(squeeze({AllGLMgainstore_H{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),E.Settings.LocalTrialNo)));
            %             OKN_GainAll = (cell2mat(reshape(squeeze({AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),numel(ContInd),E.Settings.LocalTrialNo)));
            % load pupil data
            pupilAll = (reshape(squeeze({AllPstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),E.Settings.LocalTrialNo));
            timeAll = (reshape(squeeze({AllTstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),E.Settings.LocalTrialNo));
            
            pupilMedian= cellfun(@median, pupilAll);
            pupilMAD = cellfun(@mad, pupilAll);
            
            % transposing from drifting direction x repetition to
            % repetition x drifting direction; (from 4x8 to 8x4)
            OKN_GainMatrix = OKN_GainAll';
            PupilMatirx = pupilMedian';
            
            AllSubOKNGain_Direction(subLoop,eyeLoop,:,:)  = OKN_GainMatrix;
            AllSubPupilData_Direction(subLoop,eyeLoop,:,:)  = PupilMatirx;
            % % plotting
            AllXdata_Direction = Xdata_Direction;
            
            if 0 % plot
                figure((subLoop)*80)
                FigNameStr = sprintf('Name: %s',ObserverName{subLoop});
                set(gcf,'Name',FigNameStr);
                
                if 0 % plot OKN data
                    subplot(1,numel(TheEyes),mod(conLoop,2)+1);
                    errorbar(Xdata,median(PupilMatirx,1),mad(PupilMatirx),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                    %           errorbar(Xdata,mean(OKN_GainMatrix,1),std(OKN_GainMatrix),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                    hold on;
                    plot(Xdata_Direction+0.1.*Xdata_Direction,OKN_GainMatrix,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                    if eyeLoop ==1
                        ylabel('OKN-Gain')
                        xlabel('Drifting direction')
                        legend('Median � MAD','Gain of each trial','location','best')
                    end
                    
                end
                
                if 0 % plot pupil data
                    errorbar(Xdata,median(OKN_GainMatrix,1),mad(OKN_GainMatrix),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                    hold on;
                    plot(Xdata_Direction+0.1.*Xdata_Direction,OKN_GainMatrix,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                    if eyeLoop ==1
                        ylabel('Pupil')
                        xlabel('Drifting direction')
                        legend('Median � MAD','Gain of each trial','location','best')
                    end
                    
                    
                end
                
                
                
                if ismember(TheEyes(eyeLoop),OpenEyeNoInCondition(subLoop,conLoop,:))
                    OKNtypeStr ='Direct Response';
                else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
                    OKNtypeStr ='Indirect Response';
                end
                
                title(sprintf('%sE %s',EyeList(TheEyes(eyeLoop)), OKNtypeStr))
                xticks(AllXdata_Direction)
                xticklabels(DriftingDirections_DirectionCondition)
                set(gca,'xscale','linear','box','off');
                axis([0 AllXdata_Direction(end)+1 -0.2 0.9])
            end
        end
    end
    
    % asign better eye and worse eye the gain of direction data; save it
    % for all observers
    REGain_Direction = squeeze(AllSubOKNGain_Direction(subLoop,2,:,:));
    REmean_Direction = mean(REGain_Direction);
    RESD_Direction = std(REGain_Direction);
    LEGain_Direction = squeeze(AllSubOKNGain_Direction(subLoop,1,:,:));
    LEmean_Direction = mean(LEGain_Direction);
    LESD_Direction = std(LEGain_Direction);
    
    AllSubjectsREGain_Direction{subLoop} = REGain_Direction;
    AllSubjectsLEGain_Direction{subLoop} = LEGain_Direction;
    
    % also add pupil data of the RE and LE (and better eye and worse eye)
    REPupil_Direction = squeeze(AllSubPupilData_Direction(subLoop,2,:,:)); % 2: Right eye
    REPupilmean_Direction = nanmean(REPupil_Direction);
    REPupilmedian_Direction = nanmedian(REPupil_Direction);
    REPupilSD_Direction = std(REPupil_Direction);
    REPupilMAD_Direction = mad(REPupil_Direction,1);
    
    LEPupil_Direction = squeeze(AllSubPupilData_Direction(subLoop,1,:,:)); % 1: left eye
    LEPupilmean_Direction = nanmean(LEPupil_Direction);
    LEPupilmedian_Direction = nanmedian(LEPupil_Direction);
    LEPupilSD_Direction = std(LEPupil_Direction);
    LEPupilMAD_Direction = mad(LEPupil_Direction,1);
    
    if BetterEye(subLoop) == 2 % i.e the rigth eye is the better eye
        % gain
        ThisSubBetterEyeGain_Direction = REGain_Direction;
        ThisSubWorseEyeGain_Direction = LEGain_Direction;
        % pupil
        ThisSubBetterEyePupil_Direction = REPupil_Direction;
        ThisSubWorseEyePupil_Direction = LEPupil_Direction;
        
    elseif BetterEye(subLoop) ==1 % i.e the left eye is the better eye
        % gain
        ThisSubBetterEyeGain_Direction = LEGain_Direction;
        ThisSubWorseEyeGain_Direction = REGain_Direction;
        % pupil
        ThisSubBetterEyePupil_Direction = LEPupil_Direction;
        ThisSubWorseEyePupil_Direction = REPupil_Direction;
    end
    % gain
    AllSubjectsBetterEyeGain_Direction{subLoop} = ThisSubBetterEyeGain_Direction;
    AllSubjectsWorseEyeGain_Direction{subLoop}  = ThisSubWorseEyeGain_Direction;
    % pupil
    AllSubjectsBetterEyePupil_Direction{subLoop} = ThisSubBetterEyePupil_Direction;
    AllSubjectsWorseEyePupil_Direction{subLoop}  = ThisSubWorseEyePupil_Direction;
    
end

%% plot shaded graph of the means between better eye and worse eye across all participants
ObserverVFIratio =  [WorseEyeVFI./BetterEyeVFI  nanmean(WorseEyeVFI./BetterEyeVFI )].*100;% the last number is the mean; the higher this ratio the more similar the afected eye to the better eye
[SortedObserverVFI, sortIdx] = sort(ObserverVFIratio(1:end-1)); % Will sort the VFI from the worst (most affected) to the best (least affected) patient
for subLoop = ObserverRagne
    WEmean_Direction = nanmean(cell2mat(AllSubjectsWorseEyeGain_Direction(subLoop)));
    BEmean_Direction = nanmean(cell2mat(AllSubjectsBetterEyeGain_Direction(subLoop)));
    subjName = ObserverName{subLoop};
    
    % pupil
    WE_Pupilmedian_Direction = nanmean(cell2mat(AllSubjectsWorseEyePupil_Direction(subLoop)));
    BE_Pupilmedian_Direction = nanmean(cell2mat(AllSubjectsBetterEyePupil_Direction(subLoop)));
    WE_Pupilmad_Direction = mad(cell2mat(AllSubjectsWorseEyePupil_Direction(subLoop)),1);
    BE_Pupilmad_Direction = mad(cell2mat(AllSubjectsBetterEyePupil_Direction(subLoop)),1);
    
    OKNLossIndex_Direction(subLoop) = (nansum(BEmean_Direction-WEmean_Direction)./nansum(BEmean_Direction)).*100;
    OKNVFRatio_Direction(subLoop) = nansum(WEmean_Direction)./nansum(BEmean_Direction).* 100; % to campare with VFI ratio. area under two graphs  [what about nanmean((WEmean)./(BEmean)).* 100;]%
    AllSubjWEmean_Direction(subLoop,:)=WEmean_Direction;
    AllSubjBEmean_Direction(subLoop,:)=BEmean_Direction;
    
    AllSubjWE_PupilMedian_Direction(subLoop,:) = WE_Pupilmedian_Direction;
    AllSubjBE_PupilMedian_Direction(subLoop,:) = BE_Pupilmedian_Direction;
    
    
    if 1 % i.e draw plots or no?
        if 0 % plot OKN gian data
            figure(2000)
            subplot(4,11,subLoop)
            WEPLOT_Direction = plot(Xdata_Direction, WEmean_Direction,'o-','LineWidth',2,'Markersize',9,'color',red,'MarkerFaceColor',red);
            hold on
            BEPLOT_Direction = plot(Xdata_Direction, BEmean_Direction,'o-','LineWidth',2,'Markersize',9,'color',red',MarkerFaceColor',green);
            patch([Xdata_Direction fliplr(Xdata_Direction)], [WEmean_Direction fliplr(BEmean_Direction)], 'k','FaceAlpha',0.2,'EdgeAlpha',0.1)
            hold off
            %         title(sprintf('%s\nVFR: %2.0f\nOKNR:
            %         %2.2f',subjName,round(ObserverVFIratio(subLoop)),OKNVFRatio(subLoop))); % if you want the patient Inits
            if subLoop == 1
                ylabel('OKN-Gain')
                xlabel('Drifting direction')
                title(sprintf('Patient %i  VFI ratio: %2.0f%% \nOKN ratio: %2.0f%%',subLoop,round(ObserverVFIratio(subLoop)),round(OKNVFRatio_Direction(subLoop))));
                legend([BEPLOT_Direction,WEPLOT_Direction], 'Better eye', 'Worse eye','location', 'best' )
                xticks(Xdata_Direction)
                xticklabels({'R', 'U', 'L', 'D'})
            else
                set(gca,'xscale','linear','box','off','FontSize',14,'XMinorTick','off','XTickLabel','','YTickLabel','');
                title(sprintf('P%i  [%2.0f%%]   [%2.0f%%]',subLoop,round(ObserverVFIratio(subLoop)),round(OKNVFRatio_Direction(subLoop))));
                
                if subLoop == numel(ObserverName)+1
                    title(sprintf('Mean [%2.0f%%]  [%2.0f%%]',round(ObserverVFIratio(subLoop)),round(OKNVFRatio_Direction(subLoop))));
                elseif subLoop == 6
                    title(sprintf('OKN vs. drifting direction\n\nMean [%2.0f%%]  [%2.0f%%]',round(ObserverVFIratio(subLoop)),round(OKNVFRatio_Direction(subLoop))));
                end
            end
            set(gca,'xscale','linear','box','off','FontSize',14,'XMinorTick','off');
            axis([-1 Xdata_Direction(end)+1  0.1-0.02 1.05+0.02])
        end
        
        if 1 % plot pupil data
            figure(2001)
            subplot(4,11,subLoop)
            WE_pupilPLOT_Direction = plot(Xdata_Direction, WE_Pupilmedian_Direction,'o','LineWidth',2,'Markersize',9,'color',red,'MarkerFaceColor',red);
            hold on
                            BE_pupilPLOT_Direction = plot(Xdata_Direction, BE_Pupilmedian_Direction,'o','LineWidth',2,'Markersize',9,'color',green,'MarkerFaceColor',green);

            if 0
                patch([Xdata_Direction fliplr(Xdata_Direction)], [WE_Pupilmedian_Direction fliplr(BE_Pupilmedian_Direction)], 'k','FaceAlpha',0.2,'EdgeAlpha',0.1)
            hold off
            end
            
            if 1 % plot the standard deviation error bars neg bars for the WE and pos bars for the BE
                for i = 1: numel(Xdata_Direction)
                    ThisPointNegErrBarVals = [WE_Pupilmedian_Direction(i) WE_Pupilmedian_Direction(i)+WE_Pupilmad_Direction(i)];
                    ThisPointPosErrBarVals = [BE_Pupilmedian_Direction(i) BE_Pupilmedian_Direction(i)-BE_Pupilmad_Direction(i)];
                    WEPLOTnegErrBar = plot([ Xdata_Direction(i) Xdata_Direction(i)], ThisPointNegErrBarVals,'-','LineWidth',2,'Color',[red ,0.8]);
                    BEPLOTnegErrBar = plot([ Xdata_Direction(i) Xdata_Direction(i)], ThisPointPosErrBarVals,'-','LineWidth',2,'Color',[green ,0.8]);
                end
            end
            %         title(sprintf('%s\nVFR: %2.0f\nOKNR:
            %         %2.2f',subjName,round(ObserverVFIratio(subLoop)),OKNVFRatio(subLoop))); % if you want the patient Inits
            if subLoop == 1
                ylabel('Pupil size � mad')
                xlabel('Drifting direction')
                title(sprintf('%i B:W%2.0f:%2.0f',subLoop,BetterEyeVFI(subLoop), WorseEyeVFI(subLoop)));
%                 legend([BE_pupilPLOT_Direction,WE_pupilPLOT_Direction], 'Better eye', 'Worse eye','location', 'best' )
                xticks(Xdata_Direction)
                xticklabels({'R', 'U', 'L', 'D'})
            else
                set(gca,'xscale','linear','box','off','FontSize',14,'XMinorTick','off','XTickLabel','','YTickLabel','');
                title(sprintf('%i %2.0f:%2.0f',subLoop,BetterEyeVFI(subLoop), WorseEyeVFI(subLoop)));
                
                %         if subLoop == numel(ObserverName)+1
                %             title(sprintf(' [%2.0f%%]  [%2.0f%%]',round(ObserverVFIratio(subLoop)),round(OKNVFRatio_Direction(subLoop))));
                %         elseif subLoop == 6
                %             title(sprintf('OKN vs. drifting direction\n\nMean [%2.0f%%]  [%2.0f%%]',round(ObserverVFIratio(subLoop)),round(OKNVFRatio_Direction(subLoop))));
                %         end
            end
            set(gca,'xscale','linear','box','off','FontSize',14,'XMinorTick','off');
%             axis([-1 Xdata_Direction(end)+1  0.1-0.02 1.05+0.02])
        end
        
        
        
    end
end


%% export Direction experiment Data to an excel file from data
% export XLS file
if 0
    
    for subLoop = ObserverRagne
        % define all relevant vars for this subject to put in atable and
        % wite into an xls file
        ID = ObserverID{subLoop};
        AffectedEye = WorseEye(subLoop); % 1=LE 2=RE
        REGain_Direction =  round(AllSubjectsREGain_Direction{subLoop},2);
        LEGain_Direction =  round(AllSubjectsLEGain_Direction{subLoop},2);
        TableRowNameStrs_Direction = ['ID', 'Affected eye', 'RE Gain_Direction', 'LE Gain_Direction'];
        xlsOutputNameStr_Direction = fullfile( 'C:\Users\smoh205\Desktop\Temp Figs','GOKN_Directionout41P.xlsx');
        sheetNameStr = sprintf('(P%i) %s',subLoop,ID);
        thisSubTable_Direction = table(ID, AffectedEye);
        writetable(thisSubTable_Direction,xlsOutputNameStr_Direction,'Sheet',sheetNameStr,'Range','A3')
        
        DriftingDirectionStr = table([DriftingDirections, DriftingDirections]);
        writetable(DriftingDirectionStr,xlsOutputNameStr_Direction,'Sheet',sheetNameStr,'Range','D4','WriteVariableNames',0)
        writetable(table('direction'),xlsOutputNameStr_Direction,'Sheet',sheetNameStr,'Range','D3','WriteVariableNames',0)
        thisSubGainTable_Direction= table(REGain_Direction, LEGain_Direction);
        writetable(thisSubGainTable_Direction,xlsOutputNameStr_Direction,'Sheet',sheetNameStr,'Range','D5')
    end
end
% *********************** ANALYSIS OF CONTRAST CONDITION ********************
%% prepare the data for plot and statistics CONTRAST Data for RE and LE (notice that each of RE and LE has both eyes recorded)
% clear GainMatrix    MeanOfGain    STDofGain
% Experimental conditions
% 1 = RE Direction
% 2 = LE Direction
% 3 = RE Contrast
% 4 = LE Contrast
% conLoop=3:4;   % 3 = RE contrast % 4 = LE contrast
runLoop =1;
IntLeRept = 1: E.Settings.LocalTrialNo;
% Cmap = [0.4 0.4 0.4]; use previous Cmap
% eyeLoop = 1:2; % both eye are recorded
if E.Settings.DirectionNums ==2
    DriftingDirections = {'Right', 'Left'};
elseif E.Settings.DirectionNums ==4
    DriftingDirections = {'Right', 'Up', 'Left', 'Down'};
end
for subLoop = ObserverRagne % 1: numel(ObserverName)
    for conLoop =  3:4 % direction conditions 1:RE open    2:LE open
        for eyeLoop = 1: numel(TheEyes) % 1: LE data in ET 2: RE data in ET
            clear OKN_GainAllmean    OKN_GainAllStd    OKN_GainAll
            IntLe = 1: NoInterleavedInCondition(subLoop,conLoop);
            StimInd = 1: numel(StimValsInCondition(subLoop,conLoop,:));
            ContInd = 1: sum(squeeze(ContrastValsInCondition(subLoop,conLoop,:))>0);
            MaskInd  = 1: numel(MaskValsInCondition(subLoop,conLoop,:));
            Xdata = squeeze(ContrastValsInCondition(subLoop,conLoop,:));
            Xdata =Xdata(Xdata>0);
            OKN_GainAll = (cell2mat(reshape(squeeze(AllGLMgainstore_H(subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept)),max(IntLe),max(ContInd),E.Settings.LocalTrialNo)));
            OKN_ScoreAll = (cell2mat(reshape(squeeze(AllOKNscorestore(subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept)),max(IntLe),max(ContInd),E.Settings.LocalTrialNo)));
             
           OKN_GainAll_rightward = squeeze(OKN_GainAll(1,:,:))'; % 1: rightwards drift interleave
           OKN_GainAll_leftward = squeeze(OKN_GainAll(2,:,:))'; % 2: leftwards drift interleave
           
           OKN_ScoreAll_rightward = squeeze(OKN_ScoreAll(1,:,:))';
           OKN_ScoreAll_leftward = squeeze(OKN_ScoreAll(1,:,:))';

            
            % (Drifting directions*repetition), contrast levels
            OKN_Gain = reshape(OKN_GainMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(P.PossContrastValues{1}));
            OKN_Score = reshape(OKN_ScoreMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(P.PossContrastValues{1}));
            
            OKN_GainAllmean = nanmean(OKN_GainAll,3); % mean calculated for each drifting direction and contrast level
            OKN_GainAllStd  = nanstd (OKN_GainAll,0,3);
            
            OKN_GainMean_rightward = mean(OKN_GainAll_rightward);
            OKN_GainMean_leftward = mean(OKN_GainAll_leftward);

            
            % load pupil data
            pupilAll = (reshape(squeeze({AllPstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),max(IntLe),max(ContInd),E.Settings.LocalTrialNo));
            timeAll = (reshape(squeeze({AllTstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),max(IntLe),max(ContInd),E.Settings.LocalTrialNo));
            
            pupilMedian = cellfun(@median, pupilAll);
            pupilMAD = cellfun(@mad, pupilAll);
            %             GainMatrix{subLoop,conLoop,eyeLoop,:} = OKN_Gain;
            %             MeanOfGain(subLoop,:) = mean((GainMatrix{subLoop}));
            %             STDofGain(subLoop,:) =std(GainMatrix{subLoop});
            
            if 0
                % % plotting
                figure((subLoop-1)*4+conLoop)
                FigNameStr = sprintf('Name:%s Open eye: O%s ',ObserverName{subLoop}, EyeListFileNames(conLoop));
                set(gcf,'Name',FigNameStr);
                %             figure(20+conLoop),
                subplot(1,numel(TheEyes),eyeLoop);
                errorbar(Xdata,mean(OKN_Gain,1),std(OKN_Gain),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                
                hold on;
                plot(Xdata+0.1.*Xdata,OKN_Gain,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                if eyeLoop ==1
                    ylabel('OKN-Gain')
                    xlabel('Contrast (%)')
                    legend('Mean � SD','Gain of each trial','location','best')
                end
                
                if ismember(TheEyes(eyeLoop),OpenEyeNoInCondition(subLoop,conLoop,:))
                    OKNtypeStr ='Direct Response';
                else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
                    OKNtypeStr ='Indirect Response';
                end
                
                title(sprintf('%sE %s',EyeList(TheEyes(eyeLoop)), OKNtypeStr))
                xticks(Xdata)
                set(gca,'xscale','log','box','off');
                axis([-1 max(Xdata)+0.2*max(Xdata) -0.3 0.9])
            end
        end
    end
end
% BetweenObserverSTD = std(MeanOfGain);
% WithinObserverSTD = rms(STDofGain);

%% Summary plot contrast + 100 percent (Left-ward and right-ward movement from direction condition); JUST THE DIRECT RESPONSES

% Experimental conditions
% 1 = RE Direction
% 2 = LE Direction
% 3 = RE Contrast
% 4 = LE Contrast
% conLoop=3:4;   % 3 = RE contrast % 4 = LE contrast
runLoop =1;
IntLeRept = 1: E.Settings.LocalTrialNo;
% Cmap = [0.4 0.4 0.4]; use previous Cmap
% eyeLoop = 1:2; % both eye are recorded
Cmap = linspecer(numel(ObserverName));

colMap = linspecer(3,'Qualitative');
red = colMap(2,:);
green = colMap(3,:);
blue = colMap(1,:);

DriftingDirections_ContrastCondition = {'Right', 'Left'};
AllSubOKNGain  = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo.*NoInterleavedInCondition(ObserverRagne(1),3),numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubOKNScore = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo.*NoInterleavedInCondition(ObserverRagne(1),3),numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubOKNGain_Rward = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubOKNGain_Lward = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubOKNScore_Rward = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubOKNScore_Lward = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubContrastPupil = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo.*NoInterleavedInCondition(ObserverRagne(1),3),numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubContrastPupilMAD = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo.*NoInterleavedInCondition(ObserverRagne(1),3),numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubContrastPupil_Rward = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%
AllSubContrastPupil_Lward = nan(numel(ObserverName),numel(EyeList),E.Settings.LocalTrialNo,numel(squeeze(ContrastValsInCondition(1,3,:)))+1); % size of all contrast + 100%

for subLoop = ObserverRagne % 1: numel(ObserverName)
    for conLoop =  1:4 % direction conditions 1:RE open    2:LE open
        for eyeLoop = OpenEyeNoInCondition(subLoop,conLoop,:) % Just load the stimulated eye 1: LE data in ET 2: RE data in ET
            clear OKN_GainAllmean    OKN_GainAllStd    OKN_GainAll pupilAll timeAll OKN_Gain_rightward OKN_Gain_leftward
            
            StimInd = 1: numel(StimValsInCondition(subLoop,conLoop,:));
            ContInd = 1: sum(squeeze(ContrastValsInCondition(subLoop,conLoop,:))>0);
            MaskInd  = 1: numel(MaskValsInCondition(subLoop,conLoop,:));
            Xdata = squeeze(ContrastValsInCondition(subLoop,conLoop,:));
            Xdata =Xdata(Xdata>0);
            if conLoop>=3 % i.e for contrast conditions
                IntLe = 1: NoInterleavedInCondition(subLoop,conLoop);
                ColNo = ContInd;
            else % i.e for direction conditions
                IntLe = [1 3]; % i.e RightWard and Leftward drifting
                ColNo = 6;
            end
            OKN_GainAll = (cell2mat(reshape(squeeze({AllGLMgainstore_H{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),numel(ContInd),E.Settings.LocalTrialNo)));
            OKN_ScoreAll = (cell2mat(reshape(squeeze({AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),numel(ContInd),E.Settings.LocalTrialNo)));
            
            % Drifting directions , repetition , contrast levels
            OKN_GainMatrix = (permute(OKN_GainAll,[1 3 2]));
            OKN_ScoreMatrix = (permute(OKN_ScoreAll,[1 3 2]));
            % (Drifting directions*repetition), contrast levels
            OKN_Gain = reshape(OKN_GainMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(Xdata));
            OKN_Score = reshape(OKN_ScoreMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(Xdata));

           OKN_Gain_Rward = squeeze(OKN_GainAll(1,:,:))'; % 1: rightwards drift interleave
           OKN_Gain_Lward = squeeze(OKN_GainAll(2,:,:))'; % 2: leftwards drift interleave
           
           OKN_Score_Rward = squeeze(OKN_ScoreAll(1,:,:))';
           OKN_Score_Lward = squeeze(OKN_ScoreAll(2,:,:))';
           
            % load pupil data
            pupilAll = (reshape(squeeze({AllPstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),numel(ContInd),E.Settings.LocalTrialNo));
%             timeAll = (reshape(squeeze({AllTstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),numel(IntLe),numel(ContInd),E.Settings.LocalTrialNo));
            
               % Repetition, drifting directions , contrast levels
            pupilAllMatrix = (permute(pupilAll,[3 1 2]));
%             timeAllMatrix = (permute(timeAll,[3 1 2]));
            
            pupilMatrix = reshape(pupilAllMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(Xdata)); % stack all rightwards then all leftwards on top of eachother
%             timeMatrix = reshape(timeAllMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(Xdata));
            
            pupilMedian = (cellfun(@median, pupilAllMatrix));
            pupilMAD = (cellfun(@mad, pupilAllMatrix));
            
            
            AllSubOKNGain(subLoop,eyeLoop,:,ColNo)  = OKN_Gain;
            AllSubOKNScore(subLoop,eyeLoop,:,ColNo) = OKN_Score;
            
            AllSubOKNGain_Rward(subLoop,eyeLoop,:,ColNo)  = OKN_Gain_Rward;
            AllSubOKNGain_Lward(subLoop,eyeLoop,:,ColNo)  = OKN_Gain_Lward;

            AllSubOKNScore_Rward(subLoop,eyeLoop,:,ColNo) = OKN_Score_Rward;
            AllSubOKNScore_Lward(subLoop,eyeLoop,:,ColNo) = OKN_Score_Lward;
            
            AllSubContrastPupil(subLoop,eyeLoop,:,ColNo) = (cellfun(@median, pupilMatrix)) ;
            AllSubContrastPupilMAD(subLoop,eyeLoop,:,ColNo) = (cellfun(@mad, pupilMatrix));
            
            AllSubContrastPupil_Rward (subLoop,eyeLoop,:,ColNo) = squeeze(pupilMedian(:,1,:));
            AllSubContrastPupil_Lward (subLoop,eyeLoop,:,ColNo) = squeeze(pupilMedian(:,2,:));
            % % plotting
            AllXdata = [round(squeeze(ContrastValsInCondition(ObserverRagne(1),3,:)),2)' 1];
            if 0 % indivisual plots
                figure((subLoop)*80)
                FigNameStr = sprintf('Name: %s',ObserverName{subLoop});
                set(gcf,'Name',FigNameStr);
                
                subplot(1,numel(TheEyes),mod(conLoop,2)+1);
                AverageData = mean([median(OKN_Gain_Rward);median(OKN_Gain_Lward)]) ; errData = hypot(mad(OKN_Gain_Rward,1),mad(OKN_Gain_Lward,1));
                errorbar(Xdata, AverageData,errData,'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                
                hold on;
                plot(Xdata+0.1.*Xdata,OKN_Gain,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap(subLoop,:),'Color',Cmap(subLoop,:));
                if eyeLoop ==1
                    ylabel('OKN-Gain')
                    xlabel('Contrast (%)')
                    legend('Mean � SD','Gain of each trial','location','best')
                end
                
                if ismember(TheEyes(eyeLoop),OpenEyeNoInCondition(subLoop,conLoop,:))
                    OKNtypeStr ='Direct Response';
                else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
                    OKNtypeStr ='Indirect Response';
                end
                
                title(sprintf('%sE %s',EyeList(TheEyes(eyeLoop)), OKNtypeStr))
                xticks(AllXdata)
                set(gca,'xscale','log','box','off');
                axis([-1 AllXdata(end)+0.2*AllXdata(end) -0.2 0.9])
            end
        end
    end
    
    REGain = squeeze(AllSubOKNGain(subLoop,2,:,:)); % 2: right eye
    REGain_Rward = squeeze(AllSubOKNGain_Rward(subLoop,2,:,:));
    REGain_Lward = squeeze(AllSubOKNGain_Lward(subLoop,2,:,:));
    REScore = squeeze(AllSubOKNScore(subLoop,2,:,:));
    REScore_Rward = squeeze(AllSubOKNScore_Rward(subLoop,2,:,:));
    REScore_Lward = squeeze(AllSubOKNScore_Lward(subLoop,2,:,:));
    REmean = mean(REGain);
    RESD = std(REGain);
    
    LEGain = squeeze(AllSubOKNGain(subLoop,1,:,:)); % 1: left eye
    LEGain_Rward = squeeze(AllSubOKNGain_Rward(subLoop,1,:,:));
    LEGain_Lward = squeeze(AllSubOKNGain_Lward(subLoop,1,:,:));
    LEScore = squeeze(AllSubOKNScore(subLoop,1,:,:));
    LEScore_Rward = squeeze(AllSubOKNScore_Rward(subLoop,1,:,:));
    LEScore_Lward = squeeze(AllSubOKNScore_Lward(subLoop,1,:,:));
    LEmean = mean(LEGain);
    LESD = std(LEGain);
    
    REpupil = squeeze(AllSubContrastPupil(subLoop,2,:,:)); % 2: right eye
    REpupil_Rward = squeeze(AllSubContrastPupil_Rward(subLoop,2,:,:));
    REpupil_Lward = squeeze(AllSubContrastPupil_Lward(subLoop,2,:,:));
    
    LEpupil = squeeze(AllSubContrastPupil(subLoop,1,:,:)); %  1: left eye
    LEpupil_Rward = squeeze(AllSubContrastPupil_Rward(subLoop,1,:,:));
    LEpupil_Lward = squeeze(AllSubContrastPupil_Lward(subLoop,1,:,:));
    
    AllSubjectsREGain_Contrast{subLoop} = REGain;
    AllSubjectsREGain_Contrast_Rward{subLoop} = REGain_Rward;
    AllSubjectsREGain_Contrast_Lward{subLoop} = REGain_Lward;
    
    AllSubjectsLEGain_Contrast{subLoop} = LEGain;
    AllSubjectsLEGain_Contrast_Rward{subLoop} = LEGain_Rward;
    AllSubjectsLEGain_Contrast_Lward{subLoop} = LEGain_Lward;
    
    AllSubjectsREScore_Contrast{subLoop} = REScore;
    AllSubjectsREScore_Contrast_Rward{subLoop} = REScore_Rward;
    AllSubjectsREScore_Contrast_Lward{subLoop} = REScore_Lward;
    
    AllSubjectsLEScore_Contrast{subLoop} = LEScore;
    AllSubjectsLEScore_Contrast_Rward{subLoop} = LEScore_Rward;
    AllSubjectsLEScore_Contrast_Lward{subLoop} = LEScore_Lward;
    
    AllSubjectsREpupil_Contrast{subLoop} = REpupil;
    AllSubjectsREpupil_Contrast_Rward{subLoop} = REpupil_Rward;
    AllSubjectsREpupil_Contrast_Lward{subLoop} = REpupil_Lward;
    
    AllSubjectsLEpupil_Contrast{subLoop} = LEpupil;
    AllSubjectsLEpupil_Contrast_Rward{subLoop} = LEpupil_Rward;
    AllSubjectsLEpupil_Contrast_Lward{subLoop} = LEpupil_Lward;
    
    if BetterEye(subLoop) == 2 % 2: right eye is the better eye
        ThisSubBetterEyeGain = REGain;
        ThisSubBetterEyeGain_Rward = REGain_Rward;
        ThisSubBetterEyeGain_Lward = REGain_Lward;
        
        ThisSubWorseEyeGain = LEGain;
        ThisSubWorseEyeGain_Rward = LEGain_Rward;
        ThisSubWorseEyeGain_Lward = LEGain_Lward;
        
        ThisSubBetterEyeScore = REScore;
        ThisSubBetterEyeScore_Rward = REScore_Rward;
        ThisSubBetterEyeScore_Lward = REScore_Lward;
        
        ThisSubWorseEyeScore = LEScore;
        ThisSubWorseEyeScore_Rward = LEScore_Rward;
        ThisSubWorseEyeScore_Lward = LEScore_Lward;
        
        ThisSubBetterEyePupil = REpupil;
        ThisSubBetterEyePupil_Rward = REpupil_Rward;
        ThisSubBetterEyePupil_Lward = REpupil_Lward;
        
        ThisSubWorseEyePupil = LEpupil;
        ThisSubWorseEyePupil_Rward = LEpupil_Rward;
        ThisSubWorseEyePupil_Lward = LEpupil_Lward;
        
    elseif BetterEye(subLoop) ==1 % 1: left eye is the better eye
        ThisSubBetterEyeGain = LEGain;
        ThisSubBetterEyeGain_Rward = LEGain_Rward;
        ThisSubBetterEyeGain_Lward = LEGain_Lward;
        
        ThisSubWorseEyeGain = REGain;
        ThisSubWorseEyeGain_Rward = REGain_Rward;
        ThisSubWorseEyeGain_Lward = REGain_Lward;
        
        ThisSubBetterEyeScore = LEScore;
        ThisSubBetterEyeScore_Rward = LEScore_Rward;
        ThisSubBetterEyeScore_Lward = LEScore_Lward;
        
        ThisSubWorseEyeScore = REScore;
        ThisSubWorseEyeScore_Rward = REScore_Rward;
        ThisSubWorseEyeScore_Lward = REScore_Lward;
        
        ThisSubBetterEyePupil = LEpupil;
        ThisSubBetterEyePupil_Rward = LEpupil_Rward;
        ThisSubBetterEyePupil_Lward = LEpupil_Lward;
        
        ThisSubWorseEyePupil = REpupil;
        ThisSubWorseEyePupil_Rward = REpupil_Rward;
        ThisSubWorseEyePupil_Lward = REpupil_Lward;
    end
    
    AllSubjectsBetterEyeGain_Contrast{subLoop} = ThisSubBetterEyeGain;
    AllSubjectsBetterEyeGain_Contrast_Rward{subLoop} = ThisSubBetterEyeGain_Rward;
    AllSubjectsBetterEyeGain_Contrast_Lward{subLoop} = ThisSubBetterEyeGain_Lward;
    
    AllSubjectsWorseEyeGain_Contrast{subLoop}  = ThisSubWorseEyeGain;
    AllSubjectsWorseEyeGain_Contrast_Rward{subLoop} = ThisSubWorseEyeGain_Rward;
    AllSubjectsWorseEyeGain_Contrast_Lward{subLoop} = ThisSubWorseEyeGain_Lward;
    
    AllSubjectsBetterEyeScore_Contrast{subLoop} = ThisSubBetterEyeScore;
    AllSubjectsBetterEyeScore_Contrast_Rward{subLoop} = ThisSubBetterEyeScore_Rward;
    AllSubjectsBetterEyeScore_Contrast_Lward{subLoop} = ThisSubBetterEyeScore_Lward;
    
    AllSubjectsWorseEyeScore_Contrast{subLoop}  = ThisSubWorseEyeScore;
    AllSubjectsWorseEyeScore_Contrast_Rward{subLoop} = ThisSubWorseEyeScore_Rward;
    AllSubjectsWorseEyeScore_Contrast_Lward{subLoop} = ThisSubWorseEyeScore_Lward;
    
    AllSubjectsBetterEyePupil_Contrast{subLoop} = ThisSubBetterEyePupil;
    AllSubjectsBetterEyePupil_Contrast_Rward{subLoop} = ThisSubBetterEyePupil_Rward;
    AllSubjectsBetterEyePupil_Contrast_Lward{subLoop} = ThisSubBetterEyePupil_Lward;
    
    AllSubjectsWorseEyePupil_Contrast{subLoop}  = ThisSubWorseEyePupil;
    AllSubjectsWorseEyePupil_Contrast_Rward{subLoop} = ThisSubWorseEyePupil_Rward;
    AllSubjectsWorseEyePupil_Contrast_Lward{subLoop} = ThisSubWorseEyePupil_Lward;
    
    ThisSubDifBetweenEyes = LEGain-REGain;
    
    % calculate critical t for these data
    SampleSize = size(ThisSubDifBetweenEyes,1);
    ConfidenceLevel = .95;
    SampleMean = mean(ThisSubDifBetweenEyes);
    SampleSD = std(ThisSubDifBetweenEyes);
    
    DegreeofFredom = SampleSize- 1;
    Alpha = (1- ConfidenceLevel)./2;
    
    criticalT = abs(tinv(Alpha,DegreeofFredom));
    
    Int = (SampleSD./sqrt(SampleSize)).*criticalT;
    UpperCI = SampleMean+Int;
    LowerCI = SampleMean-Int;
    DifferenceBetweenEyes(subLoop,:) =  nanmean(ThisSubDifBetweenEyes);
    DifferenceBetweenEyesConfInt(subLoop,:) = Int;
    
    if 0
        figure;
        errorbar(AllXdata, SampleMean, SampleSD,'o-','LineWidth',2,'MarkerSize',12,'MarkerFaceColor',Cmap(subLoop,:),'Color', Cmap(subLoop,:))
        % errorbar(AllXdata, mean(ThisSubDifBetweenEyes), std(ThisSubDifBetweenEyes),'o-','LineWidth',2,'MarkerSize',12,'MarkerFaceColor',Cmap(subLoop,:),'Color', Cmap(subLoop,:))
        hold on
        plot(AllXdata+0.1*AllXdata, ThisSubDifBetweenEyes,'.','LineWidth',2,'MarkerSize',5,'Color', Cmap(subLoop,:))
        ylabel('RE and LE gian diff')
        xlabel('Contrast (%)')
        legend('Mean � SD','Gain of each trial','location','best')
        xticks(AllXdata)
        set(gca,'xscale','log','box','off');
        axis([-1 AllXdata(end)+0.2*AllXdata(end) -0.55 0.75])
    end
end

%% Saving RE and LE gain values in a mat file for both Direction and contrast condition
if 0
    save('All41SubjectsGains_Contrast_n_Direction','AllSubjectsREGain_Contrast', 'AllSubjectsLEGain_Contrast' , 'AllSubjectsBetterEyeGain_Contrast', 'AllSubjectsWorseEyeGain_Contrast' ,...
        'AllSubjectsREGain_Direction', 'AllSubjectsLEGain_Direction' , 'AllSubjectsBetterEyeGain_Direction', 'AllSubjectsWorseEyeGain_Direction' ,...
        'ContrastLevels', 'DriftingDirections_DirectionCondition', 'DriftingDirections_ContrastCondition',...
        'ObserverName','ObserverID', 'WorseEye','BetterEye','WorseEyeVFI', 'BetterEyeVFI', 'WorseEyeMD', 'BetterEyeMD','E', 'S', 'P',...; % , 'AllXstore', 'AllYstore', 'AllTstore', 'AllXSpdstore',...
        'NoInterleavedInCondition', 'ContrastValsInCondition', 'MaskValsInCondition', 'StimValsInCondition', 'OpenEyeNoInCondition');
    %         'AllYSpdstore', 'AllTSpdstore' , 'AllSpdVecstore' , 'AllOKNscorestore', 'AllOKNConsistencystore', 'AllKeyRespstore',...
    %         'AllGLMgainstore', 'AllTotalDisRatiostore',  'AllConsistentDistRatiostore')
    %
end

if 0
    load('All41SubjectsGains_Contrast_n_Direction.mat');
end

if 0
    save('All41SubjectsGains_Contrast_n_Direction.mat','NR_param_WE_C50','NR_params_AllSubjs_WE','NR_param_BE_C50','NR_params_AllSubjs_BE','-append')
end
%% plot shaded graph of the means between better eye and worse eye across all participants
ObserverVFIratio =  [WorseEyeVFI./BetterEyeVFI  nanmean(WorseEyeVFI./BetterEyeVFI )].*100;% the last number is the mean; the higher this ratio the more similar the afected eye to the better eye
[SortedObserverVFI, sortIdx] = sort(ObserverVFIratio(1:end-1)); % Will sort the VFI from the worst (most affected) to the best (least affected) patient
% ObserverRagnePlusMean = [ObserverRagne numel(ObserverName)+1];
ContrastLevels = [P.PossContrastValues{1}, 1].*100;
ObserverRagne = 1:numel(ObserverName);

close all
for subLoop = ObserverRagne %    1: numel(ObserverName)+1 % +1 one for ploting the pooled data across all participants
%     if subLoop <= numel(ObserverName) % means each subject
        if 0 % use mean
            BEaverage = nanmean([nanmean(AllSubjectsBetterEyeGain_Contrast_Rward{subLoop}); nanmean(AllSubjectsBetterEyeGain_Contrast_Lward{subLoop})]);
            WEaverage = nanmean([nanmean(AllSubjectsWorseEyeGain_Contrast_Rward{subLoop}); nanmean(AllSubjectsWorseEyeGain_Contrast_Lward{subLoop})]);
            BEerr = hypot(nanstd(AllSubjectsBetterEyeGain_Contrast_Rward{subLoop}), nanstd(AllSubjectsBetterEyeGain_Contrast_Lward{subLoop}));
            WEerr = hypot(nanstd(AllSubjectsWorseEyeGain_Contrast_Rward{subLoop}), nanstd(AllSubjectsWorseEyeGain_Contrast_Lward{subLoop}));
            
             errStr = '�1SD';
             
        else % use median
            % gain
            BEaverage = nanmean([nanmedian(AllSubjectsBetterEyeGain_Contrast_Rward{subLoop}); nanmedian(AllSubjectsBetterEyeGain_Contrast_Lward{subLoop})]);
            WEaverage = nanmean([nanmedian(AllSubjectsWorseEyeGain_Contrast_Rward{subLoop}); nanmedian(AllSubjectsWorseEyeGain_Contrast_Lward{subLoop})]);
            BEerr = hypot(mad(AllSubjectsBetterEyeGain_Contrast_Rward{subLoop},1), mad(AllSubjectsBetterEyeGain_Contrast_Lward{subLoop},1));
            WEerr = hypot(mad(AllSubjectsWorseEyeGain_Contrast_Rward{subLoop},1), mad(AllSubjectsWorseEyeGain_Contrast_Lward{subLoop},1));
            % pupil
            BEaverage_pupil = nanmean([nanmedian(AllSubjectsBetterEyePupil_Contrast_Rward{subLoop}); nanmedian(AllSubjectsBetterEyePupil_Contrast_Lward{subLoop})]);
            WEaverage_pupil = nanmean([nanmedian(AllSubjectsWorseEyePupil_Contrast_Rward{subLoop}); nanmedian(AllSubjectsWorseEyePupil_Contrast_Lward{subLoop})]);
            BEerr_pupil = hypot(mad(AllSubjectsBetterEyePupil_Contrast_Rward{subLoop},1), mad(AllSubjectsBetterEyePupil_Contrast_Lward{subLoop},1));
            WEerr_pupil  = hypot(mad(AllSubjectsWorseEyePupil_Contrast_Rward{subLoop},1), mad(AllSubjectsWorseEyePupil_Contrast_Lward{subLoop},1));
            
            errStr = '�1MAD';
        end
        subjName = ObserverName{subLoop};
%     else % means calculate the mean across all participants
%         WEmean = nanmean(cell2mat(AllSubjectsWorseEyeGain_Contrast'));
%         BEmean = nanmean(cell2mat(AllSubjectsBetterEyeGain_Contrast'));
%         subjName = sprintf('Mean across %i participants\n', numel(ObserverName));
%     end
    OKNLossIndex(subLoop) = (nansum(BEaverage-WEaverage)./nansum(BEaverage)).*100;
    OKNVFRatio(subLoop) = nansum(WEaverage)./nansum(BEaverage).* 100; % to campare with VFI ratio. area under two graphs  [what about nanmean((WEaverage)./(BEaverage)).* 100;]%
    OKNmidFreqVFRatio(subLoop) = nansum(WEaverage(2:5))./nansum(BEaverage(2:5)).* 100; % the equivalent of VFI ratio just with mid freqs
    AllSubjBEaverage(subLoop,:)=BEaverage;
    AllSubjWEaverage(subLoop,:)=WEaverage;
    AllSubjBEaverage_pupil(subLoop,:)=BEaverage_pupil;
    AllSubjWEaverage_pupil(subLoop,:)=WEaverage_pupil;
   
    %     ContrastLevels = AllXdata*100;
    if 1 % i.e draw gain plots or no?
        figure(1000)
        set (gcf, 'Units','normalized','Position',[0  0.0278 1.0000  0.9218]);
        
        subplot(4,11,subLoop)
        
        if 0 % normal plot with lines
            WEPLOT = plot(ContrastLevels, WEaverage,'ro-','LineWidth',2,'Markersize',9,'MarkerFaceColor',red,'Color',red);
            hold on
            BEPLOT = plot(ContrastLevels, BEaverage,'Go-','LineWidth',2,'Markersize',9,'MarkerFaceColor',green,'Color',green);
        else % i.e. scatter plot
            WEPLOT = scatter(ContrastLevels, WEaverage,[],'MarkerFaceColor',red,'SizeData',100,'MarkerFaceAlpha',0.80,'MarkerEdgeColor','k','LineWidth',1);
            hold on
            BEPLOT = scatter(ContrastLevels, BEaverage,[],'MarkerFaceColor',green,'SizeData',100,'MarkerFaceAlpha',0.80,'MarkerEdgeColor','k','LineWidth',1);
        end
        if 1 % plot the standard deviation error bars neg bars for the WE and pos bars for the BE
            for i = 1: numel(ContrastLevels)
                ThisPointNegErrBarVals = [WEaverage(i) WEaverage(i)-WEerr(i)];
                ThisPointPosErrBarVals = [BEaverage(i) BEaverage(i)+BEerr(i)];
                WEPLOTnegErrBar = plot([ ContrastLevels(i) ContrastLevels(i)], ThisPointNegErrBarVals,'-','LineWidth',2,'Color',[red ,0.8]);
                BEPLOTnegErrBar = plot([ ContrastLevels(i) ContrastLevels(i)], ThisPointPosErrBarVals,'-','LineWidth',2,'Color',[green ,0.8]);
            end
        end
        
        if 1 % Fit and plot Naka_Rushton functions
            BaseLine = 0;
            fitXdataIn = ContrastLevels;
            fitYDataIn_WE = WEaverage;
            fitYDataIn_BE = BEaverage;
            %     fitYDataIn = [ThisSubjWEaverage(NR_inputRange(1:end-1)),max(ThisSubjWEaverage(end),ThisSubjBEaverage(end))]-BaseLine;
            RmaxUppBound_WE = 1000; %max(fitYDataIn_WE);
            RmaxUppBound_BE = 1000; %  max(fitYDataIn_BE);
            
            [NR_params_WE, f_WE]= FitNakaRushton(fitXdataIn,fitYDataIn_WE,[],[RmaxUppBound_WE    100000	100]);
            [NR_params_BE, f_BE]= FitNakaRushton(fitXdataIn,fitYDataIn_BE,[],[RmaxUppBound_BE   100000	100] );
            
            FineX=linspace(ContrastLevels(1),ContrastLevels(end),1000);
            
            NR_prediction_WE = BaseLine+(ComputeNakaRushton(NR_params_WE,FineX));
            NR_ExpectedVals_WE = BaseLine+(ComputeNakaRushton(NR_params_WE,fitXdataIn));
            NR_Prediction_WE_Rmax(subLoop) = NR_params_WE(1);
            NR_Prediction_WE_C50(subLoop) = NR_params_WE(2);
            NR_Prediction_WE_C50sGain(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_WE,NR_Prediction_WE_C50(subLoop) )); % it is redundant becasue the gain of C50 is Rmax/2.
            NR_params_AllSubjs_WE(subLoop,:) =NR_params_WE;
            NR_ExpectedVals_AllSubjs_WE(subLoop,:) =NR_ExpectedVals_WE;
            
            
            NR_prediction_BE = BaseLine+(ComputeNakaRushton(NR_params_BE,FineX));
            NR_ExpectedVals_BE = BaseLine+(ComputeNakaRushton(NR_params_BE,fitXdataIn));
            NR_Prediction_BE_Rmax(subLoop) = NR_params_BE(1);
            NR_Prediction_BE_C50(subLoop) = NR_params_BE(2);
            NR_Prediction_BE_C50sGain(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_BE,NR_Prediction_BE_C50(subLoop) )); % it is redundant becasue the gain of C50 is Rmax/2.
            NR_Prediction_WE_C50sGain_basedOn_BEC50(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_WE,NR_Prediction_BE_C50(subLoop) )); % This is NOT redundant
            NR_params_AllSubjs_BE(subLoop,:) =NR_params_BE;
            NR_ExpectedVals_AllSubjs_BE(subLoop,:) =NR_ExpectedVals_BE;
            
            % plot the NR fits
            WEnrPlot = plot(FineX,NR_prediction_WE,'Color',[red-0.2, 0.7]','LineWidth',4) ;
            BEnrPlot = plot(FineX,NR_prediction_BE,'Color',[green-0.2, 0.7]','LineWidth',4) ;
        end
        
        if 0 % linear plot the patch
            patch([ContrastLevels fliplr(ContrastLevels)], [WEaverage fliplr(BEaverage)], 'k','FaceAlpha',0.2,'EdgeAlpha',0.1)
        elseif 0 %i.e NR patch
            patch([FineX(1:1:end) fliplr(FineX(1:1:end))], [NR_prediction_WE(1:1:end) fliplr(NR_prediction_BE(1:1:end))], 'k','FaceAlpha',0.2,'EdgeAlpha',0.1)
            
        end
        
        hold off
        %         title(sprintf('%s\nVFR: %2.0f\nOKNR:
        %         %2.2f',subjName,round(ObserverVFIratio(subLoop)),OKNVFRatio(subLoop))); % if you want the patient Inits
        if subLoop == 1
            ylabel('OKN-Gain')
            xlabel('Contrast (%)')
            %         title(sprintf('Patient %i  VFI ratio: %2.0f%% \nOKN ratio: %2.0f%%',subLoop,round(ObserverVFIratio(subLoop)),round(OKNVFRatio(subLoop))));
            legend([BEPLOT,WEPLOT,BEPLOTnegErrBar,], 'Better eye', 'Worse eye',errStr,'location', 'best' )
            if 0
                title(sprintf('P%i',subLoop));
            end
            
        else
            set(gca,'xscale','log','box','off','FontSize',10,'XMinorTick','off','XTickLabel','','YTickLabel','');
            if 0
                title(sprintf('P%i  [%2.0f%%]   [%2.0f%%]',subLoop,round(ObserverVFIratio(subLoop)),round(OKNVFRatio(subLoop))));
            end
        end
        axis([ContrastLevels(1)-0.2*ContrastLevels(1) ContrastLevels(end)+0.2*ContrastLevels(end)  -.10 1.1])
        xticks(round(ContrastLevels))
        xtickangle(70)
        set(gca,'xscale','log','box','off','FontSize',10,'XMinorTick','off','FontName', 'Verdana');
        
        if 1 % i.e. annotate the patient No and VFI
            TextXcoord = log(50);
            TextYcoord = 1.22;
            if subLoop<10%(WEaverage(4)-WEerr(4)<=.1) || (BEaverage(4)<=.1) || (BEaverage(3)+BEstd(3) <=.8)
                text (TextXcoord-0.0, TextYcoord, sprintf('\n#%i (    ,    )', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
                text (TextXcoord+0.01, TextYcoord, sprintf('\n       %2.0f', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
                text (TextXcoord+0.8, TextYcoord, sprintf('\n           %2.0f', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
                
            elseif BetterEyeVFI(subLoop)==100
                text (TextXcoord-0.0, TextYcoord, sprintf('\n#%i (      ,    )', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
                text (TextXcoord+2.6, TextYcoord, sprintf('\n     %2.0f', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
                text (TextXcoord+4, TextYcoord, sprintf('\n           %2.0f', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
            else
                text (TextXcoord-0.0, TextYcoord, sprintf('\n#%i (     ,    )', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
                text (TextXcoord+3, TextYcoord, sprintf('\n     %2.0f', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
                text (TextXcoord+4, TextYcoord, sprintf('\n          %2.0f', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
                
            end
            
            %         text (TextXcoord, TextYcoord, sprintf('\nP%i', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
            %         text (TextXcoord, TextYcoord-0.02, sprintf('\n\nVFI = %2.0f%%', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
            %         text (TextXcoord, TextYcoord-0.04, sprintf('\n\n\nVFI = %2.0f%%', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
            
        end
        
    end
    
    if 1 % i.e draw pupil plots or no?
        figure(1002)
        set (gcf, 'Units','normalized','Position',[0  0.0278 1.0000  0.9218]);
        
        subplot(4,11,subLoop)
        BEplotData = BEaverage_pupil;
        WEplotData = WEaverage_pupil;
        BEplotDataErr = BEerr_pupil;
        WEplotDataErr = WEerr_pupil;
        if 0 % normal plot with lines
            WEPLOT = plot(ContrastLevels, WEplotData,'ro-','LineWidth',2,'Markersize',9,'MarkerFaceColor',red,'Color',red);
            hold on
            BEPLOT = plot(ContrastLevels, BEplotData,'Go-','LineWidth',2,'Markersize',9,'MarkerFaceColor',green,'Color',green);
        else % i.e. scatter plot
            WEPLOT = scatter(ContrastLevels, WEplotData,[],'MarkerFaceColor',red,'SizeData',100,'MarkerFaceAlpha',0.80,'MarkerEdgeColor','k','LineWidth',1);
            hold on
            BEPLOT = scatter(ContrastLevels, BEplotData,[],'MarkerFaceColor',green,'SizeData',100,'MarkerFaceAlpha',0.80,'MarkerEdgeColor','k','LineWidth',1);
        end
        if 1 % plot the standard deviation error bars neg bars for the WE and pos bars for the BE
            for i = 1: numel(ContrastLevels)
                ThisPointNegErrBarVals = [WEplotData(i) WEplotData(i)-WEplotDataErr(i)];
                ThisPointPosErrBarVals = [BEplotData(i) BEplotData(i)+BEplotDataErr(i)];
                WEPLOTnegErrBar = plot([ ContrastLevels(i) ContrastLevels(i)], ThisPointNegErrBarVals,'-','LineWidth',2,'Color',[red ,0.8]);
                BEPLOTnegErrBar = plot([ ContrastLevels(i) ContrastLevels(i)], ThisPointPosErrBarVals,'-','LineWidth',2,'Color',[green ,0.8]);
            end
        end
        
        if 1 % Fit and plot Naka_Rushton functions
            BaseLine = 0;
            fitXdataIn = ContrastLevels;
            fitYDataIn_WE = WEplotData;
            fitYDataIn_BE = BEplotData;
            %     fitYDataIn = [ThisSubjWEaverage(NR_inputRange(1:end-1)),max(ThisSubjWEaverage(end),ThisSubjBEaverage(end))]-BaseLine;
            RmaxUppBound_WE = 1000; %max(fitYDataIn_WE);
            RmaxUppBound_BE = 1000; %  max(fitYDataIn_BE);
            
            [NR_params_WE, f_WE]= FitNakaRushton(fitXdataIn,fitYDataIn_WE,[],[RmaxUppBound_WE    100000	100]);
            [NR_params_BE, f_BE]= FitNakaRushton(fitXdataIn,fitYDataIn_BE,[],[RmaxUppBound_BE   100000	100] );
            
            FineX=linspace(ContrastLevels(1),ContrastLevels(end),1000);
            
            NR_prediction_WE = BaseLine+(ComputeNakaRushton(NR_params_WE,FineX));
            NR_ExpectedVals_WE = BaseLine+(ComputeNakaRushton(NR_params_WE,fitXdataIn));
            NR_Prediction_WE_Rmax(subLoop) = NR_params_WE(1);
            NR_Prediction_WE_C50(subLoop) = NR_params_WE(2);
            NR_Prediction_WE_C50sGain(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_WE,NR_Prediction_WE_C50(subLoop) )); % it is redundant becasue the gain of C50 is Rmax/2.
            NR_params_AllSubjs_WE(subLoop,:) =NR_params_WE;
            NR_ExpectedVals_AllSubjs_WE(subLoop,:) =NR_ExpectedVals_WE;
            
            
            NR_prediction_BE = BaseLine+(ComputeNakaRushton(NR_params_BE,FineX));
            NR_ExpectedVals_BE = BaseLine+(ComputeNakaRushton(NR_params_BE,fitXdataIn));
            NR_Prediction_BE_Rmax(subLoop) = NR_params_BE(1);
            NR_Prediction_BE_C50(subLoop) = NR_params_BE(2);
            NR_Prediction_BE_C50sGain(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_BE,NR_Prediction_BE_C50(subLoop) )); % it is redundant becasue the gain of C50 is Rmax/2.
            NR_Prediction_WE_C50sGain_basedOn_BEC50(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_WE,NR_Prediction_BE_C50(subLoop) )); % This is NOT redundant
            NR_params_AllSubjs_BE(subLoop,:) =NR_params_BE;
            NR_ExpectedVals_AllSubjs_BE(subLoop,:) =NR_ExpectedVals_BE;
            
            % plot the NR fits
            WEnrPlot = plot(FineX,NR_prediction_WE,'Color',[red-0.2, 0.7]','LineWidth',4) ;
            BEnrPlot = plot(FineX,NR_prediction_BE,'Color',[green-0.2, 0.7]','LineWidth',4) ;
        end
        
        if 0 % linear plot the patch
            patch([ContrastLevels fliplr(ContrastLevels)], [WEaverage fliplr(BEaverage)], 'k','FaceAlpha',0.2,'EdgeAlpha',0.1)
        elseif 0 %i.e NR patch
            patch([FineX(1:1:end) fliplr(FineX(1:1:end))], [NR_prediction_WE(1:1:end) fliplr(NR_prediction_BE(1:1:end))], 'k','FaceAlpha',0.2,'EdgeAlpha',0.1)
            
        end
        
        hold off
        %         title(sprintf('%s\nVFR: %2.0f\nOKNR:
        %         %2.2f',subjName,round(ObserverVFIratio(subLoop)),OKNVFRatio(subLoop))); % if you want the patient Inits
        if subLoop == 1
            ylabel('Pupil diameter (mm)')
            xlabel('Contrast (%)')
            %         title(sprintf('Patient %i  VFI ratio: %2.0f%% \nOKN ratio: %2.0f%%',subLoop,round(ObserverVFIratio(subLoop)),round(OKNVFRatio(subLoop))));
            legend([BEPLOT,WEPLOT,BEPLOTnegErrBar,], 'Better eye', 'Worse eye',errStr,'location', 'best' )
            if 0
                title(sprintf('P%i',subLoop));
            end
            
        else
            set(gca,'xscale','log','box','off','FontSize',10,'XMinorTick','off','XTickLabel','','YTickLabel','');
            if 0
                title(sprintf('P%i  [%2.0f%%]   [%2.0f%%]',subLoop,round(ObserverVFIratio(subLoop)),round(OKNVFRatio(subLoop))));
            end
        end
        axis([ContrastLevels(1)-0.2*ContrastLevels(1) ContrastLevels(end)+0.2*ContrastLevels(end)  2 4.5])
        xticks(round(ContrastLevels))
        xtickangle(70)
        set(gca,'xscale','log','box','off','FontSize',10,'XMinorTick','off','FontName', 'Verdana');
        
        if 1 % i.e. annotate the patient No and VFI
            TextXcoord = log(50);
            TextYcoord = 4.75;
             if subLoop<10%(WEaverage(4)-WEerr(4)<=.1) || (BEaverage(4)<=.1) || (BEaverage(3)+BEstd(3) <=.8)
                text (TextXcoord-0.0, TextYcoord, sprintf('\n#%i (    ,    )', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
                text (TextXcoord+0.01, TextYcoord, sprintf('\n       %2.0f', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
                text (TextXcoord+0.8, TextYcoord, sprintf('\n           %2.0f', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
                
            elseif BetterEyeVFI(subLoop)==100
                text (TextXcoord-0.0, TextYcoord, sprintf('\n#%i (      ,    )', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
                text (TextXcoord+2.6, TextYcoord, sprintf('\n     %2.0f', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
                text (TextXcoord+4, TextYcoord, sprintf('\n           %2.0f', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
            else
                text (TextXcoord-0.0, TextYcoord, sprintf('\n#%i (     ,    )', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
                text (TextXcoord+3, TextYcoord, sprintf('\n     %2.0f', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
                text (TextXcoord+4, TextYcoord, sprintf('\n          %2.0f', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
            end
            
            %         text (TextXcoord, TextYcoord, sprintf('\nP%i', round(subLoop)),'FontSize', 10,'FontWeight','Bold', 'FontName','Verdana')
            %         text (TextXcoord, TextYcoord-0.02, sprintf('\n\nVFI = %2.0f%%', BetterEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',green)
            %         text (TextXcoord, TextYcoord-0.04, sprintf('\n\n\nVFI = %2.0f%%', WorseEyeVFI(subLoop)),'FontSize', 10,'FontWeight','normal', 'FontName','Verdana','Color',red)
            
        end
        
    end
    
end

%% Correlation between C50 vals and MD /VFI

% input1Vars =      {'BetterEyeVFI',          'WorseEyeVFI',           'BetterEyeVFI',               'WorseEyeVFI',                'WorseEyeVFI' };
input1Vars =      {'BetterEyeMD',          'WorseEyeMD',           'BetterEyeMD',               'WorseEyeMD',                'WorseEyeMD' };
input2Vars =      {'NR_Prediction_BE_C50', 'NR_Prediction_WE_C50', 'NR_Prediction_BE_C50sGain', 'NR_Prediction_WE_C50sGain', 'NR_Prediction_WE_C50sGain_basedOn_BEC50' };
goodFitIdxVars = {'goodFitIdx_BE',       'goodFitIdx_WE',       'goodFitIdx_BE',            'goodFitIdx_WE',            'goodFitIdx_BE'}; % the 5th is based on BE
% find the good NR fit indexes
[goodFitIdx_BE,~]  =  find(NR_params_AllSubjs_BE(:,2)<80);
[goodFitIdx_WE,~]  =  find(NR_params_AllSubjs_WE(:,2)<80);
for corrLoop =   6% : length(input1Vars)
    corrInput1 = (eval(input1Vars{corrLoop}))';
    corrInput2 = (eval(input2Vars{corrLoop}))';
    
    IdxBE = eval(goodFitIdxVars{corrLoop});
    corrInput1 = corrInput1(IdxBE);
    corrInput2 = corrInput2(IdxBE);
    
    
        figure(22)
    %   subplot(2,3,corrLoop)
    [rho,pval] = corr(corrInput1,corrInput2,'Type','pearson');
    
    [p,s]=polyfit(corrInput1,corrInput2,1);
    pred=polyval(p,(corrInput1));
    p1=plot(corrInput1,pred,'k-',corrInput1,corrInput2,'o'); hold on;
    if 0
        % create a normal distribtion based on Liu study to create norma MDs
        %    if not exist 'FullFieldC50sControl' load
        %    'OKN_Glaucoma_WorkeSpcae16042019.mat'
        
        MDmean = -0.48; MDsd = 1.03; % reported by liu et al. 2011
        theControlMDs = normrnd(MDmean,  MDsd,[length(FullFieldC50sControl) 1]); % plot C50s from SPVFL study
        
        plot(theControlMDs, FullFieldC50sControl,'ko','MarkerFaceColor', [1 1 1].*0.4,'MarkerSize',8);
    end
    set(p1(1),'LineWidth',2, 'Color',[1 1 1].*0.7);
    set(p1(2),'MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor',[1 1 1].*0.75);
    legend(sprintf('R=%3.3f\n(p=%5f)',rho,pval),'Glaucoma', 'Control','Location','northwest');
    xlabel(input1Vars{corrLoop},'Interpreter','none')
    ylabel(input2Vars{corrLoop},'Interpreter','none')
    % axis ([min(min([corrInput1, pred])).*1.1 max(max([corrInput1])).*.09 min(min([corrInput2; -0.2]))./1.5 max(max([corrInput2; 1])).*1.2],'square')
    axis ([min(min([corrInput1, pred])).*1.1 max(max([corrInput1]))+6 min(min([corrInput2; -0.2]))./1.5 max(max([corrInput2; 1])).*1.2],'square')
    set(gca, 'FontSize', 16)
    shg
end
%% Correlation of MD to C50 , BE and WE TOGETHER


% input1Vars =      {'BetterEyeVFI',          'WorseEyeVFI',           'BetterEyeVFI',               'WorseEyeVFI',                'WorseEyeVFI' };
% input1Vars =      {'BetterEyeMD',          'WorseEyeMD',           'BetterEyeMD',               'WorseEyeMD',                'WorseEyeMD' };
% input2Vars =      {'NR_Prediction_BE_C50', 'NR_Prediction_WE_C50', 'NR_Prediction_BE_C50sGain', 'NR_Prediction_WE_C50sGain', 'NR_Prediction_WE_C50sGain_basedOn_BEC50' };
% goodFitIdxVars = {'goodFitIdx_BE',       'goodFitIdx_WE',       'goodFitIdx_BE',            'goodFitIdx_WE',            'goodFitIdx_BE'}; % the 5th is based on BE
% find the good NR fit indexes
[goodFitIdx_BE,~]  =  find(NR_params_AllSubjs_BE(:,2)<80);
[goodFitIdx_WE,~]  =  find(NR_params_AllSubjs_WE(:,2)<80);
% for corrLoop =   1 : length(input1Vars)


% corrInput1 = ([BetterEyeMD(goodFitIdx_BE), WorseEyeMD(goodFitIdx_WE)]);
% corrInput2 = ([NR_Prediction_BE_C50(goodFitIdx_BE), NR_Prediction_WE_C50(goodFitIdx_WE)]);
% ContrastLevels
WhichContrast  = 1;
corrInput1 = ([BetterEyeVFI, WorseEyeVFI]); % ([BetterEyeMD, WorseEyeMD]);
corrInput2 = ([AllSubjBEaverage(:,WhichContrast)', AllSubjWEaverage(:,WhichContrast)']);
% corrInput2 = ([AllSubjBEaverage_pupil(:,WhichContrast)', AllSubjWEaverage_pupil(:,WhichContrast)']);

figure(22)
clf

[rho,pval] = corr(corrInput1',corrInput2','Type','spearman');

[p,S]=polyfit(corrInput1,corrInput2,1);
pred=polyval(p,(corrInput1));
BEpoints = 1:numel(goodFitIdx_BE);
WEpoints = numel(goodFitIdx_BE)+ (1:numel(goodFitIdx_WE));
if 1 % robust fit
    [xData, yData] = prepareCurveData( corrInput1, corrInput2 );
    
    % Set up fittype and options.
    ft = fittype( 'poly1' );
    opts = fitoptions( 'Method', 'LinearLeastSquares' );
    opts.Robust = 'Bisquare'; % LAR  Bisquare  According to mathwork : LAR is recommended for data with less outliers, Bisquare is recommended when outliers exist.
    opts.Lower = [-inf -Inf];
    opts.Upper = [inf Inf];
    % Fit model to data.
    [fitresult, gof,out] = fit( xData, yData, ft, opts );
    RobustPredictionLine = fitresult(corrInput1);
    figure(22),
    pl1 = plot(corrInput1, RobustPredictionLine, 'k-');
    set(pl1,'LineWidth',3, 'Color',[1 1 1].*0.5);hold on
end
if 0 % plot normal fit
    pl1=plot(corrInput1,pred,'k-')
    set(p11,'LineWidth',3, 'Color',[1 1 1].*0.5);,hold on;
end
pl2= scatter(corrInput1(WEpoints),corrInput2(WEpoints),'SizeData',170,'MarkerFaceColor', red,'MarkerEdgeColor','k','MarkerFaceAlpha',1,'LineWidth',1);
pl3= scatter(corrInput1(BEpoints),corrInput2(BEpoints),'SizeData',170,'MarkerFaceColor', green,'MarkerEdgeColor','k','MarkerFaceAlpha',1,'LineWidth',1);

% ref = refline(1,max(max([corrInput1]))+1);
% set(ref,'LineStyle','-.','Color',[.5 .5 .5])

legend([pl1, pl3, pl2 ],{sprintf('R=%3.3f\n(p=%5f)',rho,pval),'Better Eye', 'Worse Eye'},'Location','northwest');
xlabel('MD','Interpreter','none')
% ylabel('C_{50}')
ylabel('OKN gain for 25% contrast')
% axis ([min(min([corrInput1, pred])).*1.04  max(max([corrInput1]))+1  min(min([corrInput2, -0.2]))./0.5  max(max([corrInput2, 1])).*1.02],'square')
axis ([min(min([corrInput1, pred])).*1.04  max(max([corrInput1]))+1  min(min([corrInput2, -0.2]))./0.8  max(max([corrInput2, 1])).*.82],'square')
set(gca, 'FontSize', 16)
shg


box off
grid off


%% export an excel file from data
% export XLS file
if 0
    
    for subLoop = ObserverRagne
        % define all relevant vars for this subject to put in atable and
        % wite into an xls file
        ID = ObserverID{subLoop};
        AffectedEye = WorseEye(subLoop); % 1=LE 2=RE
        REGain =  round(AllSubjectsREGain_Contrast{subLoop},2);
        LEGain =  round(AllSubjectsLEGain_Contrast{subLoop},2);
        TableRowNameStrs = ['ID', 'Affected eye', 'RE Gain', 'LE Gain'];
        xlsOutputNameStr = fullfile( 'C:\Users\smoh205\Desktop\Temp Figs','GOKNout41P.xlsx');
        sheetNameStr = sprintf('(P%i) %s',subLoop,ID);
        thisSubTable = table(ID, AffectedEye);
        writetable(thisSubTable,xlsOutputNameStr,'Sheet',sheetNameStr,'Range','A3')
        
        ContrastLevelsStr = table([string(ContrastLevels), string(ContrastLevels)]);
        writetable(ContrastLevelsStr,xlsOutputNameStr,'Sheet',sheetNameStr,'Range','D4','WriteVariableNames',0)
        writetable(table('contrast'),xlsOutputNameStr,'Sheet',sheetNameStr,'Range','D3','WriteVariableNames',0)
        thisSubGainTable= table(REGain, LEGain);
        writetable(thisSubGainTable,xlsOutputNameStr,'Sheet',sheetNameStr,'Range','D5')
    end
end
%% fit Naka-Rushton using the data on first and end point of affected eye
% for this we need to guess the NR_params based of the first and end points
% in the graph
% first part obtain the NR parameters for BEs
BaseLine = 0;
NR_inputRange= find(ismember(ContrastLevels,ContrastLevels([1:end]))); % using the desired contrast levels as NR fit input
dotColor = [0.2 0.2 0.2];
for subLoop = ObserverRagne
    clear NR_params_WE NR_params_BE
    ThisSubjWEmean = nanmean(cell2mat(AllSubjectsWorseEyeGain_Contrast(subLoop))); % mode(round(cell2mat(AllSubjectsWorseEyeGain_Contrast(subLoop)),2));
    ThisSubjBEmean = nanmean(cell2mat(AllSubjectsBetterEyeGain_Contrast(subLoop)));%  mode(round(cell2mat(AllSubjectsBetterEyeGain_Contrast(subLoop)),2));
    
    fitXdataIn = ContrastLevels(NR_inputRange);
    fitYDataIn_WE = ThisSubjWEmean;
    fitYDataIn_BE = ThisSubjBEmean;
    %     fitYDataIn = [ThisSubjWEmean(NR_inputRange(1:end-1)),max(ThisSubjWEmean(end),ThisSubjBEmean(end))]-BaseLine;
    [NR_params_WE, f_WE]= FitNakaRushton(fitXdataIn,fitYDataIn_WE);
    [NR_params_BE, f_BE]= FitNakaRushton(fitXdataIn,fitYDataIn_BE);
    
    FineX=linspace(ContrastLevels(1),ContrastLevels(end),1000);
    
    %     ThePointsBE(subLoop) =
    ThisSubj_WE_Trials = cell2mat(AllSubjectsWorseEyeGain_Contrast(subLoop)); % individual trials to be plotted
    ThisSubj_BE_Trials = cell2mat(AllSubjectsBetterEyeGain_Contrast(subLoop));
    
    
    %     figure(10),plot(ContrastLevels, ThisSubj_WE_Trials,'.k', ContrastLevels,mean(ThisSubj_WE_Trials),'.r',ContrastLevels+1,median(ThisSubj_WE_Trials),'.g',ContrastLevels+2,mode(round(ThisSubj_WE_Trials,1)),'.b','MarkerSize',20)
    
    [NR_prediction_WE] = BaseLine+(ComputeNakaRushton(NR_params_WE,FineX));
    NR_Prediction_WE_Rmax(subLoop) = NR_params_WE(1);
    NR_Prediction_WE_C50(subLoop) = NR_params_WE(2);
    NR_Prediction_WE_C50sGain(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_WE,NR_Prediction_WE_C50(subLoop) )); % it is redundant becasue the gain of C50 is Rmax/2.
    NR_params_AllSubjs_WE(subLoop,:) =NR_params_WE;
    
    
    [NR_prediction_BE] = BaseLine+(ComputeNakaRushton(NR_params_BE,FineX));
    NR_Prediction_BE_Rmax(subLoop) = NR_params_BE(1);
    NR_Prediction_BE_C50(subLoop) = NR_params_BE(2);
    NR_Prediction_BE_C50sGain(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_BE,NR_Prediction_BE_C50(subLoop) )); % it is redundant becasue the gain of C50 is Rmax/2.
    NR_Prediction_WE_C50sGain_basedOn_BEC50(subLoop) = BaseLine+(ComputeNakaRushton(NR_params_WE,NR_Prediction_BE_C50(subLoop) )); % This is NOT redundant
    NR_params_AllSubjs_BE(subLoop,:) =NR_params_BE;
    
    if 1 % plotting NR fits
        figure(3000),
        subplot(4,11,subLoop)
        hold on
        %     figure,plot(FineX,NR_prediction_BE,'g',NR_Prediction_BE_C50, NR_Prediction_BE_C50sGain,'go' ,'LineWidth',3)
        p1 = plot(FineX,NR_prediction_WE,'Color','r','LineWidth',3) ;
        hold on, plot(ContrastLevels, ThisSubj_WE_Trials,'.r','MarkerSize',5)
        plot( NR_Prediction_BE_C50(subLoop), NR_Prediction_WE_C50sGain_basedOn_BEC50(subLoop),'rd','MarkerSize',10,'MarkerFaceColor','r')
        
        p2 = plot(FineX,NR_prediction_BE,'Color','g','LineWidth',3) ;
        hold on, plot(ContrastLevels, ThisSubj_BE_Trials,'.g','MarkerSize',5)
        plot( NR_Prediction_BE_C50(subLoop), NR_Prediction_BE_C50sGain(subLoop),'gd','MarkerSize',10,'MarkerFaceColor','g')
        
        
        p3 = plot(ContrastLevels,ThisSubjWEmean,'.r','MarkerSize',25) ;
        p4 = plot(ContrastLevels,ThisSubjBEmean,'.g','MarkerSize',25) ;
        set(gca,'xscale','log','box','off','FontSize',14,'XMinorTick','off');
        xticks(ContrastLevels)
    end
end

OKN_Ratio_C50_BEoverWE = 1 ./ (NR_Prediction_WE_C50 ./ NR_Prediction_BE_C50); % it is the ratio between the 1/contrast valus

OKN_Ratio_Rmax_WEoverBE = (NR_Prediction_WE_Rmax ./ NR_Prediction_BE_Rmax).*100;

OKN_Ratio_gainOfBEs_C50_WEoverBE = (NR_Prediction_WE_C50sGain_basedOn_BEC50 ./ NR_Prediction_BE_C50sGain).*100;

if 0 % plot the VFI ratio and MD ratio vs OKN OKN_Ratio_gainOfBEs_C50_WEoverBE ratio
    
    figure(1),
    [~, ascendingIndx] = sort(WorseEyeVFI);
    plot(WorseEyeVFI(ascendingIndx), OKN_Ratio_Rmax_WEoverBE(ascendingIndx),'o','MarkerSize',10); % OKN_Ratio_gainOfBEs_C50_WEoverBE
    xlabel('VFI ratio (%)')
    ylabel('C50 OKN-gain ratio (%)')
    % axis([0 101 0 100])
    set(gca,'FontSize',16);
end




% ******************* Normalized VFI and OKN comparison****************
%% comparing the normalized OKN score of the BE and the WE to the VFI of the BE and WE
% [WorseEyeVFI_Sorted, SortIdx_WE] = sort(WorseEyeVFI);
% [BetterEyeVFI_Sorted, SortIdx_BE] = sort(BetterEyeVFI);
% 
% AllSubj_OKNarea_BE = nansum(AllSubjBEaverage(1:end,:),2);
% AllSubj_OKNarea_WE = nansum(AllSubjWEaverage(1:end,:),2);
% 
% fullfieldSubjsIdx = find(BetterEyeVFI>=99);
% normalizationFactor = mean(nansum(AllSubjBEaverage(fullfieldSubjsIdx,:),2));% max(nansum(AllSubjBEmean(1:41,:),2));
% AllSubj_OKNarea_BE_Normalized = nansum(AllSubjBEaverage(1:end,:),2) ./ 1;%normalizationFactor; % normalized with the max of BE
% AllSubj_OKNarea_WE_Normalized = nansum(AllSubjWEaverage(1:end,:),2) ./ 1;%normalizationFactor; % normalized to the BE of the same patient
% 
% AllSubj_OKNarea_WE_NormalizedRatio = AllSubj_OKNarea_WE_Normalized./AllSubj_OKNarea_BE_Normalized.*100;
% % AllSubj_OKNarea_BE_NormalizedRatio = AllSubj_OKNarea_BE_Normalized.*100;
% figure(11)
% clf,
% % subplot(1,2,1)
% hold on ,plot(WorseEyeVFI,WorseEyeVFI(SortIdx_WE),'r.',WorseEyeVFI,AllSubj_OKNarea_WE_NormalizedRatio(SortIdx_WE'),'ro','MarkerSize',10)
% title ('WE VFI and OKN-area ratio sorted based on WE VFI')
% xlabel('Patient#')
% ylabel('%')
% axis ([0 105 0 160])
% 
% % subplot(1,2,2)
% % plot(BetterEyeVFI,BetterEyeVFI(SortIdx_BE),'g.',BetterEyeVFI,AllSubj_OKNarea_BE_NormalizedRatio(SortIdx_BE),'go','MarkerSize',10)
% % title ('BE VFI and OKN-area sorted based on BE VFI')
% % xlabel('Patient#')
% % ylabel('%')
% % axis ([75 105 0 160])
% % legend('VFI', 'OKN gain area', 'NR fit')
% 
% % fit Naka_Rushton on sorted VFI and OKN areas
% fitXdataIn = WorseEyeVFI;
% fitYDataIn_WE = AllSubj_OKNarea_WE_NormalizedRatio(SortIdx_WE)';
% fitYDataIn_BE = AllSubj_OKNarea_BE_NormalizedRatio(SortIdx_BE)';
% %     fitYDataIn = [ThisSubjWEmean(NR_inputRange(1:end-1)),max(ThisSubjWEmean(end),ThisSubjBEmean(end))]-BaseLine;
% [NR_params_WE, f_WE]= FitNakaRushton(fitXdataIn,fitYDataIn_WE);
% [NR_params_BE, f_BE]= FitNakaRushton(fitXdataIn,fitYDataIn_BE);
% 
% WorseEyeFineX= WorseEyeVFI_Sorted;%linspace(ObserverRagne(1),ObserverRagne(end),1000);
% BetterEyeFineX= BetterEyeVFI_Sorted;%linspace(ObserverRagne(1),ObserverRagne(end),1000);
% 
% [NR_prediction_WE] = BaseLine+(ComputeNakaRushton(NR_params_WE,WorseEyeFineX));
% [NR_prediction_BE] = BaseLine+(ComputeNakaRushton(NR_params_BE,BetterEyeFineX));
% if 1 % plotting NR
%     figure(11),
%     subplot(1,2,1)
%     hold on
%     p1 = plot(WorseEyeFineX,NR_prediction_WE,'Color','r','LineWidth',4);
%     legend('VFI', 'OKN gain area', 'NR fit on OKN gain area')
%     set(gca,'FontSize',16)
%     subplot(1,2,2)
%     hold on
%     p2 = plot(BetterEyeFineX,NR_prediction_BE,'Color','g','LineWidth',4);
%     legend('VFI', 'OKN gain area', 'NR fit on OKN gain area')
%     set(gca,'FontSize',16)
% end
% %% can we predict VFI using a randomly selected OKN gain area
% lookUpVector = AllSubj_OKNarea_WE_NormalizedRatio(SortIdx_WE);
% NR_lookUpVector = NR_prediction_WE;
% for bootStrapLoop = 1:100000
%     thisOKNareaSamp(bootStrapLoop) = randsample(lookUpVector,1,'true'); %numel(ObserverRagne) randomly select form OKn gain area with placement
%     
%     thisSampIndx =  find(abs(lookUpVector-thisOKNareaSamp(bootStrapLoop)) < 0.01);
%     thisSamplEstimatedPatientIndx = round(mean(ObserverRagne(thisSampIndx)));
%     ThisPatientIdxNRVFI(bootStrapLoop) = (NR_lookUpVector(thisSamplEstimatedPatientIndx));
%     
%     ThisPatientIdxVFI(bootStrapLoop)  = WorseEyeVFI_Sorted(thisSamplEstimatedPatientIndx);
%     if 0 %plotting
%         figure(10),
%         plot(bootStrapLoop,thisOKNareaSamp(bootStrapLoop),'RO',bootStrapLoop,ThisPatientIdxNRVFI(bootStrapLoop),'Rd',bootStrapLoop,ThisPatientIdxVFI(bootStrapLoop),'R.')
%         hold on
%         drawnow
%     end
% end
% estimationError_OKNarea_VFI = thisOKNareaSamp - ThisPatientIdxVFI;
% estimationError_NRVFI_VFI = ThisPatientIdxNRVFI - ThisPatientIdxVFI;
% 
% figure(20)
% [rho,pval] = corrcoef(ThisPatientIdxVFI',ThisPatientIdxNRVFI');
% [p,S]=polyfit(ThisPatientIdxVFI,ThisPatientIdxNRVFI,1);
% pred=polyval(p,ThisPatientIdxVFI);
% p1=plot(ThisPatientIdxVFI,pred,'k-',ThisPatientIdxVFI,ThisPatientIdxNRVFI,'o');
% set(p1(1),'LineWidth',2);
% set(p1(2),'MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor',[1 1 1].*0.75);
% legend(sprintf('R=%3.3f\n(p=%5f)',rho(1,2),pval(1,2)),'Location','northwest');
% xlabel('Actual VFI')
% ylabel('Predicted VFI based on Naka_Rushton fir','Interpreter','none')
% axis ([0 100 0 100],'square')
% shg
%%  ploting the loss ration OKN vs VFI

AllSubj_OKNauc_BE = nansum(AllSubjBEaverage(1:end,:),2);
AllSubj_OKNauc_WE = nansum(AllSubjWEaverage(1:end,:),2);

AllSubj_OKNauc_LossRatio = ((AllSubj_OKNauc_BE - AllSubj_OKNauc_WE) ./ AllSubj_OKNauc_BE)*100;
AllSubj_VFI_LossRatio = ((BetterEyeVFI - WorseEyeVFI)./BetterEyeVFI)*100;

figure(11)
clf,
hold on ,scatter(AllSubj_VFI_LossRatio,AllSubj_OKNauc_LossRatio, 100, Cmap, 'filled')
title ('')
xlabel('% VFI loss ratio')
ylabel('% OKN loss ratio')
axis ([0 105 0 90])



% fit Naka_Rushton on sorted VFI and OKN areas
[AllSubj_VFI_LossRatio_Sorted, SortIdx] = sort(AllSubj_VFI_LossRatio);
fitXdataIn = AllSubj_VFI_LossRatio_Sorted;
fitYDataIn = (AllSubj_OKNauc_LossRatio(SortIdx))';
%     fitYDataIn = [ThisSubjWEmean(NR_inputRange(1:end-1)),max(ThisSubjWEmean(end),ThisSubjBEmean(end))]-BaseLine;
[NR_params, f_WE]= FitNakaRushton(fitXdataIn,fitYDataIn);

FineX= AllSubj_VFI_LossRatio_Sorted;%linspace(ObserverRagne(1),ObserverRagne(end),1000);

[NR_prediction] = BaseLine+(ComputeNakaRushton(NR_params,FineX));

if 1 % plotting NR
    figure(11),
    hold on
    p1 = plot(FineX,NR_prediction,'Color',red,'LineWidth',4);
    legend('VFI', 'OKN gain area', 'NR fit on OKN gain area')
    set(gca,'FontSize',16)
end

%% compute correlation

corrInput1 = AllSubj_VFI_LossRatio';%NR_prediction_WE';%AllSubj_OKNscore_WE_NormalizedRatio; %(OKNVFRatio(1:end-1))';%OKN_Ratio_gainOfBEs_C50_WEoverBE'; % OKN_Ratio_gainOfBEs_C50_WEoverBE';
corrInput2 = AllSubj_OKNauc_LossRatio;%WorseEyeVFI(SortIdx_WE)';

[rho,pval] = corr(corrInput1,corrInput2,'Type','spearman')

%% find similar VFI% and use the mean of them and the mean of their gain
% plot the histogram of the VFI of the BE and WE
nbins = 100;
thresh_low = min([WorseEyeVFI, BetterEyeVFI]);
thresh_hi  = max([WorseEyeVFI, BetterEyeVFI]);
binedges = linspace(thresh_low,thresh_hi,nbins);
figure(10)

subplot(1,3,1)
histogram(WorseEyeVFI,binedges)
hold on
histogram(BetterEyeVFI,binedges)
title('Histogram of the VFI of the BE and WE')
xlabel('VFI (%)')
set(gca,'FontSize',18)
legend('WE', 'BE')

% plot the histogram of the OKN score of the BE and the WE
clear thresh_low thresh_hi
nbins = 100;
thresh_low = min([AllSubj_OKNarea_WE_NormalizedRatio', AllSubj_OKNarea_BE_NormalizedRatio']);
thresh_hi  = max([AllSubj_OKNarea_WE_NormalizedRatio', AllSubj_OKNarea_WE_NormalizedRatio']);
binedges = linspace(thresh_low,thresh_hi,nbins);
figure(10)

subplot(1,3,2)
histogram(AllSubj_OKNarea_WE_NormalizedRatio,binedges)
hold on
histogram(AllSubj_OKNarea_BE_NormalizedRatio,binedges)
title('Histogram of the OKN gain area of the BE and WE')
xlabel('OKN gain area ratio (%)')
set(gca,'FontSize',18)




%% compare OKN gian, MD and VFIbetween BE and WE
% AllSubjBEmean(AllSubjBEmean<0)=1e-2;
% AllSubjWEmean(AllSubjWEmean<0)=1e-2;

BE_OKNgianValues = nansum(AllSubjBEaverage(1:end-1,:),2);
WE_OKNgianValues = nansum(AllSubjWEaverage(1:end-1,:),2);

WEvBE_OKN_intOculBal = WE_OKNgianValues./BE_OKNgianValues;
WEvBE_VFI_intOculBal = (WorseEyeVFI./BetterEyeVFI)./1 ;
WEvBE_MD_intOculBal = - (WorseEyeMD - BetterEyeMD) ./ min(WorseEyeMD); % log(exp(WorseEyeMD)./exp(BetterEyeMD)) similar answers

Normalized_BE_OKNgianValues = BE_OKNgianValues./(BE_OKNgianValues); %sum(AllSubjBEmean(1:40,:),2); %sum?
Normalized_WE_OKNgianValues = WE_OKNgianValues./(BE_OKNgianValues); %sum(AllSubjWEmean(1:40,:),2); %sum?

if 1 % plot the normalized okn
    figure(1010)
    clf
    subplot(131),BEPLOT=plot(BetterEyeMD,'g.','MarkerSize',20);
    hold on,WEPLOT=plot(WorseEyeMD,'R.','MarkerSize',20);
    ylabel('MD')
    xlabel('Patient#')
    title('Mean Deviation');
    legend([BEPLOT,WEPLOT], 'Better eye', 'Worse eye','location', 'best' )
    set(gca,'FontSize',14)
    xlim([0 41])
    % figure,plot(WorseEyeMD-BetterEyeMD)./BetterEyeMD.*100/)
    subplot(132),plot(BetterEyeVFI,'g.','MarkerSize',20)
    hold on,plot(WorseEyeVFI,'R.','MarkerSize',20)
    ylabel('VFI')
    xlabel('Patient#')
    title('Visual Field Index');
    set(gca,'FontSize',14)
    axis([0 41 0 101])
    
    subplot(133),plot(Normalized_BE_OKNgianValues,'g.','MarkerSize',20)
    hold on,plot(Normalized_WE_OKNgianValues,'R.','MarkerSize',20)
    ylabel('OKN gain ratio')
    xlabel('Patient#')
    title('Normalized area under OKN gain plot');
    set(gca,'FontSize',14)
    axis([0 41 0 1.2])
end

figure(1011)
textOn = 1; % 1 = show the patient number on top of dot
clf
colMap = linspecer(numel(ObserverRagne));
for subLoop = ObserverRagne
    subplot(231), hold on
    plot(WorseEyeMD(subLoop),BetterEyeMD(subLoop),'.','MarkerSize',20,'Color',colMap(subLoop,:))
    plot([-30 2], [-30 2], '--', 'Color', [1 1 1].*0.75)
    
    if textOn
        text(WorseEyeMD(subLoop),BetterEyeMD(subLoop),sprintf('%i',subLoop),...
            'VerticalAlignment','bottom','HorizontalAlignment','center','Color',[0.5 0.5 0.5]);
    end
    ylabel('Better eye MD')
    xlabel('Worse eye MD')
    title('Mean Deaviation');
    set(gca,'FontSize',14)
    grid on
    axis([-30 2 -30 2],'square')
    
    subplot(232),hold on
    plot(WorseEyeVFI(subLoop),BetterEyeVFI(subLoop),'.','MarkerSize',20,'Color',colMap(subLoop,:))
    plot([0 105], [0 105], '--', 'Color', [1 1 1].*0.75)
    
    if textOn
        text(WorseEyeVFI(subLoop),BetterEyeVFI(subLoop),sprintf('%i',subLoop),...
            'VerticalAlignment','bottom','HorizontalAlignment','center','Color',[0.5 0.5 0.5]);
    end
    ylabel('Better eye VFI')
    xlabel('Worse eye VFI')
    title('Visual Field Index');
    set(gca,'FontSize',14)
    grid on
    axis([0 105 0 105],'square')
    
    subplot(233),hold on
    plot(WE_OKNgianValues(subLoop),BE_OKNgianValues(subLoop),'.','MarkerSize',20,'Color',colMap(subLoop,:))
    plot([0 5], [0 5], '--', 'Color', [1 1 1].*0.75)
    if textOn
        text(WE_OKNgianValues(subLoop),BE_OKNgianValues(subLoop),sprintf('%i',subLoop),...
            'VerticalAlignment','bottom','HorizontalAlignment','center','Color',[0.5 0.5 0.5]);
    end
    ylabel('Better eye OKN gain score')
    xlabel('Worse eye OKN gain score')
    title('Area under OKN gain plot');
    set(gca,'FontSize',14)
    grid on
    axis([0 5 0 5],'square')
    
    % Plot IOB (VFI or MD ratio) vs OKN gain ratio
    subplot(234), hold on
    plot(WEvBE_MD_intOculBal(subLoop), WEvBE_OKN_intOculBal(subLoop),'.','MarkerSize',20,'Color',colMap(subLoop,:))
    % plot([-1 0 ], [0 1], '--', 'Color', [1 1 1].*0.75)
    
    if textOn
        text(WEvBE_MD_intOculBal(subLoop),WEvBE_OKN_intOculBal(subLoop),sprintf('%i',subLoop),...
            'VerticalAlignment','bottom','HorizontalAlignment','center','Color',[0.5 0.5 0.5]);
    end
    ylabel('OKN gain IOB')
    xlabel('MD IOB')
    title('Inter Ocular Ballance OKN vs MD');
    set(gca,'FontSize',14)
    grid on
    axis([-1 0 0 1.4],'square')
    
    subplot(235), hold on
    plot(WEvBE_VFI_intOculBal(subLoop), WEvBE_OKN_intOculBal(subLoop),'.','MarkerSize',20,'Color',colMap(subLoop,:))
    % plot([0 1.4], [0 1.4 ], '--', 'Color', [1 1 1].*0.75)
    
    if textOn
        text(WEvBE_VFI_intOculBal(subLoop),WEvBE_OKN_intOculBal(subLoop),sprintf('%i',subLoop),...
            'VerticalAlignment','bottom','HorizontalAlignment','center','Color',[0.5 0.5 0.5]);
    end
    ylabel('OKN gain IOB')
    xlabel('VFI IOB')
    title('Inter Ocular Ballance OKN vs VFI');
    set(gca,'FontSize',14)
    grid on
    axis([0 1 0 1.4],'square')
    
    
    
end



%% ploting the VFI ratio and OKN based VF ratio
%
figure,
ColMap = linspecer(numel(ObserverName));
SortedOKNVFRatio = OKNVFRatio(sortIdx);%OKNVFRatio
SortedOKNmidFreqVFRatio = OKNmidFreqVFRatio(sortIdx);
for i = 1:numel(ObserverName)
    hold on
    plot(i,SortedOKNVFRatio(i),'.r','MarkerSize',25)
    plot(i,SortedObserverVFI(i),'.g','MarkerSize',25)
    plot(i,SortedOKNmidFreqVFRatio(i),'.b','MarkerSize',25)
    drawnow
end
ylabel('VFI Ratio (%)')
xlabel('Patient #')
title('VFI vs. OKN VF Ratio');
legend('OKN VF Ratio', 'VFI Ratio', 'OKN VFR Mid Frequncies', 'location', 'best' )
set(gca,'box','off','FontSize',14,'XMinorTick','off');
%     xticks(1:3:numel(ObserverName))
%% plot MD against VFI
[SortedVFI_WE, sortIdx] = sort(WorseEyeVFI);%WorseEyeMD
figure(100)
plot(BetterEyeVFI(sortIdx),BetterEyeMD(sortIdx),'g.',WorseEyeVFI(sortIdx), WorseEyeMD(sortIdx), 'r.', 'MarkerSize',25)
xlabel('VFI (%)')
ylabel('MD')
title('MD vs. VFI');
legend('Better eye', 'Worse eye', 'location', 'best' )
set(gca,'box','off','FontSize',14,'XMinorTick','off');
% plot C50_BE OKN-gain of the worse eye
figure(101)
plot(BetterEyeVFI(sortIdx),(NR_Prediction_BE_C50sGain(sortIdx).*100),'g.',WorseEyeVFI(sortIdx), NR_Prediction_WE_C50sGain_basedOn_BEC50(sortIdx).*100, 'r.', 'MarkerSize',25)
xlabel('VFI (%)')
ylabel('C_{50} OKN-gain')
title('C_{50} OKN gain vs. VFI');
legend('Better eye', 'Worse eye', 'location', 'best' )
set(gca,'box','off','FontSize',14,'XMinorTick','off');
ylim([0 101])
axis square
%% plot the ratio gain in log space vs MD (diff in log space)

for contLoop = 3%1: numel(ContrastLevels) % the contrast level idx
    figure(102)
    clf
    BE_OKNgianValues = AllSubjBEmean(:,contLoop);
    WE_OKNgianValues = AllSubjWEmean(:,contLoop);
    Vals = {'MD', 'VFI'};
    yAxisData = (BE_OKNgianValues - WE_OKNgianValues)'; %  log(BE_OKNgianValues ./ WE_OKNgianValues) == log(BE_OKNgianValues) - log(WE_OKNgianValues)
    
    for i = 1: numel(Vals) % plot MD for 1 and VFI for 2
        xAxisData = eval(sprintf('BetterEye%s',Vals{i})) - eval(sprintf('WorseEye%s',Vals{i})); % MD is already in log scale so ratio is calculated through subtraction
        subplot(1,numel(Vals),i);
        figHand = scatter(xAxisData, yAxisData, 'ko','MarkerFaceAlpha',0.55,'MarkerFaceColor',[1 1 1].*0.5,'SizeData', 200 ,'MarkerEdgeAlpha',1,'LineWidth',2);
        xlabel(sprintf('%s better-worse',Vals{i}))
        if i == 1
            ylabel('OKN gain (better - worse)')
        end
        title(sprintf('Correlation %2.2f percent contrast',ContrastLevels(contLoop)));
        
        set(gca,'box','off','FontSize',14,'XMinorTick','off','Xscale','linear','YScale','linear');
        %         axis(([ min(xAxisData)-2 max(xAxisData)+ 2  min(yAxisData)-0.1 max(yAxisData)+0.1   ]))
        axis square
        
        % fit line and correlation
        [R Prob]=corrcoef((xAxisData), (yAxisData))
        [p,S]=polyfit(xAxisData,(yAxisData),1);
        fineMDs = (linspace(min(xAxisData), max(xAxisData), 100));
        pred=polyval(P, fineMDs);
        figure(102),
        hold on,pl1(i) = plot((fineMDs), (pred), 'b-','LineWidth',4,'color',[[1 1 1].*0.25 0.80]);
        legend(pl1(i), sprintf('r = %2.2f \np = %2.8f',R(1,2), Prob(1,2)),'location','SE','FontSize', 14)
    end
    
    if 0 % i.e save the figs
        
        fileNameStr = sprintf('Correlation - %2.0f percent contrast2',ContrastLevels(contLoop));
        SaveThisFig(102,fileNameStr)
    end
end
%% MD VFI gain 3d plot
figure(11),
plot3 (BetterEyeMD-WorseEyeMD, BetterEyeVFI-WorseEyeVFI, log(BE_OKNgianValues./WE_OKNgianValues)','ro','MarkerSize',10, 'MarkerFaceColor','r')
grid on

xlabel('MD better-worse')
ylabel('VFI better-worse')
zlabel('log gain better-worse')

set(gca,'XScale','log', 'YScale', 'log', 'XMinorGrid','off','YMinorGrid','off')


%% Bland Altman
input1 =  (((AllSubj_OKNarea_WE -  AllSubj_OKNarea_BE)./AllSubj_OKNarea_BE).*100)';%(SortIdx_WE)';%OKN_Ratio_Rmax_WEoverBE; % OKN_Ratio_gainOfBEs_C50_WEoverBE; % OKNVFRatio  ; %SortedOKNVFRatio OKN VFI
input2 =  WorseEyeMD; % (SortIdx_WE) ObserverVFIratio(1:end-1) ; %SortedObserverVFI Real VFI. The oberverRange+1 is the mean across all observers
BlandAltmanPlot(input2,input1,0);

%% Monte-Carlo for all subjects
clear BESimData WESimData BEIndiSimMeans WEIndiSimMeans
AllSubWEGain =[];   AllSubBEGain = []; AllSubWEstdGain=[]; AllSubBEstdGain=[];
AllSubWEmeanGain =[]; AllSubBEmeanGain=[];
for subLoop = 1: numel(ObserverName)
    ThisSubWEGain = squeeze(AllSubOKNGain(subLoop,WorseEye(subLoop),:,:));
    ThisSubBEGain = squeeze(AllSubOKNGain(subLoop,BetterEye(subLoop),:,:));
    
    ThisSubjWEmeanGain = nanmean(ThisSubWEGain);
    ThisSubjWEstdGain = nanstd(ThisSubWEGain);
    
    ThisSubjBEmeanGain = nanmean(ThisSubBEGain);
    ThisSubjBEstdGain = nanstd(ThisSubBEGain);
    
    AllSubWEGain = [AllSubWEGain;ThisSubWEGain];
    AllSubBEGain = [AllSubBEGain;ThisSubBEGain];
    
    AllSubWEstdGain  = [AllSubWEstdGain;ThisSubjWEstdGain];
    AllSubBEstdGain = [AllSubBEstdGain;ThisSubjBEstdGain];
    
    AllSubWEmeanGain  = [AllSubWEmeanGain;ThisSubjWEmeanGain];
    AllSubBEmeanGain = [AllSubBEmeanGain;ThisSubjBEmeanGain];
end
WE_WithinObserverSTD = rms(AllSubWEstdGain);
BE_WithinObserverSTD = rms(AllSubBEstdGain);

WE_BetweenObserverSTD = std(AllSubWEmeanGain);
BE_BetweenObserverSTD = std(AllSubBEmeanGain);

WEGroupMean = nanmean(AllSubWEGain);
BEGroupMean = nanmean(AllSubBEGain);

WEIndiSD = WE_WithinObserverSTD;
WEGroupSD = WE_BetweenObserverSTD;

BEIndiSD = BE_WithinObserverSTD;
BEGroupSD = BE_BetweenObserverSTD;

NumObservations = 16;
NumSimSubjects = 1000;
NoContLevels = numel(ContrastLevels);

for ContLoop=1:NoContLevels
    WEIndiSimMeans = WEGroupMean(ContLoop) + WEGroupSD(ContLoop).*randn(1,NumSimSubjects);
    BEIndiSimMeans = BEGroupMean(ContLoop) + BEGroupSD(ContLoop).*randn(1,NumSimSubjects);
    
    for i=1:NumSimSubjects
        WESimData(:,i)=WEIndiSimMeans(i)+ WEIndiSD(ContLoop).*randn(1,NumObservations);
        BESimData(:,i)=BEIndiSimMeans(i)+ BEIndiSD(ContLoop).*randn(1,NumObservations);
    end
    WESimCont(ContLoop,:) = mean(WESimData,1);
    WEContMean(ContLoop) = mean(WESimCont(ContLoop,:));
    WEContSD(ContLoop) = std(WESimCont(ContLoop,:));
    WEContSE(ContLoop) = std(WESimCont(ContLoop,:))./sqrt(NumSimSubjects);
    
    
    BESimCont(ContLoop,:) = mean(BESimData,1);
    BEContMean(ContLoop) = mean(BESimCont(ContLoop,:));
    BEContSD(ContLoop) = std(BESimCont(ContLoop,:));
    BEContSE(ContLoop) = std(BESimCont(ContLoop,:))./sqrt(NumSimSubjects);
    
end
figure(8),

BWDiff = (BESimCont- WESimCont);
BWDiffMean = mean(BWDiff,2);
BWDiffSD = std(BWDiff,0,2);
BWDiffSE  = BWDiffSD./sqrt(NumSimSubjects);
% plot(ContrastLevels+0.1*ContrastLevels,BWDiff','.','LineWidth',2,'MarkerSize',12,'MarkerFaceColor',Cmap(subLoop,:),'Color', Cmap(subLoop,:))
subplot(1,3,1),
errorbar(ContrastLevels,BEContMean,BEContSD,'o-','LineWidth',2,'MarkerSize',9,'MarkerFaceColor','g','Color', 'g')
patch([ContrastLevels fliplr(ContrastLevels)], [zeros(1,numel(ContrastLevels)) fliplr(BEContMean)], 'g','FaceAlpha',0.15, 'EdgeAlpha',0.1)

axis([-1 ContrastLevels(end)+0.2*ContrastLevels(end) -0.1 0.8])
set(gca,'xscale','log','box','off');
xticks(ContrastLevels)
xlabel('Contrast (%)')
ylabel('BE OKN-Gain')
title(sprintf('Better Eye;  OKN score:%2.2f',sum(BEContMean)))
set(gca, 'FontSize',14)

subplot(1,3,2),
errorbar(ContrastLevels,WEContMean,WEContSD,'o-','LineWidth',2,'MarkerSize',9,'MarkerFaceColor','r','Color', 'r')
patch([ContrastLevels fliplr(ContrastLevels)], [zeros(1,numel(ContrastLevels)) fliplr(WEContMean)], 'r','FaceAlpha',0.15,'EdgeAlpha',0.1)

axis([-1 ContrastLevels(end)+0.2*ContrastLevels(end) -0.1 0.8])
set(gca,'xscale','log','box','off');
xticks(ContrastLevels)
ylabel('WE OKN-Gain')
title(sprintf('Monte-Carlo simulation\nWorse Eye; OKN score:%2.2f',sum(WEContMean)))
set(gca, 'FontSize',14)

subplot(1,3,3),
% errorbar(ContrastLevels,BWDiffMean,BWDiffSD,'o-','LineWidth',2,'MarkerSize',2,'MarkerFaceColor',[0.5 0.5 0.5],'Color', [0.5 0.5 0.5])
plot(ContrastLevels,BWDiffMean,'o-','LineWidth',2,'MarkerSize',9,'MarkerFaceColor',[0.5 0.5 0.5],'Color', [0.5 0.5 0.5])

patch([ContrastLevels fliplr(ContrastLevels)], [zeros(1,numel(ContrastLevels)) fliplr(BWDiffMean')], 'k','FaceAlpha',0.15,'EdgeAlpha',0.1)
axis([-1 ContrastLevels(end)+0.2*ContrastLevels(end) -0.1 0.8])
xticks(ContrastLevels)
set(gca,'xscale','log','box','off');
ylabel('WE and BE OKN-Gain difference'),
title(sprintf('Diference: %2.2f\n OKN VF ratio: %2.2f%%', sum(BWDiffMean),sum(WEContMean)/sum(BEContMean)*100))
set(gca, 'FontSize',14)
% export XLS file
if 0
    MeanGainvalues = num2cell(BWDiffMean);
    STdGainValues = num2cell(BWDiffSD);
    headers = {'difference','SD'};
    xlswrite('MC_Diff.xlsx',[headers; MeanGainvalues,STdGainValues],1,'A10');
end



%%
figure
x1=WorseEyeMD;
y1=squeeze(AllSubjWEmean(1:(end),2)');%./squeeze(AllSubjBEmean(1:(end-1),2)');

semilogy(x1, y1,'o');
[R p]=corrcoef(x1,(y1));

[h p]=kstest(AllSubjWEmean(1:(end-1),2)');
