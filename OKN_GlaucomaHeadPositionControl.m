% Check for head tilt and rotation
if length(E.WhichEye)>1
    if strcmpi(E.Eyetracker,'Tobii')
        
        %Get gaze origin points
        if isempty(TobiiSample.left_gaze_origin_in_user_coordinate_system)&&isempty(TobiiSample.right_gaze_origin_in_user_coordinate_system)
            LE_origin = E.EyePos.LE_Gazeorigin(intLe,intLeTrial,sampleCount,:); %X, Y, and Z in user coordinate system
            LE_Point  = [0 0 0]';
            RE_origin = E.EyePos.RE_Gazeorigin(intLe,intLeTrial,sampleCount,:);
            RE_Point  = [0 0 0]';
        elseif isempty(TobiiSample.left_gaze_origin_in_user_coordinate_system)
            LE_origin = E.EyePos.LE_Gazeorigin(intLe,intLeTrial,sampleCount,:);
            LE_Point  = [0 0 0]';
            RE_origin = TobiiSample.right_gaze_origin_in_user_coordinate_system(end,:); 
            RE_Point  = TobiiSample.right_gaze_point_in_user_coordinate_system(end,:);
        elseif isempty(TobiiSample.right_gaze_origin_in_user_coordinate_system)
            LE_origin = TobiiSample.left_gaze_origin_in_user_coordinate_system(end,:);
            LE_Point  = TobiiSample.left_gaze_point_in_user_coordinate_system(end,:);
            RE_origin = E.EyePos.RE_Gazeorigin(intLe,intLeTrial,sampleCount,:);
            RE_Point  = [0 0 0]';
        else
            LE_origin = TobiiSample.left_gaze_origin_in_user_coordinate_system(end,:);
            LE_Point  = TobiiSample.left_gaze_point_in_user_coordinate_system(end,:);
            RE_origin = TobiiSample.right_gaze_origin_in_user_coordinate_system(end,:);
            RE_Point  = TobiiSample.right_gaze_point_in_user_coordinate_system(end,:);

        end
        E.EyePos.LE_Gazeorigin(intLe,intLeTrial,sampleCount,:) = double(LE_origin);
        E.EyePos.RE_Gazeorigin(intLe,intLeTrial,sampleCount,:) = double(RE_origin);
        % calculate ditance from LCD, distance between gaze point and gaze
        % origin in UCS
            LE_Delta = LE_Point - LE_origin;
            LE_DistCm= sqrt(nansum(LE_Delta.^2))./10;
       
            RE_Delta = RE_Point - RE_origin;
            RE_DistCm= sqrt(nansum(RE_Delta.^2))./10;
       
        E.EyePos.LE_DistCm(intLe,intLeTrial,sampleCount,:) = LE_DistCm;
        E.EyePos.RE_DistCm(intLe,intLeTrial,sampleCount,:) = RE_DistCm;

        X_EyeSeparation_UCS = abs(LE_origin(1)-RE_origin(1)); %ucs user coordinate system x= rightward more positive
        Y_EyeSeparation_UCS = abs(LE_origin(2)-RE_origin(2)); % Y : upward more positive
        Z_EyeSeparation_UCS = abs(LE_origin(3)-RE_origin(3)); % Z : far from center of Tobii toward participant  perpendicular to X and Y
        
        
        
        TiltAngle_aroundZ_UCS = atand((0.5*Y_EyeSeparation_UCS)/X_EyeSeparation_UCS);
        TiltAngle_aroundY_UCS = atand((0.5*Z_EyeSeparation_UCS)/X_EyeSeparation_UCS);
        if (TiltAngle_aroundZ_UCS<5) && (TiltAngle_aroundY_UCS<4)
            E.GoodHeadPositionFlag = true;
            E.HeadPositionFlagCol  = [0 E.Window.White/2 0]; % green code
        else
            E.GoodHeadPositionFlag = false;
            E.HeadPositionFlagCol  = [E.Window.White/2 0  0];  % red code
        end
        E.GoodHeadPositionFlags(intLe,intLeTrial,sampleCount) = E.GoodHeadPositionFlag;
        
        if S.CurrentTrialFrame>1
            if sum(squeeze(double(E.GoodHeadPositionFlags(intLe,intLeTrial,sampleCount-1:sampleCount)))) == 0
                E.HeadPositionFlagCol  = [E.Window.White/2 0  0];  % red code
            else % i.e.  at least one of the last two head position flags is acceptable
                E.HeadPositionFlagCol  = [0 E.Window.White/2 0]; %green code
            end
            
        end
        
    elseif strcmpi(E.Eyetracker,'SimEyelink')
        E.GoodHeadPositionFlag = true;
        E.HeadPositionFlagCol  = E.Window.Gray;
    end
else % i.e. monocular recording
    E.GoodHeadPositionFlag = true;
    E.HeadPositionFlagCol  = E.Window.Gray;
end

