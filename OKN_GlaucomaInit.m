%function [E,P,S] = OKN_GlaucomaInit(E,P,S)

S.AudioFeedback          = 0; %1=yes, provide audio feedback please, 0=no, no feedback
S.VisualFeedback         = 0; %1=yes, provide visual feedback please, 0=no, no feedback
S.TextVisualFeedBack     = 0;  %1=yes, provide text visual feedback please, 0=no, no feedback
% S.MaskOpenLoop         = 0; %1 = mask folow the eye 0 : static mask
S.PepperMode             = 0;
E.Settings.CoveredFrames = 0; %30; % number of frames to be covered at the beginnig to avoid jittering frames
S.CycleTime              = 2;  % stimulus presenting time
S.StimulusLength         = (S.CycleTime + E.Settings.CoveredFrames.*E.Window.InterFrameInterval).*ones(P.NoInterleaved,max(P.NoFixedTrialsPerInter)); % IN seconds would be 3 s for 60 frames initial covering
S.DirPeak                = P.StimulusDirection; %[0 180];%[0 0 0 0 0 180 180 180 180 180] ; % [0 90 180 270];
S.NoSamps                = 2; %2; % stim pattern samples
S.NoMaskSamps            = 2; %8; % number of different noise samples for the same mask properties
S.WhichSamp              = Randi(S.NoSamps,[1 sum(P.NoFixedTrialsPerInter)]);
S.WhichMaskSamp          = Randi(S.NoMaskSamps,[1 sum(P.NoFixedTrialsPerInter)]);

% S.FixedContrast         = 1.00;
% S.screenHeightDeg       = 20; %I have comment it out, because I have
% calculate it latter as "E.Window.DisplayHeightDeg"
E.Window.PTBScreenWidthCenter  = E.Window.PTBScreenWidth/2;
E.Window.PTBScreenHeightCenter = E.Window.PTBScreenHeight/2;

% %to be moved to PSB update 
% E.EyePos.X(1,1,1,:)           = [E.Window.PTBScreenWidthCenter E.Window.PTBScreenWidthCenter]; % nan to test the timing
% E.EyePos.Y(1,1,1,:)           = [E.Window.PTBScreenHeightCenter E.Window.PTBScreenHeightCenter]; % [0,0];
% E.EyePos.PA(intLe,intLeTrial,1,:)          = [0,0];
% E.EyePos.Time(intLe,intLeTrial,1)          = 0;



E.RepeatKeyPressed = 0; % % any key other than enswer keys is repeat

P.BaseTrialNo = 0; % will count the number of baseline pupil measurements

S.StimScalingRatio = 1; %8;% 4 % 4 means generate the stimulus using 1/4th of PTB screen dimensions.
if E.Settings.MaskOpenLoop
    S.MaskSizeMltpl = 1.7; % the interger for mask to be multipled with
    S.StimX    = ceil(E.Window.PTBScreenWidth/S.StimScalingRatio); S.StimY=ceil(E.Window.PTBScreenHeight/S.StimScalingRatio); % the stimilus size
    S.DestRect = CenterRectOnPoint([0 0 E.Window.PTBScreenWidth E.Window.PTBScreenHeight],E.Window.PTBScreenWidthCenter,E.Window.PTBScreenHeightCenter); % the destination rect in which we flip the Stim
else
    S.MaskSizeMltpl = 1; % the interger for mask to be multipled with
    S.StimX    = ceil(E.Window.PTBScreenWidth/S.StimScalingRatio); S.StimY=ceil(E.Window.PTBScreenHeight/S.StimScalingRatio); % the stimilus size; square.
    S.DestRect = CenterRectOnPoint([0 0 E.Window.PTBScreenWidth E.Window.PTBScreenHeight],E.Window.PTBScreenWidthCenter,E.Window.PTBScreenHeightCenter); % the destination rect in which we flip the Stim
end

S.FlipsPerFrame   = 1;
S.PixelsPerDegree = 19; %  @1280 pix
% S.PixelsPerDegree = 62; % the actual pixels perdegree is 62 for
% horizontal and 58.2 for vertical LG monitor 3D

S.gamVal          = 0.5;
S.sfBw            = 0.5;
% S.FreqPeak      = 4;  % default: 4
S.SpeedDegPerSec  = 10;   % default: 10

S.ShowMovies          = 0;
S.BaseContrast        = 0.5;
JumpSizePerFrameInDeg = (S.SpeedDegPerSec*E.Window.InterFrameInterval)*S.FlipsPerFrame ;
S.JumpSize            = JumpSizePerFrameInDeg*S.PixelsPerDegree; % jump size pixels/frame

S.SpeedPixPerSec = S.PixelsPerDegree.*S.SpeedDegPerSec; % stim speed in pixel per second S.SpeedPixPerMS
S.SpeedPixPerMS  = S.SpeedPixPerSec./1000; % Strim speed in pixel per milisecond
% CycleFrames=round(S.CycleTime*E.Computer.ScreenHz);

S.FreqPeak             = P.PossStimValues{1};
S.ElementCode          = 8;
S.BaseNo               = 1024;
S.SmallestElementSize  = 16; % in pixels
S.CPI                  = 8;
% S.FixedContrast        = 1.00;%0.35; % was 0.35  change if want full contrast

if E.Settings.SimPSF
    E.Settings.PSF = MakePSF(S.StimX,S.StimY,E);  % create PSF
end

E.Settings.TimingTolerance   = 0.7;  % we have overwritten the value in PSB for this experiment
E.Timing.EyetrackingRiskRate = 0.3;         % we have overwritten the value in PSB for this experiment

LE_DistCm = nan;
RE_DistCm = nan;

E.Window.PTBbackground = E.Window.Gray;
% If Tobii ==> Ration for converting the eye-position ratio into pixels
if  strcmp( E.Eyetracker,'Tobii')
    X2PixConvertRatio = E.Window.PTBScreenWidth;
    Y2PixConvertRatio = E.Window.PTBScreenHeight;
else
    X2PixConvertRatio = 1;
    Y2PixConvertRatio = 1;
end
 % [E.Window.DisplayWidthmm, E.Window.DisplayHightmm] =
% Screen('DisplaySize', E.Settings.ScreenUsed); % width and height of display in mm 
E.Window.DisplayHeightmm       = 530; % measured diplay heigth E.Window.DisplayHightmm this values measured for dell S2817Q 4k display 341
E.Window.DisplayWidthmm        = 940; % measured diplay width E.Window.DisplayHightmm dell S2817Q 4k 621
E.Window.LastFrameDelta        = 1; % it is better to put in psb init
E.Window.DisplayHeightDeg      = 2.* atand((E.Window.DisplayHeightmm./2) / E.Settings.TestDistmm); % calculate the screen hieght in degrees for the desired test distance. (i.e. = S.screenHeightDeg ) The S2817Q height is 34.128 cm and its width is 62.093 based on www.displayspecifications.com and the screen manual
E.Window.DisplayWidthDeg       = 2.* atand((E.Window.DisplayWidthmm./2) / E.Settings.TestDistmm);
E.Window.DisplayPixleHeightArcMin = E.Window.DisplayHeightDeg / E.Window.PTBScreenHeight*60; % *60 to convert from degree to ArcMin
E.Window.DisplayPixleWidthArcMin  = E.Window.DisplayWidthDeg / E.Window.PTBScreenWidth*60;
% theImages = zeros(length(S.FreqPeak),S.NoSamps,S.StimY,S.StimX);
% stimtex   = zeros(length(S.FreqPeak),S.NoSamps);
ForceGenNewTexture = false; % always generate a new texture
textureNameStr = sprintf('GOKN_ExSet%i_textures.mat',E.Settings.ExperimentSet);

if ~ForceGenNewTexture && exist(fullfile(E.Folders.DataSaveRoot,textureNameStr), 'file')
    load(fullfile(E.Folders.DataSaveRoot,textureNameStr))  %'theImages'  E.Folders.TempSaveRoot
    if exist('SavedTextureStimFreqCPDs', 'var')
        if isequal(E.Settings.StimFreqCPDs, SavedTextureStimFreqCPDs) &&...  % if the saved texture has the same properties as the desired one
                isequal(E.Settings.BaseContrastLevels, SavedTextureContrastLevels) &&...
                isequal(S.NoSamps, SavedTextureNoSamps)&&...
                isequal(E.Settings.StimType, SavedTextureStimType)&&...
                isequal(S.StimY.*2, size(theImages,4)) && isequal(S.StimX.*2, size(theImages,5))
             stimTextStr = sprintf('Loading previous textures');
            DrawFormattedText(E.Window.Pointer, stimTextStr,'center',E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.White);
            Screen('Flip' , E.Window.Pointer);
        else % the saved texure differs from the desired one
            ForceGenNewTexture  = true;
            clear theImages
        end
    else % the structre of texture is different
        ForceGenNewTexture  = true;
        clear theImages
    end
else % the textures do not exist
    ForceGenNewTexture  = true;
    clear theImages
end

% if ~ForceGenNewTexture && exist(fullfile(E.Folders.DataSaveRoot,'textures.mat'), 'file')
%         load(fullfile(E.Folders.DataSaveRoot,'textures.mat'), 'theImages')  %E.Folders.TempSaveRoot
% end

if ForceGenNewTexture % E.Folders.GitFolder,'oknmocs'
    for s=1:S.NoSamps
        for f=1:length(E.Settings.StimFreqCPDs) %S.FreqPeak
            for c = 1:length(E.Settings.BaseContrastLevels)
                S.FixedContrast = E.Settings.BaseContrastLevels(c);
%                 Scaling    = P.PossStimValues{1}(f);
%                 BaseLambda = S.SmallestElementSize/ S.CPI;
%                 ScaledCPI  = S.StimX/(Scaling*BaseLambda);
                StimFreqCPIs = E.Settings.StimFreqCPDs(f).* E.Window.DisplayHeightDeg; % stimulus frequency in cycles per image height
                if E.Settings.StimType == 1 || E.Settings.StimType == 2 % i.e. noise
                    lambdaPix  = E.Window.PTBScreenHeight./E.Settings.StimFreqCPDs(f);  %S.FreqPeak
                    Smoothing  = 1.*lambdaPix; %
                    srcNoise   = randn(S.StimY,S.StimX);
                    stimImIn   = 0.5+(NormImage(real(DoLogGabor(srcNoise,StimFreqCPIs,S.sfBw,0,10000)),0,0.5));% DegToRad(2)
                    
                    if E.Settings.SimPSF
                        PSFsimStimImIn = fftshift(ifft2(fft2(stimImIn).*fft2(E.Settings.PSF))); % do not normalize after applying PSF  to dont loose
                        [imM, imSd]=LocalStat(PSFsimStimImIn,Smoothing);
                        PSFsimStimIm=0.5+NormImage((PSFsimStimImIn-imM)./imSd,0,0.5*S.FixedContrast);
                    else
                        
                        if E.Settings.StimType == 2 % i.e Black and White noise
                        %    stimIm = double(stimImIn>0.5).*S.FixedContrast;
                            stimIm = 0.5+sign(stimImIn-0.5).*(S.FixedContrast/2);
                        else % i.e Gray noise
                            [imM, imSd]=LocalStat(stimImIn,Smoothing);
                           stimIm=0.5+NormImage((stimImIn-imM)./imSd,0,0.5*S.FixedContrast);
                           
                        end
                            
                    end  
                elseif  E.Settings.StimType == 3 % i.e. checkerboard
                    xCordinat = linspace(0,2*pi,(S.StimX));                yCordinat = linspace(0,2*pi,(S.StimY));
                    [X,Y]=meshgrid(xCordinat,yCordinat);
                    stimImIn = sign(sin(StimFreqCPIs .*X)) .* sign(sin(StimFreqCPIs .*Y)); %  checkerboard
                    
                    if E.Settings.SimPSF
                        PSFsimStimImIn = fftshift(ifft2(fft2(stimImIn).*fft2(E.Settings.PSF)));
                        PSFsimStimIm   = 0.5 + NormImage(PSFsimStimImIn,0,0.5*S.FixedContrast);
                    else
                        stimIm=0.5+NormImage(stimImIn,0,0.5*S.FixedContrast);
                    end
                    
                end
                
                if E.Settings.SimPSF
                    theImage=repmat(PSFsimStimIm,[2 2]);
                else
                    theImage=repmat(stimIm,[2 2]);
                end
                
                theImages(f,c,s,:,:) = double(theImage);
                stimtex(f,c,s)       = Screen('MakeTexture', E.Window.Pointer, theImage, [], [], 2);
            end
        end
    end
    
    SavedTextureStimFreqCPDs   = E.Settings.StimFreqCPDs;
    SavedTextureContrastLevels = E.Settings.BaseContrastLevels;
    SavedTextureNoSamps        = S.NoSamps;
    SavedTextureStimType = E.Settings.StimType;
    
    save(fullfile(E.Folders.DataSaveRoot,textureNameStr), 'theImages', 'SavedTextureStimFreqCPDs', 'SavedTextureContrastLevels', 'SavedTextureNoSamps' , 'SavedTextureStimType');  %E.Folders.TempSaveRoot
    if E.Settings.SimPSF && ~exist(fullfile(E.Folders.DataSaveRoot,['PSF_',textureNameStr]), 'file')
        save(fullfile(E.Folders.DataSaveRoot,['PSF_',textureNameStr]), 'theImages', 'SavedTextureStimFreqCPDs', 'SavedTextureContrastLevels', 'SavedTextureNoSamps', 'SavedTextureStimType');
    end
    
else % i.e Keep the loaded previously saved texture if has the same properties as current setting
    stimTextStr = sprintf('Loading previous textures');
    DrawFormattedText(E.Window.Pointer, stimTextStr,'center',E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.White);
    Screen('Flip' , E.Window.Pointer);
    
%     if E.Settings.SimPSF
%         load(fullfile(E.Folders.DataSaveRoot,'PSFsimTextures.mat'), 'theImages')  %E.Folders.TempSaveRoot
%     else
%         load(fullfile(E.Folders.DataSaveRoot,'textures.mat'), 'theImages')  %E.Folders.TempSaveRoot
%     end
    
    for s=1:S.NoSamps
        for f=1:length(S.FreqPeak)
            for c = 1:length(E.Settings.BaseContrastLevels)
                theImage = squeeze(theImages(f,c,s,:,:));
                stimtex(f,c,s)     = Screen('MakeTexture', E.Window.Pointer, theImage, [], [], 2);
            end
        end
    end
end

S.StimTex = stimtex;

con1=1;
MaskScalingRatio = 1; % create smaler mask. eight ,means 1/8 (same as stim scaling)
mask1=zeros(round(S.StimY/MaskScalingRatio),round(S.StimX/MaskScalingRatio),2);

% base for  noise mask
for MaskSample = 1: S.NoMaskSamps
    if E.Settings.MaskType == 4 || E.Settings.MaskType == 5 || E.Settings.MaskType == 6
                MaskRadius = round(S.PixelsPerDegree.* S.FovRegDiameter/(2.*S.MaskSizeMltpl.*S.StimScalingRatio*MaskScalingRatio));   % clinical macula subtends 5 deg 1.5 mm; here we used 2.5 degree diameter

%         MaskRadius = S.PixelsPerDegree.* S.FovRegDiameter/(2.*S.MaskSizeMltpl.*S.StimScalingRatio*MaskScalingRatio);   % clinical macula subtends 5 deg 1.5 mm; here we used 2.5 degree diameter
        NoiseImage = randn(size(mask1,1), size(mask1,2));
        NoiseMaskFreqCPIs = E.Settings.NoiseMaskFreqCPDs.*E.Window.DisplayHeightDeg.*S.MaskSizeMltpl;
        MaskBase = 0.5 + NormImage(real(DoLogGabor(NoiseImage,NoiseMaskFreqCPIs.*S.MaskSizeMltpl,0.5,deg2rad(0),inf)),0,0.5);
        FovReg  = 1-MakeCosineRectWindow(size(mask1,2),size(mask1,1),MaskRadius,0,0,0);
        SortMaskBase = sort(MaskBase(:));
        StimMean = mean(SortMaskBase);
        StimStd  = std(SortMaskBase);
        %     E.Settings.BaseMaskLevels = norminv(linspace(0.0001,.9999,5),StimMean,StimStd);
        
        E.Settings.BaseMaskLevels = norminv(linspace(E.Settings.MaskBaseFirstBoundary, E.Settings.MaskBaseSecondBoundary, numel(E.Settings.BaseMaskLevels)),StimMean,StimStd);
        %     E.Settings.BaseMaskLevels(1) = 0; E.Settings.BaseMaskLevels(end) = 1;
        
        P.VisblRatio  = sum(SortMaskBase<E.Settings.BaseMaskLevels)./numel(SortMaskBase);
%         DisplayAreaDeg =  E.Window.DisplayHeightDeg*E.Window.DisplayWidthDeg;
        FovRegAreaDeg = pi.*(S.FovRegDiameter./2)^2;
    end
    
        DisplayAreaDeg =  E.Window.DisplayHeightDeg*E.Window.DisplayWidthDeg;

    for MaskLoop = 1: numel(E.Settings.BaseMaskLevels)
        
        switch E.Settings.MaskType
            case 1 % circular shape mask
                res  = 1-con1.*MakeCosineRectWindow(size(mask1,2),size(mask1,1),E.Settings.BaseMaskLevels(MaskLoop)/(1.*S.MaskSizeMltpl.*S.StimScalingRatio),0,0,0);   % scrMinDim/2.1
                P.ActualVisibleRatio(MaskLoop) = sum(res(:)==0)./ sum(res(:)==1);
            case 2 % doughnut shape mask  E.Settings.BaseMaskLevels((1))
                res  =  1-con1.*(MakeCosineRectWindow(size(mask1,2),size(mask1,1),(E.Window.PTBScreenHeight./2)/(1.*S.MaskSizeMltpl.*S.StimScalingRatio),0,0,0)- ...
                    MakeCosineRectWindow(size(mask1,2),size(mask1,1),(E.Settings.BaseMaskLevels(numel(E.Settings.BaseMaskLevels))-E.Settings.BaseMaskLevels(MaskLoop))/(1.*S.MaskSizeMltpl.*S.StimScalingRatio),0,0,0));
                 P.ActualVisibleRatio(MaskLoop) = sum(res(:)==0)./ sum(res(:)==1);
            case 3 % square rectangular mask
                res = uint8(ones([size(mask1,1) size(mask1,2)])); % create a mask
                res(1:E.Window.PTBScreenRect(4),E.Window.PTBScreenRect(3)/2-S.StimY/2:E.Window.PTBScreenRect(3)/2+S.StimY/2)=E.Window.Black; % stimulus size open at center
            case 4 % noise patch with visible foveal region
                mask    = MaskBase >  E.Settings.BaseMaskLevels(MaskLoop);
                FovMask = FovReg.* mask;
                TheMask = DoGaussian(FovMask,2*MaskScalingRatio);
                TheMask(TheMask>0.999)=1;
                res     =  TheMask; %abs(TheMask - min(TheMask(:)));
                % the rectangular noise mask minus the foveal proportion of noise plus full
                % visible foveal region
                P.ActualVisibleRatio = P.VisblRatio-(P.VisblRatio.*(FovRegAreaDeg./DisplayAreaDeg))+(FovRegAreaDeg./DisplayAreaDeg);
            case 5 % noise patch with occluded foveal region
                mask    = MaskBase >  E.Settings.BaseMaskLevels(MaskLoop);
                FovMask = 1-(FovReg.* (1-mask));
                TheMask = DoGaussian(FovMask,2*MaskScalingRatio);
                TheMask(TheMask>0.999)=1;
                res     = TheMask; % abs(TheMask - min(TheMask(:))./(range(TheMask(:))));
                % the rectangular noise mask minus the foveal proportion of noise
                P.ActualVisibleRatio = P.VisblRatio-(P.VisblRatio.*(FovRegAreaDeg./DisplayAreaDeg));
            case 6 % i.e. noise patch mask with background noise pattern
                mask    = MaskBase >  E.Settings.BaseMaskLevels(MaskLoop);
                FovMask = FovReg.* mask;
                TheMask = DoGaussian(FovMask,2*MaskScalingRatio);
                TheMask(TheMask>0.999)=1;
                res     =  TheMask; %abs(TheMask - min(TheMask(:)));
                % the rectangular noise mask minus the foveal proportion of noise plus full
                % visible foveal region
                P.ActualVisibleRatio = P.VisblRatio-(P.VisblRatio.*(FovRegAreaDeg./DisplayAreaDeg))+(FovRegAreaDeg./DisplayAreaDeg);
                srcNoise   = randn(S.StimY,S.StimX);
                stimImIn   = 0.5+(NormImage(real(DoLogGabor(srcNoise,S.MaskSizeMltpl*StimFreqCPIs,S.sfBw,0,10000)),0,0.5));% DegToRad(2)
                [imM, imSd]=LocalStat(stimImIn,Smoothing);
                stimIm=NormImage((stimImIn-imM)./imSd);
                E.Window.MaskBackground = stimIm;
            case 7 % peripheral circular mask with different radii
                res  = 1-con1.*MakeCosineRectWindow(size(mask1,2),size(mask1,1),E.Settings.BaseMaskLevels(MaskLoop)/(1.*S.MaskSizeMltpl.*S.StimScalingRatio),10,-15*S.PixelsPerDegree,1.5*S.PixelsPerDegree);   % scrMinDim/2.1
                P.ActualVisibleRatio(MaskLoop) = sum(res(:)==0)./ sum(res(:)==1);
        end
%           figure(12);
%           subplot(2,5,MaskLoop);imshow(res)
%        
                
        
        mask1(:,:,1)= E.Window.MaskBackground; %E.Window.MaskBackground;%E.Window.Gray;
        mask1(:,:,2)=  E.Window.White.*res;
        mask1=squeeze(mask1(:,:,:));
        
        S.masktex(MaskLoop,MaskSample)=Screen('MakeTexture', E.Window.Pointer,mask1);
    end
    
end

% S.pepSize=255;
% 
% % load feedback images
% if S.PepperMode
%     PepIm=imread('PeppaPigIm.jpg');
%     PepImR=imread('PeppaPigImR.jpg');
%     PepImG=imread('PeppaPigImG.jpg');
% else
%     PepIm=imread('AdIm.jpg');
%     PepImR=imread('AdImR.jpg');
%     PepImG=imread('AdImG.jpg');
% end
% S.peptex=Screen('MakeTexture', E.Window.Pointer, PepIm);
% S.peptexR=Screen('MakeTexture', E.Window.Pointer, PepImR);
% S.peptexG=Screen('MakeTexture', E.Window.Pointer, PepImG);

% S.CoverTexture = Screen('MakeTexture', E.Window.Pointer, E.Window.PTBbackground); % the texture for covering some unwanted frames

