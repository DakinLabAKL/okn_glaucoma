% OKNmocsTobii
PsychDefaultSetup(2);
%% use tobii feedback to check retinal adaptation
clearvars;
close all;
screenUsed = 1;
Screen('Preference', 'SkipSyncTests', 1) % currently need this for new Dell - check this as it might be an issue we can resolve
[DisplayWidthPix, DisplayHightPix] = Screen('WindowSize', screenUsed); % width and height of display in mm
PsychImaging('PrepareConfiguration');
PsychImaging('AddTask', 'FinalFormatting', 'DisplayColorCorrection', 'SimpleGamma');
stereoMode=1;
[win, winRect]      = PsychImaging('OpenWindow', screenUsed,   0.5, [],[],[],stereoMode);   %screenid, 0.5, [], [], [], P.stereoMode);
HideCursor;
keepgoing =1;
commandwindow;

ScalingRatio = 2;
ImageWidth = round(DisplayWidthPix/ScalingRatio);
ImageHeight = round(DisplayHightPix/ScalingRatio);
BaseRect=[0 0 ImageWidth ImageHeight];

DisplayHeightDeg = atand(53/67);
StimFreqCPD = 0.1; % 1/38;
StimFreqCPI = StimFreqCPD.* DisplayHeightDeg;
offX=0;
offX2=0;
offY=0;
JumpSizeX = 4;
JumpSizeY = 0;

lambdaPix  = DisplayHightPix./StimFreqCPD;  %S.FreqPeak
Smoothing  = 1.*lambdaPix; %
srcNoise   = randn(ImageHeight,ImageWidth);
srcNoise2   = randn(ImageHeight,ImageWidth);
StimIm   = 0.5+(NormImage(sign(real(DoLogGabor(srcNoise,StimFreqCPI,0.5,0,10000))),0,0.5));% DegToRad(2)
StimIm2   = 0.5+(NormImage(sign(real(DoLogGabor(srcNoise2,StimFreqCPI,0.5,0,10000))),0,0.5));% DegToRad(2)
image = repmat(StimIm,[2, 2]);
image2 = repmat(StimIm2,[2, 2]);
imtext = Screen('MakeTexture', win, image, [], [], 2);
imtext2 = Screen('MakeTexture', win, image2, [], [], 2);
Sourcerect = [0 0  ImageWidth ImageHeight];
% DestRect =  CenterRectOnPoint([0 0 DisplayWidthPix*Multiplyer DisplayHightPix*Multiplyer], DisplayWidthPix/2 ,DisplayHightPix/2);
Tobii.ET          = EyeTrackingOperations().find_all_eyetrackers;
likely_licence_filename = sprintf('*University_of_Auckland_%s',Tobii.ET.SerialNumber);
Tobii.licenses_path       = dir(fullfile(FindGitFolder,'psychsandbox','TobiiLicences',likely_licence_filename));
fileID              = fopen(Tobii.licenses_path(1).name,'r');
input_licenses(1)   = LicenseKey(fread(fileID));
fclose(fileID);
failed_licenses     = Tobii.ET.apply_licenses(input_licenses); % how long does this persist?

cntr=1; % number of flips counter
LastFlipTime(1) = Screen('Flip' , win);
try
    TobiixEyePos(cntr,:) = [0 0];
    TobiiyEyePos(cntr,:) = [0 0];
    cntr=2;
    Screen('Flip', win);
    
    while keepgoing
        Screen('BlendFunction', win, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        [ keyIsDoqwn, seconds, keyCode ] = KbCheck(-1);
        xEyePos=[0 0];
        yEyePos=[0 0];
        TobiiSample = Tobii.ET.get_gaze_data('flat');
        if isempty(TobiiSample.left_gaze_point_on_display_area)&isempty(TobiiSample.right_gaze_point_on_display_area)
            xEyePos = TobiixEyePos(cntr-1,:);
            yEyePos =  TobiiyEyePos(cntr-1,:);
        elseif isempty(TobiiSample.left_gaze_point_on_display_area)
            xEyePos = [TobiixEyePos(cntr-1,1),TobiiSample.right_gaze_point_on_display_area(end,1).*DisplayWidthPix];
            yEyePos = [TobiiyEyePos(cntr-1,1),TobiiSample.right_gaze_point_on_display_area(end,2).*DisplayHightPix];
        elseif isempty(TobiiSample.right_gaze_point_on_display_area)
            xEyePos = [TobiiSample.left_gaze_point_on_display_area(end,1).*DisplayWidthPix, TobiixEyePos(cntr-1,2)];
            yEyePos = [TobiiSample.left_gaze_point_on_display_area(end,2).*DisplayHightPix, TobiiyEyePos(cntr-1,2)];
        else
            xEyePos = [TobiiSample.left_gaze_point_on_display_area(end,1),TobiiSample.right_gaze_point_on_display_area(end,1)].*DisplayWidthPix;
            yEyePos = [TobiiSample.left_gaze_point_on_display_area(end,2),TobiiSample.right_gaze_point_on_display_area(end,2)].*DisplayHightPix;
        end
        TobiixEyePos(cntr,:) = double(xEyePos);
        TobiiyEyePos(cntr,:) = double(yEyePos);
        srcRect = OffsetRect(BaseRect, offX, offY);
        srcRect2 = OffsetRect(BaseRect, offX2, offY);
        cntr = cntr+1;
        DestRect = CenterRectOnPoint([0 0 DisplayWidthPix DisplayHightPix], DisplayWidthPix/2, DisplayHightPix/2);
        Screen('SelectStereoDrawBuffer', win,0);
        Screen('DrawTexture', win,imtext,srcRect ,DestRect,0);   % S.DestRect % draw the mask over stimulus
        Screen('SelectStereoDrawBuffer', win, 1);
        Screen('DrawTexture', win,imtext2,srcRect ,DestRect,180);   % S.DestRect % draw the mask over stimulus
        PsychColorCorrection('SetEncodingGamma', win, 0.5 );
        LastFlipTime(cntr) = Screen('Flip',win);%     KbReleaseWait;
        offX = mod((offX+JumpSizeX)-1,ImageWidth)+1;
        offX2 = mod((offX-JumpSizeX)-1,ImageWidth)+1;
        offY = mod((offY+JumpSizeY)-1,ImageHeight)+1;
        if keyCode(KbName('ESCAPE'))
            keepgoing =0;
        end
    end
    KbWait;
    sca
catch ME
    sca
    rethrow(ME)
end
%% tming data
d1=1./diff(LastFlipTime);
d1(d1<0)=0;
figure(1),clf;plot(1:length(d1),d1,'b.-',find(d1<20),d1(d1<20),'ro');
title('Effective frame rate')
xlabel('frames');ylabel('Frame Rate Hz')
axis([0 length(d1) 0 max(d1)])
legend('all frames','dropped frames')
DropedFrames = find(d1<20);
theDiff = diff(DropedFrames);
mode(theDiff(theDiff>2))

%% plot eye position
TobiixEyePosL = squeeze(TobiixEyePos(:,1));
TobiiyEyePosL = squeeze(TobiiyEyePos(:,1));
TobiixEyePosR = squeeze(TobiixEyePos(:,2));
TobiiyEyePosR = squeeze(TobiiyEyePos(:,2));
figure(10),plot(1:length(TobiixEyePosL),TobiixEyePosL,'b.-',1:length(TobiixEyePosR),TobiixEyePosR,'r.-'),ylim([0 DisplayWidthPix]);

