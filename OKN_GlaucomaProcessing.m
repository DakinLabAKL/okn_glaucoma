
% OKN_GlaucomaProcessing

if ~isfield(S,'NewSamplingSepMS')
    S.NewSamplingSepMS      = 0; %  1 for one mili second.
end

%maxJumpPixelsPerMs        = SpeedPixelsPerDegree*MaxSaccSpeedDegPerSec/(1000*S.NewSamplingSepMS);
if S.NewSamplingSepMS
    maxJumpPixelsPerSample        = S.PixelsPerDegree*MaxSaccSpeedDegPerSec/(1000*S.NewSamplingSepMS); % expressed in  pixels per sample
    StimSpeedPixPerSample = S.PixelsPerDegree*S.SpeedDegPerSec/(1000*S.NewSamplingSepMS);
else
    maxJumpPixelsPerSample        = S.PixelsPerDegree*MaxSaccSpeedDegPerSec/(1/E.Window.InterFrameInterval); % now  pixels per frame & pixels per sample are the same thing
    StimSpeedPixPerSample =  S.PixelsPerDegree*S.SpeedDegPerSec/(1/E.Window.InterFrameInterval);
end

goodVals    = find(tDataIn>0); % used to clip end of each stream of data
xData0      = xDataIn(goodVals); yData0=yDataIn(goodVals); paData0=paDataIn(goodVals); tData0=tDataIn(goodVals);
tBase       = tData0-tData0(1);% in milisecs

FrameFlipData = FrameFlipDataIn(FrameFlipDataIn>0)*1000; % convert to mili secs
FrameFlipData= FrameFlipData- FrameFlipData(1);
% figure,plot(tBase,tBase,'b.',tBaseS1,tBaseS1,'ro',tBaseS1, FrameFlipData,'m*')

% figure,plot(1./diff(tBaseS1/1000),'bo')

x00     = xData0 - E.Window.PTBScreenWidthCenter; % change the center of rotation to the center of data
y00     = yData0 - E.Window.PTBScreenHeightCenter;  %  E.Window.PTBScreenWidthCenter

% rotate the signal. Note the input angle should be negative here
xData00 = x00.*cos(-StimulusDriftAngle) - y00.*sin(-StimulusDriftAngle);
yData00 = x00.*sin(-StimulusDriftAngle) + y00.*cos(-StimulusDriftAngle);

xData00 = xData00 + E.Window.PTBScreenWidthCenter; % change back the center to the original coordinate center %   E.Window.PTBScreenWidth;
yData00 = yData00 + E.Window.PTBScreenHeightCenter;%   E.Window.PTBScreenHeight;

AllSxData = xData00; AllSyData = yData00; AllSpaData = paData0;  AllStData = tBase; % All samples data   

if isfield(E.EyePos,'OverSample') % i.e. we have first samples indices
    EachFrameSamples = (squeeze(E.EyePos.OverSample(ThisTrialIntLe, ThisIntLeRept, :,:)))'; % keep the frame flip time for later processing
    NoSampsPerFrame = sum(EachFrameSamples>0);
    EachFrameSamplesGT0 = (EachFrameSamples(EachFrameSamples>0))'; % length(EachFrameSamples)== length(xData0)? : yes
    FSInds  = find(EachFrameSamplesGT0==1); % first sample indices
    % tBaseS1 = tBase(FSInds);
else %i.e we stimate first samples indices for EYELINK
    if strcmpi(E.Eyetracker,'EyeLink')
        tDataDiff = diff([ 0 AllStData' 0]);
        FSInds    = [1, find(tDataDiff>E.Settings.MaxSamplesPerFrame.*2)]; % FS: first Sample.  *2 due te 500Hz sampling rate
    end
end

%  tBasex = 1:length(tBase);
%  figure,plot(tBasex,tBase,'b.',tBasex(FSInds),tBase(FSInds),'ro',tBasex(71),tBase(71),'kd');
%  % evaluate second FSinds and plot as following
%  hold on, plot(tBasex(FSInds),tBase(FSInds),'kx');
%  figure,plot(1:length(tBaseS1),tBaseS1,'b.',1:length(FrameFlipData), FrameFlipData,'mo','MarkerSize',10)
%  figure,plot(1./diff(tData./1000))

firstSeen = (E.Settings.CoveredFrames+1); % the first frame after covered frame
if JustFirstSample
    SampMargin = 3; % the margin before and after blinks, means 5 frames.
    tDataFS   = AllStData(FSInds);
    xDataFS   = AllSxData(FSInds);
    yDataFS   = AllSyData(FSInds);
    paDataFS  = AllSpaData(FSInds);
else % i.e we want Full samples
    SampMargin = 5; % the margin before and after blinks, means 5 samples.
%     firstSeen  = (find(tBase==tBaseS1(E.Settings.CoveredFrames+1))); % the first frame after covered frame
    tDataFS   = AllStData;
    xDataFS   = AllSxData;
    yDataFS   = AllSyData;
    paDataFS  = AllSpaData;
end

xData     = xDataFS(firstSeen:end); yData=yDataFS(firstSeen:end); paData=paDataFS(firstSeen:end); tData=tDataFS(firstSeen:end);
%

xData=xData(paData>0);
yData=yData(paData>0);
tData=tData(paData>0);
paData=paData(paData>0);

paMedian    = nanmedian(paData);
paMad       = mad(paData,1); % median absolute deviation
paThresh    = paMedian - 3 *paMad; % The multiplyer of paStd has been chosen from a range of values to minimize the variation of the gain.
goodVals    = ((xData>=0)&(yData>=0)&(paData>paThresh));

% plot(tData,paData,'b-',tData,tData.*0+paThresh,'g--')
% axis([0 max(tData) 0 1.1*max(paData)])

%
% OutlierThresh=3;
% AbsMADdev   = abs(paData-median(paData))./median(abs(paData-median(paData)));
% goodVals    = ((xData>=0)&(yData>=0)&(AbsMADdev<OutlierThresh));%(paData>paThresh)); % was the eye open? LH - changed x to y incase an error - check with steven;SMD Add >= to include zero inside x- or y- data


% paData=paData(goodVals);
% xData=xData(goodVals);
% yData=yData(goodVals);
% tData=tData(goodVals);

% figure(6), plot(tData,xData,'r-o')
 

SecB        = (find(diff(goodVals)>0)+SampMargin)'; % LH reworked so I understand it and can concatinate
beginInd    = [1 SecB];
SecE        = (find(diff(goodVals)<0)-SampMargin)'; % LH reworked so I understand it and can concatinate
endInd      = [SecE length(goodVals)];
% deal with blink cases in which the end of the blink is after the end of the trial
if length(beginInd)~=length(endInd) %this means that there was a blink overlapping with the start or the end of the trial
    if length(beginInd)>length(endInd)
        beginInd=beginInd(2:end); %remove first start, as it blink overlaps with beginning of trial
    else
        endInd=endInd(1:end-1); %remove last end as it blink overlaps with end of trial
    end
end
NumGoodSects = length(beginInd); %now count numbers
clear newX newY newP newTD newT
xDall = []; yDall = []; Xall = []; Yall = []; Tall = [];  tDall = [];  Pall = []; DeltaTall=[];



for sect=1:NumGoodSects
    if beginInd(sect)<endInd(sect)
        %                     [beginInd(sect) endInd(sect)];  %deal with overlaping disruptions
        x0      = (xData(beginInd(sect):endInd(sect)));
        y0      = (yData(beginInd(sect):endInd(sect)));
        t0      = tData(beginInd(sect):endInd(sect));
        pa0     = paData(beginInd(sect):endInd(sect));
        if S.NewSamplingSepMS 
         newT    = (0:S.NewSamplingSepMS:t0(end));
            newX    = interp1(t0,x0,newT,'linear'); % evenly sampled in time  'pchip'  'v5cubic' 'nearest' 'spline' : v5cubic produces smoother
            newY    = interp1(t0,y0,newT,'linear'); % evenly sampled in time  'pchip'  'v5cubic'
            newP    = interp1(t0,pa0,newT,'linear'); % evenly sampled in time  'pchip'  'v5cubic'
        else
            newT = t0';
            newX = x0';
            newY = y0';
            newP = pa0';
            
        end
        notNans = (~isnan(newX))&(~isnan(newY));
        newX    = newX(notNans);
        newY    = newY(notNans);
        newT    = newT(notNans);
        newDeltaTperSample = diff(newT);%/E.Settings.EyetrackingSampleRate))) delta t in micro seconds for tobii
        tD1     = newT(2:end);
        newP    = newP(notNans);
        Xall = [Xall(:)' newX];  % the newX s from all GoodSectors
        Yall = [Yall(:)' newY];
        Tall = [Tall(:)' newT];
        Pall = [Pall(:)' newP];
        
        %                                 figure(90),clf,plot(t0,pa0,'gx',t0,x0,'ro',newT,newX,'r.-',t0,y0,'bo',newT,newY,'b.-');
        %                                 legend('pupil area','X position', 'newX position','Y position','newY position')
        %                                 drawnow,WaitSecs(0.5);
        if Smooth4SpeedCalc && length(newX)>= Sgolength
            try
            newX = sgolayfilt(double(newX),2,Sgolength);
            newY = sgolayfilt(double(newY),2,Sgolength);
            catch previous_error
%                 rethrow(previous_error)
            fprintf('Unable to apply Sgolay filter to the eye trace data; TrialNo: %i \nErr Message:%s\n ', P.CurrentTrialNo, previous_error.message)
            end
        end
        xD1PixPerSample      = diff(newX); % ./round((diff(newT)/11103)); % 11103 is in microseconds is the sampling rate of Tobii
        yD1PixPerSample      = diff(newY); % ./round((diff(newT)/11103));
        
        goodVals = ((abs(xD1PixPerSample)<maxJumpPixelsPerSample)&(abs(yD1PixPerSample)<maxJumpPixelsPerSample)); %diff between 2 data points is less than max jump
        xD1PixPerSample      = xD1PixPerSample(goodVals);
        yD1PixPerSample      = yD1PixPerSample(goodVals);
        tD1      = tD1(goodVals);
        newDeltaTperSample      = newDeltaTperSample(goodVals);
        
        xDall    = [xDall(:)' xD1PixPerSample];
        yDall    = [yDall(:)' yD1PixPerSample];
        tDall    = [tDall(:)' tD1];
        DeltaTall =[DeltaTall(:)' newDeltaTperSample];
        
        
    end
end

%
% yDall=0.*yDall;
TimeDurationSamplesPerFlippedFrame = round(DeltaTall/( E.Window.InterFrameInterval*ETtimeConvtRatio)); % will show the number of samples we should have between two sample points.
yDall = 1.*yDall./TimeDurationSamplesPerFlippedFrame; % it is tured off because we have already rotated the signal
xDall = 1.*xDall./TimeDurationSamplesPerFlippedFrame;

Speed      = sqrt(xDall.^2 + yDall.^2);
SpeedAng   = atan2(yDall, xDall);
Acceleration = diff(Speed);

% polarplot(log(SpeedAng),log(Speed),'ro')
% figure,plot(tDall,xDall,'bs-',tDall,yDall,'ro-',tDall,SignedSpeed,'g*-',tDall,Speed,'k*-')
% legend('horizontal derivatives', 'vertical derivatives', 'signed speed', 'resultand speed')

clear DisRec AngRec
possthreshNo = 256;
AngRec = zeros(1,possthreshNo);
DisRec = zeros(1,possthreshNo);
if ~isempty(Speed)
    maxSpeed   = max(Speed); % default is multiplied by 2
    minSpeed   = StimSpeedPixPerSample; % 1.2/2; %maxSpeed./(2.^12);
    possthresh = 2.^(linspace(log2((minSpeed)),log2((maxSpeed)),possthreshNo));  %2.^linspace(2,7,32)2.^(linspace(log2((minSpeed)),log2((maxSpeed)),32   )); % in d
else
    possthresh = 2.^linspace(log2(1.2),log2(50),64);
end
% Sac2PursRatio = 10;
% possthresh      = linspace((S.SpeedPixPerMS/Sac2PursRatio),(S.SpeedPixPerMS*Sac2PursRatio),32);
for tLoop         = 1:length(possthresh)
    threshVal     = possthresh(tLoop);
    AboveVals     = find(Speed>=threshVal); %  Speed
    BelowVals     = find(Speed<threshVal); %
    VerComponent  = sum([sin(SpeedAng(BelowVals)).*Speed(BelowVals) -sin(SpeedAng(AboveVals)).*Speed(AboveVals)]);
    HorComponent  = sum([cos(SpeedAng(BelowVals)).*Speed(BelowVals) -cos(SpeedAng(AboveVals)).*Speed(AboveVals)]);
    AngRec(tLoop) = atan2(VerComponent,HorComponent);
    DisRec(tLoop) = sqrt(VerComponent.^2+HorComponent.^2); % it is the vector of magnitude of pursuit eye speed, calculated for different saccade threshold
end
%  figure(3), semilogx((possthresh),DisRec,'o-')

[~, maxInd] = max(DisRec);
Threshold = possthresh(maxInd); % saccade threshold

%% scoring
PredictedAng     = AngRec(maxInd); % an angle in radians
LookupAngles     = deg2rad(E.Settings.StimulusDirection);
[~, minInd]      = min(abs(DirDiff(LookupAngles,PredictedAng)));
whichDir         = minInd;

% we always rotate the eye-traces for glaucom okn processing
EffectiveDriftDirectionIdx = 1; % because of rotation it should be 1: rightwards drift     
ActualEyeDriftAngDeg = rad2deg(PredictedAng + StimulusDriftAngle);

EffectiveDriftDirectionRad = LookupAngles(EffectiveDriftDirectionIdx);

if range(DisRec) ~= 0  % this is added to avoid scoring no movement as the rightward movement
    if (whichDir == EffectiveDriftDirectionIdx)    % i.e. movement in the direction of stimulus.    before rotation was (E.interLoop==whichDir)
        OKNscore =  1;
        %         elseif (whichDir == 5) % i.e. movement in the opposite
        %             OKNscore = -1;
    else
        OKNscore =  0;  % other angle movements
    end
else % i.e. no movement in the signal
    OKNscore =  0;
end

if range(AngRec) ~= 0
    OKNConsistency = (pi-abs(DirDiff(EffectiveDriftDirectionRad,PredictedAng)))/(pi);
else
    OKNConsistency = 0 ;
end

% possibleDist     = S.SpeedPixPerMS * (S.CycleTime.*1000).*2; % the maximum possible movement if just tracking is occured;  Multiplied by two because of saccade it will return
possibleDistPixsPerTrial     = StimSpeedPixPerSample.*(S.CycleTime./E.Window.InterFrameInterval).*2; % the maximum possible movement if just tracking is occured; 

TotalDisRatio_H    = (nansum(abs(xDall)))./possibleDistPixsPerTrial;     % total distance eye traveled

TotalDisRatio_V    = (nansum(abs(yDall)))./possibleDistPixsPerTrial;    
TotalDisRatio = sqrt(TotalDisRatio_H.^2 + TotalDisRatio_V.^2);

%% preparing pursuit speeds for gain calculation
% PursSpeed = SignedSpeed(Speed < Threshold);

if DiscardFirst500MS4GainCalc   % discad first 500 ms of signal and analyse the rest
    EvaluatedSampsIndx = (tDall> 0.5 .* ETtimeConvtRatio);
else % i.e all the data
    EvaluatedSampsIndx = (tDall> 0);
end
EvaluatedSpeed = Speed(EvaluatedSampsIndx);
% EvaluatedSignedSpeed = SignedSpeed(EvaluatedSampsIndx);% xDall
EvaluatedSpeed_H = xDall(EvaluatedSampsIndx);% xDall   SignedSpeed
EvaluatedSpeed_V = yDall(EvaluatedSampsIndx);
EvaluatedtDall = tDall(EvaluatedSampsIndx);

% for ploting the gausian fit over speed histogram
% subplot(2,4,4)



%%%% GLM fit to signed speed data
GainSaccadeThreshold = Threshold; % StimSpeedPixPerSample;
ThePursuitInds = find(EvaluatedSpeed < GainSaccadeThreshold);%  +2*0.15*StimSpeedPixPerSample) ; % used stim speed as pursuit threshold StimSpeedPixPerSample

ThePursuitSpeed = EvaluatedSpeed(ThePursuitInds);
ThePursuitSpeed_H = EvaluatedSpeed_H(ThePursuitInds);
ThePursuitSpeed_V = EvaluatedSpeed_V(ThePursuitInds);
ThePursuitTime = EvaluatedtDall(ThePursuitInds);

% [b,dev,stats] = glmfit(ThePursuitTime,ThePursuitSpeed);% SignedSpeed
% yfit =  glmval(b,ThePursuitTime,'identity');
% SpeedFromGLMPixPerSamp = mean(yfit) + 2*std(yfit); % other options:  max(yfit); % median(yfit); % +2*std(yfit);

%%%% MAD gain
cutoff =3;
exclOutlier=ThePursuitSpeed(abs(ThePursuitSpeed-median(ThePursuitSpeed))<cutoff*mad(ThePursuitSpeed)); 
exclOutlier_H=ThePursuitSpeed(abs(ThePursuitSpeed_H-median(ThePursuitSpeed_H))<cutoff*mad(ThePursuitSpeed_H)); 
exclOutlier_V=ThePursuitSpeed(abs(ThePursuitSpeed_V-median(ThePursuitSpeed_V))<cutoff*mad(ThePursuitSpeed_V)); 
  

SpeedFromMADPixPerSamp = mean(exclOutlier); % mean(ThePursuitSpeed); % UpperCI;
SpeedFromMADPixPerSamp_H = mean(exclOutlier_H); % mean(ThePursuitSpeed); % UpperCI;
SpeedFromMADPixPerSamp_V = mean(exclOutlier_V);

SpeedFromMADPixPerSec = SpeedFromMADPixPerSamp*(1/E.Window.InterFrameInterval);
SpeedFromMADDegPerSec = SpeedFromMADPixPerSec/S.PixelsPerDegree;

SpeedFromMADPixPerSec_H = SpeedFromMADPixPerSamp_H*(1/E.Window.InterFrameInterval);
SpeedFromMADDegPerSec_H = SpeedFromMADPixPerSec_H/S.PixelsPerDegree;

SpeedFromMADPixPerSec_V = SpeedFromMADPixPerSamp_V*(1/E.Window.InterFrameInterval);
SpeedFromMADDegPerSec_V = SpeedFromMADPixPerSec_V/S.PixelsPerDegree;

MADgain = SpeedFromMADDegPerSec./ S.SpeedDegPerSec;
MADgain_H = SpeedFromMADDegPerSec_H./ S.SpeedDegPerSec;
MADgain_V = SpeedFromMADDegPerSec_V./ S.SpeedDegPerSec;
  
try
[xData_H, yData_H] = prepareCurveData( double(ThePursuitTime), double(ThePursuitSpeed_H) );

% Set up fittype and options.
ft_H = fittype( 'poly1' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Robust = 'Bisquare'; % LAR  Bisquare  According to mathwork : LAR is recommended for data with less outliers, Bisquare is recommended when outliers exist.
opts.Lower = [0 -Inf];
opts.Upper = [0 Inf];

% Fit model to data.
[fitresult_H, gof_H] = fit( xData_H, yData_H, ft_H, opts );

SpeedFromGLMPixPerSamp_H = fitresult_H.p2; % mean(ThePursuitSpeed); % UpperCI;

SpeedFromGLMPixPerSec_H = SpeedFromGLMPixPerSamp_H*(1/E.Window.InterFrameInterval);
SpeedFromGLMDegPerSec_H = SpeedFromGLMPixPerSec_H/S.PixelsPerDegree;
GLMgain_H = SpeedFromGLMDegPerSec_H./ S.SpeedDegPerSec;

catch  previous_error
%     rethrow(previous_error)
     GLMgain_H = nan;
    fprintf('The GLM fit to speed_H data is not possible; TrialNo: %i \n', P.CurrentTrialNo)
end

try
[xData_V, yData_V] = prepareCurveData( double(ThePursuitTime), double(ThePursuitSpeed_V) );

% Set up fittype and options.
ft_V = fittype( 'poly1' );
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Robust = 'Bisquare'; % LAR  Bisquare  According to mathwork : LAR is recommended for data with less outliers, Bisquare is recommended when outliers exist.
opts.Lower = [0 -Inf];
opts.Upper = [0 Inf];

% Fit model to data.
[fitresult_V, gof_V] = fit( xData_V, yData_V, ft_V, opts );

SpeedFromGLMPixPerSamp_V = fitresult_V.p2; % mean(ThePursuitSpeed); % UpperCI;

SpeedFromGLMPixPerSec_V = SpeedFromGLMPixPerSamp_V*(1/E.Window.InterFrameInterval);
SpeedFromGLMDegPerSec_V = SpeedFromGLMPixPerSec_V/S.PixelsPerDegree;
GLMgain_V = SpeedFromGLMDegPerSec_V./ S.SpeedDegPerSec;

catch previous_error
%      rethrow(previous_error)
     GLMgain_V = nan;
    fprintf('The GLM fit to speed_V data is not possible; TrialNo: %i \n', P.CurrentTrialNo)
end

GLMgain = sqrt(GLMgain_V^2 + GLMgain_H^2);

%% average gain (based on simple mean)
AveragePursSpeedPixPerSec_H = nanmean(ThePursuitSpeed_H)*(1/E.Window.InterFrameInterval);
AveragePursSpeedDegPerSec_H = AveragePursSpeedPixPerSec_H/S.PixelsPerDegree;
OKNGain_H = AveragePursSpeedDegPerSec_H./ S.SpeedDegPerSec;

AveragePursSpeedPixPerSec_V = nanmean(ThePursuitSpeed_V)*(1/E.Window.InterFrameInterval);
AveragePursSpeedDegPerSec_V = AveragePursSpeedPixPerSec_V/S.PixelsPerDegree;
OKNGain_V = AveragePursSpeedDegPerSec_V./ S.SpeedDegPerSec;

OKNGain = sqrt(OKNGain_V^2 + OKNGain_H^2);
%%
% NoBins=16;
% 
% % subplot(2,4,8)
% 
% [pAFreq pABins]=hist((Pall),NoBins);
% pABins=double(pABins);
% pAFreq=double(pAFreq);
% [uEst,varEst,scaleEst] = FitGaussian((pABins),pAFreq./sum(pAFreq));
% fineX=(linspace((min(Pall)),(max(Pall)),1000));
% pred=scaleEst.*NormalPDF((fineX),uEst,varEst^2);
% PupilAreaFromHistPix0 = (uEst);
% % plot((pABins),pAFreq./sum(pAFreq),'o',(fineX),pred,'r-')
% PupilAreaFromHistPix = mean(Pall);

% Baseline Pupil size
if 0 %isfield(P, 'BaseTrialNo') % If we have baseline pupil measurement
for i = 1: P.BaseTrialNo
BaselinePupilTimeInMS = E.BaseEyePos.Time(i,:);
BaselinePupilArea = E.BaseEyePos.PA(i,:,2);
goodVals = find(BaselinePupilTimeInMS>0 & BaselinePupilArea>0);
PupilTimeInMS=BaselinePupilTimeInMS(goodVals);
PupilArea = BaselinePupilArea(goodVals);
% figure(10+i), plot(PupilTimeInMS,PupilArea,'ro' )

[pAFreq, pABins]=hist((PupilArea),NoBins);
pABins=double(pABins);
pAFreq=double(pAFreq);
[uEst,varEst,scaleEst] = FitGaussian((pABins),pAFreq./sum(pAFreq));
fineX=(linspace((min(PupilArea)),(max(PupilArea)),1000));
pred=scaleEst.*NormalPDF((fineX),uEst,varEst^2);
BasePupilAreaFromHistPix0(i) = (uEst);
% plot((pABins),pAFreq./sum(pAFreq),'o',(fineX),pred,'r-')
BasePupilArea(i) = mean(PupilArea);
end
BasePupilAreaFromHist = mean(BasePupilAreaFromHistPix0);
BasePupilAreaFromHisMax  = max(BasePupilAreaFromHistPix0);
end
%%