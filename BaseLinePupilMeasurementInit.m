
% Alocating memory, might need to move them to the first part of code
E.Window.FrameCountBase      = zeros(1,E.Settings.BaseTotalTrialNo); % for pupil baseline measurement

E.Settings.MaxGrayLevelStepsPerBaseTrial = E.Settings.NoGrayLevelStepsRampPAbaseMeasure*2; % *2 because we want to calculate a cycle
E.Settings.GrayLevelsPerBaseTrial = round([linspace(E.Window.Black,E.Window.White,E.Settings.NoGrayLevelStepsRampPAbaseMeasure),linspace(E.Window.White,E.Window.Black,E.Settings.NoGrayLevelStepsRampPAbaseMeasure)]);
E.Settings.MaxFramesPerBaseTrial = E.Settings.EachStepPAbaseMeasureDurSec.*E.Settings.MaxGrayLevelStepsPerBaseTrial.*E.Computer.ScreenHz; 
E.Settings.MaxSamplesPerBaseTrial= (double(E.Settings.MaxSamplesPerFrame) * double(E.Settings.MaxFramesPerBaseTrial) + double(E.Settings.MaxSamplesPerFrame)*3); % add 'buffer' in case last flip is missed
E.SamplesPerBaseTrial        = zeros(E.Settings.BaseTotalTrialNo, max(P.NoFixedTrialsPerInter),'single');
E.BaseEyePos.X               = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxSamplesPerBaseTrial,2, 'single');
E.BaseEyePos.Y               = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxSamplesPerBaseTrial,2, 'single');
E.BaseEyePos.PA              = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxSamplesPerBaseTrial,2, 'single');
E.BaseEyePos.Time            = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxSamplesPerBaseTrial); % one less dim for Time
E.BaseEyePos.OverSample      = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxFramesPerBaseTrial,E.Settings.MaxSamplesPerFrame); % as Time
E.BaseEyePos.ResXY           = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxSamplesPerBaseTrial,2, 'single');
E.LastBaseEyePos             = single([NaN, NaN]);
E.Window.BaseFrameFlipTime  = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxFramesPerBaseTrial);
E.Window.BaseFrameDelta     = zeros(E.Settings.BaseTotalTrialNo, E.Settings.MaxFramesPerBaseTrial);