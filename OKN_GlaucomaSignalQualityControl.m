% function [E] = OKN_GlaucomaSignalQualityControl(E,P)
% have to initialize new structures
% theEye = 2 ; % 1 = left; 2 = right 
% coimmented out to deal with timing problem first
intLe           = P.InterList(P.CurrentTrialNo);
intLeTrial      = P.TrialNumbers(P.CurrentTrialNo);
theEye = E.WhichEye;
% theEye = (1:2);
GoodDataRatioThresh = 0.8;

StimulusDirection = DegToRad(E.Settings.StimulusDirection(intLe));

% SpeedQualityFlag =[];

 ThisTrialX = squeeze(E.EyePos.X(intLe,intLeTrial,:,theEye)).* X2PixConvertRatio;
 ThisTrialY = squeeze(E.EyePos.Y(intLe,intLeTrial,:,theEye)).* Y2PixConvertRatio;
 ThisTrialPA = squeeze(E.EyePos.PA(intLe,intLeTrial,:,theEye));
 ThisTrialTime      = squeeze(E.EyePos.Time(intLe,intLeTrial,:));
 
% rotate the signal
ThisTrialX0     = ThisTrialX - E.Window.PTBScreenWidthCenter; % change the center of rotation to the center of data
ThisTrialY0     = ThisTrialY - E.Window.PTBScreenHeightCenter;  %  E.Window.PTBScreenWidthCenter

% rotate the signal. Note the input angle should be negative here
ThisTrialX = ThisTrialX0.*cos(-StimulusDirection) - ThisTrialY0.*sin(-StimulusDirection);
ThisTrialY = ThisTrialX0.*sin(-StimulusDirection) + ThisTrialY0.*cos(-StimulusDirection);

ThisTrialX = ThisTrialX + E.Window.PTBScreenWidthCenter; % change back the center to the original coordinate center %   E.Window.PTBScreenWidth;
ThisTrialY = ThisTrialY + E.Window.PTBScreenHeightCenter;%   E.Window.PTBScreenHeight;
 
 
 ThisTrialInterpolatedFrames = sum(E.Window.InterpolatedFrames(intLe,intLeTrial,:));
 ThisTrialFlipTime =  squeeze(E.Window.FrameFlipTime(intLe,intLeTrial, :));
 ThisTrialFlipTimeFreq = 1./diff(ThisTrialFlipTime);
 % clipping off end of sequence
 goodTimes = find(ThisTrialTime>0);
ThisTrialPA = ThisTrialPA(goodTimes,:); % remove time vector's zero tail before processing
ThisTrialX  = ThisTrialX(goodTimes,:);
ThisTrialY  = ThisTrialY(goodTimes,:);
ThisTrialTime = ThisTrialTime(goodTimes,:);
 
% % first sample to check Gaussian fit-ablility
% ThisTrialEachFrameSamples = (squeeze(E.EyePos.OverSample(intLe, intLeTrial, :,:)))'; % keep the frame flip time for later processing
% NoSampsPerTrial = sum(ThisTrialEachFrameSamples>0);
% if strcmpi(E.Eyetracker,'EyeLink') && E.Computer.EyeLinkConnected
%     EachFrameSamplesGT0 = (ThisTrialEachFrameSamples(ThisTrialEachFrameSamples>0))'; % length(EachFrameSamples)== length(xData0)? : yes
%     FSInds  = find(EachFrameSamplesGT0==1);
% elseif strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
%     FSInds  = find(ThisTrialEachFrameSamples==1);
% end

% ThisTrialFSX =ThisTrialX(FSInds,1:length(theEye));
% ThisTrialFSY =ThisTrialY(FSInds,1:length(theEye));
% ThisTrialFSPA =ThisTrialPA(FSInds,1:length(theEye));
% ThisTrialFSTime =ThisTrialTime(FSInds);

paMedian    = nanmedian(ThisTrialPA);
paMad       = mad(ThisTrialPA,1); % median absolute deviation
paThresh    = paMedian - 3 *paMad; % The multiplyer of paStd has been chosen from a range of values to minimize the variation of the gain.
goodDataInds    = ((ThisTrialX>=0)&(ThisTrialY>=0)&(paMedian>paThresh));
goodDataInds = goodDataInds(:,1);

ThisTrialgoodX = ThisTrialX(goodDataInds,:);
ThisTrialgoodY = ThisTrialY(goodDataInds,:);
ThisTrialgoodPA = ThisTrialPA(goodDataInds,:);
ThisTrialgoodTime =ThisTrialTime(goodDataInds,:);

% Blinks and "no data" are nan in Tobii
NotisNaNsNumber = sum(([ThisTrialgoodX(:,1:length(theEye)); ThisTrialgoodY(:,1:length(theEye))])>0);
GoodGazeRatio = mean((NotisNaNsNumber(1:length(theEye))./(length(ThisTrialgoodX)*2)));%theEye
% time length QC
    if GoodGazeRatio >= GoodDataRatioThresh 
    ThisTrialgoodDuration = ThisTrialgoodTime(end) - ThisTrialgoodTime(1);
    ThisTrialTimeQualityFlag = abs(ThisTrialgoodDuration - S.CycleTime.* 1e6) < (0.2*S.CycleTime.* 1e6);%(3.* E.Window.InterFrameInterval.* 1e6); % 1000000 becasue Tobii time is in microsecs
    else
    ThisTrialTimeQualityFlag = false;        
    end 

    % ratio of positive x and y
    IsInScreenThreshDeg = 10; % a circle with 10 degree radius around centre of the screen
    IsInScreenThreshRatio =  IsInScreenThreshDeg./E.Window.DisplayHeightDeg;
    XposRatio = sum(sum((ThisTrialgoodX>(1-IsInScreenThreshRatio).*E.Window.PTBScreenWidthCenter) & (ThisTrialgoodX<(1+IsInScreenThreshRatio).*E.Window.PTBScreenWidthCenter))/ numel(ThisTrialgoodX));
    YposRatio = sum(sum((ThisTrialgoodY>(1-IsInScreenThreshRatio).* E.Window.PTBScreenHeightCenter) & (ThisTrialgoodY<(1+IsInScreenThreshRatio).* E.Window.PTBScreenHeightCenter))/ numel(ThisTrialgoodY));
    IsInScreenGazeFlag = (XposRatio>= GoodDataRatioThresh) & (YposRatio>= GoodDataRatioThresh) ;

% calculate the amount of good head position in this trial
if length(E.WhichEye)>1
E.GoodHeadPositionRatio = sum(E.GoodHeadPositionFlags(intLe,intLeTrial,:))./ length(E.GoodHeadPositionFlags(intLe,intLeTrial,:));
else %i.e monocular Tobii recording
E.GoodHeadPositionRatio = 0.9999;
end
 E.TrialDropedFrames = sum(ThisTrialFlipTimeFreq<57);
 E.TrialGoodDataRatio = GoodGazeRatio .* mean([XposRatio,YposRatio]);%(numel(ThisTrialGoodPA)./numel(ThisTrialPA));%(intLe,intLeTrial)
 GoodDataRatioThresh = 0.4.*length(E.WhichEye); % binocular threshold is 0.8
 E.TrialGoodDataFlag  = (E.TrialGoodDataRatio >= GoodDataRatioThresh) & (ThisTrialInterpolatedFrames <= 5) & (E.TrialDropedFrames<5) & (E.GoodHeadPositionRatio > 0.7) & (ThisTrialTimeQualityFlag) & (IsInScreenGazeFlag); % acc good data hiher than  80 percent and no more than one droped frames
 
%  figure,plot(ThisTrialTime,ThisTrialPA,ThisTrialTime(goodVals),ThisTrialPA(goodVals),'go')
%   figure,plot(ThisTrialTime(goodVals),ThisTrialGoodPA)

% [P.CurrentTrialNo  E.TrialGoodDataRatio  (ThisTrialInterpolatedFrames <= 5)  (E.TrialDropedFrames<2)  (SpeedQualityFlag)]