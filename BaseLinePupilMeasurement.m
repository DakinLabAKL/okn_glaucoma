

EachGrayLevelLengthFrames = E.Settings.MaxFramesPerBaseTrial / E.Settings.MaxGrayLevelStepsPerBaseTrial;
ThisBaseTrialOrderOfFramesEnd = linspace(EachGrayLevelLengthFrames,E.Settings.MaxFramesPerBaseTrial,E.Settings.MaxGrayLevelStepsPerBaseTrial);
ThisBaseTrialOrderOfFramesStart = ThisBaseTrialOrderOfFramesEnd - (EachGrayLevelLengthFrames-1);


for frameloop = 1 : E.Settings.MaxFramesPerBaseTrial
    
    for GrayLevelLoop = 1: E.Settings.MaxGrayLevelStepsPerBaseTrial
        if ismember(frameloop,ThisBaseTrialOrderOfFramesStart(GrayLevelLoop):ThisBaseTrialOrderOfFramesEnd(GrayLevelLoop))
            thisFrameGL = E.Settings.GrayLevelsPerBaseTrial(GrayLevelLoop);
            Screen('FillRect', E.Window.Pointer, thisFrameGL, E.Window.PTBScreenRect);
            if strcmp(E.Eyetracker, 'EyeLink')
                Eyelink('command', 'record_status_message "Set%d PAbaseTrial %d/%d GrayLevel %d"',...
                    E.Settings.ExperimentSet,P.BaseTrialNo ,E.Settings.BaseTotalTrialNo ,thisFrameGL );
            end
        end
    end
    
    entranceTime = GetSecs;
    numETSamps   = uint8(0);
    sampleCount     = E.SamplesPerBaseTrial(P.BaseTrialNo);
    E.Window.FrameCountBase(P.BaseTrialNo) = E.Window.FrameCountBase(P.BaseTrialNo) + 1;
    frameThisTrial  = E.Window.FrameCountBase(P.BaseTrialNo);
    % flip the rect
  
    if frameThisTrial == 1 %the first frame of a trial has to be good
        E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer); % ok if halting, as first
        E.Window.LastFrameDelta = 1;
    else
        E.Window.LastFrameDelta = 1;
        if entranceTime < (E.Window.LastFlipTime + (E.Window.InterFrameInterval * E.Settings.TimingTolerance)) % If have enough time
            E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer, (E.Window.LastFlipTime + (1-E.Settings.TimingTolerance) * E.Window.InterFrameInterval)); % here we flip the stimulus to the screen E.Timing.EyetrackingRiskRate
        else % if we don't have enough time
            E.Window.LastFrameDelta = E.Window.LastFrameDelta + 1;
            E.Window.InterpolatedFrames(P.BaseTrialNo,frameThisTrial) = 1; %E.Window.LastFrameDelta-1;
            E.Window.LastFlipTime = E.Window.LastFlipTime + (E.Window.InterFrameInterval * E.Settings.TimingTolerance);
        end
    end% get the Eyelink valuse
    E.Window.BaseFrameFlipTime(P.BaseTrialNo,frameThisTrial)= E.Window.LastFlipTime;
    E.Window.BaseFrameDelta(P.BaseTrialNo,frameThisTrial)   = E.Window.LastFrameDelta;
    exitTime         = E.Window.LastFlipTime + (E.Window.InterFrameInterval*E.Timing.EyetrackingRiskRate);
    timeNow          = GetSecs;
    
    while  timeNow < exitTime && E.Settings.EyetrackingOn
        if strcmpi(E.Eyetracker,'EyeLink') && E.Computer.EyeLinkConnected% EL is setup and wanted
            try %to get a EyeLink Sample
                if Eyelink('NewFloatSampleAvailable') > 0
                    evt                         = Eyelink('NewestFloatSample');
                    sampleCount = sampleCount + 1;
                    E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)        = single(evt.gx);
                    E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:)        = single(evt.gy);
                    E.BaseEyePos.PA(P.BaseTrialNo,sampleCount,:)       = single(evt.pa);
                    E.BaseEyePos.ResXY(P.BaseTrialNo,sampleCount,:)    = single([evt.rx, evt.ry]);
                    E.BaseEyePos.Time(P.BaseTrialNo,sampleCount)       = evt.time;
                    status = sprintf('OK');
                    E.LastBaseEyePos = [single(evt.gx), single(evt.gy)];
                    numETSamps = numETSamps + 1;
                    E.BaseEyePos.OverSample(P.BaseTrialNo,frameThisTrial,numETSamps)       = numETSamps; % timestamp is returned
                    
                end
            catch  % graceful failure if EyeLink fails for some reason?
                sampleCount = sampleCount + 1;
                E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)        = single(NaN);
                E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:)        = single(NaN);
                E.BaseEyePos.PA(P.BaseTrialNo,sampleCount,:)       = single(NaN);
                E.BaseEyePos.ResXY(P.BaseTrialNo,sampleCount,:)    = single(NaN);
                E.BaseEyePos.Time(P.BaseTrialNo,sampleCount)       = timeNow;
                E.LastBaseEyePos = [NaN NaN];
                status = sprintf('No Sample');
            end
        elseif strcmpi(E.Eyetracker,'SimEyelink')% ET wanted, but not connected = simulate
            sampleCount = sampleCount + 1;
            E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)        = single(E.Window.PTBScreenWidth*rand([1,2]));
            E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:)        = single(E.Window.PTBScreenHeight*rand([1,2]));
            E.BaseEyePos.PA(P.BaseTrialNo,sampleCount,:)       = single(500*rand([1,2]));
            E.BaseEyePos.ResXY(P.BaseTrialNo,sampleCount,:)    = single(25+2*rand([1,2]));
            E.BaseEyePos.Time(P.BaseTrialNo,sampleCount)       = timeNow;
            E.LastBaseEyePos = [squeeze(E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)),squeeze(E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:))];
            status = sprintf('OK');
            WaitSecs(1/E.Settings.EyetrackingSampleRate); % simulate ?NewFloatAvailable.
            numETSamps = numETSamps + 1;
            E.BaseEyePos.OverSample(P.BaseTrialNo,frameThisTrial,numETSamps)       = numETSamps;
        else %Eyetracker requested but not recognised?
            E.Settings.CriticalError = 1;
        end
        timeNow = GetSecs;  % Keep calm and carry on
    end
    E.SamplesPerBaseTrial(P.BaseTrialNo) = sampleCount;
    E = PsychSandboxGetKeyboardAndMouse(E);
end

%     if P.BaseTrialNo==1 || P.BaseTrialNo==2
%         NewTrial=1;
%     end
% end
