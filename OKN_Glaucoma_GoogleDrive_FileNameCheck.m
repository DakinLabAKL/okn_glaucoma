% File Names Control code
% OKN_Glaucoma_GoogleDrive_FileNameCheck

clearvars;
close all;
if exist(fullfile('C:\Users\smoh205\Google Drive\STORE\ALL WORKS\Soheil''s Works\SMD_PhD\ALL PhD Experiments Data\GOKN Study\GOKN_Data'),'dir')
    RootDir = fullfile('C:\Users\smoh205\Google Drive\STORE\ALL WORKS\Soheil''s Works\SMD_PhD\ALL PhD Experiments Data\GOKN Study\GOKN_Data');
    SpreadSheetDir = fullfile('C:\Users\smoh205\Google Drive');
elseif exist(fullfile('G:\My Drive\GOKN_Data'),'dir')
    RootDir = fullfile('G:\My Drive\GOKN_Data');
    SpreadSheetDir = fullfile('G:\My Drive');
else
    RootDir=uigetdir('C:/', 'Please select the repository contating GOKN data');
    SpreadSheetDir = uigetdir('C:/', 'Please selecct the repository contating the spreadsheet');
end

% load the list of names from google spread sheet,
% the DOCID for GOKN Data Input file is  '1gIjMCatFVxsdB9zjVrnoXfwtENp0JLgWckytXlvWYB0'
TheSpreadSheetDocID = '1gIjMCatFVxsdB9zjVrnoXfwtENp0JLgWckytXlvWYB0';
TheGOKNgSheetTable =  GetGoogleSpreadsheet(TheSpreadSheetDocID); % load the google spread shert
TheGOKNgSheetObserverIDsAll = TheGOKNgSheetTable(2:end,1); % load the name column
TheGOKNgSheetObserverIDs = (TheGOKNgSheetObserverIDsAll)'; % transpose the name column
TheGOKNgSheetObserverIDs = strtok(TheGOKNgSheetObserverIDs,' '); % just pick the initials that we have on the spread sheed
TheControlIDS = {'WK_06091953', 'IR_02081941'}; % the IDS for control individuals
TheOmittedIDs = [{'JB_25111936', 'ER_19111941','PG_19071973', 'CH_22091942', ''},TheControlIDS{:}]; % , TheControlIDS{:}  PG is yet to be collected (probabely will not be collected)
TheOmittedIDsIdx = find(ismember(TheGOKNgSheetObserverIDs,TheOmittedIDs)); % input the names for loading Google drive list
% TheGOKNgSheetObserverIDsTable = cell2table(TheGOKNgSheetObserverIDs);
% TheGOKNgSheetObserverIDs = table2cell(TheGOKNgSheetObserverIDsTable(1,[1:TheOmittedIDsIdx-1,TheOmittedIDsIdx+1:end])); % remove the unwanted ID
TheGOKNgSheetObserverIDs(TheOmittedIDsIdx) = [];% remove the unwanted IDs
[TheGOKNgSheetObserverNames, Remain] =  strtok(TheGOKNgSheetObserverIDs,'_');
TheGOKNgSheetObserverDOB   = strtok(Remain,'_');

% load which eye is recorded as the affected eye
% WorseEye     =  [  2  , 2  , 2  , 2  , 1  ,  2 , 2  ,  1  , 1  , 2  ,  2 , 2  ,  2  ,     2 ,  2 , 2  , 2  , 2  , 2  ,  1 ,  2,  2  ,  2 , 1  , 2]; % 1: left eye worst eye    2: right eye worst eye
WorseEyecolumnIdx = find(strcmpi(TheGOKNgSheetTable(1,:),'Worst-affected eye (0=R, 1 = L)')); % PLS DO NOT EDIT THIS STRING
TheGOKNgSheetWorseEye = TheGOKNgSheetTable(2:end,WorseEyecolumnIdx);
TheGOKNgSheetWorseEye(TheOmittedIDsIdx) = [];% remove the unwanted ID

for i = 1:numel(TheGOKNgSheetWorseEye)
    if    ismember(TheGOKNgSheetWorseEye{i},'0') 
        TheGOKNgSheetWorseEye{i} = '2'; % 2 beacause in the eye tracker space the LE is 1 and the RE is 2. Here we channged the zeros into 2s.
    elseif isempty(TheGOKNgSheetWorseEye{i})
          TheGOKNgSheetWorseEye{i} = '1';
    elseif sum(ismember( 'control',TheGOKNgSheetWorseEye{i})) ==7 % that patient is recorded as a control patient
        TheGOKNgSheetWorseEye{i} =  '1'; % if control we assigned the left eye as the effected eye, although they don't have one
    end
end

WorseEye  = str2num(cell2mat(TheGOKNgSheetWorseEye))'; % 1 is LE and 2 is RE
% WorseEye(TheOmittedIDsIdx)=[]; % remove the unwanted ID
BetterEye = (~(WorseEye-1))+1;

% load Visual field index (VFI) from gSheet for both eyes
RE_VFIcolumnIdx = find(strcmpi(TheGOKNgSheetTable(1,:),'VF VFI RE'));
RE_VFIstr = TheGOKNgSheetTable(2:end,RE_VFIcolumnIdx)';
RE_VFIstr(TheOmittedIDsIdx)=[]; % remove the unwanted ID
RE_VFIstr{12} = RE_VFIstr{12}(1:2); % remove the % sign from the 12th input

LE_VFIcolumnIdx = find(strcmpi(TheGOKNgSheetTable(1,:),'VF VFI LE ')); % note space after LE was in the Gsheet
LE_VFIstr = TheGOKNgSheetTable(2:end,LE_VFIcolumnIdx)';
LE_VFIstr(TheOmittedIDsIdx)=[]; % remove the unwanted ID
LE_VFIstr{12} = LE_VFIstr{12}(1:2);% remove the % sign from the 13th input

% load mean deviatio MD from gSheet
RE_MDcolumnIdx = find(strcmpi(TheGOKNgSheetTable(1,:),'VF MD RE'));
RE_MDstr = TheGOKNgSheetTable(2:end,RE_MDcolumnIdx)';
RE_MDstr(TheOmittedIDsIdx)=[]; % remove the unwanted ID

LE_MDcolumnIdx = find(strcmpi(TheGOKNgSheetTable(1,:),'VF MD LE')); % note space after LE was in the Gsheet
LE_MDstr = TheGOKNgSheetTable(2:end,LE_MDcolumnIdx)';
LE_MDstr(TheOmittedIDsIdx)=[]; % remove the unwanted ID

% convert VFI and MD strings into numbers
RE_VFI=[]; LE_VFI=[];  RE_MD=[]; LE_MD=[];
for i = 1: numel(LE_VFIstr)
    try
    RE_VFI(i) = str2num(RE_VFIstr{i});
    LE_VFI(i) = str2num(LE_VFIstr{i});
    
    RE_MD(i) = str2num(RE_MDstr{i});
    LE_MD(i) = str2num(LE_MDstr{i});
    catch
        RE_VFI(i) = 99.9; % Notice that these valus are not real and will not present if the Gsheet is complete
        LE_VFI(i) = 99.8;
        RE_MD(i)  = -0.1;
        LE_MD(i)  = -0.2;
        waitfor(errordlg(sprintf('Patient %s; gSheet is not complete.\nThe BE/WE values for this ID are arbitrory!\n',...
            TheGOKNgSheetObserverIDs{i})))
    end
end

% choose  better eye (BE) and worst eye (WE) based on VFI and MD
    BetterEyeVFI = max(LE_VFI,RE_VFI); % The higher the VFI the better the eye
    WorseEyeVFI  = min(LE_VFI,RE_VFI);
    BetterEyeMD = max(LE_MD,RE_MD); % The higher MD the better the eye (e.g -2> -12, therefore -2 is better). 
    WorseEyeMD  = min(LE_MD,RE_MD);
    
      % double check if got the correct better eye based on VFI
    if isequal( BetterEye==1, (LE_VFI==BetterEyeVFI)) % ie check if LE is listed as the better eye in gSheet and LE is actually the better eye based on VFI
      fprintf('The VFI list confirms the list of affected eyes.\n\n')
    else
        waitfor(errordlg(sprintf('Check the Worse Eye list!\nInconsistency between VFI and Worse Eye list.')))
    end
    
          % double check if got the correct better eye based on MD
    if isequal( BetterEye==1, (LE_MD==BetterEyeMD)) % ie check if LE is listed as the better eye in gSheet and LE is actually the better eye based on VFI
      fprintf('The MD list confirms the list of affected eyes.\n\n')
    else
        waitfor(errordlg(sprintf('Check the Worse Eye list!\nInconsistency between MD and Worse Eye list.')))
    end

% load the list of patients located in the google drive folder
TheGOKNFolderFileList = dir(RootDir); % file list in the GOKN google drive 
% [TheGOKNFolderFileListAllInitials,Remain] = strtok({TheGOKNFolderFileList(3:end).name},'_');
% the OD/OS and DOB is not consistent so make find DOB correctly
TheGOKNFolderFileListAllDOB ={}; TheGOKNFolderFileListAllInitials=[];
for i = 3: numel({TheGOKNFolderFileList.name})
    ThisSubjStrs = strsplit(TheGOKNFolderFileList(i).name,'_');
    if length(ThisSubjStrs)==5 && ismember(length(ThisSubjStrs{1}),[2 3])
         ThisSubjGOKNFolderInitials = ThisSubjStrs{1};
        if ismember(length(ThisSubjStrs{2}),8)
            ThisSubjGOKNFolderDOB = ThisSubjStrs{2};
        elseif ismember(length(ThisSubjStrs{3}),8)
            ThisSubjGOKNFolderDOB = ThisSubjStrs{3};
        end
        TheGOKNFolderFileListAllDOB = [TheGOKNFolderFileListAllDOB,{ThisSubjGOKNFolderDOB}];
        TheGOKNFolderFileListAllInitials =[TheGOKNFolderFileListAllInitials,{ThisSubjGOKNFolderInitials}];
    end
end
% [~,Remain2] = strtok(Remain,'_%i');
% TheGOKNFolderFileListAllDOB = strtok(Remain2,'_');

TheGOKNFolderFileListAllIDs ={};
for i = 1: numel(TheGOKNFolderFileListAllInitials)
    TheGOKNFolderFileListID = [TheGOKNFolderFileListAllInitials{i},'_',TheGOKNFolderFileListAllDOB{i}];
    if ~sum(ismember({'SMD', 'HMK', 'JY'},TheGOKNFolderFileListAllInitials{i})) % if its an actual patient add it to the list of IDs
    TheGOKNFolderFileListAllIDs = [TheGOKNFolderFileListAllIDs, {TheGOKNFolderFileListID}];
    end
end

% Select just the unique IDs for GOKN folder like Gsheet
[TheGOKNFolderFileListIDs , iIn, iOut] = unique(TheGOKNFolderFileListAllIDs);
TheGOKNFolderOmittedIDsIdx = find(ismember(TheGOKNFolderFileListIDs,TheOmittedIDs)); % input the names for loading Google drive list
TheGOKNFolderFileListIDs(TheGOKNFolderOmittedIDsIdx) =[];
[TheGOKNFolderFileListInitials, Remain] = strtok(TheGOKNFolderFileListIDs,'_');
TheGOKNFolderFileListDOB = strtok(Remain,'_');

% Comparing the numebr of IDs in each location
nIDsGOKNFolder = numel(TheGOKNFolderFileListIDs);
nIDsgSheetFolder =numel(TheGOKNgSheetObserverIDs);

% Finding the missing IDs if any

    [TheGsheetMissingID,MissingIDGOKNIdx ]= setdiff(TheGOKNFolderFileListIDs,TheGOKNgSheetObserverIDs); % find if a file is in GOKN folder but not in the Gsheet
    if ~isempty(TheGsheetMissingID) % i.e something is missing in the spread sheet
        waitfor(msgbox(sprintf('%s\nis missing in the spread sheet',cell2mat(TheGsheetMissingID)),'Missing File!'))
    else % i.e the spread sheed is not missing any file compared to google drive
        waitfor(msgbox('The spread sheet contains all IDs','Great!'))
    end
    
    [TheGOKNFolderMissingID,MissingIDgSheetIdx ]= setdiff(TheGOKNgSheetObserverIDs,TheGOKNFolderFileListIDs); % find if a file is in GOKN folder but not in the Gsheet
    if ~isempty(TheGOKNFolderMissingID) % i.e something is missing in the spread sheet
        waitfor(msgbox(sprintf('%s\nis missing in the Google drive',cell2mat(TheGOKNFolderMissingID)),'Missing File!'))
    else % i.e the spread sheed is not missing any file compared to google drive
        waitfor(msgbox('The Google drive contains all IDs','Great!'))
    end
        
 
    % put the GOKN folder in the right order (similar to gsheet)
    for i = 1: numel(TheGOKNgSheetObserverIDs)
        SortLikeGsheetIdx(i) = find(ismember(TheGOKNFolderFileListIDs,TheGOKNgSheetObserverIDs{i}));
    end
    TheGOKNFolderFileListIDsSorted = TheGOKNFolderFileListIDs(SortLikeGsheetIdx);


% observer DOB based on  Gsheet
ObserverName = TheGOKNgSheetObserverNames;
ObserverID   = TheGOKNgSheetObserverIDs;
ObserverDOB  = TheGOKNgSheetObserverDOB; % strtok(ObserverID,'_');


EyeList       = 'LR';
EyeListFileNames = ['D', 'S', 'D', 'S'];
EyeListLatin ='SD';
ExperimentConditions = {'direction', 'direction', 'contrast' ,'contrast'};
ExperimentConditionList = {'direction', 'contrast'};
% conditions used in GOKN
% 1 = RE Direction
% 2 = LE Direction
% 3 = RE Contrast
% 4 = LE Contrast

for subLoop  = 1:length(ObserverName) % subjects
    for conLoop = 1: length(ExperimentConditions)
        clear fNameString TheName
        % load up the this subject name files
        fNameString  = sprintf('%s%c%s*O%s*%s*%s*.mat',RootDir,filesep,ObserverName{subLoop},EyeListFileNames(conLoop),ObserverDOB{subLoop},ExperimentConditions{conLoop});
        fNameString2 = sprintf('%s%c%s*%s*O%s*%s*.mat',RootDir,filesep,ObserverName{subLoop},ObserverDOB{subLoop},EyeListFileNames(conLoop),ExperimentConditions{conLoop}); % the naming scheme of recorded data was changed
        AllFiles    = dir(fNameString);
        AllFiles2   = dir(fNameString2);
        TheMissedFileFound = 0;
        if isempty([{AllFiles.name}]) && isempty([{AllFiles2.name}]) 
            ErrStr = sprintf('Check the file name; "%s O%s%s %s" is not there!\nI will check if the right file is there.',ObserverName{subLoop},EyeListFileNames(conLoop),ObserverDOB{subLoop},ExperimentConditions{conLoop});
            waitfor(errordlg(ErrStr,'File Name Error'));
            fNameString  = sprintf('%s%c%s*.mat',RootDir,filesep,ObserverName{subLoop});
            AllFilesWithThisInit    = dir(fNameString);
            fileNameLoop =0;
            while fileNameLoop <= numel({AllFilesWithThisInit.name})
                fileNameLoop = fileNameLoop+1;
                TheName     =  sprintf('%s%c%s',RootDir,filesep,AllFilesWithThisInit(fileNameLoop).name); % runLoop
                load(TheName);
                if iscell(E)
                    E = E{1};
                    P = P{1};
                    S = S{1};
                end
                if E.Settings.UncoveredEye == strfind(EyeListLatin,EyeListFileNames(conLoop)) &&...   % eye 1= OS 2 = OD
                        E.Settings.ExperimentSet== find(ismember(ExperimentConditionList,ExperimentConditions{conLoop}))  %  condition 1= direction 2= contrast 3=demo
                    TheMissedFileFound = 1;
                    TheMissedFileName = TheName;
                    msgStr = sprintf('The file name for "%s  O%s %s" is:\n "%s" ',ObserverName{subLoop},EyeListFileNames(conLoop),ExperimentConditions{conLoop},TheName);
                    waitfor(msgbox(msgStr,'Succeed'));
                    break
                end
            end
        end
    end
end

HppyMsgStr = sprintf('Great!\nEvery file name is correct.');
                    f=msgbox(HppyMsgStr,'Succeed');
                  