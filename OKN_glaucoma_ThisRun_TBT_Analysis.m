% OKN glaucoma analysis trial based

% Analysis of just recorded data
% close all
%convert cells to structures
if iscell(E)
    E = E{1};
    P = P{1};
    S = S{1};
end

% check for droped frames
if 0
    for i = 1:1:length(P.TrialNumbers)
        theInt= P.InterList(i);
        TrialofTheInt = P.TrialNumbers(i);
        InterpolatedFrames   = squeeze(squeeze(E.Window.InterpolatedFrames(theInt,TrialofTheInt,1:120)));
        InterpolatedFramesNo = sum(InterpolatedFrames);
        FrameRate = double(1./diff(squeeze(E.Window.FrameFlipTime(theInt, TrialofTheInt, 1:end))));%.*double(~InterpolatedFrames); %  double(InterpolatedFrames);
        ETsamplesPerFrame = squeeze(E.EyePos.OverSample(theInt,TrialofTheInt,:));
        DroppedSamples = length(ETsamplesPerFrame)-sum(ETsamplesPerFrame);
        figure(40),
        subplot(121);plot(FrameRate,'ro')
        title(sprintf('T:%2.2f Int:%2.2f  Rep:%2.2f IntrpFs:%3.0f'  ,i, theInt, TrialofTheInt, InterpolatedFramesNo ))
        subplot(122);plot(ETsamplesPerFrame,'ro');
        title(sprintf('Dropped ET samples: %2.2f / %2.2f', DroppedSamples, length(ETsamplesPerFrame)  ))
        ylim([0 max(ETsamplesPerFrame(:))+0.5]);
        drawnow,pause;
    end
end
ObserverName = 'test';%{E.Folders.SaveFileName(1:9)}; % SMD_VFLF' 'SCD_VFLF'};
JustFirstSample = 0; % i.e. just use the first sample per each frame
EyeList       = 'LR';
TheRuns       = {1}; % We have one final run runs of 288 trials for four contrast levelsE.Settings.BaseContrastLevels
TheConditions = 4; % AFC
conLoop       = 1;
MaxSaccSpeedDegPerSec = 500; % fixed - determined by minimisation
paStdThresh           = 2.75; %  used to find and remove blinks
FrameMargin           = 12; % number of eye tracking samples to disgard either side of a blink. was 5
SSpdWS                = 4; % window in milisecond, after and before each point to calculate sliding window speed.
Smooth4SpeedCalc =1; % 1= use sgolay filter to smooth eye-position for speed calculation
Sgolength = 7;
IVvalues= [1];
IVloop=1;
DiscardFirst500MS4GainCalc = 1; % 1: discard the first half a second
tightPlot = 0; % 1: plot in tight plots

TheEyes = E.WhichEye; % The examined eyes; 1=LE 2=RE [1,2]= Binocular
SubplotRowSize = E.Settings.DirectionNums;
SubplotColSize = P.TotalTrials./E.Settings.DirectionNums;

if tightPlot
    if ismember(1,TheEyes)
        figLE = figure(91);
        FigHandLE = tight_subplot(SubplotRowSize,SubplotColSize,0,0.1,0.1);
    end
    if ismember(2,TheEyes)
        FigRE = figure(92);
        FigHandRE = tight_subplot(SubplotRowSize,SubplotColSize,0,0.1,0.1);
    end
end
runLoop = 1;
if  strcmp( E.Eyetracker,'Tobii')
    X2PixConvertRatio = E.Window.PTBScreenWidth;
    Y2PixConvertRatio = E.Window.PTBScreenHeight;
    ETtimeConvtRatio = 1e6; % used to convert speed per sample to speed per microsecs
    
else
    X2PixConvertRatio = 1;
    Y2PixConvertRatio = 1;
    ETtimeConvtRatio = 1e3; % used to convert speed per sample to speed per ms
end

% predefine the structures,
AllHgainstore = cell(1,1,length(TheEyes),1,P.NoInterleaved,length(P.PossStimValues{1}), length(P.PossContrastValues{1}), length(P.PossMaskValues{1}), E.Settings.LocalTrialNo);
% subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep

% Restructure the collected data
for subLoop = 1
    for TrialLoop = 1 : P.CurrentTrialNo %P.TotalTrials
        ThisTrialIntLe=[]; ThisIntLeRept=[]; ThisTrialMaskInd=[]; ThisTrialStimInd=[]; ThisTrialContInd=[];
        ThisTrialIntLe    = P.InterList(TrialLoop);     % the interleaved condition of this trial (1=rightward  2=leftward stimulus movement )
        ThisIntLeRept     = P.TrialNumbers(TrialLoop);  % the number of repetitions of a direction reached by this trial
        ThisTrialMaskInd  = P.MaskInds(TrialLoop);      % the mask index of this trial
        ThisTrialStimInd  = P.StimInds(TrialLoop);      % the stimulus index of this trial
        ThisTrialContInd  = P.ContrastInds(TrialLoop);  % the contrast index of this trial
        TheSimilarTrialIndices=find((P.InterList==ThisTrialIntLe) & (P.MaskInds==ThisTrialMaskInd) & (P.ContrastInds==ThisTrialContInd) & (P.StimInds'==ThisTrialStimInd));
        ThisTrialOverallIntLeavedRep = find(TheSimilarTrialIndices==TrialLoop);
        for eyeLoop = 1:length(TheEyes)
            % Get eye-position data
            xDataIn         = squeeze(E.EyePos.X(ThisTrialIntLe, ThisIntLeRept, :,TheEyes(eyeLoop) )).* X2PixConvertRatio;
            yDataInvIn      = squeeze(E.EyePos.Y(ThisTrialIntLe, ThisIntLeRept, :, TheEyes(eyeLoop))).* Y2PixConvertRatio;
            yDataIn         = -(yDataInvIn(:) - E.Window.PTBScreenHeight);% turn the Y direction from screen coordiante to mathematic coordinate. if this line is in the code, make sure that in dirdiff the angles are in correct order which should be [0 pi/2 pi 3pi/2]
            FrameFlipDataIn = squeeze(E.Window.FrameFlipTime(ThisTrialIntLe, ThisIntLeRept, :)); % keep the frame flip time for later processing
            
            paDataIn      = squeeze(E.EyePos.PA(ThisTrialIntLe,  ThisIntLeRept, :, TheEyes(eyeLoop)));  % pa: pupil area
            tDataIn       = squeeze(E.EyePos.Time(ThisTrialIntLe, ThisIntLeRept, :));
            
            StimulusDriftAngle = DegToRad(E.Settings.StimulusDirection(ThisTrialIntLe));
            
            SamplingRate =IVvalues(IVloop);
            %500; % 800 deg
            %             try
            OKN_GlaucomaProcessing
            %             catch
            %                 HistGain = nan;
            %             end
            
            if 1    
                
                if   1% SldGain<0.5 && (ThisTrialContInd==4) %(ThisTrialMaskInd==9)&&SldGain<0.3 OKNscore==0 %(subLoop==3)% &(abs(OKNGain) <0.4)& % ismember(ThisTrialMaskInd, [1 5 9 ] ) && ismember(ThisIntLeRept, [ 1 8])  && ismember(ThisTrialIntLe,[1 2])%|| ThisTrialStimInd == 5
                    
                    
                    BelowThresh = find(Speed<Threshold);
                    AboveThresh = find(Speed>=Threshold);
                    PursOrNot = Speed<Threshold;
                    
%                     SeqBelowThresh = find((Speed) < Threshold);
%                     SeqAboveThresh = find((Speed) >= Threshold);
                    
                    SpeedDb = 20.*log10(Speed);
                    xDallDb = 20.*log10(abs(xDall));
                    yDallDb = 20.*log10(abs(yDall));
                    
%                     PursSpeedDb = 20.*log10(PursSpeed);
                    
                    if ~isempty(Speed)
                        maxSpdVal = max(Speed);
                    else
                        maxSpdVal = 1;
                    end
                    %                     StimSpeedPlt = S.SpeedPixPerMS.*Tall;
                    StimPosition = (S.SpeedPixPerSec/ETtimeConvtRatio).*Tall;
%                     EstOffsetFromHist = Tall.*(SpeedFromHistPixPerSec/ETtimeConvtRatio);
                    EstOffsetFromGLM_H = Tall.*(SpeedFromGLMPixPerSec_H/ETtimeConvtRatio);
                    EstOffsetFromGLM_V = Tall.*(SpeedFromGLMPixPerSec_V/ETtimeConvtRatio);

                    StimPosition =   StimPosition+ abs(Xall(round(numel(Tall)/2)) - StimPosition(round(numel(Tall)/2)));
%                     EstOffsetFromHist = EstOffsetFromHist+ abs(Xall(round(numel(Tall)/2)) - EstOffsetFromHist(round(numel(Tall)/2)));
                    EstOffsetFromGLM_H  = EstOffsetFromGLM_H+ abs(Xall(round(numel(Tall)/2)) - EstOffsetFromGLM_H(round(numel(Tall)/2)));
                    EstOffsetFromGLM_V  = EstOffsetFromGLM_V+ abs(Xall(round(numel(Tall)/2)) - EstOffsetFromGLM_V(round(numel(Tall)/2)));
                                        
                    %                                             subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,((S.SpeedPixPerSec/1e6).*Tall),'k-.', Tall,((SpeedFromHistPixPerSec/1e6).*Tall),'m-.' );
                    %                                             subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall,EstOffsetFromHist,'b-',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimSpeedPlt,'k-.',Tall,(StimSpeedPlt.*HistGain)+E.Window.PTBScreenWidthCenter,'m-.' );
                    %                                                                                 legend('Rotated Eye X Position', 'Saccade', 'Tracking', '10 deg/s', sprintf('Gain %2.2f',HistGain), 'Location', 'Best')
                    %                         ylim([0 E.Window.PTBScreenWidth]),
                    %                         title(sprintf('Subject:%i %sE %iAFC Contrast:%2.2f ; Mask:%i Inter:%i Repetition:%i   XData',subLoop, EyeList(eyeLoop), TheConditions(conLoop), E.Settings.BaseContrastLevels(ThisTrialContInd)*100, ThisTrialMaskInd, ThisTrialIntLe , ThisIntLeRept)); shg
                    %                         subplot(2,4,2);plot( Tall,Yall,'b-',Tall(AboveThresh),Yall(AboveThresh),'r.',Tall(BelowThresh),Yall(BelowThresh),'g.');
                    %                         title('YData');
                    %                         ylim([0 E.Window.PTBScreenHeight]),
                    %                         subplot(2,4,3);plot( xData0,yData0,'b-',xData00, yData00,'r.');
                    %                         title(sprintf('Unprocessed Eye Gaze on Screen (%2.0f deg) ',E.Settings.StimulusDirection(ThisTrialIntLe)));
                    %                         ylim([0 E.Window.PTBScreenHeight]),  xlim([0 E.Window.PTBScreenWidth]),
                    %                         legend('Original Eye Gaze', 'Rotated Eye Gaze')
                    %
                    %                         %                                     subplot(2,4,4),plot( Tall,Xall,'b-',Tall(PursStartInd),Xall(PursStartInd),'r*',Tall(PursEndInd),Xall(PursEndInd),'ks');ylim([0 E.Window.PTBScreenWidth]),
                    %                         %                                     legend('eye position', 'Star of Pursuit', 'End of Pursuit')
                    %                         subplot(2,4,5);plot( tDall,xDall,'b-',tDall(AboveThresh),xDall(AboveThresh),'r.',tDall(BelowThresh),xDall(BelowThresh),'g.',tDall,HistGain.*ones(size(tDall)),'K-');
                    %                         title('XSpeed'); legend('Speed','Saccade', 'Tracking',sprintf('Gain=%2.2f',HistGain)),ylim([-maxSpdVal maxSpdVal])
                    %                         subplot(2,4,6);plot(tDall,yDall,'b-',tDall(AboveThresh),yDall(AboveThresh),'r.',tDall(BelowThresh),yDall(BelowThresh),'g.',tDall,OKNGain.*ones(size(tDall)),'K-');
                    %                         ylim([-maxSpdVal maxSpdVal]),title('YSpeed');
                    %                         subplot(2,4,7);plot(tDall,Speed,'b-',tDall(AboveThresh),Speed(AboveThresh),'r.',tDall(BelowThresh),Speed(BelowThresh),'g.',tDall,OKNGain.*ones(size(tDall)),'K-');
                    %                         ylim([-maxSpdVal maxSpdVal]),title('Directional Speed');
                    %                         subplot(2,4,7);polarplot(SpeedAng,SpeedDb,'b.',SpeedAng(AboveThresh),SpeedDb(AboveThresh),'ro',SpeedAng(BelowThresh),SpeedDb(BelowThresh),'go')
                    %                         %                                     subplot(2,4,8),plot(2:length(tData),1./(diff(tData./1000)),'b.-'),
                    %                         %                                     title(sprintf('Mean first sample frequency %2.2f',mean(1./(diff(tData./1000)))))
                    %                         %                                     %                                     subplot(2,4,8);plot(1:length(tData0),tData0,'b.')
                    %                         %                                         polarplot(PursSpeedAng,PursSpeedDb,'b.',PursSpeedAng(SeqAboveThresh),PursSpeedDb(SeqAboveThresh),'ro',PursSpeedAng(SeqBelowThresh),PursSpeedDb(SeqBelowThresh),'go')
                    %                         set (gcf, 'Units', 'normalized', 'Position', [0  0.1  1  0.8]); %  [1.5000    0.0778    0.7500    0.6718]);
                    %
                    %                         fprintf('Trial: %i; %sE: OKNscore: %i;  Threshold: %2.2f;  HistGain: %2.2f; OKN Consistency: %2.2f  TotalDisRatio: %2.2f; ConsDistRatio: %2.2f\n',...
                    %                             TrialLoop, EyeList(eyeLoop), OKNscore, Threshold, HistGain, OKNConsistency, TotalDisRatio, ConsistentDistRatio);
                    %                         drawnow
                    
                    %                     SubPlotCntr(eyeLoop) = SubPlotCntr(eyeLoop)+1;
                    if tightPlot
                        SubpltNo = ((ThisTrialIntLe-1)*(P.TotalTrials/E.Settings.DirectionNums))+ ThisIntLeRept;
                        if TheEyes(eyeLoop)==1
                            axes(FigHandLE(SubpltNo));
                        end
                        if TheEyes(eyeLoop)==2
                            axes(FigHandRE(SubpltNo));
                        end
                        
                        %                                         title(sprintf('T%i Int%i Rep%i XData', TrialLoop, ThisTrialIntLe, ThisIntLeRept)); shg
                        %                     figure,
                        plot(Tall,Xall,'b.-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimPosition,'k-.', Tall,EstOffsetFromGLM_H,'m-.' );%SpeedFromHistPixPerSec
                        %                    plot(tDall,xDall,'b-',tDall(AboveThresh),xDall(AboveThresh),'r.',tDall(BelowThresh),xDall(BelowThresh),'g.',Tall,((S.SpeedPixPerSec/1e6).*Tall),'k-.', Tall,((SpeedFromHistPixPerSec/1e6).*Tall),'m-.' );
                        %                     title (sprintf('est gain %3.3f',HistGain))
                        %                     SpeedVector(eyeLoop,:) = xDall;
                        
                        %                     subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall,EstOffsetFromHist,'b-',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimSpeedPlt,'k-.',Tall,(StimSpeedPlt.*HistGain)+E.Window.PTBScreenWidthCenter,'m-.' );
                        %                                                         legend('Rotated Eye X Position', 'Saccade', 'Tracking', '10 deg/s', sprintf('Gain %2.2f',HistGain), 'Location', 'Best')
                        if TheEyes(eyeLoop)==1
                            set(FigHandLE,'XTickLabel',''); set(FigHandLE,'YTickLabel','');
                        end
                        if TheEyes(eyeLoop)==2
                            set(FigHandRE,'XTickLabel',''); set(FigHandRE,'YTickLabel','');
                        end
                        
                        if TrialLoop == P.TotalTrials
                            theLablesSubplotNO = (E.Settings.DirectionNums-1)*(P.TotalTrials/E.Settings.DirectionNums)+1;
                            if TheEyes(eyeLoop)==1
                                set(FigHandLE(theLablesSubplotNO),'XTick',(linspace(0,2e6,6)),'XTickLabel',(linspace(0,2,6))); set(FigHandLE(theLablesSubplotNO),'YTickLabel',round(linspace(1,E.Window.PTBScreenWidth,6)),'XTickLabelRotation',70);
                            end
                            if TheEyes(eyeLoop)==2
                                set(FigHandRE(theLablesSubplotNO),'XTick',(linspace(0,2e6,6)),'XTickLabel',(linspace(0,2,6))); set(FigHandRE(theLablesSubplotNO),'YTickLabel',round(linspace(1,E.Window.PTBScreenWidth,6)),'XTickLabelRotation',70);
                            end
                        end
                        
                        ylim([1 E.Window.PTBScreenWidth]), xlim([0 2e6])
                        %                      ylim([-20 20]), xlim([0 2e6])
                        if  SubpltNo == round((P.TotalTrials/E.Settings.DirectionNums)/2)
                            % title(sprintf('%sE %iAFC Contrast:%2.2f ; Inter:%i Repetition:%i   XData', EyeList(eyeLoop), E.Settings.DirectionNums, E.Settings.BaseContrastLevels(ThisTrialContInd)*100, ThisTrialIntLe , ThisIntLeRept)); shg
                            title(sprintf('%sE %iAFC OKN signal\n', EyeList(TheEyes(eyeLoop)), E.Settings.DirectionNums)); shg
                            xtitleHndl = text( 0.77, -(E.Settings.DirectionNums-1)-0.2 , 'Time (s)','units','normalized');
                            ytitleHndl = text(-round((P.TotalTrials/E.Settings.DirectionNums)/2)+0.7 ,-(E.Settings.DirectionNums/2)+0.5 , 'Eye position (pixels)','units','normalized','Rotation',90);
                            
                        end
                        %pause
                        drawnow
                        set(gcf,'position',[1 41 1280 603]);
                        
                        if E.Settings.ExperimentSet == 1 % just show title in experiment one, to reduce cluttering
                            txt1Str = sprintf('T%i Int%i Rep%i\nGain:%2.2f',TrialLoop, ThisTrialIntLe, ThisIntLeRept, GLMgain);
                            TxtHndl = text(.05 , 0.87 , txt1Str,'units','normalized');
                            uistack(TxtHndl, 'top')
                        end
                        
                    else % not tight subplots
                        figure(33);
                        ThisEyeSubplotNo = eyeLoop;
                        subplot(1,2,ThisEyeSubplotNo);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall(BelowThresh),Xall(BelowThresh),'g.',...
                            Tall,StimPosition,'k-.',Tall,EstOffsetFromGLM_H,'b-.',...
                            'LineWidth', 1.4);
                        legend('Horizontal eye Position', 'Saccade', 'Tracking', '10 deg/s', sprintf('GLM Gain %2.2f',GLMgain), 'Location', 'NE')
                        xlabel('time (micro seconds)'), ylabel('Eye position (pixels)')
                        set(gca,'fontsize',14);
                        axis([Tall(1) Tall(end) 1  E.Window.PTBScreenWidth]),
                        if ismember(TheEyes(eyeLoop),E.Settings.UncoveredEye)
                            OKNtypeStr = 'open';
                        else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
                            OKNtypeStr = 'patched';
                        end
                        title(sprintf('%sE %s %iAFC C:%2.2f M:%i Int:%i Rep:%i', EyeList(eyeLoop), OKNtypeStr, P.NoInterleaved, E.Settings.BaseContrastLevels(ThisTrialContInd)*100, ThisTrialMaskInd, ThisTrialIntLe , ThisIntLeRept)); shg
                        fprintf('Trial: %i; %sE: OKNscore: %i;  Threshold: %2.2f;  gain: %2.2f; OKN Consistency: %2.2f  TotalDisRatio: %2.2f\n',...
                            TrialLoop, EyeList(eyeLoop), OKNscore, Threshold, GLMgain_H, OKNConsistency, TotalDisRatio);
                        if ThisEyeSubplotNo ==2
                        pause;
                        end
                    end
                end
            end
            
            % saving the result
            AllXstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                  = Xall;
            AllYstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                  = Yall;
            AllTstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}                  = Tall;
            AllXSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}               = xDall;
            AllYSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}               = yDall;
            AllTSpdstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}               = tDall;
            AllSpdVecstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}             = Speed;
            AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}           = OKNscore;
            AllOKNConsistencystore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}     = double(OKNConsistency);
            AllKeyRespstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}            = P.Response(TrialLoop);
            %                     AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = HistGain; %SpeedFromHistDegPerSec/S.SpeedDegPerSec;
            Allgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = OKNGain;
            Allgainstore_H{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = OKNGain_H;
            Allgainstore_V{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}              = OKNGain_V;
            
            AllMADgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = MADgain;
            AllMADgainstore_H{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = MADgain_H;
            AllMADgainstore_V{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = MADgain_V;
            
            
            AllGLMgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = GLMgain;
            AllGLMgainstore_H{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = GLMgain_H;
            AllGLMgainstore_GofH{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = gof_H.rsquare;
            AllGLMgainstore_V{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = GLMgain_V;
            AllGLMgainstore_GofV{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = gof_V.rsquare;
            AllTotalDisRatiostore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep} = TotalDisRatio;
        end
    end
end


%% ploting the data
% try
subLoop =1;
conLoop=1;
runLoop =1;
IntLe = 1: P.NoInterleaved;
StimInd = 1: numel(P.PossStimValues{1});
ContInd = 1: numel(P.PossContrastValues{1});
MaskInd  = 1: numel(P.PossMaskValues{1});
IntLeRept = 1: E.Settings.LocalTrialNo;
Xdata = P.PossContrastValues{1}*100;
Cmap = [0.4 0.4 0.4];
if E.Settings.DirectionNums ==2
    DriftingDirections = {'Right', 'Left'};
elseif E.Settings.DirectionNums ==4
    DriftingDirections = {'Right', 'Up', 'Left', 'Down'};
end
for eyeLoop = 1:numel(TheEyes)
    % Drifting directions , contrast levels, repetition AllHgainstore
    OKN_GainAll = (cell2mat(reshape(squeeze({AllGLMgainstore_H{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),P.NoInterleaved,numel(P.PossContrastValues{1}),E.Settings.LocalTrialNo)));
    %    ThresholdsAA = (cell2mat(reshape(squeeze({AllThreshstore{subLoop, conLoop, eyeLoop, runLoop, IntLe, StimInd, ContInd, MaskInd, IntLeRept}}),P.NoInterleaved,numel(P.PossContrastValues{1}),E.Settings.LocalTrialNo)));
    
    % repetition, Drifting directions , contrast levels
    OKN_GainMatrix = permute(OKN_GainAll,[3 1 2]);
    % (Drifting directions*repetition), contrast levels
    OKN_Gain = reshape(OKN_GainMatrix, P.NoInterleaved.*E.Settings.LocalTrialNo,numel(P.PossContrastValues{1}));
    OKN_GainAllmean = nanmean(OKN_GainAll,3); % mean calculated for each drifting direction and contrast level
    OKN_GainAllmedian = (nanmedian(OKN_GainAll,3));
    OKN_GainAllStd  = nanstd (OKN_GainAll,0,3);
    
    for i = 1:P.NoInterleaved
        for j = 1: length(P.PossContrastValues{1})
            if (TheEyes(eyeLoop)==1 && i==1) || (TheEyes(eyeLoop)==2 && i==P.NoInterleaved/2+1)% LE rightward or RE leftward
                fprintf('%sE Nasal Gain : %2.2f � %2.2f\n',EyeList(TheEyes(eyeLoop)), OKN_GainAllmean(i,j),OKN_GainAllStd(i,j));
            elseif (TheEyes(eyeLoop)==1 && i==P.NoInterleaved/2+1) || (TheEyes(eyeLoop)==2 && i==1)% LE leftward or RE rightward
                fprintf('%sE Temporal Gain : %2.2f � %2.2f\n',EyeList(TheEyes(eyeLoop)), OKN_GainAllmean(i,j),OKN_GainAllStd(i,j));
            end
            %                 fprintf('%sE Gain, Contrast %2.1f%% : %2.2f � %2.2f\n',EyeList(TheEyes(eyeLoop)),E.Settings.BaseContrastLevels(j).*100, nanmean(OKN_Gain(:,j)),nanstd(OKN_Gain(:,j)))
            fprintf('%sE Contrast %2.1f%% %sward Gain : %2.2f � %2.2f\n',EyeList(TheEyes(eyeLoop)), E.Settings.BaseContrastLevels(j).*100, DriftingDirections{i}, OKN_GainAllmean(i,j),OKN_GainAllStd(i,j));
            
        end
    end
    
    %     figure(9),
    %     errorbar(1:4,OKN_GainAllmean',OKN_GainAllStd','-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap,'Color',Cmap);
    
    if  E.Settings.ExperimentSet == 1 % direction condition
        figure(10),
        subplot(1,numel(TheEyes),eyeLoop);
        %     errorbar(Xdata,mean(OKN_Gain,1),std(OKN_Gain),'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap,'Color',Cmap);
        errorbar(1:4,OKN_GainAllmedian',OKN_GainAllStd','o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap,'Color',Cmap);
        
        hold on;
        plot(Xdata,OKN_Gain,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap-0.2,'Color',Cmap-0.2);
        if eyeLoop ==1
            ylabel('OKN-Gain')
            xlabel('Drifting Direction (%)')
            legend('Median � SD','Gain of each trial','location','best')
        end
        
        if ismember(TheEyes(eyeLoop),E.Settings.UncoveredEye)
            OKNtypeStr ='Direct Response';
        else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
            OKNtypeStr ='Indirect Response';
        end
        
        title(sprintf('%sE %s',EyeList(TheEyes(eyeLoop)), OKNtypeStr))
        %     axis([min(Xdata)-5 max(Xdata)+5 0.2 1.3])
        axis([0 5 -0.2 1.4])
        xticks(1:4)
        xticklabels({'Right', 'Up', 'Left', 'Down' })
        if E.Settings.ExperimentSet ==2
            set(gca,'xscale','log','box','off');
        end
        set(gcf,'position', [360, 198, 343.*length(TheEyes), 420])
    else %% contrast condition
        figure(11),
        subplot(1,numel(TheEyes),eyeLoop);
        
        OKNgain_Rightward = OKN_Gain(1:E.Settings.LocalTrialNo,:);
        OKNgain_Leftward = OKN_Gain(E.Settings.LocalTrialNo+1: end,:);
        
        OKNgain_Rightward_mean = nanmean(OKN_Gain(1:E.Settings.LocalTrialNo,:));
        OKNgain_Leftward_mean = nanmean(OKN_Gain(E.Settings.LocalTrialNo+1: end,:));
        
        OKNgain_Rightward_median = nanmedian(OKN_Gain(1:E.Settings.LocalTrialNo,:));
        OKNgain_Leftward_median = nanmedian(OKN_Gain(E.Settings.LocalTrialNo+1: end,:));
        
         OKNgain_Rightward_sd = nanstd(OKN_Gain(1:E.Settings.LocalTrialNo,:));
        OKNgain_Leftward_sd = nanstd(OKN_Gain(E.Settings.LocalTrialNo+1: end,:));
        
        OKNgain_median = nanmean([OKNgain_Rightward_median;OKNgain_Leftward_median]);
        OKNgain_mean = nanmean([OKNgain_Rightward_mean;OKNgain_Leftward_mean]);

        OKNgain_sd = rms([OKNgain_Rightward_sd;OKNgain_Leftward_sd]);

        errorbar(Xdata,OKNgain_median,OKNgain_sd,'-o','LineWidth',1.5,'MarkerSize',9,'MarkerFaceColor',Cmap,'Color',Cmap);
        
        hold on;
        plot(Xdata,OKN_Gain,'.','LineWidth',2,'MarkerSize',5,'MarkerFaceColor',Cmap-0.2,'Color',Cmap-0.2);
        if eyeLoop ==1
            ylabel('OKN-Gain')
            xlabel('Contrast (%)')
            legend('Median � SD','Gain of each trial','location','best')
        end
        
        if ismember(TheEyes(eyeLoop),E.Settings.UncoveredEye)
            OKNtypeStr ='Direct Response';
        else % i.e. the indirect responce the recorded eye behind the IR-visible occluder
            OKNtypeStr ='Indirect Response';
        end
        
        title(sprintf('%sE %s',EyeList(TheEyes(eyeLoop)), OKNtypeStr))
        xticks(Xdata)
        if E.Settings.ExperimentSet ==2
            set(gca,'xscale','log','box','off');
        end
        axis([-1 max(Xdata)+5 -0.3 0.9])

        set(gcf,'position', [360, 198, 343.*length(TheEyes), 420])
    end
end

% eyeLoop = [1,2];
% OKN_Gain2 = cell2mat(reshape(squeeze({AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, ThisTrialIntLe, ThisTrialStimInd, ThisTrialContInd, ThisTrialMaskInd, ThisTrialOverallIntLeavedRep}}),2,2,8));
%  set(gcf,'PaperPositionMode','auto')
%  print(figure(10), 'C:\Users\smoh205\Desktop\Temp Figs\AI gain based on GLM model OD', '-dpng','-noui','-r0')


