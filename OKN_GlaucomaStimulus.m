% function [S]=OKN_GlaucomaStimulus(E,P,S)
% tic
ThisJump=S.PixelsPerDegree*(S.SpeedDegPerSec*(1/E.Computer.ScreenHz))* E.Window.LastFrameDelta./S.StimScalingRatio;
S.ThisTrialDir  = pi-DegToRad(S.DirPeak(P.InterList(P.CurrentTrialNo)));    %(S.AllDirs(TrialNo)-1).*(pi/2);

if S.CurrentTrialFrame==1
    S.OffXback=0; S.OffYback=0;
    S.SymbolID=Randi(10);
else
    S.OffXback=mod(S.OffXback+cos(S.ThisTrialDir).*ThisJump-1,S.StimX)+1;
    S.OffYback=mod(S.OffYback+sin(S.ThisTrialDir).*ThisJump-1,S.StimY)+1;
end
% fprintf(1,'%i, %i\n',S.OffXback, S.OffYback);
srcRect=OffsetRect([0 0 S.StimX S.StimY],S.OffXback(1),S.OffYback(1));
Screen('BlendFunction', E.Window.Pointer, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

if E.Settings.MaskOpenLoop
    if strcmpi(E.Eyetracker,'EyeLink') && E.Computer.EyeLinkConnected% EL is setup and wanted
        %          %to get a new EyeLink Sample for mask open loop feedback
        %          if Eyelink('NewFloatSampleAvailable') > 0
        %              evt = Eyelink('NewestFloatSample');
        %              xEyePos = (evt.gx);
        %              yEyePos = (evt.gy);
        %          else
        xEyePos = E.LastEyePos([1 2]);
        yEyePos = E.LastEyePos([3 4]);
        %          end
        xEyePos(xEyePos<0) =[];
        yEyePos(yEyePos<0) =[];
        EyeX(S.CurrentTrialFrame)= double(round(mean(xEyePos)));
        EyeY(S.CurrentTrialFrame)= double(round(mean(yEyePos)));
        
    elseif strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
        
        xEyePos = E.LastEyePos(1).*E.Window.PTBScreenWidth;
        yEyePos = E.LastEyePos(2).*E.Window.PTBScreenHeight;
        
        EyeX(S.CurrentTrialFrame)= double(nanmean(xEyePos));
        EyeY(S.CurrentTrialFrame)= double(nanmean(yEyePos));
    elseif strcmpi(E.Eyetracker,'SimEyelink')
        %         [EyeX(S.CurrentTrialFrame), EyeY(S.CurrentTrialFrame)] = GetMouse(E.Settings.ScreenUsed);
        EyeX(S.CurrentTrialFrame)= E.Window.PTBScreenWidthCenter;
        EyeY(S.CurrentTrialFrame)= E.Window.PTBScreenHeightCenter;
    end
end



if E.Settings.MaskOpenLoop  && EyeX(S.CurrentTrialFrame) >= 0 && EyeY(S.CurrentTrialFrame) >= 0
    maskX = EyeX(S.CurrentTrialFrame);
    maskY = EyeY(S.CurrentTrialFrame);
else
    maskX = E.Window.PTBScreenWidthCenter;
    maskY = E.Window.PTBScreenHeightCenter;
end
%  [maskX,maskY] = GetMouse(E.Settings.ScreenUsed);
% [MouseX(S.CurrentTrialFrame), MouseY(S.CurrentTrialFrame)] = GetMouse(E.Settings.ScreenUsed);
%  maskX = MouseX(S.CurrentTrialFrame);
%  maskY = MouseY(S.CurrentTrialFrame);

S.MaskDestRect = CenterRectOnPoint([0 0 E.Window.PTBScreenWidth*S.MaskSizeMltpl E.Window.PTBScreenHeight*S.MaskSizeMltpl],maskX,maskY);
% full-screen noise
% S.MaskSrcRect=OffsetRect(CenterRect([0 0 E.Window.PTBScreenWidth E.Window.PTBScreenHeight],[0 0 E.Window.PTBScreenWidth*3 E.Window.PTBScreenHeight*3]),maskX,maskY);
Screen('DrawTexture', E.Window.Pointer,  S.StimTex(P.StimInds(P.CurrentTrialNo),P.ContrastInds(P.CurrentTrialNo),S.WhichSamp(P.CurrentTrialNo))  , srcRect, S.DestRect); % draw stimulus
Screen('DrawTexture', E.Window.Pointer,  S.masktex(P.MaskInds(P.CurrentTrialNo),S.WhichMaskSamp(P.CurrentTrialNo) ), [], S.MaskDestRect);   % S.DestRect % draw the mask over stimulus

if strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
    if length(E.WhichEye)>1
        if S.CurrentTrialFrame>1
            Screen('TextSize',E.Window.Pointer, 10);
            DrawFormattedText(E.Window.Pointer, 'HP', 50, E.Window.PTBScreenRect(4)-50, E.HeadPositionFlagCol);%
            %         DrawFormattedText(E.Window.Pointer, 'HP', 'center', 'center', E.HeadPositionFlagCol);%
            DrawFormattedText(E.Window.Pointer, sprintf('\n\n\nLE%3.0fcm RE%3.0fcm',round(LE_DistCm),round(RE_DistCm)),50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);

        else
            Screen('TextSize',E.Window.Pointer, 10);
            DrawFormattedText(E.Window.Pointer, 'HP', 50, E.Window.PTBScreenRect(4)-50, [0 1 0]);%E.Window.PTBScreenRect(4)-50
            DrawFormattedText(E.Window.Pointer, sprintf('\n\n\nLE%3.0fcm RE%3.0fcm',round(LE_DistCm),round(RE_DistCm)),50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);
        end
    end
end
TrialNoStr = sprintf('\nTrial %i / %i',P.CurrentTrialNo, P.TotalTrials);
Screen('TextSize',E.Window.Pointer, 10);
DrawFormattedText(E.Window.Pointer, TrialNoStr, 50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);

% % cover the beggining jittering frames
if 0%S.CurrentTrialFrame < E.Settings.CoveredFrames
    Screen('DrawTexture', E.Window.Pointer, S.CoverTexture,[],  S.DestRect)
    %     bvlDrawCrosshair(E.Window.Pointer, E.Window.PTBScreenWidth/2, E.Window.PTBScreenHeight/2 ,32 , [255 0 0], 5);
end