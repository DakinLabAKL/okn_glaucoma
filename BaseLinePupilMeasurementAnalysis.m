% BaseLinePupilMeasurementAnalysis not ready yet

goodVals = find(BaselinePupilTimeInMS>0 & BaselinePupilAreaIn>0);
PupilTimeInMS0=BaselinePupilTimeInMS(goodVals);
PupilTimeInMS = PupilTimeInMS0 - PupilTimeInMS0(1);
PupilArea = BaselinePupilAreaIn(goodVals);
figure(11),hold on, plot(PupilTimeInMS,PupilArea,'r-' )
stat(PupilArea);

% PupilAreaMM = sqrt(PupilArea/pi)/5.5;
% % pre process the input data
% if isfield(E.BaseEyePos,'OverSample') % i.e. we have first samples indices
%     EachFrameSamples = (squeeze(E.BaseEyePos.OverSample(BaseTrialLoop, :,:)))'; % keep the frame flip time for later processing
%     NoSampsPerFrame = sum(EachFrameSamples>0);
%     EachFrameSamplesGT0 = (EachFrameSamples(EachFrameSamples>0))'; % length(EachFrameSamples)== length(xData0)? : yes
%     FSInds  = find(EachFrameSamplesGT0==1); % first sample indices
% end
% 
% if JustFirstBaseSample
%     SampMargin = 5; % the margin before and after blinks, means 5 frames.
%     firstSeen = (E.Settings.CoveredFrames+1); % the first frame after covered frame
%  
% else % i.e we want Full samples
%     SampMargin = 15; % the margin before and after blinks, means 5 frames.
%   
% end
% 
% figure(11), plot(PupilTimeInMS,PupilArea,'ro' )
EachGrayLevelLengthFrames = E.Settings.MaxFramesPerBaseTrial / E.Settings.MaxGrayLevelStepsPerBaseTrial;
ThisBaseTrialOrderOfFramesEnd = linspace(EachGrayLevelLengthFrames,E.Settings.MaxFramesPerBaseTrial,E.Settings.MaxGrayLevelStepsPerBaseTrial);
ThisBaseTrialOrderOfFramesStart = ThisBaseTrialOrderOfFramesEnd - (EachGrayLevelLengthFrames-1);

BaselienMaxPupilAreaMean(BaseTrialLoop) = nanmean(PupilArea(ThisBaseTrialOrderOfFramesStart(1):ThisBaseTrialOrderOfFramesEnd(1)));