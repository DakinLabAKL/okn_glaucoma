% OKNmocsTobii
clear all
PsychDefaultSetup(2);
%% use tobii feedback to check retinal adaptation
clearvars;
close all;
screenUsed = 0;
Screen('Preference', 'SkipSyncTests', 1) % currently need this for new Dell - check this as it might be an issue we can resolve
[DisplayWidthPix, DisplayHeightPix] = Screen('WindowSize', screenUsed); % width and height of display in mm
PsychImaging('PrepareConfiguration');
PsychImaging('AddTask', 'FinalFormatting', 'DisplayColorCorrection', 'SimpleGamma');
[win, winRect]      = PsychImaging('OpenWindow', screenUsed,   0.5, [],[],[],0);   %screenid, 0.5, [], [], [], P.stereoMode);
HideCursor;
commandwindow;
ScalingRatio = 2;
ImageWidth = round(DisplayWidthPix/ScalingRatio);
ImageHeight = round(DisplayHeightPix/ScalingRatio);
BaseRect=[0 0 ImageWidth ImageHeight];

DisplayHeightDeg = atand(53/67);
StimFreqCPD = 0.1; % 1/38;
StimFreqCPI = StimFreqCPD.* DisplayHeightDeg;
offX=0;
offY=0;
JumpSizeX = 45;
JumpSizeY = 0;

lambdaPix       = DisplayHeightPix./StimFreqCPD;  %S.FreqPeak
Smoothing       = 1.*lambdaPix; %
srcNoise        = randn(ImageHeight,ImageWidth);
StimIm          = 0.5+(NormImage((real(DoLogGabor(srcNoise,StimFreqCPI,0.5,0,10000))),0,0.5));% DegToRad(2)
image           = repmat(StimIm,[2, 2]);
Sourcerect      = [0 0  ImageWidth ImageHeight];
Tobii.ET        = EyeTrackingOperations().find_all_eyetrackers;
license_fname   = sprintf('*University_of_Auckland_%s',Tobii.ET.SerialNumber);
Tobii.licenses_path = dir(fullfile(FindGitFolder,'psychsandbox','TobiiLicences',license_fname));
fileID              = fopen(Tobii.licenses_path(1).name,'r');
input_licenses(1)   = LicenseKey(fread(fileID));
fclose(fileID);
failed_licenses     = Tobii.ET.apply_licenses(input_licenses); % how long does this persist?
CentWin=255.*(1-PadIm(MakeCosineWindow(DisplayHeightPix,DisplayHeightPix/2.1,DisplayHeightPix/10),[ DisplayHeightPix DisplayWidthPix],0)); 
TheWin=repmat(CentWin,[1 1 2]); TheWin(:,:,1)=TheWin(:,:,2).*0+0.5;
TheWinBack=repmat(255-CentWin,[1 1 2]); TheWin(:,:,1)=TheWin(:,:,2).*0+0.5;
        MaskText          = Screen('MakeTexture', win, TheWin, [], [], 2);
        MaskTextBack          = Screen('MakeTexture', win, TheWinBack, [], [], 2);

cntr=1; % number of flips counter
LastFlipTime(1) = Screen('Flip' , win);

nAFC            = 10;
baseRandState   = rng('shuffle');
rng(baseRandState);
tActual         = [0.5];
tGuess          = [0.5];
tGuessSd        = [0.5];
trialPerInter   = 4;
noInterleaved   = length(tActual);
theInterList    = Shuffle(All(repmat([1:noInterleaved],[1 trialPerInter])));

pThreshold      = 0.501; % desiredlevel of performance
beta            = 3.5;  % slope of psychometric fn
delta           = 0.01; % lapse rate
gamma           = 0.5;  % %correct for guessing

TotalTrials     = trialPerInter*noInterleaved;
for i=1:noInterleaved
    q(i)                = QuestCreate(tGuess(i),tGuessSd(i),pThreshold,beta,delta,gamma);    % Provide our prior knowledge to QuestCreate, and receive the data struct "q".
    qSim(i)             = QuestCreate(tGuess(i),tGuessSd(i),pThreshold,beta,delta,1/nAFC);    % Provide our prior knowledge to QuestCreate, and receive the data struct "q".
    q(i).normalizePdf   = 1;
    qSim(i).normalizePdf   = 1;
end
stimSign = 1-0.*(rand(1,TotalTrials)>0.5);
try
    TobiixEyePos(cntr) = [0];
    TobiiyEyePos(cntr) = [0];
    cntr=2;
    Screen('Flip', win);
    k=1;
    KeepDoingTrials=1;
    TobiCnt=zeros(1,TotalTrials);
    while KeepDoingTrials
        keepgoing =1;
        frameNo=1;
        contr(k)=1;%0.025+0.1*rand;
        imtext          = Screen('MakeTexture', win, 0.5+contr(k).*(image-0.5), [], [], 2);
        randState(k)    = rng; % set random seed
        rng(randState(k));
        theInter        = theInterList(k); % select the interval
        tTest           = QuestQuantile(q(theInter));	% Recommended by Pelli (1987), and still our favorite.
        response        = QuestSimulate(qSim(theInter),tTest,tActual(theInter));
        fprintf('Int %d Trial %3d at %5.2f is %d\n',theInter,k,tTest,char((response+1)));
        q(theInter)     = QuestUpdate(q(theInter),tTest,response); % Add the new datum (actual test intensity and observer response) to the database.
        Screen('BlendFunction', win, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        while keepgoing
             [ keyIsDoqwn, seconds, keyCode ] = KbCheck(-1);
            xEyePos=[0];  yEyePos=[0];
            TobiiSample = Tobii.ET.get_gaze_data('flat');
            if ~isempty(TobiiSample.left_gaze_point_on_display_area)
                NoTobiiSampRead=size(TobiiSample.left_gaze_point_on_display_area,1);
                TobiixEyePos(k,TobiCnt(k)+[1:NoTobiiSampRead])=(TobiiSample.left_gaze_point_on_display_area(:,1)).*DisplayWidthPix;
                TobiiyEyePos(k,TobiCnt(k)+[1:NoTobiiSampRead])=(TobiiSample.left_gaze_point_on_display_area(:,2)).*DisplayHeightPix;
 TobiiPupilSz(k,TobiCnt(k)+[1:NoTobiiSampRead])=(TobiiSample.left_pupil_diameter(:));
                
                TobiCnt(k)=TobiCnt(k)+NoTobiiSampRead;
            end
          
            offX = mod((offX+stimSign(k)*JumpSizeX)-1,ImageWidth)+1;
            offY = mod((offY+JumpSizeY)-1,ImageHeight)+1; 
            offX2=0; offY2=0; 
            srcRect = OffsetRect(BaseRect, offX, offY);
            srcRectBack = OffsetRect(BaseRect, offX2, offY2);
            cntr = cntr+1;
            DestRect = CenterRectOnPoint([0 0 DisplayWidthPix DisplayHeightPix], DisplayWidthPix/2, DisplayHeightPix/2);
                   Screen('DrawTexture', win,imtext,srcRect ,DestRect,0);   % S.DestRect % draw the mask over stimulus
              Screen('DrawTexture', win, MaskText,[], [],0,[],0.5); % draw overlay window
  Screen('DrawTexture', win,imtext,srcRectBack ,DestRect,0);   % S.DestRect % draw the mask over stimulus
              Screen('DrawTexture', win, MaskTextBack,[], [],0,[],0.5); % draw overlay window
             PsychColorCorrection('SetEncodingGamma', win, 0.5 );
            LastFlipTime(cntr) = Screen('Flip',win);%     KbReleaseWait;
            if keyCode(KbName('ESCAPE'))|(frameNo>90)
                keepgoing =0;
            end
            frameNo=frameNo+1;
        end
        KeepDoingTrials = (k<TotalTrials);
        k=k+1;
        Screen('Close',imtext);
        pause(0.1);
    end
    KbWait;
    sca
catch ME
    sca
    rethrow(ME)
end
%% tming data
d1=1./diff(LastFlipTime);
d1(d1<0)=0;
figure(1),clf;plot(1:length(d1),d1,'b.-',find(d1<20),d1(d1<20),'ro');
title('Effective frame rate')
xlabel('frames');ylabel('Frame Rate Hz')
axis([0 length(d1) 0 max(d1)])
legend('all frames','dropped frames')
DropedFrames = find(d1<20);
theDiff = diff(DropedFrames);
mode(theDiff(theDiff>2))
%% plot eye position
TobiixEyePosL = squeeze(TobiixEyePos);
TobiiyEyePosL = squeeze(TobiiyEyePos);
figure(10)
cols=linspecer(TotalTrials);
for i=1:TotalTrials
    x1 = squeeze(TobiixEyePos(i,1:TobiCnt(i)));
    y1 = squeeze(TobiiyEyePos(i,1:TobiCnt(i)));
    p1 = squeeze(TobiiPupilSz(i,1:TobiCnt(i)));
    x1 = x1-nanmean(x1);
    t1=floor([0:TobiCnt(i)-1].*(1000/90));
  %  [ Xall, Yall, Pall, Tall, xDall, yDall, Speed, SpeedAng, Threshold, PursuitOrSaccade ] = OKNprocessing(x1, y1, p1, t1, 1, 50);
    
    
    x1=x1+1000*(i-1);
    xPosPlot{i}=x1;
    plot([1:TobiCnt(i)],x1,'k-','Color',cols(i,:));
    if i==1
        hold on
    end
end
hold off
%%

%
%
% for i=1:noInterleaved
%     t(i)    = QuestMean(q(i));% Ask Quest for the final estimate of threshold.
%     sd(i)   = QuestSd(q(i));
%     fprintf('Int %d Final threshold estimate (mean�sd) is %.2f � %.2f\n',i,t(i),sd(i));
%     subplot(1,noInterleaved,i)
%     plot([1:trialPerInter],q(i).intensity(1:trialPerInter),'o-',[1 trialPerInter],[t(i) t(i)],'k--')
% end
% subname='test';
% fName=sprintf('%s_%d_%s_Data.mat',subname,nAFC,MyDate)
% save(fName,'theInterList','trialPerInter','q')
