%  function OKN_GlaucomaMain
% Before runing please make sure that you have checkd out into the soheil branch of PSB
% Name of each experimend Initials_VFL_L/R#Runs_Ring/Circ Masks

% Name of each experiment:
% Initials_VF_M1_R#Runs   where REF is refractive error

clearvars;

E.AddTask{1}='PsychImaging(''AddTask'', ''FinalFormatting'', ''DisplayColorCorrection'', ''SimpleGamma'')';
% E.Window.PTBbackground = 0; % to override gray BG
eyeList = {1, 2, [1,2]};
eyeListStr = {'OS', 'OD', 'OU'};
UncoveredEye   = cell2mat(eyeList(centmenu('Which eye is open?', eyeListStr))); % 1=LE and 2=RE
if length(UncoveredEye)<2
MonocEyeStr = {sprintf('%s',eyeListStr{UncoveredEye}), 'OU'};
MonocEyeList = {UncoveredEye, [1,2]};
WhichEye = cell2mat(MonocEyeList(centmenu('Which eye is recorded?', MonocEyeStr))); % 2 is right eye
else
WhichEye = [1,2];
end
ExperimentSet = centmenu('Which experimental settings?', {'Direction', 'Contrast', 'DEMO', 'Simulated Central Scotoma'});

E = PsychSandboxInitialiseEquipment(E); % initialise equipment
E.WhichEye = WhichEye; % which eye is recorded 1=LE 2= RE (1,2)=OU
E.Settings.UncoveredEye =  UncoveredEye; %which eyederives OKN 1=LE 2= RE (1,2)=OU
E.Settings.ExperimentSet = ExperimentSet;
% E.Settings.ExperimentSet = 2; % 1 = pupil ,easurement and VFL simulation 2 = 100 and 50 percent field and various contrast contrast levels
switch E.Settings.ExperimentSet
    case 1 % i.e pupil measurement and VFL study
        %         delete textures.mat
        E.Settings.TestDistmm        = 700; % 1000;   % test distance in mm. distance between eye and LCD
        E.Settings.DirectionNums     = 4;      % number of test directions 8,4 or 2. Start from 2AFC -->4AFC -->8AFC (turn on NumLOCK for 8AFC)
        E.Settings.MaskNo            = 1; % 9;     % number of different mask sizes
        E.Settings.MaskType          = 3;     % 1 = complete circular mask    2 = ring shape circular mask (showing the stim on the doughnut area) 3 = square mask 4 = noise mask foveal visible 5 =  noise patch with occluded foveal region
        E.Settings.StimType          = 1;     % 1 = Gray scale noise,   2 = black and white noise,  3 = checkerboard,
        E.Settings.SimPSF            = 0;     % 0 = No PSF Simulation, 1= Perform PSf Simulation
        % E.Settings.EyeTrackFeedBack= 0;   % 1 = get the gaze position feed back to show the mask using that. i.e. showing the stimulus where the eye aims. 0 = stimulus on the center of screen
        E.Settings.GetKbresponse     = 0;   % 0 = do not record key responses and 1= record them
        E.Settings.MaskOpenLoop      = 0;
        S.FovRegDiameter             = 5; %2.5;   % i.e visual angle diameter (deg) of the always visible circle at the center
        E.Settings.StimulusDirection = rad2deg(0:pi/(E.Settings.DirectionNums /2):(2*pi-pi/(E.Settings.DirectionNums /2))); % [ 0    45   90   135  180  225   270  315];
        % E.Settings.BaseStimLevels  = 10.^[ 0.3 0.5 0.7 0.9 1.1 1.3];  % 0.0 is 60cpd (pr limit), 0.3 = 30 cycles per degree (0 logMAR) [4 40];%
        % E.Settings.BaseStimLevels    = (10.^(1.277))*2; % 1.277 @ 1m is 1 (0.9955) LogMAR on a monitor with 2160 pixle height
        E.Settings.StimFreqCPDs      =  1.3241; % Stimulus frequency cycle per degree; Dell S2817Q @ 1m is  18.8807 x  31.8816 degrees
        E.Settings.NoiseMaskFreqCPDs =  0.088;%0.2648; % Stimulus frequency cycle per degree;
        % E.Settings.TotalTrialPerCon = 72;
        E.Settings.BaseContrastLevels = 1; % 2.^[-6:0];%[0. 0.0625 0.125 0.25 0.5 1.00];%[0.0156  0.0449   0.1250   1.0000]; % inf (1/512).*round(2.^[3:1.5:10])   %0.125; % 0.35; % 10.^ (linspace(-2,0,5)); % from lowest contrast to hieghest contrast
        E.Settings.LocalTrialNo       = 8; %4;
        % E.Settings.LocalTrialNo = E.Settings.TotalTrialPerCon/(E.Settings.DirectionNums*length(E.Settings.BaseStimLevels).*E.Settings.MaskNo.*length(E.Settings.BaseContrastLevels) );   % number of trials per each stimulus (different for 2, 4and 8AFC)
        
        
        
        E.Settings.ifPAbaseMeasure = 0;  %  0 = donot measure pupil at the begining 1 = measure pupil at the begining
        E.Settings.EachStepPAbaseMeasureDurSec   = 2; % seconds for measuring pupil baseline value using a full black screen
        E.Settings.NoGrayLevelStepsRampPAbaseMeasure = 5; % five gray level measurement in each ramp for baseline pupil measurement
        % E.Settings.NoPAbaseMeasurements  = 2;
        E.Settings.BaseTotalTrialNo = 4; % i.e the number of ramp pupil measurement repetition
        
        E.Window.MaskBackground = E.Window.Gray;%  E.Window.PTBbackground; % Background color of mask
        E.Settings.MaskBaseFirstBoundary  = 1;  % the minimum ratio of visible carrier base (without visible foveal region)
        E.Settings.MaskBaseSecondBoundary = 1; % the maximum ratio of visible carrier base
        E.Settings.HalfWayFdBk = 1; % give visual feed back half way and quarter to end
    case 2 % i.e. 100% and 50% Field and various contrast contrast levels
        %         delete textures.mat
        E.Settings.TestDistmm        = 700;   % test distance in mm. distance between eye and LCD
        E.Settings.DirectionNums     = 2;      % number of test directions 8,4 or 2. Start from 2AFC -->4AFC -->8AFC (turn on NumLOCK for 8AFC)
        E.Settings.MaskNo            = 1;     % number of different mask sizes
        E.Settings.MaskType          = 3;     % 1 = complete circular mask    2 = ring shape circular mask (showing the stim on the doughnut area) 3 = square mask 4 = noise mask foveal visible 5 =  noise patch with occluded foveal region
        E.Settings.StimType          = 1;     % 1 = Gray scale noise,   2 = black and white noise,  3 = checkerboard,
        E.Settings.SimPSF            = 0;     % 0 = No PSF Simulation, 1= Perform PSf Simulation
        % E.Settings.EyeTrackFeedBack= 0;   % 1 = get the gaze position feed back to show the mask using that. i.e. showing the stimulus where the eye aims. 0 = stimulus on the center of screen
        E.Settings.GetKbresponse     = 0;   % 0 = do not record key responses and 1= record them
        E.Settings.MaskOpenLoop      = 0;
        S.FovRegDiameter             = 5; %2.5;   % i.e visual angle diameter (deg) of the always visible circle at the center
        E.Settings.StimulusDirection = rad2deg(0:pi/(E.Settings.DirectionNums /2):(2*pi-pi/(E.Settings.DirectionNums /2))); % [ 0    45   90   135  180  225   270  315];
        % E.Settings.BaseStimLevels  = 10.^[ 0.3 0.5 0.7 0.9 1.1 1.3];  % 0.0 is 60cpd (pr limit), 0.3 = 30 cycles per degree (0 logMAR) [4 40];%
        % E.Settings.BaseStimLevels    = (10.^(1.277))*2; % 1.277 @ 1m is 1 (0.9955) LogMAR on a monitor with 2160 pixle height
        E.Settings.StimFreqCPDs      = 1.3241; % Stimulus frequency cycle per degree; Dell S2817Q @ 1m is  18.8807 x  31.8816 degrees
        E.Settings.NoiseMaskFreqCPDs = 0.088; %0.2648; % Stimulus frequency cycle per degree;
        % E.Settings.TotalTrialPerCon = 72;
        E.Settings.BaseContrastLevels = 2.^[-4:1:-2,0];% phase-I study used 2.^[-5:-1];%[0. 0.0625 0.125 0.25 0.5 1.00];%[0.0156  0.0449   0.1250   1.0000]; % inf (1/512).*round(2.^[3:1.5:10])   %0.125; % 0.35; % 10.^ (linspace(-2,0,5)); % from lowest contrast to hieghest contrast
        E.Settings.LocalTrialNo       = 8; %4;
        
        E.Settings.ifPAbaseMeasure = 0; % 0l= donot measure pupil at the begining 1 = measure pupil at the begining
        E.Settings.EachStepPAbaseMeasureDurSec   = 2; % seconds for measuring pupil baseline value using a full black screen
        E.Settings.NoGrayLevelStepsRampPAbaseMeasure = 5; % five gray level measurement in each ramp for baseline pupil measurement
        % E.Settings.NoPAbaseMeasurements  = 2;
        E.Settings.BaseTotalTrialNo = 0; % i.e the number of ramp pupil measurement repetition
        
        
        E.Window.MaskBackground = E.Window.Gray;%PTBbackground; % Background color of mask
        E.Settings.MaskBaseFirstBoundary = 0.5;  % the minimum ratio of visible mask base (without visible foveal region)
        E.Settings.MaskBaseSecondBoundary = 1; % the maximum ratio of visible mask base
        E.Settings.HalfWayFdBk = 1; % give visual feed back half way and quarter to end
        
    case 3 % i.e Demo
        %         delete textures.mat
        E.Settings.TestDistmm        = 700;   % test distance in mm. distance between eye and LCD
        E.Settings.DirectionNums     = 4;      % number of test directions 8,4 or 2. Start from 2AFC -->4AFC -->8AFC (turn on NumLOCK for 8AFC)
        E.Settings.MaskNo            = 1;     % number of different mask sizes
        E.Settings.MaskType          = 3;     % 1 = complete circular mask    2 = ring shape circular mask (showing the stim on the doughnut area) 3 = square mask 4 = noise mask foveal visible 5 =  noise patch with occluded foveal region; 6 = Noise mask with noise background
        E.Settings.StimType          = 1;     % 1 = Gray scale noise,   2 = black and white noise,  3 = checkerboard,
        E.Settings.SimPSF            = 0;     % 0 = No PSF Simulation, 1= Perform PSf Simulation
        % E.Settings.EyeTrackFeedBack= 0;     % 1 = get the gaze position feed back to show the mask using that. i.e. showing the stimulus where the eye aims. 0 = stimulus on the center of screen
        E.Settings.GetKbresponse     = 0;   % 0 = do not record key responses and 1= record them
        E.Settings.MaskOpenLoop      = 0;
        S.FovRegDiameter             = 5; %2.5;   % i.e visual angle diameter (deg) of the always visible circle at the center
        E.Settings.StimulusDirection = rad2deg(0:pi/(E.Settings.DirectionNums /2):(2*pi-pi/(E.Settings.DirectionNums /2))); % [ 0    45   90   135  180  225   270  315];
        % E.Settings.BaseStimLevels  = 10.^[ 0.3 0.5 0.7 0.9 1.1 1.3];  % 0.0 is 60cpd (pr limit), 0.3 = 30 cycles per degree (0 logMAR) [4 40];%
        % E.Settings.BaseStimLevels    = (10.^(1.277))*2; % 1.277 @ 1m is 1 (0.9955) LogMAR on a monitor with 2160 pixle height
        E.Settings.StimFreqCPDs      = 1.3241; % Stimulus frequency cycle per degree; Dell S2817Q @ 1m is  18.8807 x  31.8816 degrees
        E.Settings.NoiseMaskFreqCPDs = 0.088; % Stimulus frequency cycle per degree;
        % E.Settings.TotalTrialPerCon = 72;
        E.Settings.BaseContrastLevels = 2.^[-4:1:-2,0]; % 2.^[-6:0];%[0. 0.0625 0.125 0.25 0.5 1.00];%[0.0156  0.0449   0.1250   1.0000]; % inf (1/512).*round(2.^[3:1.5:10])   %0.125; % 0.35; % 10.^ (linspace(-2,0,5)); % from lowest contrast to hieghest contrast
        E.Settings.LocalTrialNo       = 2; %4;
        % E.Settings.LocalTrialNo = E.Settings.TotalTrialPerCon/(E.Settings.DirectionNums*length(E.Settings.BaseStimLevels).*E.Settings.MaskNo.*length(E.Settings.BaseContrastLevels) );   % number of trials per each stimulus (different for 2, 4and 8AFC)
        
        E.Settings.ifPAbaseMeasure = 0; % 0= donot measure pupil at the begining 1 = measure pupil at the begining
        E.Settings.EachStepPAbaseMeasureDurSec   = 2; % seconds for measuring pupil baseline value using a full black screen
        E.Settings.NoGrayLevelStepsRampPAbaseMeasure = 5; % five gray level measurement in each ramp for baseline pupil measurement
        % E.Settings.NoPAbaseMeasurements  = 2;
        E.Settings.BaseTotalTrialNo = 2; % i.e the number of ramp pupil measurement repetition
        
        E.Window.MaskBackground = E.Window.Gray; % Background color of mask
        E.Settings.MaskBaseFirstBoundary  = 0;  % the minimum ratio of visible mask base (without visible foveal region)
        E.Settings.MaskBaseSecondBoundary = 1; % the maximum ratio of visible mask base
        E.Settings.HalfWayFdBk = 1; % give visual feed back half way and quarter to end
          case 4 % i.e simulated central scotoma, not target at the center
        %         delete textures.mat
        E.Settings.TestDistmm        = 700; % 1000;   % test distance in mm. distance between eye and LCD
        E.Settings.DirectionNums     = 2;      % number of test directions 8,4 or 2. Start from 2AFC -->4AFC -->8AFC (turn on NumLOCK for 8AFC)
        E.Settings.MaskNo            = 1; % 9;     % number of different mask sizes
        E.Settings.MaskType          = 5;     % 1 = complete circular mask    2 = ring shape circular mask (showing the stim on the doughnut area)  3 =square mask  4 =noise mask foveal visible 5 =noise patch with occluded foveal region
        E.Settings.StimType          = 1;     % 1 = Gray scale noise,   2 = black and white noise,  3 = checkerboard,
        E.Settings.SimPSF            = 0;     % 0 = No PSF Simulation, 1= Perform PSf Simulation
        % E.Settings.EyeTrackFeedBack= 0;   % 1 = get the gaze position feed back to show the mask using that. i.e. showing the stimulus where the eye aims. 0 = stimulus on the center of screen
        E.Settings.GetKbresponse     = 0;   % 0 = do not record key responses and 1= record them
        E.Settings.MaskOpenLoop      = 1;
        S.FovRegDiameter             = 5; %2.5;   % i.e visual angle diameter (deg) of the always visible circle at the center
        E.Settings.StimulusDirection = rad2deg(0:pi/(E.Settings.DirectionNums /2):(2*pi-pi/(E.Settings.DirectionNums /2))); % [ 0    45   90   135  180  225   270  315];
        % E.Settings.BaseStimLevels  = 10.^[ 0.3 0.5 0.7 0.9 1.1 1.3];  % 0.0 is 60cpd (pr limit), 0.3 = 30 cycles per degree (0 logMAR) [4 40];%
        % E.Settings.BaseStimLevels    = (10.^(1.277))*2; % 1.277 @ 1m is 1 (0.9955) LogMAR on a monitor with 2160 pixle height
        E.Settings.StimFreqCPDs      =  1.3241; % Stimulus frequency cycle per degree; Dell S2817Q @ 1m is  18.8807 x  31.8816 degrees
        E.Settings.NoiseMaskFreqCPDs =  0.088;%0.2648; % Stimulus frequency cycle per degree;
        % E.Settings.TotalTrialPerCon = 72;
        E.Settings.BaseContrastLevels = 1; % 2.^[-6:0];%[0. 0.0625 0.125 0.25 0.5 1.00];%[0.0156  0.0449   0.1250   1.0000]; % inf (1/512).*round(2.^[3:1.5:10])   %0.125; % 0.35; % 10.^ (linspace(-2,0,5)); % from lowest contrast to hieghest contrast
        E.Settings.LocalTrialNo       = 8; %4;
        % E.Settings.LocalTrialNo = E.Settings.TotalTrialPerCon/(E.Settings.DirectionNums*length(E.Settings.BaseStimLevels).*E.Settings.MaskNo.*length(E.Settings.BaseContrastLevels) );   % number of trials per each stimulus (different for 2, 4and 8AFC)
        
        
        E.Settings.ifPAbaseMeasure = 0;  %  0 = donot measure pupil at the begining 1 = measure pupil at the begining
        E.Settings.EachStepPAbaseMeasureDurSec   = 2; % seconds for measuring pupil baseline value using a full black screen
        E.Settings.NoGrayLevelStepsRampPAbaseMeasure = 5; % five gray level measurement in each ramp for baseline pupil measurement
        % E.Settings.NoPAbaseMeasurements  = 2;
        E.Settings.BaseTotalTrialNo = 4; % i.e the number of ramp pupil measurement repetition
        
        E.Window.MaskBackground = E.Window.Gray;%  E.Window.PTBbackground; % Background color of mask
        E.Settings.MaskBaseFirstBoundary  = 1;  % the minimum ratio of visible carrier base (without visible foveal region)
        E.Settings.MaskBaseSecondBoundary = 1; % the maximum ratio of visible carrier base
        E.Settings.HalfWayFdBk = 1; % give visual feed back half way and quarter to end
end


if E.Settings.SimPSF
    ZernikeCoefInit % define or edit Zernike coefficients
end
HideCursor;

E.Settings.TotalTrialPerCon = length(E.Settings.BaseContrastLevels).* E.Settings.DirectionNums .* length(E.Settings.StimFreqCPDs).*E.Settings.MaskNo .* E.Settings.LocalTrialNo;

FixedTrialsPerInter = numel(E.Settings.BaseContrastLevels ).*numel(E.Settings.StimFreqCPDs ).*E.Settings.LocalTrialNo.*E.Settings.MaskNo;
P  = SetFieldIfEmpty(struct,{'ThreshEstProcedure' 'NoAFC' 'NoInterleaved'               'NoFixedTrialsPerInter'                            'ThreshUnits' },...
    {   'MOCS'               1    E.Settings.DirectionNums    repmat(FixedTrialsPerInter ,1,E.Settings.DirectionNums)   'LOGMAR' });
P.StimulusDirection = (E.Settings.StimulusDirection);
% P.StimulusDirection = P.StimulusDirection(:)';

if     E.Settings.DirectionNums == 2
    P.Responses     =  {'6'  '4'}; % upper({'RightArrow' 'LeftArrow'});
elseif E.Settings.DirectionNums == 4
    P.Responses     = {'6'  '8'   '4'   '2'  }; %upper({'RightArrow' 'UpArrow' 'LeftArrow' 'DownArrow'});
else  %i.e. 8
    P.Responses     =  {'6'  '9'   '8'  '7'  '4'  '1'   '2'  '3' }; % use keypad numlock on  i.e. {'6':Right  '9':UpperRight   '8':Up  '7': UpLeft  '4':Left  '1':DownLeft  '2':Down  '3':DownRight };
end
%     P.Responses     = (repmat(P.Responses,[E.Settings.MaskNo 1]));
%     P.Responses     =  P.Responses(:)';
P.RepeatKey={'r'};
P.PauseKey ={'space'}; % for pause/resume

% the first mask is the largest portion of visible stimulus and the last mask
% is the lowest portion of the visible stimulus
E.Settings.BaseMaskLevels    = E.Window.PTBScreenHeight.*(0.5.^(E.Settings.MaskNo:-1:1)); % 1:E.Settings.MaskNo)); % the radius of circular mask to show the stimulus through
P.PossStimValues             = repmat({E.Settings.StimFreqCPDs}, [ 1 E.Settings.DirectionNums]);
P.PossMaskValues             = repmat({E.Settings.BaseMaskLevels}, [ 1 E.Settings.DirectionNums]);
P.PossContrastValues         = repmat({E.Settings.BaseContrastLevels}, [ 1 E.Settings.DirectionNums]);

P        = PsychSandboxInitialisePsychophysics(P); % no argument = make from scratch
%[E,P,S]  = OKN_GlaucomaInit(E,P,S);     % load the initial settings and textures
OKN_GlaucomaInit;     % load the initial settings and textures

E.Window.GamVal          = 0.4892; % calibrated for S2817Q under game mode, (75% brightness and 75% contrast)
PsychColorCorrection('SetEncodingGamma', E.Window.Pointer, E.Window.GamVal );

if E.Settings.ifPAbaseMeasure
    Screen('FillRect', E.Window.Pointer, E.Window.Black, E.Window.PTBScreenRect); % Black for VFL Pupil Baseline Measurement
end
[E,P,S]      = PsychSandboxUpdate(E,P,S);   % Now that S is known
% initialize the tobii gazedata UCS
E.EyePos.LE_Gazeorigin = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,3, 'single'); % 3 because of x,y,z
E.EyePos.RE_Gazeorigin = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,3, 'single');
E.GoodHeadPositionFlags = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial, 'single');
E.EyePos.LE_DistCm      = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,3, 'single'); % 3 because of x,y,z
E.EyePos.RE_DistCm      = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,3, 'single'); % 3 because of x,y,z
P.InterList  = repmat(P.theInterList(1,:),[1 P.MaxTrialsPerInter(1)]); % sort(P.InterList)


[bInters, bMaskInd, bRepeat, bCon]=ndgrid((1:P.NoInterleaved),(1:E.Settings.MaskNo),(1:E.Settings.LocalTrialNo),(1:length(P.PossContrastValues{1})));
bInters=All(bInters);
bMaskInd=All(bMaskInd);
bRepeat=All(bRepeat);
bCon=All(bCon);


[~, indices]=Shuffle(bInters);

P.InterList=bInters(indices)';
P.StimInds=P.StimInds(indices)';
P.StimLevels=P.StimLevels(indices)';
P.ContrastInds=bCon(indices)';
P.MaskInds   = bMaskInd(indices)';

%mm=P.PossMaskValues{1};
P.MaskLevels = P.PossMaskValues{1}(P.MaskInds);
P.ContrastLevels = P.PossContrastValues{1}(P.ContrastInds);

% ComplementaryPSBupdate
% texttiming   = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxFramesPerTrial);
% fliptiming   = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxFramesPerTrial);
P.ResponseKeyName  = cell(1, P.TotalTrials);


KbReleaseWait
% *************  Main experiment loop **************
% Full black screen as a baseline measurment tool for pupil size
if E.Settings.ifPAbaseMeasure
    BaseLinePupilMeasurementInit
    while P.BaseTrialNo < E.Settings.BaseTotalTrialNo  && (~E.ExitKeyPressed)
        P.BaseTrialNo = P.BaseTrialNo+1;
        
        BaseLinePupilMeasurement
    end
end


%%%% distance calculation using Tobii
if strcmpi(E.Eyetracker,'Tobii') && E.Computer.TobiiConnected
    while ~KbCheck
        TobiiSample = E.Settings.Tobii.ET.get_gaze_data('flat');
        if ~isempty(TobiiSample.left_gaze_point_on_display_area) && ismember(1,E.WhichEye)
            LE_Point=TobiiSample.left_gaze_point_in_user_coordinate_system(end,:);
            LE_Origin=TobiiSample.left_gaze_origin_in_user_coordinate_system(end,:);
            Delta = LE_Point-LE_Origin;
            dist = sqrt(sum(Delta.^2));
            DrawFormattedText(E.Window.Pointer, sprintf('Sit at %2.1fcm (you are at %d cm)\nHit a key to start',round(E.Settings.TestDistmm/10),round(dist/10)), 'center','center', E.Window.Gray-E.Window.Gray*0.25);
       
        elseif ~isempty(TobiiSample.right_gaze_point_on_display_area) && ismember(2,E.WhichEye)
            RE_Point=TobiiSample.right_gaze_point_in_user_coordinate_system(end,:);
            RE_Origin=TobiiSample.right_gaze_origin_in_user_coordinate_system(end,:);
            Delta = RE_Point-RE_Origin;
            dist = sqrt(sum(Delta.^2));
            DrawFormattedText(E.Window.Pointer, sprintf('Sit at %2.1fcm (you are at %d cm)\nHit a key to start',round(E.Settings.TestDistmm/10),round(dist/10)), 'center','center', E.Window.Gray-E.Window.Gray*0.25);
            
        end
         Screen('Flip' , E.Window.Pointer);
    end
    KbReleaseWait;
    clear LE_Point LE_Origin RE_Point RE_Origin
end
NewTrial=1;
% FirstTrialRpt = 1; % used to always repeat once the first trial
P.OverallTrialCntr = 0;
% equal sign in "<=" menas we can repeat the final trial
while (P.CurrentTrialNo <= P.TotalTrials) && (~E.ExitKeyPressed) % Start Experiment.
    P.OverallTrialCntr = P.OverallTrialCntr+1; % count all the number of new trials and or repetitions
    if NewTrial
        % this condition means do not update after one occurance of final trial
        if P.CurrentTrialNo == P.TotalTrials
            break
        end
        P                   = GetTestValue(P);
        trialNum            = P.CurrentTrialNo;
        
    end
    %send feedback to Eylink screen title box
    if strcmp(E.Eyetracker, 'EyeLink')
        Eyelink('command', 'record_status_message "Set%d TRIAL %d/%d  OA%d M%d/%d C%d/%d"',...
            E.Settings.ExperimentSet, P.CurrentTrialNo, P.TotalTrials, P.OverallTrialCntr, P.MaskInds(P.CurrentTrialNo),E.Settings.MaskNo ,P.ContrastInds(P.CurrentTrialNo),length(E.Settings.BaseContrastLevels));
    end
    %  trialNum=P.InterTrialNo(P.WhichInter);
    %  S.MaskLoop = P.WhichInter;
    KeyPressedDuringMovie=0;
    for frameLoop = 1:E.TrialFramesPerInterleave(P.WhichInter) % shows stimulus
        S.CurrentTrialFrame = frameLoop;
        %                 DrawFormattedText(E.Window.Pointer, sprintf('Trial %i / %i (Int %i) running for %.2fs', trialNum, P.TotalTrials, P.WhichInter, S.StimulusLength(P.WhichInter)),'center',E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.White);% Stimulus Code
       OKN_GlaucomaStimulus
%         S = OKN_GlaucomaStimulus(E,P,S); % this writes the stimulus texture into the back buffer
        %         E = PsychSandboxGetBehaviour(E,P,S); % flips to display back buffer
        SimpleTimingGetBehaviour
%         E = SimpleTimingGetBehaviour(E,P,S); % flips to display back buffer with no while in ET loop
        
        if ~isnan(E.LastKeyPressedOnFrame)
            KeyPressedDuringMovie = 1;
            LastKeyStored = E.LastKeyPressed;
        end
    end
    
    
    %     [E] = OKN_GlaucomaSignalQualityControl(E,P);
    OKN_GlaucomaSignalQualityControl
    %     E = PSBCheckMemoryLeaks(E,P,S,P.CurrentTrialNo);
    %PSBCheckMemoryLeaks(E,P,S,trialNum);
    %     Screen('DrawTexture', E.Window.Pointer, S.CoverTexture,[],  S.DestRect)
    
    Screen('FillRect', E.Window.Pointer, E.Window.PTBbackground, E.Window.PTBScreenRect);
    
    % show feedback on experiment screen
    
%     QCstring = sprintf('\nTrial %i / %i int%i\nQ: %2.2f',P.CurrentTrialNo, P.TotalTrials,P.InterList(P.CurrentTrialNo), E.TrialGoodDataRatio);
%     P.InterList(P.CurrentTrialNo)
    QCstring = sprintf('\nTrial %i / %i \nQ: %2.2f',P.CurrentTrialNo, P.TotalTrials,E.TrialGoodDataRatio);
    Screen('TextSize',E.Window.Pointer, 10);
    DrawFormattedText(E.Window.Pointer, QCstring, 50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);
    DrawFormattedText(E.Window.Pointer, 'HP', 50, E.Window.PTBScreenRect(4)-50, E.HeadPositionFlagCol);%
    DrawFormattedText(E.Window.Pointer, sprintf('\n\n\nLE%3.0fcm RE%3.0fcm',round(LE_DistCm),round(RE_DistCm)),50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);

    if E.Settings.HalfWayFdBk && ismember(P.CurrentTrialNo,round(P.TotalTrials.*[0.5,0.75])) && E.TrialGoodDataFlag
        if P.CurrentTrialNo == round(P.TotalTrials/2)
            HalfWayTxtStr = 'Great!\nYou are half way through';
            Screen('TextSize',E.Window.Pointer, 18);
            DrawFormattedText(E.Window.Pointer, HalfWayTxtStr, 'center', 'center', E.Window.White);
            
        elseif P.CurrentTrialNo == round(P.TotalTrials*0.75)
            Qtr2EndTxtStr = sprintf('You are doing AMAZING!\n Just %i more left',(P.TotalTrials-P.CurrentTrialNo));
            Screen('TextSize',E.Window.Pointer, 18);
            DrawFormattedText(E.Window.Pointer, Qtr2EndTxtStr, 'center', 'center', E.Window.White);
        end
    end
    %     DrawFormattedText(E.Window.Pointer, sprintf('Trial %i / %i (Mask %i) running for %.2fs ', P.CurrentTrialNo, P.TotalTrials, P.MaskInds(P.CurrentTrialNo), S.StimulusLength(P.WhichInter)),'center',E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Gray-25);% Stimulus Code
    Screen('Flip' , E.Window.Pointer);
    
    
    if E.Settings.HalfWayFdBk && ismember(P.CurrentTrialNo,round(P.TotalTrials.*[0.5,0.75])) && E.TrialGoodDataFlag
        WaitSecs(2);
        Screen('FillRect', E.Window.Pointer, E.Window.PTBbackground, E.Window.PTBScreenRect);
        Screen('Flip' , E.Window.Pointer);
    end
    
    if E.Settings.GetKbresponse
        KbReleaseWait
        GoodKey = 0; E.KeyIsDown=0;
        while ~GoodKey
            E = PsychSandboxGetKeyboardAndMouse(E);
            if  KeyPressedDuringMovie
                E.LastKeyPressed = LastKeyStored;
                E.KeyIsDown=1;
            end
            KeyMatches = (strncmpi(P.Responses,E.LastKeyPressed,6));
            E.AnyRespKeyPressed = sum(KeyMatches);
            E.RepeatKeyPressed = (strncmpi(P.RepeatKey,E.LastKeyPressed,6));
            E.ExitKeyPressed = strcmp(E.LastKeyPressed,'ESCAPE');
            GoodKey=E.RepeatKeyPressed|E.AnyRespKeyPressed|E.ExitKeyPressed;
            if KeyPressedDuringMovie && ~GoodKey % they hit an invalid key during the movie
                KeyPressedDuringMovie=0;
            end
        end
        KbReleaseWait
        
        
        % keyIsDown added to wait until key release after this point as well
        
        %          [ keyIsDown, seconds, keyCode ] = KbCheck(-1);
        %         while keyIsDown
        %             [ keyIsDown, seconds, keyCode ] = KbCheck(-1);
        %         end
        
        if 1%E.AnyRespKeyPressed
            P.ResponseKeyName{P.CurrentTrialNo} = E.LastKeyPressed;
            %             P.ResponseKeyNo(P.CurrentTrialNo)   = find(KeyMatches);
            %             P.Response(P.CurrentTrialNo)        = (P.ResponseKeyNo(P.CurrentTrialNo) == P.InterList(P.CurrentTrialNo))  ;%rand>0.5;%P.SimResponse(trialNum); % Save (Simulated) Response
            P.Response(P.CurrentTrialNo)        = (strncmpi(P.Responses(P.InterList(P.CurrentTrialNo)),E.LastKeyPressed,6)) ; % new ker response scoring method
            if S.TextVisualFeedBack
                if E.RepeatKeyPressed
                    DrawFormattedText(E.Window.Pointer, 'Repeat', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
                elseif E.ExitKeyPressed % exiting
                    DrawFormattedText(E.Window.Pointer, 'Exiting', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
                elseif P.Response(P.CurrentTrialNo)==1 %correct?
                    DrawFormattedText(E.Window.Pointer, 'Correct', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
                else %incorrect (default)
                    DrawFormattedText(E.Window.Pointer, 'Incorrect', 'center', E.Window.PTBScreenRect(4)-E.Settings.InstructionOffset,E.Window.Black);% Stimulus Code
                end
                Screen('Flip' , E.Window.Pointer);
                WaitSecs(0.25);% define length
            end
            if S.VisualFeedback
                
                if E.RepeatKeyPressed
                    Screen('FrameRect', E.Window.Pointer, E.Window.PTBbackground+10, E.Window.PTBScreenRect,25);
                elseif E.ExitKeyPressed % exiting
                    Screen('FrameRect', E.Window.Pointer, E.Window.White, E.Window.PTBScreenRect,25);
                elseif P.Response(P.CurrentTrialNo)==1 %correct?
                    Screen('FrameRect', E.Window.Pointer, [0 64 0], E.Window.PTBScreenRect,25);
                else %incorrect (default)
                    Screen('FrameRect', E.Window.Pointer, [64 0 0], E.Window.PTBScreenRect,25);
                end
                Screen('Flip' , E.Window.Pointer);
                WaitSecs(0.25);% define length
            end
            if E.Settings.FeedbackLevel > 1            % feedback goes here
                fprintf('%d\t Int %d\t Trial %d\t %+2.2f dir is %d resp key is %d resp is %d\n',...
                    trialNum,P.InterList(trialNum),P.TrialNumbers(trialNum),P.TestValue(trialNum),S.DirPeak(P.InterList(trialNum)),P.ResponseKeyName(trialNum),((P.Response(trialNum))));
            end
        end
        
    else % i.e. no Kb resopnse is required; just show a grey screen
        TimeNow = GetSecs;
        ExitTime  = TimeNow+1;
        while TimeNow < ExitTime % inside this one second check for Kb input
            E = PsychSandboxGetKeyboardAndMouse(E);
            if  KeyPressedDuringMovie
                E.LastKeyPressed = LastKeyStored;
                E.KeyIsDown=1;
            end
            E.RepeatKeyPressed = (strncmpi(P.RepeatKey,E.LastKeyPressed,6));
            E.ExitKeyPressed = strcmp(E.LastKeyPressed,'ESCAPE');
            E.PauseKeyPressed = strcmp(E.LastKeyPressed,'space');
            if E.PauseKeyPressed
                TimeNow = ExitTime;
            else
                TimeNow = GetSecs;
            end
        end
        KbReleaseWait;
        
    end
    
    while E.PauseKeyPressed
        E = PsychSandboxGetKeyboardAndMouse(E);
        KbReleaseWait;
        Screen('TextSize',E.Window.Pointer, 14);
        DrawFormattedText(E.Window.Pointer, 'Take a break!', 'center', 'center', E.Window.White);
        Screen('TextSize',E.Window.Pointer, 10);
        DrawFormattedText(E.Window.Pointer, QCstring, 50, E.Window.PTBScreenRect(4)-50, E.Window.Gray-E.Window.Gray*0.25);
        Screen('Flip' , E.Window.Pointer);
        E.PauseKeyPressed = ~strcmp(E.LastKeyPressed,'space');
        if E.PauseKeyPressed % i.e. the first time inside the while loop we set E.RepeatKey to be true
            E.RepeatKeyPressed = true; % repeat the last trial when pause is pressed
        else % i.e. when pause key pressed inside the while loop
            Screen('FillRect', E.Window.Pointer, E.Window.PTBbackground, E.Window.PTBScreenRect);
            Screen('Flip' , E.Window.Pointer);
            WaitSecs(1);
        end
    end
    if E.RepeatKeyPressed ||( ~E.TrialGoodDataFlag )%  || (FirstTrialRpt)
        NewTrial      = 0;
        % FirstTrialRpt = 0;
        E = OKN_GlaucomaResetRecordedValues(E,P); % reset the recorded values for the current trial (the one we want to repeat)
        
        %   [P] = UpdateTestStructure(P); % removed ",E" from input arguments because it is not needed there
        %    P.CurrentTrialNo = P.CurrentTrialNo - 1;
        %    P.InterTrialNo(P.WhichInter) =  P.InterTrialNo(P.WhichInter)-1;
    else
        NewTrial=1;
        %    [P] = UpdateTestStructure(P); % removed ",E" from input arguments because it is not needed there
    end
    
end


close all;

% P.NanPercentage = 100*sum(isnan(E.EyePos.X(:)))/numel(E.EyePos.X(:));
% if P.NanPercentage >= 0.01 % if the NaN numbers is grater than 1 percent of collected data then repeat the test
%     fprintf('Error: %2.3f%c of collected data is NaN. Please repeat this condition.\n',P.NanPercentage,'%' );
%     %     h = msgbox(MSG','Status'); set(h,'Position',[[417.5000 366.3333 500 50.5000]]);
% end

% PsychSandboxShutdown;          % Save / Shutdown
OKN_GlaucomaShutdown
% OKN_GlaucomaThisRunAnalysis
if ((centmenu('View Analysis?', {'Later' 'Now'}))-1)
OKN_glaucoma_ThisRun_TBT_Analysis
end