% OKN processing Analysis

% % ptbRoot = Find1PTB;
%
% % change directory to your experimental data directory
% RootDir = fullfile(ptbRoot,'Data','OKNMOCS','VFL');
%
% % ObserverName = {'PRT_108t_M4_FC'  'PRT_108t_M4_12c'  'SKH_108t_M4_FC'  'SKH_108t_M4_12c'  'SMD_108t_M4_FC'  'SMD_108t_M4_12c' 'LGM_108t_M4_FC'  'LGM_108t_M4_12c'...
% %                 'MLE_108t_M4_FC'  'MLE_108t_M4_12c'  'ALL_108t_M4_FC'  'ALL_108t_M4_12c'  };

%             load(TheName);

if iscell(E)
    E = E{1};
    P = P{1};
    S = S{1};
end

for i = 1:1:length(P.TrialNumbers)
    theInt= P.InterList(i);
    TrialofTheInt = P.TrialNumbers(i);
    InterpolatedFrames   = squeeze(squeeze(E.Window.InterpolatedFrames(theInt,TrialofTheInt,1:120)));
    InterpolatedFramesNo = sum(InterpolatedFrames);
    FrameRate = double(1./diff(squeeze(E.Window.FrameFlipTime(theInt, TrialofTheInt, 1:121)))).*double(~InterpolatedFrames); %  double(InterpolatedFrames);
    figure(90),plot(FrameRate,'ro')
    title(sprintf('Trial: %2.2f Int: %2.2f  TrialofThisInt: %2.2f Interpolated frames: %3.0f'  ,i, theInt, TrialofTheInt, InterpolatedFramesNo ))
    drawnow,pause
end

if P.CurrentTrialNo== P.TotalTrials
    if E.Settings.ExperimentSet ==1
        Set1NoMasks = length(P.PossMaskValues{1});
        Set1NoContLevels = length(P.PossContrastValues{1});
    elseif  E.Settings.ExperimentSet ==2
        Set2NoMasks = length(P.PossMaskValues{1});
        Set2NoContLevels = length(P.PossContrastValues{1});
        
    end
    ObserverName = 'test';%{E.Folders.SaveFileName(1:9)}; % SMD_VFLF' 'SCD_VFLF'};
    JustFirstSample = 1; % i.e. just use the first sample per each frame
    EyeList       = 'LR';
    TheRuns       = {1}; % We have one final run runs of 288 trials for four contrast levelsE.Settings.BaseContrastLevels
    TheConditions = 2; % 2AFC111
    conLoop       = 1;
    %     eyeLoop       = [1, 2]; %E.WhichEye; % because we have just Right eye
    TheEyes = [1, 2];
    MaxSaccSpeedDegPerSec = 500; % fixed - determined by minimisation
    paStdThresh           = 2.75; %  used to find and remove blinks
    FrameMargin           = 12; % number of eye tracking samples to disgard either side of a blink. was 5
    SSpdWS                = 4; % window in milisecond, after and before each point to calculate sliding window speed.
    Smooth4SpeedCalc =1; % 1= use sgolay filter to smooth eye-position for speed calculation
    Sgolength = 3;
    IVvalues= [1];
    IVloop=1;
    % for IVloop=1:length(IVvalues)
    
    
    subLoop  = 1; % we are analysing the previous run
    %     for subLoop  = 1:length(ObserverName) % subjects
    if  strcmp( E.Eyetracker,'Tobii')
        X2PixConvertRatio = E.Window.PTBScreenWidth;
        Y2PixConvertRatio = E.Window.PTBScreenHeight;
    else
        X2PixConvertRatio = 1;
        Y2PixConvertRatio = 1;
    end
    for eyeLoop = 1:length(TheEyes)
        for runLoop = 1:length(TheRuns) % 1 run
            %             clear fNameString TheName
            % load up the data file
            %             fNameString  = sprintf('%s%c%s_%c%d*.mat',RootDir,filesep,ObserverName{subLoop},EyeList(eyeLoop),TheRuns{runLoop});
            %             AllFiles    = dir(fNameString);
            %             TheName     = sprintf('%s%c%s',RootDir,filesep,AllFiles(1).name);
            %             load(TheName);
            %             if iscell(E)
            %                 E = E{1};
            %                 P = P{1};
            %                 S = S{1};
            %             end
            E.Settings.CovdRatio = round(S.StimulusLength(1)./(E.Settings.CoveredFrames.*E.Window.InterFrameInterval)+0); %  add 1 if you want to make it bigger to be safe
            S.NewSamplingSepMS   = 0;
            
            E.plotting  = 0; % 1 means plot OKN signal corresponding to desired stimulus 2 means plot the line fit as well
            
            %         SpeedPixelsPerDegree = S.PixelsPerDegree;
            whichEye = eyeLoop;
            %         for BaseTrialLoop = 1: E.Settings.BaseTotalTrialNo
            %             BaselinePupilTimeInMS = E.BaseEyePos.Time(BaseTrialLoop,:);
            %             BaselinePupilArea = E.BaseEyePos.PA(BaseTrialLoop,:,whichEye);
            %             goodVals = find(BaselinePupilTimeInMS>0 & BaselinePupilArea>0);
            %             PupilTimeInMS0=BaselinePupilTimeInMS(goodVals);
            %             PupilTimeInMS = PupilTimeInMS0 - PupilTimeInMS0(1);
            %             PupilArea = BaselinePupilArea(goodVals);
            %             figure, plot(PupilTimeInMS,PupilArea,'ro' )
            %             BaselienPupilSizeMean = nanmean(PupilArea);
            %         end
            for interLoop=1:P.NoInterleaved % for directional trials individually 1=Right, 2=Left
                E.interLoop = interLoop;
                PossStimValues=(P.PossStimValues{interLoop}); %cpd log10 back to 'logMAR' type scale
                PossMaskValues = P.PossMaskValues{interLoop};
                for stimLoop = 1:numel(PossStimValues) % add 1: for each SF individually
                    S.stimLoop = stimLoop;
                    for maskLoop = 1:numel(PossMaskValues)  %  not just the biggest proportion of visible stimulus
                        for contrastLoop = 1: numel(E.Settings.BaseContrastLevels) % contrast levels
                            TheIndices=find((P.InterList==interLoop) & (P.MaskInds==maskLoop) & (P.ContrastInds==contrastLoop)); %(P.StimInds==stimLoop) &
                            P.RespList(interLoop,stimLoop,contrastLoop,maskLoop)=sum(P.Response(TheIndices));
                            P.NoPresList(interLoop,stimLoop,contrastLoop,maskLoop)=length(P.Response(TheIndices));
                            LocalTrialNo=P.TrialNumbers(TheIndices);
                            for LocTrialLoop=1:length(LocalTrialNo) %1:
                                
                                xDataIn         = squeeze(E.EyePos.X(interLoop, LocalTrialNo(LocTrialLoop), :, whichEye)).* X2PixConvertRatio;
                                yDataInvIn      = squeeze(E.EyePos.Y(interLoop, LocalTrialNo(LocTrialLoop), :, whichEye)).* Y2PixConvertRatio;
                                yDataIn         = -(yDataInvIn(:) - E.Window.PTBScreenHeight);% turn the Y direction from screen coordiante to mathematic coordinate. if this line is in the code, make sure that in dirdiff the angles are in correct order which should be [0 pi/2 pi 3pi/2]
                                FrameFlipDataIn = squeeze(E.Window.FrameFlipTime(interLoop, LocalTrialNo(LocTrialLoop), :)); % keep the frame flip time for later processing
                                
                                paDataIn      = squeeze(E.EyePos.PA(interLoop,  LocalTrialNo(LocTrialLoop),   :, whichEye));  %_SMD pa: pupil area
                                tDataIn       = squeeze(E.EyePos.Time(interLoop,LocalTrialNo(LocTrialLoop),   :));
                                
                                StimulusDirection = DegToRad(E.Settings.StimulusDirection(interLoop));
                                
                                
                                %                     drownow;
                                
                                %                         SSpdWS = 10; % Slide Speed window size
                                
                                SamplingRate =IVvalues(IVloop);
                                %500; % 800 deg
                                
                                OKN_GalucomaProcessing % Hist gain and scoring
                                %                             OKNprocessingCore2;  % for optimization
                                % OKNOptimprocessing2; %the original one  calculate actual visible proportion of the stimulus
                                if ismember(E.Settings.MaskType,[4,5,6])
                                    if ~isfield(S,'FovRegDiameter')
                                        S.FovRegDiameter = 5 ; % 2.5 deg diameter of central visible circle
                                    end
                                    %                                 DisplayAreaDeg =  E.Window.DisplayHeightDeg*E.Window.DisplayWidthDeg;
                                    %                                 FovRegAreaDeg = pi.*(S.FovRegDiameter./2)^2;
                                    % the rectangular noise mask minus the foveal proportion plus
                                    %                                 ActualVisblRatio = P.VisblRatio-(P.VisblRatio.*(FovRegAreaDeg./DisplayAreaDeg))+(FovRegAreaDeg./DisplayAreaDeg);
                                    
                                    FovRegDiameter{subLoop} = S.FovRegDiameter;
                                    if isfield(P,'ActualVisibleRatio')
                                        AllVisblRatio{subLoop} = P.ActualVisibleRatio;
                                    else
                                        DisplayAreaDeg =  E.Window.DisplayHeightDeg*E.Window.DisplayWidthDeg;
                                        FovRegAreaDeg = pi.*(S.FovRegDiameter./2)^2;
                                        
                                        ActualVisblRatio = P.VisblRatio-(P.VisblRatio.*(FovRegAreaDeg./DisplayAreaDeg))+(FovRegAreaDeg./DisplayAreaDeg);
                                        AllVisblRatio{subLoop} = ActualVisblRatio;
                                        
                                    end
                                end
                                %plotting
                                if 1%E.plotting % TheIndices(LocTrialLoop)==259%
                                    if   1% SldGain<0.5 && (contrastLoop==4) %(maskLoop==9)&&SldGain<0.3 OKNscore==0 %(subLoop==3)% &(abs(OKNGain) <0.4)& % ismember(maskLoop, [1 5 9 ] ) && ismember(LocTrialLoop, [ 1 8])  && ismember(interLoop,[1 2])%|| stimLoop == 5
                                        
                                        BelowThresh = find(Speed<Threshold);
                                        AboveThresh = find(Speed>=Threshold);
                                        PursOrNot = Speed<Threshold;
                                        
                                        SeqBelowThresh = find((PursSpeed) < Threshold);
                                        SeqAboveThresh = find((PursSpeed) >= Threshold);
                                        
                                        SpeedDb = 20.*log10(Speed);
                                        xDallDb = 20.*log10(abs(xDall));
                                        yDallDb = 20.*log10(abs(yDall));
                                        
                                        PursSpeedDb = 20.*log10(PursSpeed);
                                        
                                        if ~isempty(Speed)
                                            maxSpdVal = max(Speed);
                                        else
                                            maxSpdVal = 1;
                                        end
                                        StimSpeedPlt = S.SpeedPixPerMS.*Tall;
                                        
                                        EstOffsetFromHist = Tall.*(SpeedFromHistPixPerSec/1000);
                                        subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,((S.SpeedPixPerSec/1e6).*Tall),'k-.', Tall,((SpeedFromHistPixPerSec/1e6).*Tall),'m-.' );
                                        % subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall,EstOffsetFromHist,'b-',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimSpeedPlt,'k-.',Tall,(StimSpeedPlt.*HistGain)+E.Window.PTBScreenWidthCenter,'m-.' );
                                        %                                     legend('Rotated Eye X Position', 'Saccade', 'Tracking', '10 deg/s', sprintf('Gain %2.2f',HistGain), 'Location', 'Best')
                                        ylim([0 E.Window.PTBScreenWidth]),
                                        title(sprintf('Subject:%i %iAFC Contrast:%2.2f ; Mask:%i Inter:%i Repetition:%i   XData',subLoop, TheConditions(conLoop), E.Settings.BaseContrastLevels(contrastLoop)*100, maskLoop, interLoop , LocTrialLoop)); shg
                                        subplot(2,4,2);plot( Tall,Yall,'b-',Tall(AboveThresh),Yall(AboveThresh),'r.',Tall(BelowThresh),Yall(BelowThresh),'g.');
                                        title('YData');
                                        ylim([0 E.Window.PTBScreenHeight]),
                                        subplot(2,4,3);plot( xData0,yData0,'b-',xData00, yData00,'r.');
                                        title(sprintf('Unprocessed Eye Gaze on Screen (%2.0f deg) ',E.Settings.StimulusDirection(E.interLoop)));
                                        ylim([0 E.Window.PTBScreenHeight]),  xlim([0 E.Window.PTBScreenWidth]),
                                        legend('Original Eye Gaze', 'Rotated Eye Gaze')
                                        
                                        %                                     subplot(2,4,4),plot( Tall,Xall,'b-',Tall(PursStartInd),Xall(PursStartInd),'r*',Tall(PursEndInd),Xall(PursEndInd),'ks');ylim([0 E.Window.PTBScreenWidth]),
                                        %                                     legend('eye position', 'Star of Pursuit', 'End of Pursuit')
                                        subplot(2,4,5);plot( tDall,xDall,'b-',tDall(AboveThresh),xDall(AboveThresh),'r.',tDall(BelowThresh),xDall(BelowThresh),'g.',tDall,HistGain.*ones(size(tDall)),'K-');
                                        title('XSpeed'); legend('Speed','Saccade', 'Tracking',sprintf('Gain=%2.2f',HistGain)),ylim([-maxSpdVal maxSpdVal])
                                        subplot(2,4,6);plot(tDall,yDall,'b-',tDall(AboveThresh),yDall(AboveThresh),'r.',tDall(BelowThresh),yDall(BelowThresh),'g.',tDall,OKNGain.*ones(size(tDall)),'K-');
                                        ylim([-maxSpdVal maxSpdVal]),title('YSpeed');
                                        subplot(2,4,7);plot(tDall,Speed,'b-',tDall(AboveThresh),Speed(AboveThresh),'r.',tDall(BelowThresh),Speed(BelowThresh),'g.',tDall,OKNGain.*ones(size(tDall)),'K-');
                                        ylim([-maxSpdVal maxSpdVal]),title('Directional Speed');
                                        subplot(2,4,7);polarplot(SpeedAng,SpeedDb,'b.',SpeedAng(AboveThresh),SpeedDb(AboveThresh),'ro',SpeedAng(BelowThresh),SpeedDb(BelowThresh),'go')
                                        %                                     subplot(2,4,8),plot(2:length(tData),1./(diff(tData./1000)),'b.-'),
                                        %                                     title(sprintf('Mean first sample frequency %2.2f',mean(1./(diff(tData./1000)))))
                                        %                                     %                                     subplot(2,4,8);plot(1:length(tData0),tData0,'b.')
                                        %                                         polarplot(PursSpeedAng,PursSpeedDb,'b.',PursSpeedAng(SeqAboveThresh),PursSpeedDb(SeqAboveThresh),'ro',PursSpeedAng(SeqBelowThresh),PursSpeedDb(SeqBelowThresh),'go')
                                        set (gcf, 'Units', 'normalized', 'Position', [0  0.1  1  0.8]); %  [1.5000    0.0778    0.7500    0.6718]);
                                        
                                        fprintf('Trial: %i; keyResponse: %i;  OKNscore: %i;  Threshold: %2.2f;  HistGain: %2.2f; SldGain: %2.2f; OKN Consistency: %2.2f  TotalDisRatio: %2.2f; ConsDistRatio: %2.2f\n',...
                                            TheIndices(LocTrialLoop), P.Response(TheIndices(LocTrialLoop)), OKNscore, Threshold, HistGain, SldGain, OKNConsistency, TotalDisRatio, ConsistentDistRatio);
                                        drawnow
                                        
                                    end
                                end
                                
                                % saving the result
                                AllXstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Xall;
                                AllYstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Yall;
                                AllPstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Pall;
                                AllTstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Tall;
                                AllXSpdstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}               = xDall;
                                AllYSpdstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}               = yDall;
                                AllTSpdstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}               = tDall;
                                AllSpdVecstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = Speed;
                                AllSpdAngstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = SpeedAng;
                                AllThreshstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = Threshold;
                                AllPursuitstore{subLoop, conLoop,eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = PursuitOrSaccade;
                                AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}           = OKNscore;
                                AllOKNConsistencystore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}     = double(OKNConsistency);
                                AllOKNConsDistRatio{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}        = ConsistentDistRatio;
                                AllOKNgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}            = OKNGain;
                                AllOKNSldgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}         = SldGain;
                                AllConsOKNgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}        = ConsistentGainRec;
                                AllThetaVariRatiostore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}     = TheThetaVariablilityRatio;
                                AllTotDistRatiostore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}       = TotalDisRatio;
                                AllKeyRespstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}            = P.Response(TheIndices(LocTrialLoop));
                                AllMeanPupilAreastore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}      = PupilAreaFromHistPix;
                                AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}              = HistGain; %SpeedFromHistDegPerSec/S.SpeedDegPerSec;
                            end
                        end
                    end
                end
            end
        end
    end
    %%
    maskLoop = 1:numel(P.PossMaskValues{1}); %1: % just the full field stimulus
    stimLoop = 1;
    %     eyeLoop  = 1 ;
    %     conLoop  = 1;
    interLoop = 1:P.NoInterleaved;
    %     for subLoop  = 1:length(ObserverName)
    for eyeLoop = 1:length(TheEyes)
        for contrastLoop = 1:length(E.Settings.BaseContrastLevels)
            
            clear   OKNscoreList     KeyRespList       TotDistRatioList      OKNgainList OKNscoreConsList ConsDistRatioList OKNSldgainList ConsOKNGainList ThetaVariRatioList
            OKNscoreList=[];  KeyRespList=[];   TotDistRatioList=[];  OKNgainList = []; OKNscoreConsList=[];  ConsDistRatioList=[]; OKNSldgainList=[]; ConsOKNGainList=[]; ThetaVariRatioList=[]; TheHgainList=[]; ThePupilAreaList=[];
            for runLoop = 1:(TheRuns{subLoop})
                for LocTrialLoop = 1: E.Settings.LocalTrialNo
                    
                    TheOKNscore      = (cell2mat(squeeze(AllOKNscorestore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    OKNscoreList     = [OKNscoreList; TheOKNscore];
                    
                    TheOKNscoreCons      = (cell2mat(squeeze(AllOKNConsistencystore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    OKNscoreConsList     = [OKNscoreConsList; TheOKNscoreCons];
                    
                    
                    TheKeyResp       = (cell2mat(squeeze(AllKeyRespstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    KeyRespList      = [KeyRespList; TheKeyResp];
                    
                    TheTotDistRatio  = (cell2mat(squeeze(AllTotDistRatiostore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    TotDistRatioList = [TotDistRatioList; TheTotDistRatio];
                    
                    TheOKNgain       = real(cell2mat(squeeze(AllOKNgainstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    OKNgainList      = [OKNgainList; TheOKNgain];
                    
                    TheOKNSldgain       = real(cell2mat(squeeze(AllOKNSldgainstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    OKNSldgainList      = [OKNSldgainList; TheOKNSldgain];
                    
                    
                    TheConsDistRatio  = (cell2mat(squeeze(AllOKNConsDistRatio(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    ConsDistRatioList = [ConsDistRatioList; TheConsDistRatio];
                    
                    TheConsOKNGain =  (cell2mat(squeeze(AllConsOKNgainstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    ConsOKNGainList =  [ConsOKNGainList; TheConsOKNGain];
                    
                    TheThetaVariRatio =  (cell2mat(squeeze(AllThetaVariRatiostore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    ThetaVariRatioList =  [ThetaVariRatioList; TheThetaVariRatio];
                    
                    TheHgain =  (cell2mat(squeeze(AllHgainstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    TheHgainList =  [TheHgainList; TheHgain];
                    
                    ThePupilArea =  (cell2mat(squeeze(AllMeanPupilAreastore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                    ThePupilAreaList =  [ThePupilAreaList; ThePupilArea];
                    
                end
                
                
                AllOKNscoreList{subLoop,contrastLoop}         = OKNscoreList;
                AllOKNscoreConsList{subLoop,contrastLoop}     = OKNscoreConsList;
                AllOKNConsDistRatioList{subLoop,contrastLoop} = ConsDistRatioList;
                AllKeyRespList{subLoop,contrastLoop}          = KeyRespList;
                AllTotDistRatioList{subLoop,contrastLoop}     = TotDistRatioList;
                AllGainList{subLoop,contrastLoop}             = OKNgainList;
                AllSldgainList{subLoop,contrastLoop}          = OKNSldgainList;
                AllConsOKNGianList{subLoop,contrastLoop}      = ConsOKNGainList;
                AllThetaVariRatioList{subLoop,contrastLoop}   = ThetaVariRatioList;
                AllTheHgainList{subLoop,contrastLoop}         = TheHgainList;
                AllThePupilAreaList{subLoop,contrastLoop}     = ThePupilAreaList;
            end
        end
    end
    if E.Settings.ExperimentSet == 1
        %     PlotStrs = {'Gain' 'OKN Score' 'OKN Consistent Distance Ratio' 'OKN Consistency' 'Sliding Gain' 'Hist Gain' 'Pupil Area'};
        PlotStrs = { 'Hist Gain'};
        
        MaskTypes = {'Circular' 'Annular' 'Rectangular' 'Center Visible  Noise' 'Center Occluded  Noise'}; % different masks that could be used
        % close all
        XAxisData =  1:numel(P.PossMaskValues{1});
        for plotLoop = 1: length(PlotStrs)
            YLabelStr = PlotStrs{plotLoop};
            for     contrastLoop = 1: length(E.Settings.BaseContrastLevels) % should be 1:.....
                switch plotLoop
                    case 1
                        %                     Data = [AllGainList{subLoop,contrastLoop}];
                        %                 case 2
                        %                     Data = [AllOKNscoreList{subLoop,contrastLoop}];
                        %                 case 3
                        %                     Data = [AllOKNConsDistRatioList{subLoop,contrastLoop}];
                        %                 case 4
                        %                     Data  = [AllOKNscoreConsList{subLoop,contrastLoop}];
                        %                 case 5
                        %                     Data  = [AllSldgainList{subLoop,contrastLoop}];
                        %                 case 6
                        Data  = [AllTheHgainList{subLoop,contrastLoop}];
                        %                 case 7
                        %                     Data  = double([AllThePupilAreaList{subLoop,contrastLoop}]);
                end
                
                
                
                
                MeanData = nanmean(Data);
                StdData  = nanstd(Data);
                
                %             if E.Settings.BaseContrastLevels(contrastLoop)>= 100000
                %                 ColorSrt = 1-(E.Settings.BaseContrastLevels(contrastLoop).*0.20.*[1 1 1]).^0;
                %             else
                %                 ColorSrt =  abs(0.8- E.Settings.BaseContrastLevels(contrastLoop)).*[1 1 1];
                %
                %             end
                ColorSrt = [0.2 0.2 0.9];
                plotNumber = (subLoop-1)*length(PlotStrs)+plotLoop;
                OKNfig=figure(plotNumber);
                hold on
                
                if plotLoop==1
                    nrData=mean(Data);
                    BaseLine =0; % min(nrData);
                    [params, f]= FitNakaRushton((XAxisData),nrData-BaseLine); %fliplr
                    FineX=linspace(XAxisData(1),XAxisData(end),1000);
                    FineXplot=linspace(XAxisData(1),XAxisData(end),1000);
                    [prediction] = BaseLine+(ComputeNakaRushton(params,FineX)); % fliplr
                    p1=plot(FineXplot,prediction,'k-');
                    set(p1,'LineWidth',3,'Color',[0.6 0.3 0.9])
                end
                e1=errorbar(XAxisData, MeanData,StdData,'o','LineWidth',1,'MarkerFaceColor',ColorSrt,'Color',ColorSrt); %+gap
                set(e1,'MarkerSize',12,'MarkerEdgeColor',ColorSrt,'LineWidth',1)
                if plotLoop<7
                    ylim([min(Data(:))-0.05 max(Data(:))+0.05]);%ylim([.3 1.1]),
                end
                
                
                xlim([-.1 1.1]);
                plot(XAxisData,Data,'o','MarkerSize',4,'MarkerFaceColor',ColorSrt,'MarkerEdgeColor','none','LineWidth',1 )
                %         for i= 1:length(ObserverName)
                %             if mod(i,2)==1
                %                 XtickStr{i} = sprintf('%s',   ObserverName{i}([(1:3),(end-1:end)]));
                %             else
                %                 XtickStr{i} = sprintf('%s',   ObserverName{i}([(1:3),(end-2:end)]));
                %             end
                %
                %         end
                xticks(round(XAxisData,2));
                %                 xticklabels(round(XAxisData,2));
                xtickangle(70);
                grid on
                StrdRec(plotLoop,IVloop)=sqrt(mean(StdData.^2));
                %             title(sprintf('Subj: %s; Mask: %s %2.2f deg Dia; Number of trials: %i; Runs: %i ',ObserverName{subLoop}(1:3), MaskTypes{E.Settings.MaskType},FovRegDiameter{subLoop} ,P.TotalTrials, (TheRuns{subLoop})),'fontsize',16)
                legend('Naka-Rushton fit',sprintf('Mean %c SD',char(177)), 'Individual Poins','location','Best')
                ylabel(YLabelStr),xlabel('Visible Stimulus Proportion')
                %             if contrastLoop == 1
                %                 text (0.15,1.00,sprintf('Contrast %2.1f \n',E.Settings.BaseContrastLevels.*100),'fontsize',14)
                %             end
                set(gca,'fontsize', 14)
                set(gcf,'Units', 'normalized','Position', [0.1300    0.1100    0.38    0.7150]);
                hold off
            end
        end
    end
    %     end
    
else
    fprintf('Please Finish The Run Before Doing Analysis. \n')
end
% end


%% plot gain VS contrast

if  E.Settings.ExperimentSet == 2
    subLoop =1;
    clear  xAxisData yAxisData
    xAxisData = E.Settings.BaseContrastLevels;
    AlltheGain = [AllTheHgainList{subLoop,:}];
    
    
    yAxisDataA = (AlltheGain(:, 1: numel(P.PossMaskValues{1})*numel(P.PossContrastValues{1})));
    
    
    figure(20),conPlt = errorbar(xAxisData,nanmean(yAxisDataA),nanstd(yAxisDataA),'b-o', 'MarkerFaceColor',[0.8 0.8 0.8],'MarkerEdgeColor',[0.8 0.8 0.8],'Color',[0.8 0.8 0.8]);
    hold on
    set(conPlt,'MarkerSize',10,'MarkerEdgeColor',[0.3 0.3 0.3],'MarkerFaceColor',[0.3 0.3  0.3],'LineWidth',2)
    set(gca,'fontsize', 14,'xscale','log');
    %         set(gca,'fontsize', 14);
    plot(xAxisData,yAxisDataA,'.','MarkerSize',8,'MarkerEdgeColor',[0.5 0.5 0.5],'MarkerFaceColor',[0.5 0.5  0.5],'LineWidth',1)
    xlim([xAxisData(1)-0.2  xAxisData(end)+0.1 ]);ylim([min(yAxisDataA(:))-0.05 max(yAxisDataA(:))+0.05])
    xlabel('Contrast (%)'),ylabel('OKN-Gain')
    xticks((xAxisData))
    xticklabels(round(xAxisData,2)*100)
    set(gca,'fontsize', 14)
    hold off
    %     legend('')
end

%% timing analysis
% after each test (where the data is still in the memory,if not load manully.
%
% if iscell(E)
%     E = E{1};
%     P = P{1};
%     S = S{1};
% end
% firstframe = 1;
% lastframe  = E.TrialFramesPerInterleave(1);
% % Trial      = 24;
% Totalmissed = zeros(1,P.NoInterleaved);
% for i = 1: P.NoInterleaved
%
%     theFrames = squeeze(E.Window.FrameFlipTime(i,:,firstframe:lastframe));
%
%     Speed = 1./diff(mean(theFrames),1);
%     figure(1);
%     subplot(P.NoInterleaved/2,2,i);plot(Speed,'ro');ylim ([0 120])
%     Totalmissed(i) = sum(Speed<50)  ;
%     title(['interleaved '  num2str(i) ': number of missed frame = ' num2str(Totalmissed(i))] );   %num2str(sum(Speed<50))
%     if i == 1
%         ylabel('Mean IFI (5 Stimulus, 4 trials)')
%     elseif i== P.NoInterleaved-1
%         xlabel('Frames')
%     end
% end
% set(gcf,'Name','Mean Interframe Interval')
%
% for i = 1: P.NoInterleaved
%     framedeltas = mean(squeeze(E.Window.FrameDelta(i,:,firstframe:lastframe)),1);
%     figure(2);
%     subplot(P.NoInterleaved/2,2,i);plot(framedeltas,'ro');ylim ([0 3])
%     title(['interleaved '  num2str(i) ': doubled frame delta = ' num2str(sum(framedeltas==2))] );
% end
%
% for i = 1: P.NoInterleaved
%     MisedPerTrial = sum(squeeze(E.Window.InterpolatedFrames(i,:,firstframe:lastframe)),2)';
%     MeanMissedFrames(i,:) = MisedPerTrial;
% end
%  squeeze(MeanMissedFrames(:,1,:))