

clear all 
     
load All41SubjectsGains_Contrast_n_Direction.mat

%% clculated correlation based on more than one contrast
for subLoop = 1: numel(ObserverID)
AlLSubjectsBetterEyeGainMatrix(subLoop,:,:) = (AllSubjectsBetterEyeGain_Contrast{subLoop});
AlLSubjectsWorseEyeGainMatrix(subLoop,:,:) = (AllSubjectsWorseEyeGain_Contrast{subLoop});
end

Cont1 = 1;
Cont2 = 6;

AlLSubjectsBetterEyeGainAverage = squeeze(mean(AlLSubjectsBetterEyeGainMatrix,2));
AlLSubjectsWorseEyeGainAverage = squeeze(mean(AlLSubjectsWorseEyeGainMatrix,2));

AlLSubjectsBetterEyeGainAverageUsedConts = sum(AlLSubjectsBetterEyeGainAverage(:,Cont1:Cont2),2);
AlLSubjectsWorseEyeGainAverageUsedConts = sum(AlLSubjectsWorseEyeGainAverage(:,Cont1:Cont2),2);


% figure,
% plot(AlLSubjectsWorseEyeGainAverage)
%% comparing the normalized OKN score of the BE and the WE to the VFI of the BE and WE
[WorseEyeVFI_Sorted, SortIdx_WE] = sort(WorseEyeVFI);
[BetterEyeVFI_Sorted, SortIdx_BE] = sort(BetterEyeVFI);

AllSubj_OKN_BE = AlLSubjectsBetterEyeGainAverage;
AllSubj_OKN_WE =AlLSubjectsWorseEyeGainAverage;

AllSubj_OKNarea_BE = (AlLSubjectsBetterEyeGainAverageUsedConts); % normalized with the max of BE
AllSubj_OKNarea_WE = (AlLSubjectsWorseEyeGainAverageUsedConts ); % normalized to the BE of the same patient

AllSubj_OKNarea_ReductionRatio = 100*((AllSubj_OKNarea_BE)-(AllSubj_OKNarea_WE))./AllSubj_OKNarea_BE;%AllSubj_OKNarea_BE_Normalized
AllSubj_VFI_ReductionRatio = 100*((BetterEyeVFI)-(WorseEyeVFI))./BetterEyeVFI;%AllSubj_OKNarea_BE_Normalized

ObserverRagne = 1:numel(ObserverID);
figure(11)
clf,
subplot(1,1,1)
hold on ,plot(AllSubj_VFI_ReductionRatio,AllSubj_OKNarea_ReductionRatio,'ro','MarkerSize',10)
title ('WE VFI and OKN-area ratio sorted based on WE VFI')
xlabel('VFI reduction ratio (BE-WE)/BE (%)')
ylabel('OKN gain reduction ratio (%)')
axis ([-20 105 -20 100])

% fit Naka_Rushton on sorted VFI and OKN areas
NRfitXdataIn = AllSubj_VFI_ReductionRatio;
NRfitYDataIn = AllSubj_OKNarea_ReductionRatio';

[NR_params, f]= FitNakaRushton(NRfitXdataIn,NRfitYDataIn);

FineX= linspace(min(AllSubj_VFI_ReductionRatio),max(AllSubj_VFI_ReductionRatio),1000);

[NR_prediction] =(ComputeNakaRushton(NR_params,FineX));
if 1 % plotting NR
    figure(11),
    hold on
    p1 = plot(FineX,NR_prediction,'Color','r','LineWidth',4);
    legend('indivisual patients', 'NR fit')
    set(gca,'FontSize',16)
 
end

%% can we predict VFI using a randomly selected OKN gain area of course for it to work we should boot starp
lookUpVector = AllSubj_OKNarea_ReductionRatio;
% NR_lookUpVector = NR_prediction_WE;
for bootStrapLoop = 1:10000
    thisOKNareaSamp(bootStrapLoop,:) = randsample(lookUpVector,numel(ObserverName),'true'); %numel(ObserverRagne) randomly select form OKn gain area with placement
    
    [NR_PredictedVF] = (ComputeNakaRushton(NR_params, thisOKNareaSamp(bootStrapLoop,:)));

    [~, sampleIdx] = ismember(thisOKNareaSamp(bootStrapLoop,:),AllSubj_OKNarea_ReductionRatio);
    ActualVFIreductionratios(bootStrapLoop,:) =  lookUpVector(sampleIdx);
    
    NR_PredictedVFreduction(bootStrapLoop,:) = NR_PredictedVF;

if 0 %plotting
    figure(10),
    plot(bootStrapLoop,thisOKNareaSamp(bootStrapLoop),'RO',bootStrapLoop,ThisPatientIdxNRVFI(bootStrapLoop),'Rd',bootStrapLoop,ThisPatientIdxVFI(bootStrapLoop),'R.')
    hold on
    drawnow
end
end
  estimationError_OKNarea_VFI = thisOKNareaSamp - ThisPatientIdxVFI;
  estimationError_NRVFI_VFI = ThisPatientIdxNRVFI - ThisPatientIdxVFI;
  
  figure(20)
  [rho,pval] = corrcoef(ThisPatientIdxVFI',ThisPatientIdxNRVFI');
[P,S]=polyfit(ThisPatientIdxVFI,ThisPatientIdxNRVFI,1);
    pred=polyval(P,ThisPatientIdxVFI);
    p1=plot(ThisPatientIdxVFI,pred,'k-',ThisPatientIdxVFI,ThisPatientIdxNRVFI,'o');
    set(p1(1),'LineWidth',2);
    set(p1(2),'MarkerSize',8,'MarkerEdgeColor','k','MarkerFaceColor',[1 1 1].*0.75);
    legend(sprintf('R=%3.3f\n(p=%5f)',rho(1,2),pval(1,2)),'Location','northwest');
    xlabel('Actual VFI')
    ylabel('Predicted VFI based on Naka_Rushton fir','Interpreter','none')
axis ([0 100 0 100],'square')
shg
%% look at the OKN scores
