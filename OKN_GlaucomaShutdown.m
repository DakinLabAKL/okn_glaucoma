%OKN_GlaucomaShutdown based on PsychSandboxShutdown.m
%DrawFormattedText(E.WindowPointer, 'Closing the Sandbox...','center',E.PTBScreenRect(4)-E.Settings.InstructionOffset,E.White);
%Screen('Flip', E.WindowPointer);
try
    if ~iscell(E)
        E={E};
        P={P};
        S={S};
    end
catch
end
if  ~E{end}.Settings.CriticalError
    E{end}.Timing.TimeTestStopped       = datetime;                                         % human readable datetime
    E{end}.Timing.TimeTestDuration      = E{end}.Timing.TimeTestStopped-E{end}.Timing.TimeTestStarted;
    ShowCursor;
    if E{end}.Computer.EyeLinkConnected
        Eyelink('StopRecording');
        Eyelink('Shutdown');
        status = 'Eyetracker shutdown successful';
        if E{end}.Settings.FeedbackLevel == 2
            fprintf(1,'%s\n',status);
        end
    end
    
    if E{end}.Computer.EyeTribeConnected
        [~] = eyetribe_stop_recording(E{end}.EyetribeConnection);
        [~] = eyetribe_close(E{end}.EyetribeConnection);
        status = 'Eyetribe shutdown successful';
        system('taskkill /IM "cmd.exe"');
        if E{end}.Settings.FeedbackLevel == 2
            fprintf(1,'%s\n',status);
        end
    end
    
    if E{end}.Computer.TobiiConnected
        E{end}.Settings.Tobii.ET.stop_gaze_data();
        E{end}.Computer.TobiiConnected = 0;
        if E{end}.Settings.FeedbackLevel == 2
            fprintf(1,'Tobii Disconnected\n');
        end
    end
    
    Screen('CloseAll');
    commandwindow;
    
    E{end}.Folders.SaveFileName=sprintf('%s_%s.mat',E{end}.SubjectName,E{end}.Timing.DateSaveString);
    if E{end}.Settings.DebugMode
        MemoryUsageByTrial = table(E{end}.Debugging.MemoryArray(:,1), E{end}.Debugging.MemoryArray(:,2), E{end}.Debugging.MemoryArray(:,3), 'VariableNames', {'E', 'P', 'S'});
        fullSaveFileName = fullfile(E{end}.Folders.TempSaveRoot, E{end}.Folders.SaveFileName);
        if E{end}.Computer.IsJavaAvailable
            shouldWeSave = questdlg(sprintf('Save %s to %s?', E{end}.Folders.SaveFileName, fullSaveFileName(1:end-length(E{end}.Folders.SaveFileName))), 'Save file?', 'Yes');
        else
            responses={'No' 'Yes'};
            shouldWeSave =responses{1+DefInput(sprintf('Save %s to %s (0/1)?', E{end}.Folders.SaveFileName, fullSaveFileName(1:end-length(E{end}.Folders.SaveFileName))),1)};
        end
        if (strcmpi(shouldWeSave, 'Yes'))
            save(fullSaveFileName, 'E', 'P', 'S');
        end
    else
        fullSaveFileName = fullfile(E{end}.Folders.DataSaveRoot, 'Data', E{end}.Folders.SaveFileName);
        save(fullSaveFileName, 'E', 'P', 'S');
        
        BackupfullSaveFileName = fullfile('D:','OKN_GlaucomaData',E{end}.Folders.SaveFileName);
        if exist(fullfile('D:','OKN_GlaucomaData'),'dir')
            save(BackupfullSaveFileName, 'E', 'P', 'S');
        end
        GoogleDriveSaveFileName = fullfile('C:\Users\DakinLab\Google Drive\GOKN_Data',E{end}.Folders.SaveFileName);
        if exist(fullfile('C:\Users\DakinLab\Google Drive\GOKN_Data'),'dir')
            save(GoogleDriveSaveFileName, 'E', 'P', 'S');
        end
        
        
    end
    
else % EmergencySave - set in critical catch loops
    if exist('therr','var')
        rethrow(therr)
    end
    sca;
    fullSaveFile = fullfile(E{end}.Folders.TempSaveRoot,strjoin({E{end}.DateSaveString,'DUMP.mat'},''));
    save(fullSaveFile); %just dump everything
    fprintf(1,'\nERROR: File output saved to %s\n', fullSaveFile);
end