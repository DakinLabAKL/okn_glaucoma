function [E] = OKN_GlaucomaResetRecordedValues(E,P)

intLe           = P.InterList(P.CurrentTrialNo);
intLeTrial      = P.TrialNumbers(P.CurrentTrialNo);
% is initialised in PSB update as E.EyePos.X = zeros(P.NoInterleaved, max(P.NoFixedTrialsPerInter), E.Settings.MaxSamplesPerTrial,2);
E.EyePos.X(intLe,intLeTrial,:,:)          = 0;
E.EyePos.Y(intLe,intLeTrial,:,:)          = 0;
E.EyePos.PA(intLe,intLeTrial,:,:)         = 0;
E.EyePos.Time(intLe,intLeTrial,:,:)       = 0;
E.EyePos.OverSample(intLe,intLeTrial,:,:) = 0;
% is initialised in PSB update as E.Window.FrameCount             = zeros(P.NoInterleaved,max(P.NoFixedTrialsPerInter));
E.Window.FrameCount(intLe,intLeTrial)     = 0;
E.SamplesPerTrial(intLe,intLeTrial)       = 0;

% is initialised in PSB update as E.Window.InterpolatedFrames     = zeros(P.NoInterleaved,max(P.NoFixedTrialsPerInter),E.Settings.MaxFramesPerTrial);
E.Window.InterpolatedFrames(intLe,intLeTrial,:) = 0;
E.Window.FrameFlipTime(intLe,intLeTrial,:)      = 0;
E.Window.FrameDelta(intLe,intLeTrial,:)         = 0;
