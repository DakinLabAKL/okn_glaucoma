%% use tobii feedback to check retinal adaptation
clearvars;
close all;
try
screenUsed = 2;
[DisplayWidthPix, DisplayHightPix] = Screen('WindowSize', screenUsed); % width and height of display in mm
PsychImaging('PrepareConfiguration');
PsychImaging('AddTask', 'General', 'FloatingPoint32Bit');
PsychImaging('AddTask', 'FinalFormatting', 'DisplayColorCorrection', 'SimpleGamma');
[win, winRect]= PsychImaging('OpenWindow', screenUsed, 128, [0 0 DisplayWidthPix DisplayHightPix]);
HideCursor;
GamVal          = 0.54; % calibrated for S2817Q under game mode, (75% brightness and 75% contrast)
PsychColorCorrection('SetEncodingGamma', win, GamVal );

keepgoing =1;
commandwindow;

ScalingRatio = 1;
ImageWidth = round(DisplayWidthPix/ScalingRatio);
ImageHeight = round(DisplayHightPix/ScalingRatio);

DisplayHeightDeg = atand(53/67);
StimFreqCPD = 0.1; % 1/38;
StimFreqCPI = StimFreqCPD.* DisplayHeightDeg;
Multiplyer = 2;
Contrast = 0.25; 
lambdaPix  = DisplayHightPix./StimFreqCPD;  %S.FreqPeak
Smoothing  = 1.*lambdaPix; %
srcNoise   = randn(ImageHeight,ImageWidth);
stimIm   = 0.5+(NormImage(real(DoLogGabor(srcNoise,StimFreqCPI*Multiplyer,0.5,0,10000)),0,0.5));% DegToRad(2)
[imM, imSd]=LocalStat(stimIm,Smoothing);
image=0.5+NormImage((stimIm-imM)./imSd,0,0.5*Contrast);
% stat(image)
% image=repmat(imresize((1.*rand(ImageHeight/8,ImageWidth/8)),8,'nearest'),[2 2]);

% image = double(stimImIn);
% image = imresize(imread('cameraman.tif'),[ImageWidth*Multiplyer ImageHeight*Multiplyer]);
imtext = Screen('MakeTexture', win, image, [], [], 2);
Sourcerect = [0 0  ImageWidth ImageHeight];
% DestRect =  CenterRectOnPoint([0 0 DisplayWidthPix*Multiplyer DisplayHightPix*Multiplyer], DisplayWidthPix/2 ,DisplayHightPix/2);
cntr = 1; % number of flips counter

Tobii.ET          = EyeTrackingOperations().find_all_eyetrackers;
likely_licence_filename = sprintf('*University_of_Auckland_%s',Tobii.ET.SerialNumber);
Tobii.licenses_path       = dir(fullfile(FindGitFolder,'psychsandbox','TobiiLicences',likely_licence_filename));
fileID              = fopen(Tobii.licenses_path(1).name,'r');
input_licenses(1)   = LicenseKey(fread(fileID));
fclose(fileID);
failed_licenses     = Tobii.ET.apply_licenses(input_licenses); % how long does this persist?
LastFlipTime(1) = Screen('Flip' , win);
%try       
TobiixEyePos(cntr,:) = [0 0];
TobiiyEyePos(cntr,:) = [0 0];
cntr=2;

while keepgoing
    Screen('BlendFunction', win, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    % the destination rect in which we flip the Stim
    
    [ keyIsDoqwn, seconds, keyCode ] = KbCheck(-1);
    
   
    TobiiSample = Tobii.ET.get_gaze_data('flat');

%     while isempty(TobiiSample.left_gaze_point_on_display_area)
%         TobiiSample = Tobii.ET.get_gaze_data('flat');
%         WaitSecs(0.001);
%     end
   if isempty(TobiiSample.left_gaze_point_on_display_area)
             TobiixEyePos(cntr,:) = TobiixEyePos(cntr-1,:);
             TobiiyEyePos(cntr,:) =  TobiiyEyePos(cntr-1,:);
         else
    TobiixEyePos(cntr,:) = [TobiiSample.left_gaze_point_on_display_area(end,1),TobiiSample.right_gaze_point_on_display_area(end,1)].*DisplayWidthPix;
    TobiiyEyePos(cntr,:) = [TobiiSample.left_gaze_point_on_display_area(end,2),TobiiSample.right_gaze_point_on_display_area(end,2)].*DisplayHightPix;
%     xEyePos(xEyePos<0) =[];
%     yEyePos(yEyePos<0) =[];
    EyeX = double(nanmean(TobiixEyePos(cntr,:)));
    EyeY = double(nanmean(TobiiyEyePos(cntr,:)));
    % TobiixEyePos(cntr,:) = xEyePos;
    % TobiiyEyePos(cntr,:) = yEyePos;
    % ImageHeight,ImageWidth DisplayWidthPix DisplayHightPix
   end
    if ~isnan(EyeX) && ~isnan(EyeY)
        DestRect = CenterRectOnPoint([0 0 DisplayWidthPix*Multiplyer DisplayHightPix*Multiplyer], EyeX, EyeY);
    else
        DestRect = CenterRectOnPoint([0 0 DisplayWidthPix*Multiplyer DisplayHightPix*Multiplyer], DisplayWidthPix/2, DisplayHightPix/2);
    end
    Screen('DrawTexture', win,imtext,[] ,DestRect);   % S.DestRect % draw the mask over stimulus
    LastFlipTime(cntr) = Screen('Flip',win);%     KbReleaseWait;
        cntr = cntr+1;

    if keyCode(KbName('q')) % KbName(keyCode)== 'q' % quit
        keepgoing =0;
    end
    
end
KbWait;

sca

d1=1./diff(LastFlipTime);
d1(d1<0)=0;
catch
    sca
end
%%
figure(1),clf;plot(1:length(d1),d1,'b-o',find(d1<20),d1(d1<20),'ro');
