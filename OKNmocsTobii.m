% OKNmocsTobii
% Soheil M Doustkouhi

%% use tobii feedback to check retinal adaptation
clearvars;
close all;
screenUsed = 0;
[DisplayWidthPix, DisplayHightPix] = Screen('WindowSize', screenUsed); % width and height of display in mm
% PsychImaging('PrepareConfiguration');
% PsychImaging('AddTask', 'General', 'FloatingPoint32Bit');
% PsychImaging('AddTask', 'FinalFormatting', 'DisplayColorCorrection', 'SimpleGamma');
[win, winRect]= PsychImaging('OpenWindow', screenUsed, 128, [0 0 DisplayWidthPix DisplayHightPix]);
HideCursor;
% GamVal          = 0.54; % calibrated for S2817Q under game mode, (75% brightness and 75% contrast)
% PsychColorCorrection('SetEncodingGamma', win, GamVal );

keepgoing =1;
commandwindow;

ScalingRatio = 1;
ImageWidth = round(DisplayWidthPix/ScalingRatio);
ImageHeight = round(DisplayHightPix/ScalingRatio);
BaseRect=[0 0 ImageWidth ImageHeight];

DisplayHeightDeg = atand(53/67);
StimFreqCPD = 0.1; % 1/38;
StimFreqCPI = StimFreqCPD.* DisplayHeightDeg;
Multiplyer = 1;
offX=0;
offY=0;
JumpSizeX = 0;
JumpSizeY = -10;
% pre-allocating memory
%   TobiixEyePos = zeros(1500,2);
%   TobiiyEyePos = zeros(1500,2);
Contrast = 1;
lambdaPix  = DisplayHightPix./StimFreqCPD;  %S.FreqPeak
Smoothing  = 1.*lambdaPix; %
srcNoise   = randn(ImageHeight,ImageWidth);
StimIm   = 0.5+(NormImage(real(DoLogGabor(srcNoise,StimFreqCPI*Multiplyer,0.5,0,10000)),0,0.5));% DegToRad(2)
stimIm = repmat(StimIm,[2, 2]);
[imM, imSd]=LocalStat(stimIm,Smoothing);
image=0.5+NormImage((stimIm-imM)./imSd,0,0.5*Contrast);
% stat(image)
% image=repmat(imresize((1.*rand(ImageHeight/8,ImageWidth/8)),8,'nearest'),[2 2]);
imtext = Screen('MakeTexture', win, image, [], [], 2);
Sourcerect = [0 0  ImageWidth ImageHeight];
% DestRect =  CenterRectOnPoint([0 0 DisplayWidthPix*Multiplyer DisplayHightPix*Multiplyer], DisplayWidthPix/2 ,DisplayHightPix/2);
Tobii.ET          = EyeTrackingOperations().find_all_eyetrackers;
likely_licence_filename = sprintf('*University_of_Auckland_%s',Tobii.ET.SerialNumber);
Tobii.licenses_path       = dir(fullfile(FindGitFolder,'psychsandbox','TobiiLicences',likely_licence_filename));
fileID              = fopen(Tobii.licenses_path(1).name,'r');
input_licenses(1)   = LicenseKey(fread(fileID));
fclose(fileID);
failed_licenses     = Tobii.ET.apply_licenses(input_licenses); % how long does this persist?

cntr=1; % number of flips counter
LastFlipTime(1) = Screen('Flip' , win);
%try
TobiixEyePos(cntr,:) = [0 0];
TobiiyEyePos(cntr,:) = [0 0];
cntr=2;
while keepgoing
    Screen('BlendFunction', win, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    % the destination rect in which we flip the Stim
    
    [ keyIsDoqwn, seconds, keyCode ] = KbCheck(-1);
    if 1
        TobiiSample = Tobii.ET.get_gaze_data('flat');
        
        % while isempty(TobiiSample.left_gaze_point_on_display_area)
        %     TobiiSample = Tobii.ET.get_gaze_data('flat');
        %      WaitSecs(0.001);
        %   end
        if isempty(TobiiSample.left_gaze_point_on_display_area)
            TobiixEyePos(cntr,:) = TobiixEyePos(cntr-1,:);
            TobiiyEyePos(cntr,:) =  TobiiyEyePos(cntr-1,:);
        else
            xEyePos = [TobiiSample.left_gaze_point_on_display_area(end,1),TobiiSample.right_gaze_point_on_display_area(end,1)].*DisplayWidthPix;
            yEyePos = [TobiiSample.left_gaze_point_on_display_area(end,2),TobiiSample.right_gaze_point_on_display_area(end,2)].*DisplayHightPix;
            %             xEyePos(xEyePos<0) =[];
            %             yEyePos(yEyePos<0) =[];
            %             EyeX = double(nanmean(xEyePos));
            %             EyeY = double(nanmean(yEyePos));
            TobiixEyePos(cntr,:) = double(xEyePos);
            TobiiyEyePos(cntr,:) = double(yEyePos);
        end
    end
    srcRect = OffsetRect(BaseRect, offX, offY);
    
    DestRect = CenterRectOnPoint([0 0 DisplayWidthPix*Multiplyer DisplayHightPix*Multiplyer], DisplayWidthPix/2, DisplayHightPix/2);
    Screen('DrawTexture', win,imtext,srcRect ,DestRect);   % S.DestRect % draw the mask over stimulus
    LastFlipTime(cntr) = Screen('Flip',win);%     KbReleaseWait;
    offX = mod((offX+JumpSizeX)-1,ImageWidth)+1;
    offY = mod((offY+JumpSizeY)-1,ImageHeight)+1;
    cntr = cntr+1;
    
    %     NoQuit=~keyIsDown;
    if keyCode(KbName('q')) % KbName(keyCode)== 'q' % quit
        keepgoing =0;
    end
    
end
KbWait;

sca

d1=1./diff(LastFlipTime);
d1(d1<0)=0;
% catch
%     sca
% end
%% tming data
figure(1),clf;plot(1:length(d1),d1,'b.-',find(d1<20),d1(d1<20),'ro');
title('with Tobii')
xlabel('frames');ylabel('Frame Rate Hz')
axis([0 length(d1) 0 max(d1)])
legend('all frames','dropped frames')
DropedFrames = find(d1<20);
theDiff = diff(DropedFrames);
mode(theDiff(theDiff>2))

%% plot eye position

TobiixEyePos = squeeze(TobiixEyePos(:,1));
TobiiyEyePos = squeeze(TobiiyEyePos(:,1));

figure(10),plot(1:length(TobiixEyePos),TobiixEyePos,'b.-'),ylim([0 DisplayWidthPix]);

figure(11),plot(1:length(TobiiyEyePos),TobiiyEyePos,'b.-'),ylim([0 DisplayHightPix])
Speedx = diff(TobiixEyePos);
Track = Speedx(Speedx>0);
figure,plot(Track)
order = 3; framelen= 5;
SGolayfiltX = sgolayfilt(double(TobiixEyePos),order,framelen);
figure,plot(1:length(TobiixEyePos),TobiixEyePos,'b:',1:length(SGolayfiltX),SGolayfiltX,'r-')

SGolSpeed = diff(SGolayfiltX);
SGolTrack = SGolSpeed(SGolSpeed>0);

Sporder = 3; SPframelen=21;
SGolayfiltSpeedx = sgolayfilt(double(SGolSpeed),Sporder,SPframelen);
SGolTrackFilted = SGolayfiltSpeedx(SGolayfiltSpeedx>0);
figure,plot(1:length(Track),Track,'k.-',1:length(SGolTrack),SGolTrack,'b:',1:length(SGolTrackFilted),SGolTrackFilted,'r-')

mean(Track)
mean(SGolTrack)

%% check for up or down by manual eye movement
TobiiSample = Tobii.ET.get_gaze_data('flat');

xEyePos = [TobiiSample.left_gaze_point_on_display_area(:,1),TobiiSample.right_gaze_point_on_display_area(:,1)].*DisplayHightPix;
yEyePos = [TobiiSample.left_gaze_point_on_display_area(:,2),TobiiSample.right_gaze_point_on_display_area(:,2)].*DisplayHightPix;
figure,plot(xEyePos)
figure,plot(yEyePos)
% results: the zero of gaze on screen is top-right of the screen;
