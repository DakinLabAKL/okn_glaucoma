

numETSamps =uint8(0);
E.Settings.EachBaseTrialTotalColors = 6;
EachColorLengthFrames = E.Settings.MaxFramesPerBaseTrial / E.Settings.EachBaseTrialTotalColors;
P.BasePresentaionOrder(P.BaseTrialNo,:) = Shuffle(1:E.Settings.EachBaseTrialTotalColors);
ThisBaseTrialOrder = P.BasePresentaionOrder(P.BaseTrialNo,:);
ThisBaseTrialOrderOfFramesEnd = EachColorLengthFrames.*ThisBaseTrialOrder;
ThisBaseTrialOrderOfFramesStart = ThisBaseTrialOrderOfFramesEnd - (EachColorLengthFrames-1);

for frameloop = 1 : E.Settings.MaxFramesPerBaseTrial
    if ismember(frameloop,ThisBaseTrialOrderOfFramesStart(1):ThisBaseTrialOrderOfFramesEnd(1)) || ismember(frameloop,ThisBaseTrialOrderOfFramesStart(2):ThisBaseTrialOrderOfFramesEnd(2))
        Screen('FillRect', E.Window.Pointer, E.Window.Black, E.Window.PTBScreenRect);
    elseif ismember(frameloop,ThisBaseTrialOrderOfFramesStart(3):ThisBaseTrialOrderOfFramesEnd(3)) || ismember(frameloop,ThisBaseTrialOrderOfFramesStart(4):ThisBaseTrialOrderOfFramesEnd(4))
        Screen('FillRect', E.Window.Pointer, E.Window.Gray, E.Window.PTBScreenRect);
    elseif  ismember(frameloop,ThisBaseTrialOrderOfFramesStart(5):ThisBaseTrialOrderOfFramesEnd(5)) || ismember(frameloop,ThisBaseTrialOrderOfFramesStart(6):ThisBaseTrialOrderOfFramesEnd(6))
        Screen('FillRect', E.Window.Pointer, E.Window.White, E.Window.PTBScreenRect);
        
    end
    sampleCount     = E.SamplesPerBaseTrial(P.BaseTrialNo);
    E.Window.FrameCountBase(P.BaseTrialNo) = E.Window.FrameCountBase(P.BaseTrialNo) + 1;
    frameThisTrial  = E.Window.FrameCountBase(P.BaseTrialNo);
    % flip the rect
    entranceTime =  GetSecs;
    if frameThisTrial == 1 %the first frame of a trial has to be good
        E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer); % ok if halting, as first
        E.Window.LastFrameDelta = 1;
    else
        E.Window.LastFrameDelta = 1;
        if entranceTime < (E.Window.LastFlipTime + (E.Window.InterFrameInterval * E.Settings.TimingTolerance)) % If have enough time
            E.Window.LastFlipTime = Screen('Flip' , E.Window.Pointer, (E.Window.LastFlipTime + (1-E.Settings.TimingTolerance) * E.Window.InterFrameInterval)); % here we flip the stimulus to the screen E.Timing.EyetrackingRiskRate
        else % if we don't have enough time
            E.Window.LastFrameDelta = E.Window.LastFrameDelta + 1;
            E.Window.InterpolatedFrames(P.BaseTrialNo,frameThisTrial) = 1; %E.Window.LastFrameDelta-1;
            E.Window.LastFlipTime = E.Window.LastFlipTime + (E.Window.InterFrameInterval * E.Settings.TimingTolerance);
        end
    end% get the Eyelink valuse
    exitTime         = E.Window.LastFlipTime + (E.Window.InterFrameInterval*E.Timing.EyetrackingRiskRate);
    timeNow          = GetSecs;
    
    while  timeNow < exitTime && E.Settings.EyetrackingOn
        if strcmpi(E.Eyetracker,'EyeLink') && E.Computer.EyeLinkConnected% EL is setup and wanted
            try %to get a EyeLink Sample
                if Eyelink('NewFloatSampleAvailable') > 0
                    evt                         = Eyelink('NewestFloatSample');
                    sampleCount = sampleCount + 1;
                    E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)        = single(evt.gx);
                    E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:)        = single(evt.gy);
                    E.BaseEyePos.PA(P.BaseTrialNo,sampleCount,:)       = single(evt.pa);
                    E.BaseEyePos.ResXY(P.BaseTrialNo,sampleCount,:)    = single([evt.rx, evt.ry]);
                    E.BaseEyePos.Time(P.BaseTrialNo,sampleCount)       = evt.time;
                    status = sprintf('OK');
                    E.LastBaseEyePos = [single(evt.gx), single(evt.gy)];
                    numETSamps = numETSamps + 1;
                    E.BaseEyePos.OverSample(P.BaseTrialNo,frameThisTrial,numETSamps)       = numETSamps; % timestamp is returned

                end
            catch  % graceful failure if EyeLink fails for some reason?
                sampleCount = sampleCount + 1;
                E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)        = single(NaN);
                E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:)        = single(NaN);
                E.BaseEyePos.PA(P.BaseTrialNo,sampleCount,:)       = single(NaN);
                E.BaseEyePos.ResXY(P.BaseTrialNo,sampleCount,:)    = single(NaN);
                E.BaseEyePos.Time(P.BaseTrialNo,sampleCount)       = timeNow;
                E.LastBaseEyePos = [NaN NaN];
                status = sprintf('No Sample');
            end
        elseif strcmpi(E.Eyetracker,'SimEyelink')% ET wanted, but not connected = simulate
            sampleCount = sampleCount + 1;
            E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)        = single(E.Window.PTBScreenWidth*rand([1,2]));
            E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:)        = single(E.Window.PTBScreenHeight*rand([1,2]));
            E.BaseEyePos.PA(P.BaseTrialNo,sampleCount,:)       = single(500*rand([1,2]));
            E.BaseEyePos.ResXY(P.BaseTrialNo,sampleCount,:)    = single(25+2*rand([1,2]));
            E.BaseEyePos.Time(P.BaseTrialNo,sampleCount)       = timeNow;
            E.LastBaseEyePos = [squeeze(E.BaseEyePos.X(P.BaseTrialNo,sampleCount,:)),squeeze(E.BaseEyePos.Y(P.BaseTrialNo,sampleCount,:))];
            status = sprintf('OK');
            WaitSecs(1/E.Settings.EyetrackingSampleRate); % simulate ?NewFloatAvailable.
            numETSamps = numETSamps + 1;
            E.BaseEyePos.OverSample(P.BaseTrialNo,sampleCount,frameThisTrial,numETSamps)       = numETSamps;
        else %Eyetracker requested but not recognised?
            E.Settings.CriticalError = 1;
        end
           timeNow = GetSecs;  % Keep calm and carry on
    end
    E.SamplesPerBaseTrial(P.BaseTrialNo) = sampleCount;
    E = PsychSandboxGetKeyboardAndMouse(E);
end




snapnow;