% OKN processing Analysis

clearvars;
close all;
ptbRoot = Find1PTB;

% change directory to your experimental data directory
RootDir = fullfile(ptbRoot,'Data','OKNMOCS','VFL');


% ObserverName = {'PRT_108t_M4_FC'  'PRT_108t_M4_12c'  'SKH_108t_M4_FC'  'SKH_108t_M4_12c'  'SMD_108t_M4_FC'  'SMD_108t_M4_12c' 'LGM_108t_M4_FC'  'LGM_108t_M4_12c'...
%                 'MLE_108t_M4_FC'  'MLE_108t_M4_12c'  'ALL_108t_M4_FC'  'ALL_108t_M4_12c'  };
ObserverName = { 'SMD_VFL_SET3'};  %  'SMD_VFL_SET2' 'ALM_VFL_SET1' 'ALM_VFL_SET2' 'TYG_VFL_SET1' 'TYG_VFL_SET2'};% SMD_VFL_Pupil'SCD' 'SMD'}; % SCD' 'SMD'};%'SMD_VFL_SmplCntr'};%'SCD_VFLRp'};%'SMD_VFL_SmplCntr'};% SCD_VFLRp'};%'SMD_VFL_SmplCntr'}; % 'SMD_VFLRp' 'LMH_VFLRp'  SMD_Black SMD_BWOKN

% 
% ObserverName = { 'SMD_VFL_SET1' 'SMD_VFL_SET2' 'SCD_VFL_SET1' 'SCD_VFL_SET2' 'ALM_VFL_SET1' 'ALM_VFL_SET2' 'TYG_VFL_SET1' 'TYG_VFL_SET2'  'PRT_VFL_SET1' ...
%                  'PRT_VFL_SET2'  'ALL_VFL_SET1' 'ALL_VFL_SET2' 'SKH_VFL_SET1' 'SKH_VFL_SET2' 'SNS_VFL_SET1' 'SNS_VFL_SET2'  'SMS_VFL_SET1' 'SMS_VFL_SET2' ...
%                  'JMD_VFL_SET1' 'JMD_VFL_SET2' }; % 'HNK_VFL_SET1' 'HNK_VFL_SET2'  'JNY_VFL_SET1' 'JNY_VFL_SET2'}; %  'HNK_VFL_SET1' 'HNK_VFL_SET2'};

EyeList       = 'LR';
% TheRuns       = {1 2}; We have one or two runs of 288 trials for four contrast levelsE.Settings.BaseContrastLevels
TheConditions = 2; % 2AFC
conLoop       = 1;
eyeLoop       = 2; % 2: right eye

MaxSaccSpeedDegPerSec = 500; % fixed - determined by minimisation
paStdThresh           = 2.75; %  used to find and remove blinks
FrameMargin           = 12; %12; % number of eye tracking samples to disgard either side of a blink. was 5
SSpdWS                = 4; % window in milisecond, after and before each point to calculate sliding window speed.
JustFirstSample        =1; % 1= use just the first ET sample per each frame
IVvalues= [1];
IVloop=1;
% for IVloop=1:length(IVvalues)



for subLoop  = 1:length(ObserverName) % subjects
    clear fNameString TheName
    % load up the this subject name files
    fNameString  = sprintf('%s%c%s_%c*.mat',RootDir,filesep,ObserverName{subLoop},EyeList(eyeLoop));
    AllFiles    = dir(fNameString);
    TheRuns(subLoop) = numel({AllFiles.name}); % will find how many runs we have for this subject
    for runLoop = 1:TheRuns(subLoop)
        TheName     = sprintf('%s%c%s',RootDir,filesep,AllFiles(runLoop).name);
        load(TheName);
        if iscell(E)
            E = E{1};
            P = P{1};
            S = S{1};
        end
        E.Settings.CovdRatio = round(S.StimulusLength(1)./(E.Settings.CoveredFrames.*E.Window.InterFrameInterval)+0); %  add 1 if you want to make it bigger to be safe
        S.NewSamplingSepMS   = 0;
        if E.Settings.ExperimentSet ==1
            Set1NoMasks = length(P.PossMaskValues{1});
            Set1NoContLevels = length(P.PossContrastValues{1});
        elseif  E.Settings.ExperimentSet ==2
            Set2NoMasks = length(P.PossMaskValues{1});
            Set2NoContLevels = length(P.PossContrastValues{1});
        elseif  E.Settings.ExperimentSet ==3
            Set3NoMasks = length(P.PossMaskValues{1});
            Set3NoContLevels = length(P.PossContrastValues{1});
        end
        
       ThisConditionTotalTrial{subLoop} = P.TotalTrials; 
        E.plotting  = 1; % 1 means plot OKN signal corresponding to desired stimulus 2 means plot the line fit as well
        
        %         eyeLoop = E.WhichEye; % 2: is the right eye
        whichEye = E.WhichEye;
        JustFirstBaseSample = 1; % 1= use just the first sample of pupil measurements
        % Analysis baseLine pupil measurement
        if E.Settings.ifPAbaseMeasure 
        for BaseTrialLoop = 1: E.Settings.BaseTotalTrialNo
            BaselinePupilTimeInMS = E.BaseEyePos.Time(BaseTrialLoop,:);
            BaselinePupilAreaIn = E.BaseEyePos.PA(BaseTrialLoop,:,whichEye);
            BaselinexDataIn         = (E.BaseEyePos.X(BaseTrialLoop, :, whichEye));
            BaselineyDataInvIn      = (E.BaseEyePos.Y(BaseTrialLoop, :, whichEye));
            BaselineyDataIn         = -(BaselineyDataInvIn(:) - E.Window.PTBScreenHeight);% turn the Y direction from screen coordiante to mathematic coordinate. if this line is in the code, make sure that in dirdiff the angles are in correct order which should be [0 pi/2 pi 3pi/2]
            %             BaselineFrameFlipDataIn = (E.Window.BaseFrameFlipTime(BaseTrialLoop, :));
            
            BaseLinePupilMeasurementAnalysis
            AllBaselineMaxPupilAreastore{subLoop} = BaselienMaxPupilAreaMean(BaseTrialLoop);
        end
        end
        % Analysis of OKN main experiment
        for interLoop=1:P.NoInterleaved % for directional trials individually 1=Right, 2=Left
            E.interLoop = interLoop;
            PossStimValues=log10(P.PossStimValues{interLoop}); %back to 'logMAR' type scale
            PossMaskValues = P.PossMaskValues{interLoop};
            for stimLoop = 1:numel(PossStimValues) % add 1: for each SF individually
                S.stimLoop = stimLoop;
                for maskLoop = 1:numel(PossMaskValues)  % 1: not just the biggest proportion of visible stimulus
                    for contrastLoop =1:numel(E.Settings.BaseContrastLevels) % 1: contrast levels
                        TheIndices=find((P.InterList==interLoop) & (P.MaskInds==maskLoop) & (P.ContrastInds==contrastLoop)); %(P.StimInds==stimLoop) &
                        P.RespList(interLoop,stimLoop,contrastLoop,maskLoop)=sum(P.Response(TheIndices));
                        P.NoPresList(interLoop,stimLoop,contrastLoop,maskLoop)=length(P.Response(TheIndices));
                        LocalTrialNo=P.TrialNumbers(TheIndices);
                        for LocTrialLoop=1:length(LocalTrialNo) %1:
                            
                            xDataIn         = squeeze(E.EyePos.X(interLoop, LocalTrialNo(LocTrialLoop), :, whichEye));
                            yDataInvIn      = squeeze(E.EyePos.Y(interLoop, LocalTrialNo(LocTrialLoop), :, whichEye));
                            yDataIn         = -(yDataInvIn(:) - E.Window.PTBScreenHeight);% turn the Y direction from screen coordiante to mathematic coordinate. if this line is in the code, make sure that in dirdiff the angles are in correct order which should be [0 pi/2 pi 3pi/2]
                            FrameFlipDataIn = squeeze(E.Window.FrameFlipTime(interLoop, LocalTrialNo(LocTrialLoop), :)); % keep the frame flip time for later processing
                            paDataIn      = squeeze(E.EyePos.PA(interLoop,  LocalTrialNo(LocTrialLoop),   :, whichEye));  %_SMD pa: pupil area
                            tDataIn       = squeeze(E.EyePos.Time(interLoop,LocalTrialNo(LocTrialLoop),   :));
                            
                            StimulusDirection = DegToRad(E.Settings.StimulusDirection(interLoop));
                            
                            % %                             for i = 1: P.
                            %                             BaselinePupilTimeInMS = E.BaseEyePos.Time(2,:);
                            %                             BaselinePupilArea = E.BaseEyePos.PA(2,:,2);
                            %                             goodVals = find(BaselinePupilTimeInMS>0);
                            %                             PupilTimeInMS=BaselinePupilTimeInMS(goodVals);
                            %                             PupilArea = BaselinePupilArea(goodVals);
                            %                             figure(11), plot(PupilTimeInMS,PupilArea,'ro' )
                            
                            %                             MySamples = EachFrameSamplesIn';
                            %                             MySamples = (MySamples(MySamples>0))';
                            %                             MyFirstSampleInds = find(MySamples==1);
                            %                             size(xDataIn(xDataIn>0));
                            %                             Mytime = (tDataIn(tDataIn>0))';
                            %                             MyFirstSampletime = Mytime(MyFirstSampleInds);
                            %                             figure,plot(Mytime,Mytime,'b.',MyFirstSampletime,MyFirstSampletime,'ro')
                            %
                            %                             figure,plot(1./diff(MyFirstSampleInds/1000),'bo')
                            
                            %                         SSpdWS = 10; % Slide Speed window size
                            
                            SamplingRate =IVvalues(IVloop);
                            %500; % 800 deg
                            fig = figure(99);
                            clf;
                            OKNprocessingCore2; %NoInterp;  % for optimization
                            %                              OKNprocessingCoreNoInterp;
                            
                            % OKNOptimprocessing2; %the original one  calculate actual visible proportion of the stimulus
                            if ~isfield(S,'FovRegDiameter')
                                S.FovRegDiameter = 5 ; % 2.5 deg diameter of central visible circle
                            end
                            %
                            FovRegDiameter{subLoop} = S.FovRegDiameter;
                            if isfield(P,'ActualVisibleRatio')
                                AllVisblRatio{subLoop} = P.ActualVisibleRatio;
                            else
                                DisplayAreaDeg =  E.Window.DisplayHeightDeg*E.Window.DisplayWidthDeg;
                                FovRegAreaDeg = pi.*(S.FovRegDiameter./2)^2;
                                
                                ActualVisblRatio = P.VisblRatio-(P.VisblRatio.*(FovRegAreaDeg./DisplayAreaDeg))+(FovRegAreaDeg./DisplayAreaDeg);
                                AllVisblRatio{subLoop} = ActualVisblRatio;
                                
                            end
                            %plotting
                            if 1%E.plotting % TheIndices(LocTrialLoop)==259%
                                if  HistGain>1 %0.5 % (maskLoop==9)%  && (contrastLoop==4) %&&SldGain<0.3 OKNscore==0 %(subLoop==3)% &(abs(OKNGain) <0.4)& % ismember(maskLoop, [1 5 9 ] ) && ismember(LocTrialLoop, [ 1 8])  && ismember(interLoop,[1 2])%|| stimLoop == 5
                                    
                                    BelowThresh = find(Speed<Threshold);
                                    AboveThresh = find(Speed>=Threshold);
                                    PursOrNot = Speed<Threshold;
                                    
                                    SeqBelowThresh = find((PursSpeed) < Threshold);
                                    SeqAboveThresh = find((PursSpeed) >= Threshold);
                                    
                                    SpeedDb = 20.*log10(Speed);
                                    xDallDb = 20.*log10(abs(xDall));
                                    yDallDb = 20.*log10(abs(yDall));
                                    
                                    PursSpeedDb = 20.*log10(PursSpeed);
                                    
                                    if ~isempty(Speed)
                                        maxSpdVal = max(Speed);
                                    else
                                        maxSpdVal = 1;
                                    end
                                    StimSpeedPlt = S.SpeedPixPerMS.*Tall;
                                    
                                    EstOffsetFromHist = Tall.*(SpeedFromHistPixPerSec/1000);
                                    
                                    subplot(2,4,1);plot( Tall,Xall,'b-',Tall(AboveThresh),Xall(AboveThresh),'r.',Tall,EstOffsetFromHist,'b-',Tall(BelowThresh),Xall(BelowThresh),'g.',Tall,StimSpeedPlt,'k-.',Tall,(StimSpeedPlt.*SldGain)+E.Window.PTBScreenWidthCenter,'m-.' );
                                    legend('Rotated Eye X Position', 'Saccade', 'Tracking', '10 deg/s', sprintf('SldGain %2.2f',SldGain), 'Location', 'Best')
                                    ylim([0 E.Window.PTBScreenWidth]),
                                    title(sprintf('Subject:%i %iAFC Contrast:%2.2f ; Mask:%i Inter:%i Repetition:%i   XData',subLoop, TheConditions(conLoop), E.Settings.BaseContrastLevels(contrastLoop)*100, maskLoop, interLoop , LocTrialLoop)); shg
                                    subplot(2,4,2);plot( Tall,Yall,'b-',Tall(AboveThresh),Yall(AboveThresh),'r.',Tall(BelowThresh),Yall(BelowThresh),'g.');
                                    title('YData');
                                    ylim([0 E.Window.PTBScreenHeight]),
                                    %                                     subplot(2,4,3);plot( xData0,yData0,'b-',xData00, yData00,'r.');
                                    %                                     title(sprintf('Unprocessed Eye Gaze on Screen (%2.0f deg) ',E.Settings.StimulusDirection(E.interLoop)));
                                    %                                     ylim([0 E.Window.PTBScreenHeight]),  xlim([0 E.Window.PTBScreenWidth]),
                                    %                                     legend('Original Eye Gaze', 'Rotated Eye Gaze')
                                    subplot(2,4,3);plot( Tall,Pall,'b-');
                                    title('Pupil area data');
                                    
                                    %                                     subplot(2,4,4),plot( Tall,Xall,'b-',Tall(PursStartInd),Xall(PursStartInd),'r*',Tall(PursEndInd),Xall(PursEndInd),'ks');ylim([0 E.Window.PTBScreenWidth]),
                                    %                                     legend('eye position', 'Star of Pursuit', 'End of Pursuit')
                                    subplot(2,4,5);plot( tDall,xDall,'b-',tDall(AboveThresh),xDall(AboveThresh),'r.',tDall(BelowThresh),xDall(BelowThresh),'g.',tDall,S.SpeedPixPerMS.*HistGain.*ones(size(tDall)),'K-');
                                    title('XSpeed'); legend('Speed','Saccade', 'Tracking',sprintf('Gain=%2.2f',HistGain)),ylim([-maxSpdVal maxSpdVal])
                                    subplot(2,4,6);plot(tDall,yDall,'b-',tDall(AboveThresh),yDall(AboveThresh),'r.',tDall(BelowThresh),yDall(BelowThresh),'g.',tDall,HistGain.*ones(size(tDall)),'K-');
                                    ylim([-maxSpdVal maxSpdVal]),title('YSpeed');
                                    subplot(2,4,7);plot(tDall,Speed,'b-',tDall(AboveThresh),Speed(AboveThresh),'r.',tDall(BelowThresh),Speed(BelowThresh),'g.',tDall,HistGain.*ones(size(tDall)),'K-');
                                    ylim([-maxSpdVal maxSpdVal]),title('Directional Speed');
                                    subplot(2,4,7);polarplot(SpeedAng,SpeedDb,'b.',SpeedAng(AboveThresh),SpeedDb(AboveThresh),'ro',SpeedAng(BelowThresh),SpeedDb(BelowThresh),'go')
                                    %                                     subplot(2,4,8),plot(2:length(tData),1./(diff(tData./1000)),'b.-'),
                                    %                                     title(sprintf('Mean first sample frequency %2.2f',mean(1./(diff(tData./1000)))))
                                    %                                     %                                     subplot(2,4,8);plot(1:length(tData0),tData0,'b.')
                                    %                                         polarplot(PursSpeedAng,PursSpeedDb,'b.',PursSpeedAng(SeqAboveThresh),PursSpeedDb(SeqAboveThresh),'ro',PursSpeedAng(SeqBelowThresh),PursSpeedDb(SeqBelowThresh),'go')
                                    set (fig, 'Units', 'normalized', 'Position', [0  0.1  1  0.8]); %  [1.5000    0.0778    0.7500    0.6718]);
                                    
                                    fprintf('Trial: %i; keyResponse: %i;  OKNscore: %i;  Threshold: %2.2f;  HistGain: %2.2f; SldGain: %2.2f; OKN Consistency: %2.2f  TotalDisRatio: %2.2f; ConsDistRatio: %2.2f\n',...
                                        TheIndices(LocTrialLoop), P.Response(TheIndices(LocTrialLoop)), OKNscore, Threshold, HistGain, SldGain, OKNConsistency, TotalDisRatio, ConsistentDistRatio);
                                    
                                    drawnow
                                    pause
                                end
                            end
                            
                            % saving the result
                            AllXstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Xall;
                            AllYstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Yall;
                            AllPstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Pall;
                            AllTstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}                  = Tall;
                            AllXSpdstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}               = xDall;
                            AllYSpdstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}               = yDall;
                            AllTSpdstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}               = tDall;
                            AllSpdVecstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = Speed;
                            AllSpdAngstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = SpeedAng;
                            AllThreshstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = Threshold;
                            AllPursuitstore{subLoop, conLoop,eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}             = PursuitOrSaccade;
                            AllOKNscorestore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}           = OKNscore;
                            AllOKNConsistencystore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}     = double(OKNConsistency);
                            AllOKNConsDistRatio{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}        = ConsistentDistRatio;
                            AllOKNgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}            = OKNGain;
                            AllOKNSldgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}         = SldGain;
                            AllConsOKNgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}        = ConsistentGainRec;
                            AllThetaVariRatiostore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}     = TheThetaVariablilityRatio;
                            AllTotDistRatiostore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}       = TotalDisRatio;
                            AllKeyRespstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}            = P.Response(TheIndices(LocTrialLoop));
                            AllMeanPupilAreastore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}      = double(PupilAreaFromHistPix);
                            AllHgainstore{subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop}           = HistGain;%SpeedFromHistDegPerSec/S.SpeedDegPerSec;
                        end
                    end
                end
            end
        end
    end
end
%%
stimLoop = 1;
eyeLoop  = 2 ;
%     conLoop  = 1;
interLoop = 1:P.NoInterleaved;
for subLoop  = 1:length(ObserverName)
    if mod(subLoop,2) == 0 % i.e. set2
        LengthofContrastLoop = Set2NoContLevels;
        maskLoop = 1:Set2NoMasks; % sould change the hard coding
    elseif mod(subLoop,2) == 1  % i.e. set1
        if ~exist('Set1NoContLevels','var')
            Set1NoContLevels = Set3NoContLevels;
            Set1NoMasks = Set3NoMasks;
        end
        LengthofContrastLoop = Set1NoContLevels;
        maskLoop = 1:Set1NoMasks;
    end
    for contrastLoop = 1:LengthofContrastLoop
        
        clear   OKNscoreList     KeyRespList    OKNscoreConsList   ConsOKNGainList   TheHgainList   ThePupilAreaList
        OKNscoreList=[];  KeyRespList=[];     OKNscoreConsList=[];      TheHgainList=[];    ThePupilAreaList=[];
        for runLoop = 1:(TheRuns(subLoop))
            for LocTrialLoop = 1: E.Settings.LocalTrialNo
                
                TheOKNscore      = (cell2mat(squeeze(AllOKNscorestore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                OKNscoreList     = [OKNscoreList; TheOKNscore];
                
                TheOKNscoreCons      = (cell2mat(squeeze(AllOKNConsistencystore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                OKNscoreConsList     = [OKNscoreConsList; TheOKNscoreCons];
                
                TheKeyResp       = (cell2mat(squeeze(AllKeyRespstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                KeyRespList      = [KeyRespList; TheKeyResp];
                
%                 TheConsDistRatio  = (cell2mat(squeeze(AllOKNConsDistRatio(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
%                 ConsDistRatioList = [ConsDistRatioList; TheConsDistRatio];
                
%                 TheConsOKNGain =  (cell2mat(squeeze(AllConsOKNgainstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
%                 ConsOKNGainList =  [ConsOKNGainList; TheConsOKNGain];
                
%                 TheThetaVariRatio =  (cell2mat(squeeze(AllThetaVariRatiostore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
%                 ThetaVariRatioList =  [ThetaVariRatioList; TheThetaVariRatio];
                
                TheHgain =  (cell2mat(squeeze(AllHgainstore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                TheHgainList =  [TheHgainList; TheHgain];
                
                ThePupilArea =  (cell2mat(squeeze(AllMeanPupilAreastore(subLoop, conLoop, eyeLoop, runLoop, interLoop, stimLoop, contrastLoop, maskLoop, LocTrialLoop))));
                ThePupilAreaList =  [ThePupilAreaList; ThePupilArea];
                
            end
            AllOKNscoreList{subLoop,contrastLoop}         = OKNscoreList;
            AllOKNscoreConsList{subLoop,contrastLoop}     = OKNscoreConsList;
%             AllOKNConsDistRatioList{subLoop,contrastLoop} = ConsDistRatioList;
            AllKeyRespList{subLoop,contrastLoop}          = KeyRespList;
%             AllTotDistRatioList{subLoop,contrastLoop}     = TotDistRatioList;
%             AllConsOKNGianList{subLoop,contrastLoop}      = ConsOKNGainList;
%             AllThetaVariRatioList{subLoop,contrastLoop}   = ThetaVariRatioList;
            AllTheHgainList{subLoop,contrastLoop}         = TheHgainList;
            AllThePupilAreaList{subLoop,contrastLoop}     = ThePupilAreaList;
        end
    end

% for subLoop  = 1:2:length(ObserverName)
    %     if E.Settings.ExperimentSet == 1
    if mod(subLoop,2) ==1
        PlotStrs = {'OKN Consistency' 'Hist Gain' };%'Pupil Area'
        MaskTypes = {'Circular' 'Annular' 'Rectangular' 'Center Visible  Noise' 'Center Occluded  Noise' 'Noise background Noise'}; % different masks that could be used
        % close all
        XAxisData =  1:maskLoop(end); % length(E.Settings.BaseMaskLevels);
        for plotLoop = 1: length(PlotStrs)
            YLabelStr = PlotStrs{plotLoop};
            for     contrastLoop = 1 % :length(E.Settings.BaseContrastLevels) % here we just make the plots for set1
                switch plotLoop
                    %                         case 1
                    %                             Data = [AllOKNscoreList{subLoop,contrastLoop}];
                    %                         case 2
                    %                             Data = [AllOKNConsDistRatioList{subLoop,contrastLoop}];
                    case 1
                        Data  = [AllOKNscoreConsList{subLoop,contrastLoop}];
                    case 2
                        Data  = [AllTheHgainList{subLoop,contrastLoop}];
%                     case 3
%                         Data  = double([AllThePupilAreaList{subLoop,contrastLoop}]);
                end
                
                MeanData = nanmean(Data);
                StdData  = nanstd(Data);
                ColorSrt = [0.2 0.2 0.2] ;
                %                     ColorSrt =  abs(0.8- E.Settings.BaseContrastLevels(contrastLoop)).*[1 1 1];
                
                if plotLoop ==2
                    figure(999), hold on, pl=errorbar(XAxisData, MeanData,StdData,'o','LineWidth',1,'MarkerFaceColor',ColorSrt,'Color',ColorSrt);
                    set(pl,'MarkerSize',12,'MarkerEdgeColor',ColorSrt,'LineWidth',1);
                     xticks(XAxisData);
                     xticklabels(round(AllVisblRatio{subLoop},2));

                    hold off
                end
                gap     = 0.15*contrastLoop;
                plotNumber = (subLoop-1)*length(PlotStrs)+plotLoop;
                OKNfig=figure(plotNumber);
                hold on
                
                if  plotLoop==2
                    nrData=mean(Data);
                    BaseLine=min(nrData);
                    [params, f]= FitNakaRushton((AllVisblRatio{subLoop}),nrData-BaseLine); %fliplr
                    FineX=linspace(AllVisblRatio{subLoop}(1),AllVisblRatio{subLoop}(end),1000);
                    FineXplot=linspace(XAxisData(1)+gap,XAxisData(end)+gap,1000);
                    [prediction] = BaseLine+(ComputeNakaRushton(params,FineX)); % fliplr
                    p1 = plot(FineXplot,prediction,'k-');
                    set(p1,'LineWidth',3,'Color',[0.5 0.5 0.5])
                end
                e1=errorbar(XAxisData+gap, MeanData,StdData,'o','LineWidth',1,'MarkerFaceColor',ColorSrt,'Color',ColorSrt);
                set(e1,'MarkerSize',12,'MarkerEdgeColor',ColorSrt,'LineWidth',1)
                if plotLoop<7
                    ylim([-0.3 1.6]),
                end
                
                
                xlim([0 10]);
                plot(XAxisData+gap,Data,'o','MarkerSize',4,'MarkerFaceColor',ColorSrt,'MarkerEdgeColor','none','LineWidth',1 )
                xticks(XAxisData+0.2);
                %         for i= 1:length(ObserverName)
                %             if mod(i,2)==1
                %                 XtickStr{i} = sprintf('%s',   ObserverName{i}([(1:3),(end-1:end)]));
                %             else
                %                 XtickStr{i} = sprintf('%s',   ObserverName{i}([(1:3),(end-2:end)]));
                %             end
                %         end
                xticklabels(round(AllVisblRatio{subLoop},2));
                StrdRec(plotLoop,IVloop)=sqrt(mean(StdData.^2));
                title(sprintf('Subj: %s; Mask: %s %2.2f deg Dia; Number of trials: %i; Runs: %i ',ObserverName{subLoop}(1:3), MaskTypes{E.Settings.MaskType},FovRegDiameter{subLoop} ,ThisConditionTotalTrial{subLoop}, (TheRuns(subLoop))),'fontsize',16)
                legend('Naka-Rushton fit',sprintf('Mean %c SD',char(177)), 'Individual Poins','location','Best')
                ylabel(YLabelStr),xlabel('Visible Stimulus Proportion')
%                 if contrastLoop == 1
%                     text (0.15,1.00,sprintf('Contrast %2.1f \n',E.Settings.BaseContrastLevels.*100),'fontsize',14)
%                 end
                set(gca,'fontsize', 14)
                set(gcf,'Units', 'normalized','Position', [0.1300    0.1100    0.38    0.8150]);
                hold off
            end
        end
    end
end
%% plot all participants GAIN for VFL simulation data
contrastLoop = Set1NoContLevels;
XAxisData =  AllVisblRatio{1}; % 1:Set1NoMasks;
AllSubjsGains = [];
cmap=linspecer(length(ObserverName)); % color map
for subLoop  = 1:length(ObserverName)
    if mod(subLoop,2) == 1
        
        ThisSubjGain = AllTheHgainList{subLoop,contrastLoop};
        ThisSubjGainMean = nanmean(ThisSubjGain);
        AllSubjsGains = [AllSubjsGains;ThisSubjGainMean];
        figure(778), hold on
        
        pl1= plot(XAxisData,ThisSubjGainMean,'-o','LineWidth',1,'MarkerFaceColor',cmap(subLoop,:),'Color',cmap(subLoop,:));
    end
end

figure(778),% fit Naka_Rushton
nrData=mean(AllSubjsGains);
BaseLine=min(nrData);
[params, f]= FitNakaRushton((AllVisblRatio{1}(1:end-1)),nrData(1:end-1)-BaseLine); 
FineX=linspace(AllVisblRatio{1}(1),AllVisblRatio{1}(end-1),1000);
FineXplot=linspace(XAxisData(1),XAxisData(end-1),1000);
[prediction] = BaseLine+(ComputeNakaRushton(params,FineX)); % fliplr

fiftyPercentMaxX = params(2); % the mask that generates 50% of max gain
fiftyPercentMaxPrediction = BaseLine+(ComputeNakaRushton(params,fiftyPercentMaxX)); % fliplr
p1 = plot(FineXplot,prediction,'k-');
set(p1,'LineWidth',3,'Color',[0.8 0.4 0.4])
hold on, plot( FineXplot, fiftyPercentMaxPrediction, 'b.' );

grid on

ErPl1 = errorbar(XAxisData,mean(AllSubjsGains),nanstd(AllSubjsGains),'o','LineWidth',1,'MarkerFaceColor',[0.9 0.1 0.1],'Color',[0.9 0.1 0.1]);
set(ErPl1,'MarkerSize',12,'MarkerEdgeColor',ColorSrt,'LineWidth',1)
% legend('individual participants');
ylabel('Hist Gain'),xlabel('Visible Stimulus Proportion')

set(gca,'fontsize', 14)
xticks(sort(round([XAxisData,params(2)],2)));
xtickangle(70)
% xticklabels(round(AllVisblRatio{1},2));
xlim([-0.1 1.1]); ylim([0 1.2]); ytick(sort([0:.2:1.2,round(fiftyPercentMaxPrediction,2)]))
title(sprintf('%i participants ',length(ObserverName)/2),'fontsize',16)

legend([pl1, ErPl1 p1],'individual participants', sprintf('Mean %c SD',char(177)),'Naka-Rushton fit','location','best');
set(gcf,'Color',[1 1 1]);
hold off

%% plot all participants Pupilary area in set1 for VFL simulation data
contrastLoop = Set1NoContLevels;
XAxisData =  1:Set1NoMasks;
AllSubjsPAs = [];
for subLoop  = 1:length(ObserverName)
    if mod(subLoop,2) == 1
        ThisSubjPA = AllThePupilAreaList{subLoop,contrastLoop};
        ThisSubjPAMean = nanmean(ThisSubjPA)./ nanmean(AllBaselineMaxPupilAreastore{subLoop});
        AllSubjsPAs = [AllSubjsPAs;ThisSubjPAMean];
        figure(888), hold on
        pl1= plot(XAxisData,ThisSubjPAMean,'o','LineWidth',1,'MarkerFaceColor',[0.5 0.5 0.5],'Color',[0.5 0.5 0.5]);
    end
end

figure(888),% fit Naka_Rushton
nrData=mean(AllSubjsPAs);
BaseLine=min(nrData);
[params, f]= FitNakaRushton((AllVisblRatio{1}),nrData-BaseLine); %fliplr
FineX=linspace(AllVisblRatio{1}(1),AllVisblRatio{1}(end),1000);
FineXplot=linspace(XAxisData(1),XAxisData(end),1000);
[prediction] = BaseLine+(ComputeNakaRushton(params,FineX)); % fliplr
p1 = plot(FineXplot,prediction,'k-');
set(p1,'LineWidth',3,'Color',[0.8 0.4 0.4])

ErPl1 = errorbar(XAxisData,mean(AllSubjsPAs),nanstd(AllSubjsPAs),'o','LineWidth',1,'MarkerFaceColor',[0.9 0.1 0.1],'Color',[0.9 0.1 0.1]);
set(ErPl1,'MarkerSize',12,'MarkerEdgeColor',ColorSrt,'LineWidth',1)
% legend('individual participants');
ylabel('Pupil Area'),xlabel('Visible Stimulus Proportion')

set(gca,'fontsize', 14)
xticks(XAxisData);
xticklabels(round(AllVisblRatio{1},2));
xlim([0 10]);% ylim([0 1.2])
title(sprintf('%i participants ',length(ObserverName)/2),'fontsize',16)

legend([pl1, ErPl1 p1],'individual participants', sprintf('Mean %c SD',char(177)),'Naka-Rushton fit','location','best');

hold off

%% plot just for contrast
if 0 %E.Settings.ExperimentSet == 1
    contrastLoop = 1: length(E.Settings.BaseContrastLevels);
    
    ThisSubjGaintMean = nanmean([AllTheHgainList{subLoop,contrastLoop}]); % mean gain for different contrast levels
    ThisSubjGaintStd = nanstd([AllTheHgainList{subLoop,contrastLoop}]); % std of gain for different contrast levels
    
    PupilAreaRatio = ([AllThePupilAreaList{subLoop,contrastLoop}])./BasePupilAreaFromHist; % BasePupilAreaFromHist
    ThisSubjPAMean =  nanmean(PupilAreaRatio); % mean gain for different contrast levels as aratio of baseline pupil measurement
    ThisSubjPAStd = nanstd(PupilAreaRatio); %
    figure,
    xAxisData = E.Settings.BaseContrastLevels*100;
    for maskLoop = 1: numel()
        
        PltColrStr = [0.3 0.9 0.5];
        subplot(1,2,1),
        hold on, plot(xAxisData, (mean([AllTheHgainList{subLoop,1:7}],2))','o','MarkerFaceColor',[0.8 0.8 0.8],'MarkerEdgeColor',[0.8 0.8 0.8] )
        conPlt= errorbar(xAxisData, ThisSubjGaintMean, ThisSubjGaintStd,'ko');
        set(conPlt,'MarkerSize',12,'MarkerEdgeColor',PltColrStr,'MarkerFaceColor',PltColrStr,'LineWidth',1)
        set(gca,'fontsize', 14,'xscale','log')
        xlim([xAxisData(1)-0.1  xAxisData(end)+0.1 ]);ylim([-.1 1.2])
        xlabel('Contrast levels'),ylabel('Hist Gain')
        xticks((xAxisData))
        xticklabels((xAxisData))
        set(gca,'fontsize', 14)
        subplot(1,2,2),
        hold on, plot(xAxisData, PupilAreaRatio,'o','MarkerFaceColor',[0.8 0.8 0.8],'MarkerEdgeColor',[0.8 0.8 0.8] )
        conPlt= errorbar(xAxisData, ThisSubjPAMean, ThisSubjPAStd,'ko');
        set(conPlt,'MarkerSize',12,'MarkerEdgeColor',PltColrStr,'MarkerFaceColor',PltColrStr,'LineWidth',1)
        xlim([xAxisData(1)-0.1  xAxisData(end)+5 ]); ylim([-.1 1.1])
        xlabel('Contrast levels'),ylabel('Pupil Area Ratio')
        xticks((xAxisData))
        xticklabels((xAxisData) )
        title(sprintf('Subj: %s; Mask: %s %2.2f deg Dia; Number of trials: %i; Runs: %i ',ObserverName{subLoop}(1:3), MaskTypes{E.Settings.MaskType},FovRegDiameter{subLoop} ,P.TotalTrials, (TheRuns(subLoop))),'fontsize',16)
        set(gca,'fontsize', 14,'xscale','log')
    end
end
%% compute correlation
if 0 
for subLoop = 1: numel(ObserverName)
    HGainMean  = nanmean([AllTheHgainList{subLoop,contrastLoop}])';
    
    PupilAreaMean  = nanmean(double([AllThePupilAreaList{subLoop,contrastLoop}]))';
    
    [r, p] = corrcoef((HGainMean), PupilAreaMean);
    fprintf(1,'Correlation between gain and pupil area r=%.3f, p=%.4f\n',  r(2), p(2));
end
end
%% plot gain VS contrast

% if  E.Settings.ExperimentSet == 2
AllSubjYaxisDataA = []; AllSubjYaxisDataB=[];
    for subLoop  = 1:length(ObserverName)
        if mod(subLoop,2) ==0
            clear  xAxisData yAxisData
            xAxisData = E.Settings.BaseContrastLevels;
            AlltheGain = [AllTheHgainList{subLoop,:}];
            
            for maskLoop = 1: numel(P.PossMaskValues{1})
                if maskLoop == 1 %i.e 50 percent field
                    yAxisDataA = (AlltheGain(:, 1:2: numel(P.PossMaskValues{1})*numel(P.PossContrastValues{1})));
                elseif maskLoop == 2 % i.e full field
                    yAxisDataB =  (AlltheGain(:, 2:2: numel(P.PossMaskValues{1})*numel(P.PossContrastValues{1})));
                    
                end
            end
            
            AllSubjYaxisDataA = [AllSubjYaxisDataA; mean(yAxisDataA)];
            AllSubjYaxisDataB = [AllSubjYaxisDataB; mean(yAxisDataB)];
            figure(222);
            AllSubjPlotCont = plot(xAxisData, mean(yAxisDataA),'o', 'MarkerFaceColor',[0.2 0.2 0.8],'MarkerEdgeColor',[0.2 0.2 0.8]);
            hold on,
            AllSubjPlotCont2=plot(xAxisData, mean(yAxisDataB),'o', 'MarkerFaceColor',[0.8 0.2 0.2],'MarkerEdgeColor',[0.8 0.2 0.2]);
            figure(109+subLoop),conPlt = errorbar(xAxisData,nanmean(yAxisDataA),nanstd(yAxisDataA),'b-o', 'MarkerFaceColor',[0.8 0.8 0.8],'MarkerEdgeColor',[0.8 0.8 0.8]);
            hold on
            conPlt2= errorbar(xAxisData,nanmean(yAxisDataB),nanstd(yAxisDataB),'r-o', 'MarkerFaceColor',[0 0 0.8],'MarkerEdgeColor',[0 0 0.8]);
            set(conPlt,'MarkerSize',10,'MarkerEdgeColor',[0.2 0.2 0.8],'MarkerFaceColor',[0.2 0.2  0.8],'LineWidth',1)
            set(conPlt2,'MarkerSize',10,'MarkerEdgeColor',[0.8 0.2 0.2 ],'MarkerFaceColor',[0.8 0.2 0.2 ],'LineWidth',1)
            set(gca,'fontsize', 14,'xscale','log');
            %         set(gca,'fontsize', 14);
            xlim([xAxisData(1)-0.2  xAxisData(end)+0.1 ]);ylim([-.1 1.2])
            xlabel('Log contrast levels'),ylabel('Hist Gain')
            xticks((xAxisData))
            xticklabels((xAxisData) )
            title(sprintf('Subj: %s ', ObserverName{subLoop}(1:3)));
            set(gca,'fontsize', 14)
            hold off
            legend('50 percent field',  'Full field')
        end
    end
% end

%fit Naka-Ruston
nrDataA=mean(AllSubjYaxisDataA);
BaseLineA=min(nrDataA);
[paramsA, f]= FitNakaRushton(E.Settings.BaseContrastLevels,nrDataA-BaseLineA); %fliplr
FineXA=linspace(E.Settings.BaseContrastLevels(1),E.Settings.BaseContrastLevels(end),1000);
FineXplot=linspace(xAxisData(1),xAxisData(end),1000);
[predictionA] = BaseLineA+(ComputeNakaRushton(paramsA,FineXA)); % fliplr

% 50 percent contrast
nrDataB=mean(AllSubjYaxisDataB);
BaseLineB=min(nrDataB);
[paramsB, f]= FitNakaRushton(E.Settings.BaseContrastLevels,nrDataB-BaseLineB); %fliplr
FineXB=linspace(E.Settings.BaseContrastLevels(1),E.Settings.BaseContrastLevels(end),1000);
[predictionB] = BaseLineB+(ComputeNakaRushton(paramsB,FineXB)); % fliplr

figure(222),hold on
p1 = plot(FineXplot,predictionA,'-');
p2 = plot(FineXplot,predictionB,'-');
set(p1,'LineWidth',3,'Color',[0.2 0.2 0.8]);
set(p2,'LineWidth',3,'Color',[0.8 0.2 0.2]);

ErPlCont1 = errorbar(xAxisData,mean(AllSubjYaxisDataA),nanstd(AllSubjYaxisDataA),'o','LineWidth',1,'MarkerFaceColor',[0.1 0.1 0.8],'Color',[0.1 0.1 0.8]);
hold on;
ErPlCont2= errorbar(xAxisData,mean(AllSubjYaxisDataB),nanstd(AllSubjYaxisDataB),'o','LineWidth',1,'MarkerFaceColor',[0.8 0.1 0.1],'Color',[0.8 0.1 0.1]);
set(ErPlCont1,'MarkerSize',12,'MarkerEdgeColor',[0.1 0.1 0.8],'LineWidth',1); % ,'fontsize', 14,'xscale','log')
set(ErPlCont2,'MarkerSize',12,'MarkerEdgeColor',[0.8 0.1 0.1],'LineWidth',1); % ,'fontsize', 14,'xscale','log')

% legend('individual participants');
ylabel('Hist Gain'),xlabel('Log contrast levels')

set(gca,'fontsize', 14)
xticks(xAxisData);
xticklabels(round(E.Settings.BaseContrastLevels,3));
xlim([-0.1 1.1]); ylim([0 1.2])
title(sprintf('%i participants ',length(ObserverName)/2),'fontsize',16)
            set(gca,'fontsize', 14,'xscale','log');

legend([AllSubjPlotCont2,AllSubjPlotCont, ErPlCont1 p1],'individual participants full field','50% field ', sprintf('Mean %c SD',char(177)),'Naka-Rushton fit','location','best');

hold off
%% saving figs
if 0
    set(gcf,'PaperPositionMode','auto')
    print(figure(8), 'C:\Users\smoh205\Desktop\NIne Contrast Levels FullField', '-dpng','-noui','-r0')
end
%% pupillometry calibration
if 0
    EyeTrackerDataPA = [3075.7 , 4111.8, 4250.3, 4266.7, 4367.7, 4384.4];
    EyeTrackerDataPAstd = [65.4, 17.9, 14.4, 12.7, 9.8, 13];
    
    LenstarDataPDmm= [4.99,6.25,6.38,6.52,6.65,6.56];
    LenstarDataPDstdmm= [0.156, 0.115, 0.112, 0.11, 0.059, 0.063];
    
    figure,
    subplot(121),scatter(LenstarDataPDmm/2,sqrt(EyeTrackerDataPA/pi),'ro'),shg
    lsline
    [r, p] = corrcoef(LenstarDataPDmm/2,sqrt(EyeTrackerDataPA/pi));
    
    EyeTrackerDataPA2 = [4160.7, 4409, 4493.2, 4486.3, 4501.9, 4479.5];
    EyeTrackerDataPA2std = [6.59, 10.8, 23.3, 8.4, 4.8, 5.2];
    
    LenstarDataPDmm2= [6.02,6.66,6.82,6.85,6.92,6.93];
    LenstarDataPD2stdmm= [0.378,0.029,0.034,0.03,0.021,0.062];
    
    subplot(122),scatter(LenstarDataPDmm2/2,sqrt(EyeTrackerDataPA2/pi),'ro'),shg
    lsline
    [r2, p2] = corrcoef(LenstarDataPDmm2/2,sqrt(EyeTrackerDataPA2/pi));
end


%% mean of all pupillometry data
LenstarPDmm = 6.46;
LenstarPDmmStd = 0.097;

EyeTrackerPupilArea = 4248.933;
EyeTrackerPupilAreaStd = 16.024;

pupilDiameterLestarmm = LenstarPDmm;

pupilDiameterEyeTracker = (sqrt(EyeTrackerPupilArea/pi)).*2;

OneMilimeterPupilDiameter =  pupilDiameterEyeTracker./pupilDiameterLestarmm;

PDfromETmm = (sqrt(EyeTrackerPupilArea/pi))./OneMilimeterPupilDiameter; % pupil diameter based on Eye Tracer
%% timing analysis
% after each test (where the data is still in the memory,if not load manully.
%
% if iscell(E)
%     E = E{1};
%     P = P{1};
%     S = S{1};
% end
% firstframe = 1;
% lastframe  = E.TrialFramesPerInterleave(1);
% % Trial      = 24;
% Totalmissed = zeros(1,P.NoInterleaved);
% for i = 1: P.NoInterleaved
%
%     theFrames = squeeze(E.Window.FrameFlipTime(i,:,firstframe:lastframe));
%
%     Speed = 1./diff(mean(theFrames),1);
%     figure(1);
%     subplot(P.NoInterleaved/2,2,i);plot(Speed,'ro');ylim ([0 120])
%     Totalmissed(i) = sum(Speed<50)  ;
%     title(['interleaved '  num2str(i) ': number of missed frame = ' num2str(Totalmissed(i))] );   %num2str(sum(Speed<50))
%     if i == 1
%         ylabel('Mean IFI (5 Stimulus, 4 trials)')
%     elseif i== P.NoInterleaved-1
%         xlabel('Frames')
%     end
% end
% set(gcf,'Name','Mean Interframe Interval')
%
% for i = 1: P.NoInterleaved
%     framedeltas = mean(squeeze(E.Window.FrameDelta(i,:,firstframe:lastframe)),1);
%     figure(2);
%     subplot(P.NoInterleaved/2,2,i);plot(framedeltas,'ro');ylim ([0 3])
%     title(['interleaved '  num2str(i) ': doubled frame delta = ' num2str(sum(framedeltas==2))] );
% end
%
% for i = 1: P.NoInterleaved
%     MisedPerTrial = sum(squeeze(E.Window.InterpolatedFrames(i,:,firstframe:lastframe)),2)';
%     MeanMissedFrames(i,:) = MisedPerTrial;
% end
%  squeeze(MeanMissedFrames(:,1,:))